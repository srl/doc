import os
import sys

# Constants
dllOffset = int("0xFFFF", 0)

srcdir = sys.argv[1]
targetdir = sys.argv[2]

try:
    os.mkdir(targetdir)
except:
    print targetdir + " Directory exists\n"


indexl = os.listdir(srcdir)

idt = []
for i in indexl:
    if ".idt" in i:
        idt.append(i)


address = int("0x40000000", 0)

def makehex(i):
    return str(hex(i)).replace("0x","").upper()

for i in idt:
    idtFile = open(os.path.join(srcdir, i), "rb")
    idtData = idtFile.read().strip().split("\r\n")
    asmName = ""
    expName = ""
    
    for line in idtData:
        # get name of DLL from idt file, should succeed only once
        if line[0:7] == "0 Name=":
            asmName = line[7:].lower()
            asmName = asmName.replace(".dll",".asm")
            asmFile = open(os.path.join(targetdir, asmName), "wb")
            expName = asmName.replace(".asm",".exports")
            expFile = open(os.path.join(targetdir, expName), "wb")
            continue

        # process entry for interface function
        if "Name=" in line:
            # get name of interface function
            relevant = line[line.rindex("Name=")+5:]
            parts = relevant.split(" ")
            funName = "; " + parts[0] + "\n"
            asmFile.write(funName)
            undefLine =  makehex(address) + " . 0 UNDEF eax\n"
            asmFile.write(undefLine)
            address = address + 1

            # get number of stack bytes of interface function
            if len(parts)>1:
                try:
                	retValue = int(parts[1].split("=")[1].strip())
                except:
                	retValue = 0
            else:
                retValue = 0
            retValue = makehex(retValue)
            retLine = makehex(address) + " . 0 RETN " + retValue + "\n\n"
            asmFile.write(retLine)
            expFile.write(parts[0] + " " + str(address) + "\n")
            address = address + 4
            
    idtFile.close()
    asmFile.close()
    expFile.close()
    address += dllOffset + 1
    address &= ~dllOffset

 
        

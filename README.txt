Below is a list of tasks that will likely need to be performed when this
project is copied to a computer for the first time.

This project requires Draw2d.  Download and install GEF, which includes
Draw2d, from http://www.eclipse.org/gef/.

Add org.eclipse.ui.ide to the build path by selecting File->Import....
Select External Plugins and Fragments, Click Next, then Next again. 
Select org.eclipse.ui.ide from the list, click Add-->, then click Finish.

To use JJTree/JavaCC, download JavaCC from http://javacc.dev.java.net/. 
To use the file makeparser.bat, add the JavaCC\bin folder to your path.

JMock is used as part of the testing code.  JMock can be downloaded
from jmock.org.  You will need to add a variable in the Build Path
called JMOCK_LIB that points to the .jar file obtained from jmock.org.


INFORMING THE INTERPRETER ABOUT EXTERNAL FUNCTIONS
The interpreter needs to know certain information about
external DLL functions.  This information is stored in file
that contains stub assembly functions for the external DLL
functions.  For each file, there is also an exports file 
listing the addresses of each stub functions.

Before running the program, you must
configure the Java Virtual Machine so that the program knows
where to locate these file.  To do this, follow these steps:
1) With the DOC plugin installed, select Window->Preferences...
3) In the Stubs Path textbox, enter the path to the DLL stubs.
   Example: C:\eclipse\workspace\edu.louisiana.cacs.DetectObfuscation\Imports
   
When creating a new exports file, the file should be named <DLLNAME>.exports.  Thus,
the exports table file for kernel32.dll would be called kernel32.exports.  The
stub file must have the extension asm, so the stub for kernel32.dll would
be called kernel32.asm.
  
JAVA DEVELOPMENT KIT
This project requires JDK 1.5 (aka 5.0) or later.  The default that is shipped
with Eclipse 3.0 is an older version, so a newer version will likely need
to be downloaded from http://java.sun.com.  To install the JDK 1.5 in Eclipse,
select Window->Preferences->Java->Installed JREs.  Follow the on-screen instructions.


; This asm file creates the following situation.  
; The instructions at 00401011 pops off the return address.  The
; instruction is called from two locations, so there should be 
; two instructions associated with instruction 0401011.  At one
; time, the program would only place one of the instructions
; in the associated instructions list.

00401000 > $ E8 0C000000    CALL test.00401011
00401005   . E8 07000000    CALL test.00401011
0040100A   . 6A 00          PUSH 0                                   ;  ExitCode = 0
0040100C   . E8 03000000    CALL <JMP.&KERNEL32.ExitProcess>         ;  ExitProcess
00401011   $ 5B             POP EBX
00401012   . FFE3           JMP EBX
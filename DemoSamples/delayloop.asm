00401000 > $ B8 00000000    MOV EAX,0
00401005   > 83C0 01         ADD EAX,5
00401006   > 83C0 01         ADD EAX,1
00401008   . 3D 00000040     CMP EAX,100
0040100D   . 7C F6           JL SHORT test.00401005
0040100F   . 6A 00          PUSH 0                                   ;  ExitCode = 0
00401011   . E8 00000000    CALL <JMP.&KERNEL32.ExitProcess>         ;  ExitProcess

 
 
; ORIGINAL CODE (Compiled with TASM32)
;
; .486
;
; .Model Flat ,StdCall
;
; ; declaration of all used API-functions
; extrn ExitProcess : PROC
; 
; include \tasm\include\windows.inc
;
; ; Data
; .data
;   db 0
; 
; .code
; Main:
; 	MOV eax, 0
; Repeat:
;   ADD eax, 1
;   CMP eax, 1073741824
;   JL Repeat
;
; 	CALL ExitProcess, 0	
; end Main
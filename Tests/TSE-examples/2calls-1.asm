; Repeatedly makes 75 calls to a method.  Demonstrates
; efficiency problems with the context-insensitive interpreter.

A1 > $ 6A 01          PUSH A	       ; A1
A2   . 6A 02          PUSH B	       ; A2
A3   . E8 60040000    CALL test.A9 ; A3
A4   . 6A 03          PUSH C	       ; A4
A5   . 6A 04          PUSH D	       ; A5
A6   . E8 57040000    CALL test.A9 ; A6

A7   . 6A 00          NOP             ;  A7, ExitCode = 0
A8   . E8 0D000000    RETN                     ;  ExitProcess
A9   $ 8B4424 04      MOV EAX,DWORD PTR SS:[ESP+4] ; A9
A10   . 8B5C24 08      MOV EBX,DWORD PTR SS:[ESP+8] ; A10
A11   . 03C3           ADD EAX,EBX			 ; A11
A12   . C2 0800        RETN 8			 ; A12


 
; Repeatedly makes 75 calls to a method.  Demonstrates
; efficiency problems with the context-insensitive interpreter.

00401000 > $ 6A 01          PUSH A	       ; A1
00401002   . 6A 02          PUSH B	       ; A2
00401004   . E8 60040000    CALL test.00401469 ; A3
00401009   . 6A 03          PUSH C	       ; A4
0040100B   . 6A 04          PUSH D	       ; A5
0040100D   . E8 57040000    CALL test.00401469 ; A6

00401462   . 6A 00          NOP             ;  A7, ExitCode = 0
00401464   . E8 0D000000    RETN	    ; A8
00401469   $ 8B4424 04      MOV EAX,DWORD PTR SS:[ESP+4] ; A9
0040146D   . 8B5C24 08      MOV EBX,DWORD PTR SS:[ESP+8] ; A10
00401471   . 03C3           ADD EAX,EBX			 ; A11
00401473   . C2 0800        RETN 8			 ; A12


 
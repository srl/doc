; GetModuleHandleA
10000000 . 0 UNDEF eax
10000001 . 0 RETN 4

; ExitProcess
10000005 . 0 UNDEF dword ptr ss:[esp]	; undef stack top so next line will return to nowhere.
10000006 . 0 RETN 4

; lstrlenA
10000010 . 0 UNDEF eax	; undef return value
10000011 . 0 RETN 4

; GlobalAlloc
10000015 . 0 UNDEF eax	; undef return value
10000016 . 0 RETN 8

; lstrcpyA
10000020 . 0 UNDEF eax		; undef return value
10000021 . 0 RETN 8

; lstrcatA
10000025 . 0 UNDEF eax		; undef return value
10000026 . 0 RETN 8

; GlobalFree
10000030 . 0 UNDEF eax		; undef return value
10000031 . 0 RETN 4

; OpenProcess
10000035 . 0 UNDEF eax
10000036 . 0 RETN C		

; TerminateProcess
10000040 . 0 UNDEF eax
10000041 . 0 RETN 8

; CloseHandle
10000045 . 0 UNDEF eax
10000046 . 0 RETN 4

; GetTickCount
10000050 . 0 UNDEF eax
10000051 . 0 RETN

; GetSystemDirectoryA
10000055 . 0 UNDEF eax
10000056 . 0 RETN 8

; GetModuleFileNameA
10000060 . 0 UNDEF eax
10000061 . 0 RETN C

; SetFileAttributesA
10000065 . 0 UNDEF eax
10000066 . 0 RETN 8

; GetCommandLineA
10000070 . 0 UNDEF eax
10000071 . 0 RETN

; GetCurrentProcessId
10000075 . 0 UNDEF eax
10000076 . 0 RETN

; lstrcmpiA
10000080 . 0 UNDEF eax
10000081 . 0 RETN 8

; SetCurrentDirectoryA		
10000085 . 0 UNDEF eax
10000086 . 0 RETN 8

; FindFirstFileA 				
10000090 . 0 UNDEF eax
10000091 . 0 RETN 8

; DeleteFileA 				
10000095 . 0 UNDEF eax
10000096 . 0 RETN 4

; FindNextFileA 				
10000100 . 0 UNDEF eax
10000101 . 0 RETN 8

; GetStartupInfoA
10000105 . 0 PUSH eax
10000106 . 0 MOV  eax, dword ptr ss:[esp+8]
10000107 . 0 UNDEF dword ptr ss:[eax]	 ; undef STARTUPINFO.cb
10000108 . 0 UNDEF dword ptr ss:[eax+4]  ; undef STARTUPINFO.lpReserved
10000109 . 0 UNDEF dword ptr ss:[eax+8]  ; undef STARTUPINFO.lpDesktop
1000010A . 0 UNDEF dword ptr ss:[eax+C]  ; undef STARTUPINFO.lpTitle
1000010B . 0 UNDEF dword ptr ss:[eax+10] ; undef STARTUPINFO.dwX
1000010C . 0 UNDEF dword ptr ss:[eax+14] ; undef STARTUPINFO.dwY
1000010D . 0 UNDEF dword ptr ss:[eax+18] ; undef STARTUPINFO.dwXSize
1000010E . 0 UNDEF dword ptr ss:[eax+1C] ; undef STARTUPINFO.dwYSize
1000010F . 0 UNDEF dword ptr ss:[eax+20] ; undef STARTUPINFO.dwXCountChars
10000110 . 0 UNDEF dword ptr ss:[eax+24] ; undef STARTUPINFO.dwYCountChars
10000111 . 0 UNDEF dword ptr ss:[eax+28] ; undef STARTUPINFO.dwFillAttribute
10000112 . 0 UNDEF dword ptr ss:[eax+2C] ; undef STARTUPINFO.dwFlags
10000113 . 0 UNDEF word  ptr ss:[eax+30] ; undef STARTUPINFO.wShowWindow
10000114 . 0 UNDEF word  ptr ss:[eax+32] ; undef STARTUPINFO.cbReserved2
10000115 . 0 UNDEF dword ptr ss:[eax+34] ; undef STARTUPINFO.lpReserved2
10000116 . 0 UNDEF dword ptr ss:[eax+38] ; undef STARTUPINFO.hStdInput
10000117 . 0 UNDEF dword ptr ss:[eax+3C] ; undef STARTUPINFO.hStdOutput
10000118 . 0 UNDEF dword ptr ss:[eax+40] ; undef STARTUPINFO.hStdError
10000119 . 0 POP eax
10000120 . 0 RETN 4

; GetProcAddress
10000127 . 0 UNDEF eax
10000128 . 0 RETN 8

; LocalAlloc
10000130 . 0 UNDEF eax
10000131 . 0 RETN 8

; GetCommandLineW
10000137 . 0 UNDEF eax
10000138 . 0 RETN

; GetSystemWindowsDirectoryW
10000140 . 0 UNDEF eax
10000141 . 0 UNDEF dword ptr ss:[esp+4]
10000142 . 0 RETN 8

; lstrcpyW
10000147 . 0 UNDEF eax
10000148 . 0 UNDEF dword ptr ss:[esp+4]
10000149 . 0 RETN 8

; lstrlenW
10000150 . 0 UNDEF eax
10000151 . 0 RETN 4

; lstrcpynW
10000157 . 0 UNDEF eax
10000158 . 0 UNDEF dword ptr ss:[esp+4]
10000159 . 0 RETN C

; GlobalUnlock
10000160 . 0 UNDEF eax
10000161 . 0 RETN 4

; GlobalLock
10000167 . 0 UNDEF eax
10000168 . 0 RETN 4

; LocalFree
10000170 . 0 UNDEF eax
10000171 . 0 RETN 4

; GetThreadLocale
10000177 . 0 UNDEF eax
10000178 . 0 RETN

; GetVersionExA
10000180 . 0 UNDEF eax
10000181 . 0 RETN 4

; CopyFileA
10000187 . 0 UNDEF eax
10000188 . 0 RETN C

; CreateFileA
10000190 . 0 UNDEF eax
10000191 . 0 RETN 54

; WriteFile
10000197 . 0 UNDEF eax
10000198 . 0 RETN 14

; CreateFileMapping
100001A0 . 0 UNDEF eax
100001A1 . 0 RETN 18

; MapViewOfFile
100001A7 . 0 UNDEF eax
100001A8 . 0 RETN 14

; UnmapViewOfFile
100001B0 . 0 UNDEF eax
100001B1 . 0 RETN 4
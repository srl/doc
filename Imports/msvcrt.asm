;__set_app_type
77C3537C > 8BFF             MOV EDI,EDI
77C3537E   55               PUSH EBP
77C3537F   8BEC             MOV EBP,ESP
77C35381   8B45 08          MOV EAX,DWORD PTR SS:[EBP+8]
77C35384   A3 BC17C677      MOV DWORD PTR DS:[77C617BC],EAX
77C35389   5D               POP EBP
77C3538A   C3               RETN

; __p__fmode
77C1F1DB > B8 5C18C677      UNDEF eax
77C1F1E0   C3               RETN

; __p__commmode
77C1F1A4 > B8 FC21C677      UNDEF eax
77C1F1A9   C3               RETN

; __setusermatherr
77C4D675 > 8BFF             MOV EDI,EDI
77C4D677   55               PUSH EBP
77C4D678   8BEC             MOV EBP,ESP
77C4D67A   8B45 08          MOV EAX,DWORD PTR SS:[EBP+8]
77C4D67D   8325 F804C677 00 AND DWORD PTR DS:[77C604F8],0
77C4D684   A3 DC23C677      MOV DWORD PTR DS:[77C623DC],EAX
77C4D689   5D               POP EBP
77C4D68A   C3               RETN

; _controlfp
77C4D700 > 1				UNDEF eax
77C4D701   1				RETN

; _initterm
77C4D710 > 1 				UNDEF eax
77C4D711   1				RETN

; __getmainargs
77C4D715 > 1				UNDEF eax
77C4D716   1				RETN
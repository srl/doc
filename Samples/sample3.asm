; Contains one RET obfuscation.
00401000 > $ 6A 05          PUSH 5
00401002   . 6A 0A          PUSH 0A
00401004   . E8 0C000000    CALL test.00401015
00401009   . B8 00000000    MOV EAX,0
0040100E   > 6A 00          PUSH 0                                   ;  ExitCode = 0
00401010   . E8 13000000    CALL <JMP.&KERNEL32.ExitProcess>         ;  ExitProcess
00401015   $ 8B4424 04      MOV EAX,DWORD PTR SS:[ESP+4]
00401019   . 8B5C24 08      MOV EBX,DWORD PTR SS:[ESP+8]
0040101D   . 03C3           ADD EAX,EBX
0040101F   . 5B             POP EBX
00401020   . 68 0E104000    PUSH test.0040100E
00401025   . C2 0800        RETN 8                                   ;  RET used as a jump to 0040100E

; ORIGINAL ASM FILE (assembled with TASM)
; .486
;
; .Model Flat ,StdCall
;
; ; declaration of all used API-functions
; extrn ExitProcess : PROC
;
; ; Data
; .Data
;   db 0
;  
; .Code                                  
; Main:
;   PUSH  5
;   PUSH  10
;   Call  Add
;   MOV   eax, 0
; R:
;   PUSH  0
;   CALL  ExitProcess
;
; ; Adds two values from the stack.  Returns result in EAX
; Add:
;   MOV   eax, [esp+4]
;   MOV   ebx, [esp+8]
;   ADD   eax, ebx
;   POP   ebx
;   PUSH  offset R
;   RET   8
;  
; end Main
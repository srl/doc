00401000 > $ B8 01000000    MOV EAX,1
00401005   . 6A 01          PUSH 1
00401007   > 6A 02           PUSH 2
00401009   . 83F8 64         CMP EAX,64
0040100C   . 75 F9           JNZ SHORT test.00401007
0040100E   . 5B             POP EBX
0040100F   . 6A 00          PUSH 0                                   ;  ExitCode = 0
00401011   . E8 00000000    CALL <JMP.&KERNEL32.ExitProcess>         ;  ExitProcess

 
; .486
;
; .Model Flat ,StdCall
;
; ; declaration of all used API-functions
; extrn ExitProcess : PROC
;
; include \tasm\include\windows.inc
;
; ; Data
; .data
; db 0
;  
; .code
; Main:
; 	MOV eax, 1
; 	PUSH 1
; Repeat:
; 	PUSH 2
; 	CMP eax, 100
; 	JNE Repeat
; 	POP ebx
;	
; 	CALL ExitProcess, 0	
;      
; end Main
.486

.Model Flat ,StdCall

;declaration of all used API-functions
extrn ExitProcess : PROC

.Data
 db 0

.Code
Main:

 	PUSH  1
	PUSH  2
	PUSH  offset [a1]
	JMP   Add
a1:
	PUSH  3
	PUSH  4
	PUSH  offset [a2]
	JMP   Add
a2:	
	PUSH  5
	PUSH  6
	PUSH  offset [a3]
	JMP   Add
a3:
	PUSH  7
	PUSH  8
	PUSH  offset [a4]
	JMP   Add
a4:	
	PUSH  9
	PUSH  10
	PUSH  offset [a5]
	JMP   Add
a5:	
	PUSH  11
	PUSH  12
	PUSH  offset [a6]
	JMP   Add	
a6:
	PUSH  13
	PUSH  14
	PUSH  offset [a7]
	JMP   Add	
a7:
	PUSH  15
	PUSH  16
	PUSH  offset [a8]
	JMP   Add
a8:
	PUSH  17
	PUSH  18
	PUSH  offset [a9]
	JMP   Add
a9:
	PUSH  19
	PUSH  20
	PUSH  offset [a10]
	JMP   Add
a10:
	PUSH  21
	PUSH  22
	PUSH  offset [a11]
	JMP   Add
a11:
	PUSH  23
	PUSH  24
	PUSH  offset [a12]
	JMP   Add
a12:
	PUSH  25
	PUSH  26
	PUSH  offset [a13]
	JMP   Add
a13:
	PUSH  27
	PUSH  28
	PUSH  offset [a14]
	JMP   Add
a14:
	PUSH  29
	PUSH  30
	PUSH  offset [a15]
	JMP   Add
a15:
	PUSH  31
	PUSH  32
	PUSH  offset [a16]
	JMP   Add
a16:
	PUSH  33
	PUSH  34
	PUSH  offset [a17]
	JMP   Add
a17:
	PUSH  35
	PUSH  36
	PUSH  offset [a18]
	JMP   Add
a18:
	PUSH  37
	PUSH  38
	PUSH  offset [a19]
	JMP   Add
a19:
	PUSH  39
	PUSH  40
	PUSH  offset [a20]
	JMP   Add
a20:
	PUSH  41
	PUSH  42
	PUSH  offset [a21]
	JMP   Add
a21:
	PUSH  43
	PUSH  44
	PUSH  offset [a22]
	JMP   Add
a22:
	PUSH  45
	PUSH  46
	PUSH  offset [a23]
	JMP   Add
a23:
	PUSH  47
	PUSH  48
	PUSH  offset [a24]
	JMP   Add
a24:
	PUSH  49
	PUSH  50
	PUSH  offset [a25]
	JMP   Add
a25:
	PUSH  51
	PUSH  52
	PUSH  offset [a26]
	JMP   Add
a26:
	PUSH  53
	PUSH  54
	PUSH  offset [a27]
	JMP   Add
a27:
	PUSH  55
	PUSH  56
	PUSH  offset [a28]
	JMP   Add
a28:
	PUSH  57
	PUSH  58
	PUSH  offset [a29]
	JMP   Add
a29:
	PUSH  59
	PUSH  60
	PUSH  offset [a30]
	JMP   Add
a30:
	PUSH  61
	PUSH  62
	PUSH  offset [a31]
	JMP   Add
a31:
	PUSH  63
	PUSH  64
	PUSH  offset [a32]
	JMP   Add
a32:
	PUSH  65
	PUSH  66
	PUSH  offset [a33]
	JMP   Add
a33:
	PUSH  67
	PUSH  68
	PUSH  offset [a34]
	JMP   Add
a34:
	PUSH  69
	PUSH  70
	PUSH  offset [a35]
	JMP   Add
a35:
	PUSH  71
	PUSH  72
	PUSH  offset [a36]
	JMP   Add
a36:
	PUSH  73
	PUSH  74
	PUSH  offset [a37]
	JMP   Add
a37:
	PUSH  75
	PUSH  76
	PUSH  offset [a38]
	JMP   Add
a38:
	PUSH  77
	PUSH  78
	PUSH  offset [a39]
	JMP   Add
a39:
	PUSH  79
	PUSH  80
	PUSH  offset [a40]
	JMP   Add
a40:
	PUSH  81
	PUSH  82
	PUSH  offset [a41]
	JMP   Add
a41:
	PUSH  83
	PUSH  84
	PUSH  offset [a42]
	JMP   Add
a42:
	PUSH  85
	PUSH  86
	PUSH  offset [a43]
	JMP   Add
a43:
	PUSH  87
	PUSH  88
	PUSH  offset [a44]
	JMP   Add
a44:
	PUSH  89
	PUSH  90
	PUSH  offset [a45]
	JMP   Add
a45:
	PUSH  91
	PUSH  92
	PUSH  offset [a46]
	JMP   Add
a46:
	PUSH  93
	PUSH  94
	PUSH  offset [a47]
	JMP   Add
a47:
	PUSH  95
	PUSH  96
	PUSH  offset [a48]
	JMP   Add
a48:
	PUSH  97
	PUSH  98
	PUSH  offset [a49]
	JMP   Add
a49:
	PUSH  99
	PUSH  100
	PUSH  offset [a50]
	JMP   Add
a50:
	PUSH  101
	PUSH  102
	PUSH  offset [a51]
	JMP   Add
a51:
	PUSH  103
	PUSH  104
	PUSH  offset [a52]
	JMP   Add
a52:
	PUSH  105
	PUSH  106
	PUSH  offset [a53]
	JMP   Add
a53:
	PUSH  107
	PUSH  108
	PUSH  offset [a54]
	JMP   Add
a54:
	PUSH  109
	PUSH  110
	PUSH  offset [a55]
	JMP   Add
a55:
	PUSH  111
	PUSH  112
	PUSH  offset [a56]
	JMP   Add
a56:
	PUSH  113
	PUSH  114
	PUSH  offset [a57]
	JMP   Add
a57:
	PUSH  115
	PUSH  116
	PUSH  offset [a58]
	JMP   Add
a58:
	PUSH  117
	PUSH  118
	PUSH  offset [a59]
	JMP   Add
a59:
	PUSH  119
	PUSH  120
	PUSH  offset [a60]
	JMP   Add
a60:
    	PUSH  0
    	CALL  ExitProcess

; Adds two values from the stack.  Returns result in EAX
Add:
	MOV   eax, [esp+4]
 	MOV   ebx, [esp+8]
	ADD   eax, ebx
	RET   8

end Main 

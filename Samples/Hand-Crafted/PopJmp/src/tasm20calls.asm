.486

.Model Flat ,StdCall

;declaration of all used API-functions
extrn ExitProcess : PROC

.Data
 db 0

.Code
Main:

 	PUSH  1
	PUSH  2
	CALL  Add
	PUSH  3
	PUSH  4
	CALL  Add
	PUSH  5
	PUSH  6
	CALL  Add
	PUSH  7
	PUSH  8
	CALL  Add
	PUSH  9
	PUSH  10
	CALL  Add
	PUSH  11
	PUSH  12
	CALL  Add
	PUSH  13
	PUSH  14
	CALL  Add
	PUSH  15
	PUSH  16
	CALL  Add
	PUSH  17
	PUSH  18
	CALL  Add
	PUSH  19
	PUSH  20
	CALL  Add
	PUSH  21
	PUSH  22
	CALL  Add
	PUSH  23
	PUSH  24
	CALL  Add
	PUSH  25
	PUSH  26
	CALL  Add
	PUSH  27
	PUSH  28
	CALL  Add
	PUSH  29
	PUSH  30
	CALL  Add
	PUSH  31
	PUSH  32
	CALL  Add
	PUSH  33
	PUSH  34
	CALL  Add
	PUSH  35
	PUSH  36
	CALL  Add
	PUSH  37
	PUSH  38
	CALL  Add
	PUSH  39
	PUSH  40
	CALL  Add

    PUSH  0
    CALL  ExitProcess

; Adds two values from the stack.  Returns result in EAX
Add:
	MOV   eax, [esp+4]
 	MOV   ebx, [esp+8]
	ADD   eax, ebx
	POP   ebx
	ADD   esp, 8
	JMP   ebx

end Main 

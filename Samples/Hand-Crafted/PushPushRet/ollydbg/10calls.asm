00401000 > $ 6A 01            PUSH 1
00401002   . 6A 02            PUSH 2
00401004   . 68 0F104000      PUSH tasm10ca.0040100F
00401009   . 68 9D104000      PUSH tasm10ca.0040109D
0040100E   . C3               RETN                                             ;  RET used as a jump to 0040109D
0040100F   . 6A 03            PUSH 3
00401011   . 6A 04            PUSH 4
00401013   . 68 1E104000      PUSH tasm10ca.0040101E
00401018   . 68 9D104000      PUSH tasm10ca.0040109D
0040101D   . C3               RETN
0040101E   . 6A 05            PUSH 5
00401020   . 6A 06            PUSH 6
00401022   . 68 2D104000      PUSH tasm10ca.0040102D
00401027   . 68 9D104000      PUSH tasm10ca.0040109D
0040102C   . C3               RETN
0040102D   . 6A 07            PUSH 7
0040102F   . 6A 08            PUSH 8
00401031   . 68 3C104000      PUSH tasm10ca.0040103C
00401036   . 68 9D104000      PUSH tasm10ca.0040109D
0040103B   . C3               RETN
0040103C   . 6A 09            PUSH 9
0040103E   . 6A 0A            PUSH 0A
00401040   . 68 4B104000      PUSH tasm10ca.0040104B
00401045   . 68 9D104000      PUSH tasm10ca.0040109D
0040104A   . C3               RETN
0040104B   . 6A 0B            PUSH 0B
0040104D   . 6A 0C            PUSH 0C
0040104F   . 68 5A104000      PUSH tasm10ca.0040105A
00401054   . 68 9D104000      PUSH tasm10ca.0040109D
00401059   . C3               RETN
0040105A   . 6A 0D            PUSH 0D
0040105C   . 6A 0E            PUSH 0E
0040105E   . 68 69104000      PUSH tasm10ca.00401069
00401063   . 68 9D104000      PUSH tasm10ca.0040109D
00401068   . C3               RETN
00401069   . 6A 0F            PUSH 0F
0040106B   . 6A 10            PUSH 10
0040106D   . 68 78104000      PUSH tasm10ca.00401078
00401072   . 68 9D104000      PUSH tasm10ca.0040109D
00401077   . C3               RETN
00401078   . 6A 11            PUSH 11
0040107A   . 6A 12            PUSH 12
0040107C   . 68 87104000      PUSH tasm10ca.00401087
00401081   . 68 9D104000      PUSH tasm10ca.0040109D
00401086   . C3               RETN
00401087   . 6A 13            PUSH 13
00401089   . 6A 14            PUSH 14
0040108B   . 68 96104000      PUSH tasm10ca.00401096
00401090   . 68 9D104000      PUSH tasm10ca.0040109D
00401095   . C3               RETN
00401096   . 6A 00            PUSH 0                                           ;  ExitCode = 0
00401098   . E8 0D000000      CALL <JMP.&KERNEL32.ExitProcess>                 ;  ExitProcess
0040109D   > 8B4424 04        MOV EAX,DWORD PTR SS:[ESP+4]
004010A1   . 8B5C24 08        MOV EBX,DWORD PTR SS:[ESP+8]
004010A5   . 03C3             ADD EAX,EBX
004010A7   . C2 0800          RETN 8
004010AA   . FF25 30304000    JMP DWORD PTR DS:[<&KERNEL32.ExitProcess>]       ;  kernel32.ExitProcess

 
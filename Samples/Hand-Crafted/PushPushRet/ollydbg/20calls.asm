00401000 > $ 6A 01            PUSH 1
00401002   . 6A 02            PUSH 2
00401004   . 68 0F104000      PUSH tasm20ca.0040100F
00401009   . 68 33114000      PUSH tasm20ca.00401133
0040100E   . C3               RETN                                             ;  RET used as a jump to 00401133
0040100F   . 6A 03            PUSH 3
00401011   . 6A 04            PUSH 4
00401013   . 68 1E104000      PUSH tasm20ca.0040101E
00401018   . 68 33114000      PUSH tasm20ca.00401133
0040101D   . C3               RETN
0040101E   . 6A 05            PUSH 5
00401020   . 6A 06            PUSH 6
00401022   . 68 2D104000      PUSH tasm20ca.0040102D
00401027   . 68 33114000      PUSH tasm20ca.00401133
0040102C   . C3               RETN
0040102D   . 6A 07            PUSH 7
0040102F   . 6A 08            PUSH 8
00401031   . 68 3C104000      PUSH tasm20ca.0040103C
00401036   . 68 33114000      PUSH tasm20ca.00401133
0040103B   . C3               RETN
0040103C   . 6A 09            PUSH 9
0040103E   . 6A 0A            PUSH 0A
00401040   . 68 4B104000      PUSH tasm20ca.0040104B
00401045   . 68 33114000      PUSH tasm20ca.00401133
0040104A   . C3               RETN
0040104B   . 6A 0B            PUSH 0B
0040104D   . 6A 0C            PUSH 0C
0040104F   . 68 5A104000      PUSH tasm20ca.0040105A
00401054   . 68 33114000      PUSH tasm20ca.00401133
00401059   . C3               RETN
0040105A   . 6A 0D            PUSH 0D
0040105C   . 6A 0E            PUSH 0E
0040105E   . 68 69104000      PUSH tasm20ca.00401069
00401063   . 68 33114000      PUSH tasm20ca.00401133
00401068   . C3               RETN
00401069   . 6A 0F            PUSH 0F
0040106B   . 6A 10            PUSH 10
0040106D   . 68 78104000      PUSH tasm20ca.00401078
00401072   . 68 33114000      PUSH tasm20ca.00401133
00401077   . C3               RETN
00401078   . 6A 11            PUSH 11
0040107A   . 6A 12            PUSH 12
0040107C   . 68 87104000      PUSH tasm20ca.00401087
00401081   . 68 33114000      PUSH tasm20ca.00401133
00401086   . C3               RETN
00401087   . 6A 13            PUSH 13
00401089   . 6A 14            PUSH 14
0040108B   . 68 96104000      PUSH tasm20ca.00401096
00401090   . 68 33114000      PUSH tasm20ca.00401133
00401095   . C3               RETN
00401096   . 6A 15            PUSH 15
00401098   . 6A 16            PUSH 16
0040109A   . 68 A5104000      PUSH tasm20ca.004010A5
0040109F   . 68 33114000      PUSH tasm20ca.00401133
004010A4   . C3               RETN
004010A5   . 6A 17            PUSH 17
004010A7   . 6A 18            PUSH 18
004010A9   . 68 B4104000      PUSH tasm20ca.004010B4
004010AE   . 68 33114000      PUSH tasm20ca.00401133
004010B3   . C3               RETN
004010B4   . 6A 19            PUSH 19
004010B6   . 6A 1A            PUSH 1A
004010B8   . 68 C3104000      PUSH tasm20ca.004010C3
004010BD   . 68 33114000      PUSH tasm20ca.00401133
004010C2   . C3               RETN
004010C3   . 6A 1B            PUSH 1B
004010C5   . 6A 1C            PUSH 1C
004010C7   . 68 D2104000      PUSH tasm20ca.004010D2
004010CC   . 68 33114000      PUSH tasm20ca.00401133
004010D1   . C3               RETN
004010D2   . 6A 1D            PUSH 1D
004010D4   . 6A 1E            PUSH 1E
004010D6   . 68 E1104000      PUSH tasm20ca.004010E1
004010DB   . 68 33114000      PUSH tasm20ca.00401133
004010E0   . C3               RETN
004010E1   . 6A 1F            PUSH 1F
004010E3   . 6A 20            PUSH 20
004010E5   . 68 F0104000      PUSH tasm20ca.004010F0
004010EA   . 68 33114000      PUSH tasm20ca.00401133
004010EF   . C3               RETN
004010F0   . 6A 21            PUSH 21
004010F2   . 6A 22            PUSH 22
004010F4   . 68 FF104000      PUSH tasm20ca.004010FF
004010F9   . 68 33114000      PUSH tasm20ca.00401133
004010FE   . C3               RETN
004010FF   . 6A 23            PUSH 23
00401101   . 6A 24            PUSH 24
00401103   . 68 0E114000      PUSH tasm20ca.0040110E
00401108   . 68 33114000      PUSH tasm20ca.00401133
0040110D   . C3               RETN
0040110E   . 6A 25            PUSH 25
00401110   . 6A 26            PUSH 26
00401112   . 68 1D114000      PUSH tasm20ca.0040111D
00401117   . 68 33114000      PUSH tasm20ca.00401133
0040111C   . C3               RETN
0040111D   . 6A 27            PUSH 27
0040111F   . 6A 28            PUSH 28
00401121   . 68 2C114000      PUSH tasm20ca.0040112C
00401126   . 68 33114000      PUSH tasm20ca.00401133
0040112B   . C3               RETN
0040112C   . 6A 00            PUSH 0                                           ;  ExitCode = 0
0040112E   . E8 0D000000      CALL <JMP.&KERNEL32.ExitProcess>                 ;  ExitProcess
00401133   > 8B4424 04        MOV EAX,DWORD PTR SS:[ESP+4]
00401137   . 8B5C24 08        MOV EBX,DWORD PTR SS:[ESP+8]
0040113B   . 03C3             ADD EAX,EBX
0040113D   . C2 0800          RETN 8
00401140   . FF25 30304000    JMP DWORD PTR DS:[<&KERNEL32.ExitProcess>]       ;  kernel32.ExitProcess

 
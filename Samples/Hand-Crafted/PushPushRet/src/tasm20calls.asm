.486

.Model Flat ,StdCall

;declaration of all used API-functions
extrn ExitProcess : PROC

.Data
 db 0

.Code
Main:

 	PUSH  1
	PUSH  2
	PUSH  offset [a1]
	PUSH  offset [Add]
	RET
a1:
	PUSH  3
	PUSH  4
	PUSH  offset [a2]
	PUSH  offset [Add]
	RET
a2:	
	PUSH  5
	PUSH  6
	PUSH  offset [a3]
	PUSH  offset [Add]
	RET
a3:
	PUSH  7
	PUSH  8
	PUSH  offset [a4]
	PUSH  offset [Add]
	RET
a4:	
	PUSH  9
	PUSH  10
	PUSH  offset [a5]
	PUSH  offset [Add]
	RET
a5:	
	PUSH  11
	PUSH  12
	PUSH  offset [a6]
	PUSH  offset [Add]
	RET	
a6:
	PUSH  13
	PUSH  14
	PUSH  offset [a7]
	PUSH  offset [Add]
	RET	
a7:
	PUSH  15
	PUSH  16
	PUSH  offset [a8]
	PUSH  offset [Add]
	RET
a8:
	PUSH  17
	PUSH  18
	PUSH  offset [a9]
	PUSH  offset [Add]
	RET
a9:
	PUSH  19
	PUSH  20
	PUSH  offset [a10]
	PUSH  offset [Add]
	RET
a10:
	PUSH  21
	PUSH  22
	PUSH  offset [a11]
	PUSH  offset [Add]
	RET
a11:
	PUSH  23
	PUSH  24
	PUSH  offset [a12]
	PUSH  offset [Add]
	RET
a12:
	PUSH  25
	PUSH  26
	PUSH  offset [a13]
	PUSH  offset [Add]
	RET
a13:
	PUSH  27
	PUSH  28
	PUSH  offset [a14]
	PUSH  offset [Add]
	RET
a14:
	PUSH  29
	PUSH  30
	PUSH  offset [a15]
	PUSH  offset [Add]
	RET
a15:
	PUSH  31
	PUSH  32
	PUSH  offset [a16]
	PUSH  offset [Add]
	RET
a16:
	PUSH  33
	PUSH  34
	PUSH  offset [a17]
	PUSH  offset [Add]
	RET
a17:
	PUSH  35
	PUSH  36
	PUSH  offset [a18]
	PUSH  offset [Add]
	RET
a18:
	PUSH  37
	PUSH  38
	PUSH  offset [a19]
	PUSH  offset [Add]
	RET
a19:
	PUSH  39
	PUSH  40
	PUSH  offset [a20]
	PUSH  offset [Add]
	RET
a20:


    	PUSH  0
    	CALL  ExitProcess

; Adds two values from the stack.  Returns result in EAX
Add:
	MOV   eax, [esp+4]
 	MOV   ebx, [esp+8]
	ADD   eax, ebx
	RET   8

end Main 

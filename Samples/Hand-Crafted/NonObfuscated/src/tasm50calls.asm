.486

.Model Flat ,StdCall

;declaration of all used API-functions
extrn ExitProcess : PROC

.Data
 db 0

.Code
Main:

 	PUSH  1
	PUSH  2
	CALL  Add
	PUSH  3
	PUSH  4
	CALL  Add
	PUSH  5
	PUSH  6
	CALL  Add
	PUSH  7
	PUSH  8
	CALL  Add
	PUSH  9
	PUSH  10
	CALL  Add
	PUSH  11
	PUSH  12
	CALL  Add
	PUSH  13
	PUSH  14
	CALL  Add
	PUSH  15
	PUSH  16
	CALL  Add
	PUSH  17
	PUSH  18
	CALL  Add
	PUSH  19
	PUSH  20
	CALL  Add
	PUSH  21
	PUSH  22
	CALL  Add
	PUSH  23
	PUSH  24
	CALL  Add
	PUSH  25
	PUSH  26
	CALL  Add
	PUSH  27
	PUSH  28
	CALL  Add
	PUSH  29
	PUSH  30
	CALL  Add
	PUSH  31
	PUSH  32
	CALL  Add
	PUSH  33
	PUSH  34
	CALL  Add
	PUSH  35
	PUSH  36
	CALL  Add
	PUSH  37
	PUSH  38
	CALL  Add
	PUSH  39
	PUSH  40
	CALL  Add
	PUSH  41
	PUSH  42
	CALL  Add
	PUSH  43
	PUSH  44
	CALL  Add
	PUSH  45
	PUSH  46
	CALL  Add
	PUSH  47
	PUSH  48
	CALL  Add
	PUSH  49
	PUSH  50
	CALL  Add
	PUSH  51
	PUSH  52
	CALL  Add
	PUSH  53
	PUSH  54
	CALL  Add
	PUSH  55
	PUSH  56
	CALL  Add
	PUSH  57
	PUSH  58
	CALL  Add
	PUSH  59
	PUSH  60
	CALL  Add
	PUSH  61
	PUSH  62
	CALL  Add
	PUSH  63
	PUSH  64
	CALL  Add
	PUSH  65
	PUSH  66
	CALL  Add
	PUSH  67
	PUSH  68
	CALL  Add
	PUSH  69
	PUSH  70
	CALL  Add
	PUSH  71
	PUSH  72
	CALL  Add
	PUSH  73
	PUSH  74
	CALL  Add
	PUSH  75
	PUSH  76
	CALL  Add
	PUSH  77
	PUSH  78
	CALL  Add
	PUSH  79
	PUSH  80
	CALL  Add
	PUSH  81
	PUSH  82
	CALL  Add
	PUSH  83
	PUSH  84
	CALL  Add
	PUSH  85
	PUSH  86
	CALL  Add
	PUSH  87
	PUSH  88
	CALL  Add
	PUSH  89
	PUSH  90
	CALL  Add
	PUSH  91
	PUSH  92
	CALL  Add
	PUSH  93
	PUSH  94
	CALL  Add
	PUSH  95
	PUSH  96
	CALL  Add
	PUSH  97
	PUSH  98
	CALL  Add
	PUSH  99
	PUSH  100
	CALL  Add

    PUSH  0
    CALL  ExitProcess

; Adds two values from the stack.  Returns result in EAX
Add:
	MOV   eax, [esp+4]
 	MOV   ebx, [esp+8]
	ADD   eax, ebx
	RET   8

end Main 

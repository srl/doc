; Repeatedly makes 75 calls to a method.  Demonstrates
; efficiency problems with the context-insensitive interpreter.

00401000 > $ 6A 01          PUSH 1
00401002   . 6A 02          PUSH 2
00401004   . E8 60040000    CALL test.00401469

00401462   . 6A 00          PUSH 0                                               ;  ExitCode = 0
00401464   . E8 0D000000    CALL <JMP.&KERNEL32.ExitProcess>                     ;  ExitProcess
00401469   $ 8B4424 04      MOV EAX,DWORD PTR SS:[ESP+4]
0040146D   . 8B5C24 08      MOV EBX,DWORD PTR SS:[ESP+8]
00401471   . 03C3           ADD EAX,EBX
00401473   . C2 0800        RETN 8
00401476   . FF25 30304000  JMP DWORD PTR DS:[<&KERNEL32.ExitProcess>]           ;  kernel32.ExitProcess

 
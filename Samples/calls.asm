; Simple example code that contains multiple calls to the same function. 
; This file was added to test the context-sensitivity feature.
00401000 > $ B8 03000000    MOV EAX,3
00401005   . E8 11000000    CALL calls.0040101B
0040100A   . B8 0A000000    MOV EAX,0A
0040100F   . E8 07000000    CALL calls.0040101B
00401014   . 6A 00          PUSH 0                                        ;  ExitCode = 0
00401016   . E8 03000000    CALL <JMP.&KERNEL32.ExitProcess>              ;  ExitProcess
0040101B   $ 8BD8           MOV EBX,EAX
0040101D   . C3             RETN
0040101E   . FF25 30304000  JMP DWORD PTR DS:[<&KERNEL32.ExitProcess>]    ;  kernel32.ExitProcess
; Contains two CALL obfuscations using using PUSH-RET.

00401000 > $ 6A 05          PUSH 5
00401002   . 6A 0A          PUSH 0A
00401004   . 68 0F104000    PUSH test.0040100F
00401009   . 68 16104000    PUSH test.00401016
0040100E   . C3             RETN                                     ;  RET used as a jump to 00401016
0040100F   . 6A 00          PUSH 0                                   ;  ExitCode = 0
00401011   . E8 0D000000    CALL <JMP.&KERNEL32.ExitProcess>         ;  ExitProcess
00401016   > 8B4424 04      MOV EAX,DWORD PTR SS:[ESP+4]
0040101A   . 8B5C24 08      MOV EBX,DWORD PTR SS:[ESP+8]
0040101E   . 03C3           ADD EAX,EBX
00401020   . C2 0800        RETN 8

; ORIGINAL ASM CODE (assembled with TASM)
; .486
;
; .Model Flat ,StdCall
;
; ; declaration of all used API-functions
; extrn ExitProcess : PROC
;
; ; Data
; .Data
;   db 0
;  
; .Code                                  
; Main:
;   PUSH  5
;   PUSH  10
;   PUSH  offset R
;   PUSH  offset Add
;   RET
;
; R:
;   PUSH  0
;   CALL  ExitProcess
;
;
; ; Adds two values from the stack.  Returns result in EAX
; Add:
;   MOV   eax, [esp+4]
;   MOV   ebx, [esp+8]
;   ADD   eax, ebx
;   RET   8
;   
; end Main
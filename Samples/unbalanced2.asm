00401000 > $ B8 01000000    MOV EAX,1
00401005   . 6A 01          PUSH 1
00401007   > 6A 02           PUSH 2
00401009   . 5B              POP EBX
0040100A   . 50              PUSH EAX
0040100B   . 83C0 01         ADD EAX,1
0040100E   . 83F8 64         CMP EAX,64
00401011   . 75 F4           JNZ SHORT test.00401007
00401013   . 5B             POP EBX
00401014   . 6A 00          PUSH 0                                   ;  ExitCode = 0
00401016   . E8 00000000    CALL <JMP.&KERNEL32.ExitProcess>         ;  ExitProcess



; ORIGINAL ASM INPUT (assembled with tasm32)
; .486
;
; .Model Flat ,StdCall
;
; ; declaration of all used API-functions
; extrn ExitProcess : PROC
;
; include \tasm\include\windows.inc
;
; ; Data
; .data
;   db 0
;  
; .code
; Main:
; 	MOV eax, 1
; 	PUSH 1
; Repeat:
; 	PUSH 2
; 	POP ebx
; 	PUSH eax
; 	ADD eax, 1
; 	CMP eax, 100
; 	JNE Repeat
; 	POP ebx
;	
; 	CALL ExitProcess, 0	
;      
; end Main
; A very simple example containing a valid call-ret site.

00401000 > $ 6A 05          PUSH 5
00401002   . BB 0A000000    MOV EBX,0A
00401007   . 53             PUSH EBX
00401008   . E8 07000000    CALL test.00401014
0040100D   . 6A 00          PUSH 0                                   ;  ExitCode = 0
0040100F   . E8 0D000000    CALL <JMP.&KERNEL32.ExitProcess>         ;  ExitProcess
00401014   $ 8B4424 04      MOV EAX,DWORD PTR SS:[ESP+4]
00401018   . 8B5C24 08      MOV EBX,DWORD PTR SS:[ESP+8]
0040101C   . 03C3           ADD EAX,EBX
0040101E   . C2 0800        RETN 8

; ORIGINAL ASM FILE (assembled with TASM)
; .486
;
; .Model Flat ,StdCall
;
; ; declaration of all used API-functions
; extrn ExitProcess : PROC
;
; ; Data
; .Data
;   db 0
;  
; .Code                                  
; Main:
;   PUSH  5
;   MOV   ebx, 10
;   PUSH  ebx
;   CALL  Add
;  
;   PUSH  0
;   CALL  ExitProcess
;
; ; Adds two values from the stack.  Returns result in EAX
; Add:
;   MOV   eax, [esp+4]
;   MOV   ebx, [esp+8]
;   ADD   eax, ebx
;   RET   8
;   
; end Main 
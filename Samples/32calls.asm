; Repeatedly makes 75 calls to a method.  Demonstrates
; efficiency problems with the context-insensitive interpreter.

00401000 > $ 6A 01          PUSH 1
00401002   . 6A 02          PUSH 2
00401004   . E8 60040000    CALL test.00401469
00401009   . 6A 03          PUSH 3
0040100B   . 6A 04          PUSH 4
0040100D   . E8 57040000    CALL test.00401469
00401012   . 6A 05          PUSH 5
00401014   . 6A 06          PUSH 6
00401016   . E8 4E040000    CALL test.00401469
0040101B   . 6A 07          PUSH 7
0040101D   . 6A 08          PUSH 8
0040101F   . E8 45040000    CALL test.00401469
00401024   . 6A 09          PUSH 9
00401026   . 6A 0A          PUSH 0A
00401028   . E8 3C040000    CALL test.00401469
0040102D   . 6A 0B          PUSH 0B
0040102F   . 6A 0C          PUSH 0C
00401031   . E8 33040000    CALL test.00401469
00401036   . 6A 0D          PUSH 0D
00401038   . 6A 0E          PUSH 0E
0040103A   . E8 2A040000    CALL test.00401469
0040103F   . 6A 0F          PUSH 0F
00401041   . 6A 10          PUSH 10
00401043   . E8 21040000    CALL test.00401469
00401048   . 6A 11          PUSH 11
0040104A   . 6A 12          PUSH 12
0040104C   . E8 18040000    CALL test.00401469
00401051   . 6A 13          PUSH 13
00401053   . 6A 14          PUSH 14
00401055   . E8 0F040000    CALL test.00401469
0040105A   . 6A 15          PUSH 15
0040105C   . 6A 16          PUSH 16
0040105E   . E8 06040000    CALL test.00401469
00401063   . 6A 17          PUSH 17
00401065   . 6A 18          PUSH 18
00401067   . E8 FD030000    CALL test.00401469
0040106C   . 6A 19          PUSH 19
0040106E   . 6A 1A          PUSH 1A
00401070   . E8 F4030000    CALL test.00401469
00401075   . 6A 1B          PUSH 1B
00401077   . 6A 1C          PUSH 1C
00401079   . E8 EB030000    CALL test.00401469
0040107E   . 6A 1D          PUSH 1D
00401080   . 6A 1E          PUSH 1E
00401082   . E8 E2030000    CALL test.00401469
00401087   . 6A 1F          PUSH 1F
00401089   . 6A 20          PUSH 20
0040108B   . E8 D9030000    CALL test.00401469
00401090   . 6A 21          PUSH 21
00401092   . 6A 22          PUSH 22
00401094   . E8 D0030000    CALL test.00401469
00401099   . 6A 23          PUSH 23
0040109B   . 6A 24          PUSH 24
0040109D   . E8 C7030000    CALL test.00401469
004010A2   . 6A 25          PUSH 25
004010A4   . 6A 26          PUSH 26
004010A6   . E8 BE030000    CALL test.00401469
004010AB   . 6A 27          PUSH 27
004010AD   . 6A 28          PUSH 28
004010AF   . E8 B5030000    CALL test.00401469
004010B4   . 6A 29          PUSH 29
004010B6   . 6A 2A          PUSH 2A
004010B8   . E8 AC030000    CALL test.00401469
004010BD   . 6A 2B          PUSH 2B
004010BF   . 6A 2C          PUSH 2C
004010C1   . E8 A3030000    CALL test.00401469
004010C6   . 6A 2D          PUSH 2D
004010C8   . 6A 2E          PUSH 2E
004010CA   . E8 9A030000    CALL test.00401469
004010CF   . 6A 2F          PUSH 2F
004010D1   . 6A 30          PUSH 30
004010D3   . E8 91030000    CALL test.00401469
004010D8   . 6A 31          PUSH 31
004010DA   . 6A 32          PUSH 32
004010DC   . E8 88030000    CALL test.00401469
004010E1   . 6A 33          PUSH 33
004010E3   . 6A 34          PUSH 34
004010E5   . E8 7F030000    CALL test.00401469
004010EA   . 6A 35          PUSH 35
004010EC   . 6A 36          PUSH 36
004010EE   . E8 76030000    CALL test.00401469
004010F3   . 6A 37          PUSH 37
004010F5   . 6A 38          PUSH 38
004010F7   . E8 6D030000    CALL test.00401469
004010FC   . 6A 39          PUSH 39
004010FE   . 6A 3A          PUSH 3A
00401100   . E8 64030000    CALL test.00401469
00401105   . 6A 3B          PUSH 3B
00401107   . 6A 3C          PUSH 3C
00401109   . E8 5B030000    CALL test.00401469
0040110E   . 6A 3D          PUSH 3D
00401110   . 6A 3E          PUSH 3E
00401112   . E8 52030000    CALL test.00401469
00401117   . 6A 3F          PUSH 3F
00401119   . 6A 40          PUSH 40
0040111B   . E8 49030000    CALL test.00401469

00401462   . 6A 00          PUSH 0                                               ;  ExitCode = 0
00401464   . E8 0D000000    CALL <JMP.&KERNEL32.ExitProcess>                     ;  ExitProcess
00401469   $ 8B4424 04      MOV EAX,DWORD PTR SS:[ESP+4]
0040146D   . 8B5C24 08      MOV EBX,DWORD PTR SS:[ESP+8]
00401471   . 03C3           ADD EAX,EBX
00401473   . C2 0800        RETN 8
00401476   . FF25 30304000  JMP DWORD PTR DS:[<&KERNEL32.ExitProcess>]           ;  kernel32.ExitProcess

 
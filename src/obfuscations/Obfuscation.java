/*
 * Obfuscation.java
 * Created on Apr 18, 2005
 */
package obfuscations;

import x86InstructionSet.Instruction;

/**
 * Represents a pair of obfuscated instructions that may be of interest
 * to the user.  Contains methods for assisting in highlighting
 * the instructions to bring them to the user's attention.
 */
abstract public class Obfuscation
{
	private Instruction	first;
	private Instruction	second;
	
  /**
   * Creates a new <code>Obfuscation</code> with the specified pair of 
   * <code>Instruction</code>s.
   */
	public Obfuscation(Instruction first, Instruction second)
	{
		this.first  = first;
		this.second = second;
	}
	
	/**
	 * Returns true if <code>this</code> equals <code>rhs</code>.
   * Two <code>Obfuscation</code>s are equal if the 
   * <code>Instruction</code>s referenced in the two obfuscations
   * are the same.  
	 */
	public abstract boolean equals(Object rhsIn);
	
  /** Returns the first <code>Instruction</code> of this pair. */
	public Instruction getFirst() 	{ return first; }
  
  /** Returns the second <code>instruction</code> of this pair. */
	public Instruction getSecond() 	{ return second; }
	
  /**
   * Returns the <code>String</code> ID that identifies the visual marker
   * to be displayed next to this obfuscation on-screen. The list of
   * possible IDs are located in <code>plugin.xml</code>
   */
	abstract public String getMarkerId();
}

package obfuscations;

import x86InstructionSet.Instruction;

public class TextObfuscationVisitor extends ObfuscationVisitor {

	@Override
	public void callObfuscation(Instruction instruction) {
		System.out.println("Call Obfuscation detected at " + instruction);
		//results.output+="Call Obfuscation detected at " + instruction+"\r\n";		
	}

	@Override
	public void returnObfuscation(Instruction instruction) {
		System.out.println("Return Obfuscation detected at " + instruction);
		//results.output+="Return Obfuscation detected at " + instruction+"\r\n";
	}

	@Override
	public void callReturnSite(Instruction instruction) {
		System.out.println("Call Return Site Obfuscation detected at " + instruction);
		//results.output+="Call Return Site Obfuscation detected at " + instruction+"\r\n";
	}
}

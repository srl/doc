package obfuscations;

import x86InstructionSet.Instruction;

public abstract class ObfuscationVisitor {

	public abstract void callObfuscation(Instruction instruction);

	public abstract void returnObfuscation(Instruction instruction);

	public abstract void callReturnSite(Instruction instruction);
}

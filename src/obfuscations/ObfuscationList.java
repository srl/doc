/*
 * ObfuscationList.java
 * Created on Apr 18, 2005
 */
package obfuscations;

import java.util.LinkedList;

import analyzer.AnalysisResult;


/**
 * Maintains a list of obfuscations.
 */
public class ObfuscationList implements AnalysisResult
{
    private LinkedList<Obfuscation> obfuscations	= new LinkedList<Obfuscation>();
    
    /**
     * Adds an obfuscation to this list.  Does not check for duplicates.
     */
    public void add(Obfuscation obfuscation)
    {
        obfuscations.add(obfuscation);
    }
    
    /**
     * Returns true if this list contains an obfuscation equal
     * to the input obfuscation.  <br>
     * That is,<br>
     * <code>
     * for each o in this list<br>
     * 		if o.equals(obfuscation)<br>
     * 			return true<br>
     * return false<br>
     * </code>
     */
    public boolean has(Obfuscation obfuscation)
    {
        Iterator iter = iterator();
        while(iter.hasNext())
        {
            if(iter.next().equals(obfuscation))
                return true;
        }
        return false;
    }    
    
    /**
     * Returns an iterator that can be used to traverse this list.
     */
    public Iterator iterator() { return new Iterator(); }
    public class Iterator
    {
        private java.util.Iterator iter;
        public Iterator() 			{ iter = obfuscations.iterator(); }
        public boolean hasNext()  	{ return iter.hasNext(); }
        public Obfuscation next() 	{ return (Obfuscation)iter.next(); }  
    }

    /**
     * Returns the number of obfuscations in this list.
     */
    public int size()
    {
        return obfuscations.size();
    }

    /**
     * String representation
     */
    public String toString()
    {
        return obfuscations.toString();
    }
}

package obfuscations;

import java.util.HashMap;

import logger.Log;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.ui.texteditor.MarkerUtilities;

import ui.markerViewers.MarkerFields;
import ui.programViewer.ProgramViewer;
import analyzer.AnalysisResult;
import analyzer.AnnotationProvider;
import detectObfuscation.Strings;

public class ObfuscationAnnotationProvider implements AnnotationProvider
{ 
  public void annotate(IResource resource, AnalysisResult result, ProgramViewer viewer)
  {
    int                       addressLength;
    HashMap<String, Object>   attributes;
    ObfuscationList.Iterator  iter; 
    int                       linenumber;
    int                       lineOffset;
    Obfuscation               obfuscation;
    ObfuscationList           obfuscations;

    // set up obfuscation markers.
    obfuscations = (ObfuscationList)result;
    try
    {
      iter = obfuscations.iterator();
      while(iter.hasNext())
      {
        obfuscation   = iter.next();
        linenumber    = viewer.getLineNumber(obfuscation.getFirst());//program.getInstructionIndex(obfuscation.getFirst().getAddress());
        lineOffset    = viewer.getOffsetAtLine(linenumber);
        addressLength = obfuscation.getSecond().getAddress().toString().length();

        if(!viewer.markerExists(obfuscation.getMarkerId(), linenumber))
        {
          attributes = new HashMap<String, Object>();
          attributes.put(MarkerFields.INSTRUCTION, obfuscation.getFirst());
          attributes.put(IMarker.LINE_NUMBER, new Integer(linenumber));
          attributes.put(IMarker.CHAR_START, new Integer(lineOffset));
          attributes.put(IMarker.CHAR_END, new Integer(lineOffset + addressLength));
          MarkerUtilities.createMarker(resource, attributes, obfuscation.getMarkerId());
        }
      }
    }
    catch(CoreException e)
    {
      Log.write(Log.ERROR, Strings.getUnexpectedErrorMsg(e.getMessage()), e);
    }
  }
}

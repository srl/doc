package obfuscations;

import java.util.List;

import org.jdom.Document;
import org.jdom.Element;

import x86InstructionSet.Instruction;

public class XMLObfuscationVisitor extends ObfuscationVisitor {
	Element root;
    List<Element> rootlist;
    Element analysiselement;
    Element obfelement;
    Element ocollection;
    Element oelement;
    Element telement;
    Element ielement;
    Element aelement;
    Element celement;

    String instructionstring;
    String[] instructionparts;
    String[] registerparts;
    String addressstring;
    String commandstring;
	public XMLObfuscationVisitor(Document document) {
		super();
	    oelement = null;
	    telement = null;
	    ielement = null;
	    aelement = null;
	    celement = null;
	    
	    root = document.getRootElement();
	    rootlist = root.getChildren();
	    analysiselement = rootlist.get(rootlist.size()-1);
	    obfelement = analysiselement.getChild("DETECTEDOBFUSCATIONS");
	    ocollection = obfelement.getChild("OBFUSCATIONCOLLECTION");

	    instructionstring = null;
	    instructionparts = null;
	    registerparts = null;
	    addressstring = null;
	    commandstring = null;
	}
	@Override
	public void callObfuscation(Instruction instruction) {
		oelement = new Element("OBFUSCATION");
		ocollection.addContent(oelement);
		telement = new Element("TYPE").setText("Call Obfuscation");
		oelement.addContent(telement);
		ielement = new Element("INSTRUCTION");
		oelement.addContent(ielement);
		instructionstring = instruction.toString();
        while(instructionstring.contains("  " ))
        	instructionstring = instructionstring.replace("  ", " ");
        instructionparts = instructionstring.trim().split(" ");
        addressstring = instructionparts[0];
        if (instructionparts.length <= 2) {
        	commandstring = instructionparts[1];
        } else
           commandstring = instructionparts[1] + " " + instructionparts[2];
        aelement = new Element("ADDRESS").setText(addressstring);
        celement = new Element("COMMAND").setText(commandstring);
		ielement.addContent(aelement);
		ielement.addContent(celement);
	}
	@Override
	public void returnObfuscation(Instruction instruction) {
		oelement = new Element("OBFUSCATION");
		ocollection.addContent(oelement);
		telement = new Element("TYPE").setText("Return Obfuscation");
		oelement.addContent(telement);
		ielement = new Element("INSTRUCTION");
		oelement.addContent(ielement);
		
		instructionstring = instruction.toString();
        while(instructionstring.contains("  " ))
        	instructionstring = instructionstring.replace("  ", " ");
        instructionparts = instructionstring.trim().split(" ");
        addressstring = instructionparts[0];
        if (instructionparts.length <= 2) {
        	commandstring = instructionparts[1];
        } else
           commandstring = instructionparts[1] + " " + instructionparts[2];
        aelement = new Element("ADDRESS").setText(addressstring);
        celement = new Element("COMMAND").setText(commandstring);
		ielement.addContent(aelement);
		ielement.addContent(celement);
	}
	@Override
	public void callReturnSite(Instruction instruction) {
		oelement = new Element("OBFUSCATION");
		ocollection.addContent(oelement);
		telement = new Element("TYPE").setText("Call-Return Site");
		oelement.addContent(telement);
		ielement = new Element("INSTRUCTION");
		oelement.addContent(ielement);
		
		instructionstring = instruction.toString();
        while(instructionstring.contains("  " ))
        	instructionstring = instructionstring.replace("  ", " ");
        instructionparts = instructionstring.trim().split(" ");
        addressstring = instructionparts[0];
        if (instructionparts.length <= 2) {
        	commandstring = instructionparts[1];
        } else
           commandstring = instructionparts[1] + " " + instructionparts[2];
        aelement = new Element("ADDRESS").setText(addressstring);
        celement = new Element("COMMAND").setText(commandstring);
		ielement.addContent(aelement);
		ielement.addContent(celement);
	}
    
    
}

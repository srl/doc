/*
 * CallRetSite.java
 * Created on Apr 20, 2005
 */
package obfuscations;

import ui.markerViewers.MarkerIds;
import x86InstructionSet.Instruction;

/**
 * Contains information about a call-return site pair.
 */
public class CallRetSite extends Obfuscation 
{
  /** @see Obfuscation#Obfuscation(Instruction, Instruction) */
  public CallRetSite(Instruction callInstruction, Instruction retInstruction)
  {
  	super(callInstruction, retInstruction);
  }

  /** @see Obfuscation#equals(Object) */
	public boolean equals(Object rhsIn)
	{
	    if(rhsIn instanceof CallRetSite)
	    {
	        Obfuscation rhs = (Obfuscation)rhsIn;
	        return getFirst() == rhs.getFirst() && 
	        	   getSecond() == rhs.getSecond();
	    }
	    return false;	     
	}
    
	/** @see Obfuscation#getMarkerId() */
  public String getMarkerId()
  {
  	return MarkerIds.CALLRETSITE_MARKER;
  }

  /**
   * String represetnation
   */
  public String toString()
  {
    return "Call-Ret site " + getFirst().getAddress() + " X " + getSecond().getAddress();
  }
}

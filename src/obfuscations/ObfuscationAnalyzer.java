/*
 * Analyzer.java
 * Created on Apr 18, 2005
 */
package obfuscations;

import java.util.Iterator;

import analyzer.AnalysisResult;
import analyzer.Analyzer;

import x86InstructionSet.CallInstruction;
import x86InstructionSet.Instruction;
import x86InstructionSet.PopInstruction;
import x86InstructionSet.Program;
import x86InstructionSet.RetInstruction;

/**
 * Analyzes a program to uncover call obfuscations.
 */
public class ObfuscationAnalyzer implements Analyzer
{
	ObfuscationVisitor obfuscationVisitor;
    /**
     * Analyzes the input program for call obfuscations.
     * @param program an interpreted x86 program.
     * @return a list containing the obfuscations found.  
     */
	
    public ObfuscationAnalyzer() {
		super();
		obfuscationVisitor = new PlugInObfuscationVisitor();
	}
	public void setVisitor(ObfuscationVisitor obfuscationVisitor) {
		this.obfuscationVisitor = obfuscationVisitor;		
	}

	public AnalysisResult analyze(Program program)
    {
    	ObfuscationList	obfuscations = new ObfuscationList();
        Iterator<Instruction> iter = program.iterator();

        while(iter.hasNext())
        {
        	Instruction instruction = iter.next();
        	
        	// Look for CALL obfuscation
        	if(instruction instanceof RetInstruction && instruction.getBeginState() != null)
        	{
        		Instruction[] stackTops = instruction.getBeginState().getStackTopCreators();
        		for(int i = 0; i < stackTops.length; i++)
        		{
        			if( !(stackTops[i] instanceof CallInstruction) ){				        
        				obfuscationVisitor.callObfuscation(instruction);
        				obfuscations.add(new CallObfuscation(instruction, stackTops[i]));
        			}
        		}
        	}
        	
        	// Look for RET obfuscation	
        	if(instruction instanceof PopInstruction && instruction.getBeginState() != null)
        	{
        		Instruction[] stackTops = instruction.getBeginState().getStackTopCreators();
        		for(int i = 0; i < stackTops.length; i++)
        		{
        			if( stackTops[i] instanceof CallInstruction ){
        				obfuscationVisitor.returnObfuscation(instruction);
        				obfuscations.add(new RetObfuscation(instruction, stackTops[i]));
        			}
        		}
        	}

        	// Look for valid CALL-RET sites	
        	if(instruction instanceof RetInstruction && instruction.getBeginState() != null)
        	{
        		Instruction[] stackTops = instruction.getBeginState().getStackTopCreators();
        		for(int i = 0; i < stackTops.length; i++)
        		{
        			if( stackTops[i] instanceof CallInstruction ){        				
        				obfuscationVisitor.callReturnSite(instruction);
        				obfuscations.add(new CallRetSite(stackTops[i], instruction));
        			}
        		}
        	}
        }
        return obfuscations;
	}
}

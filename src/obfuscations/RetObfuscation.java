/*
 * RetObfuscations.java
 * Created on Apr 20, 2005
 */
package obfuscations;

import ui.markerViewers.MarkerIds;
import x86InstructionSet.Instruction;

/**
 * Contains information about a return obfuscation.
 */
public class RetObfuscation extends Obfuscation 
{
  /** @see Obfuscation#Obfuscation(Instruction, Instruction) */
  public RetObfuscation(Instruction popInstruction, Instruction callInstruction)
  {
  	super(popInstruction, callInstruction);
  }

  /** @see Obfuscation#equals(Object) */
	public boolean equals(Object rhsIn)
	{
	    if(rhsIn instanceof RetObfuscation)
	    {
	        Obfuscation rhs = (Obfuscation)rhsIn;
	        return getFirst() == rhs.getFirst() && 
	        	   getSecond() == rhs.getSecond();
	    }
	    return false;	     
	}

  /** @see Obfuscation#getMarkerId() */
  public String getMarkerId()
  {
  	return MarkerIds.RETOBFUSCATION_MARKER;
  }

  /**
   * String represetnation
   */
  public String toString()
  {
    return "Ret Obf. " + getFirst().getAddress() + " X " + getSecond().getAddress();
  }
}

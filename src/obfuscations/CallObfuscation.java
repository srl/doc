/*
 * CallObfuscation.java
 * Created on Apr 18, 2005
 */
package obfuscations;

import ui.markerViewers.MarkerIds;
import x86InstructionSet.Instruction;

/**
 * Contains information about a call obfuscation.
 */
public class CallObfuscation extends Obfuscation
{
    /** @see Obfuscation#Obfuscation(Instruction, Instruction) */
    public CallObfuscation(Instruction retInstruction, Instruction notACallInstruction)
    {
    	super(retInstruction, notACallInstruction);
    }

	/** @see Obfuscation#equals(Object) */
	public boolean equals(Object rhsIn)
	{
	    if(rhsIn instanceof CallObfuscation)
	    {
	        Obfuscation rhs = (Obfuscation)rhsIn;
	        return getFirst() == rhs.getFirst() && 
	        	   getSecond() == rhs.getSecond();
	    }
	    return false;	     
	}

  /** @see Obfuscation#getMarkerId() */
  public String getMarkerId()
  {
  	return MarkerIds.CALLOBFUSCATION_MARKER;
  }

    /**
     * String representation
     */
    public String toString()
    {
    	return "Call Obf. " + getFirst().getAddress() + " X " + getSecond().getAddress();
    }
}

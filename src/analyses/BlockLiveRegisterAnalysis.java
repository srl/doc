package analyses;

import instructionInterpreters.InstructionInterpreter;
import instructionInterpreters.LraInstructionInterpreter;
import liveRegisterAnalysisDomain.LraDomainFactory;
import x86Interpreter.BackwardBlockAnalysisWorklist;
import x86Interpreter.Worklist;
import domain.DomainFactory;

/**
 * Performs live register analysis on a per block basis.
 */
public class BlockLiveRegisterAnalysis extends AbstractAnalysis
{
  /** Human-readiable title of this <code>Analysis</code> */
  private static final String TITLE = "Block Live Register Analysis";
  
  /** the one and only instance */
  static private BlockLiveRegisterAnalysis instance = null;
  
  /**
   * Users should call getInstance to retrieve an instance of
   * this class. 
   */
  private BlockLiveRegisterAnalysis()
  {
  }
  
  /* (non-Javadoc)
   * @see analyses.Analysis#createDomainFactory()
   */
  public DomainFactory createDomainFactory()
  {
    return new LraDomainFactory();
  }
  
  /* (non-Javadoc)
   * @see analyses.Analysis#createWorklist()
   */
  public Worklist createWorklist()
  {
    return new BackwardBlockAnalysisWorklist();
  }
  
  /* (non-Javadoc)
   * @see analyses.Analysis#getDependency()
   */
  public Analysis getDependency()
  {
    return DefUseAnalysis.getInstance();
  }

  /**
   * Returns the one and only instance of BlockLiveRegisterAnalysis
   * @return an instance of BlockLiveRegisterAnalysis
   */
  static public BlockLiveRegisterAnalysis getInstance()
  {
    if(instance == null)
      instance = new BlockLiveRegisterAnalysis();
    return instance;
  }

  /* (non-Javadoc)
   * @see analyses.Analysis#getInstructionInterpreter()
   */
  public InstructionInterpreter getInstructionInterpreter()
  {
    return new LraInstructionInterpreter();
  }

  /* (non-Javadoc)
   * @see analyses.Analysis#getTitle()
   */
  public String getTitle()
  {
    return TITLE;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return getTitle();
  }
}
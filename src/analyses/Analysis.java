package analyses;

import instructionInterpreters.InstructionInterpreter;
import x86Interpreter.InstructionHistoryList;
import x86Interpreter.Worklist;
import analyzer.Analyzer;
import analyzer.AnnotationProvider;
import domain.DomainFactory;

/**
 * Specifies a set of properties that make up an analysis. Each analysis
 * can vary in such properties as the domain used during the interpretation,
 * the order in which instructions are interpreted, the expected output
 * format, etc. Each new analysis should implement this interface.
 */
public interface Analysis
{
  /**
   * Creates a new DomainFactory implementation that determines which 
   * objects make up the domain.
   * @return a new DomainFactory.
   */
  public DomainFactory createDomainFactory();

  /**
   * Creates an object that can determines which instructions have
   * already been interpreted.
   * 
   * @return a new InstructionHistoryList.
   */
  public InstructionHistoryList getInstructionHistoryList();
  
  /**
   * Creates an <code>Analyzer</code> that performs any needed
   * preprocessing.  The results of the preprocessing can be
   * used during interpretation.
   * @return an object responsible for any needed preprocessing.
   */
  public Analyzer createPreProcessor();

  /**
   * Creates an Analyzer that performs some kind of analysis
   * on a program that has been interpreted using the domain specified
   * by this Analysis object.
   * @return an Analyzer.
   */
  public Analyzer createPostProcessor();

  /**
   * Creates a new instance of a worklist that this analysis uses.
   * The worklist controls the interpretation order.
   * @return a new worklist.
   */
  public Worklist createWorklist();

  /**
   * Returns an <code>AnnotationProvider</code> that is capable of
   * annotating a resource based on the <code>AnalysisResult</code>
   * obtained by the <code>Analyzer</code>.
   * @return an <code>AnnotationProvider</code> that for this <code>Analysis</code>.
   */
  public AnnotationProvider getAnnotationProvider();
  
  /**
   * Returns an <code>Analysis</code> that should be performed before 
   * this analysis, if one exists.  If this <code>Analysis</code> does
   * not depend on any other <code>Analysis</code>, then a 
   * <code>null</code> is returned.
   * @return an <code>Analysis</code> that this <code>Analysis</code>
   *    depends on.
   */
  public Analysis getDependency();
  
  /**
   * Returns an <code>InstructionInterpreter</code> that defines
   * how each instruction should be interpreted.  Most
   * analyses can use the <code>StandardInstructionInterperter</code>.
   * @return the <code>InstructionInterpreter</code> required by this analysis.
   */
  public InstructionInterpreter getInstructionInterpreter();
  
  /**
   * Returns a human-readible title for this <code>Analysis</code>.  
   * It is expected that this title will be displayed to the user.  
   * @return title of this <code>analysis<code>.
   */
  public String getTitle();
}

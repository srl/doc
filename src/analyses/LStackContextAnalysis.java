package analyses;

import csAnalysisDomain.ContextSensitiveWorklist;
import lStackContextAnalysisDomain.LStackFactory;
import instructionInterpreters.LStackInterpreter;
import instructionInterpreters.InstructionInterpreter;
import obfuscations.ObfuscationAnalyzer;
import obfuscations.ObfuscationAnnotationProvider;
import x86Interpreter.InstructionHistoryList;
import x86Interpreter.Worklist;
import analyzer.Analyzer;
import analyzer.AnnotationProvider;
import domain.DomainFactory;

public class LStackContextAnalysis extends AbstractAnalysis
		implements Analysis 
{
	
	/** Human-readible title of this <code>Analysis</code>. */
	private final static String TITLE = "Context-Sensitive Obfuscation Analysis (l-stack-string)";
	
	/** The one and only instance of this class */
	static private LStackContextAnalysis instance = null;

	
	   /**
	   * User getInstance() to get an instance of this class.
	   */
	  private LStackContextAnalysis() {}  
	  /* (non-Javadoc)
	   * @see analyses.Analysis#createPostProcessor()
	   */
	  public Analyzer createPostProcessor()
	  {
	    return new ObfuscationAnalyzer();
	  }
	  
	  /* (non-Javadoc)
	   * @see analyses.Analysis#createDomainFactory()
	   */
	  public DomainFactory createDomainFactory()
	  {
		 return LStackFactory.getInstance();
		  
	  }

	  /* (non-Javadoc)
	   * @see analyses.Analysis#createWorklist()
	   */
	  public Worklist createWorklist()
	  {
      return new ContextSensitiveWorklist();
	  }
	  
	  /**
	   * Returns the one and only instance of this class.
	   * @return an instace of this class.
	   */
	  static public Analysis getInstance()
	  {
	    if(null == instance)
	      instance = new LStackContextAnalysis();
	    return instance;
	  }

	  /* (non-Javadoc)
	   * @see analyses.Analysis#getAnnotationProvider()
	   */
	  public AnnotationProvider getAnnotationProvider()
	  {
	    return new ObfuscationAnnotationProvider();
	  }

	  /* (non-Javadoc)
	   * @see analyses.Analysis#getTitle()
	   */
	  public String getTitle()
	  {
	    return TITLE;
	  }

    /* (non-Javadoc)
     * @see analyses.AbstractAnalysis#getInstructionHistoryList()
     */
    public InstructionHistoryList getInstructionHistoryList()
    {
      return new csAnalysisDomain.ContextAwareInstructionHistoryList();
    }

    /* (non-Javadoc)
     * @see analyses.AbstractAnalysis#getInstructionInterpreter()
     */
    public InstructionInterpreter getInstructionInterpreter()
    {
      return new LStackInterpreter();
    }
    
	  /* (non-Javadoc)
	   * @see java.lang.Object#toString()
	   */
	  public String toString()
	  {
	    return getTitle();
	  }
}

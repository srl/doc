package analyses;

/**
 * Temporary class that provides an array of all available
 * <code>Analysis</code> implementations. Eventually, this
 * class will be replaced by a more flexible method using
 * Eclipse extension points.
 */
public class AllAnalyses
{
  /**
   * Returns an array of all <code>Analysis</code> implementations.
   * @return array of all <code>Analysis</code> implementations.
   */
  public static Analysis[] get()
  {
    Analysis[] result;
    
    result = new Analysis[]
      {
        ContextInsensitiveAnalysis.getInstance(),
   //     BlockLiveRegisterAnalysis.getInstance(),
        LStackContextAnalysis.getInstance(),
        LCallContextAnalysis.getInstance(),
        KCallContextAnalysis.getInstance()
        
      };
    
    return result;
  }
}

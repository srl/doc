package analyses;

import x86InstructionSet.Program;
import analyzer.AnalysisResult;
import analyzer.Analyzer;

/**
 * An <code>Analyzer</code> that does nothing.  Used in cases
 * where the <code>Analysis</code> does not need an 
 * <code>Analyzer</code>.  In these cases, return a
 * <code>NullAnalyzer</code> instead of returning null.
 */
public class NullAnalyzer implements Analyzer
{
  /* (non-Javadoc)
   * @see analyzer.Analyzer#analyze(x86InstructionSet.Program)
   */
  public AnalysisResult analyze(Program program)
  {
    return new NullAnalysisResult();
  }
}

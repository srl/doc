package analyses;

import analyzer.AnalysisResult;

/**
 * Analys <code>AnalysisResult</code> that does nothing.  Used
 * by the <code>NullAnalyzer</code>. 
 */
public class NullAnalysisResult implements AnalysisResult
{
}

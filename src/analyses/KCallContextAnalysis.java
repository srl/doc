package analyses;

import obfuscations.ObfuscationAnalyzer;
import obfuscations.ObfuscationAnnotationProvider;
import kCallContextAnalysisDomain.KCallFactory;
import csAnalysisDomain.ContextAwareInstructionHistoryList;
import csAnalysisDomain.ContextSensitiveWorklist;
import instructionInterpreters.KCallInterpreter;
import instructionInterpreters.InstructionInterpreter;
import x86Interpreter.ForwardAnalysisWorklist;
import x86Interpreter.InstructionHistoryList;
import x86Interpreter.Worklist;
import analyzer.Analyzer;
import analyzer.AnnotationProvider;
import domain.DomainFactory;

public class KCallContextAnalysis extends AbstractAnalysis 
		implements Analysis {

	private final static String TITLE = "Context-Sensitive Obfuscation Analysis (k-call-String)";

	/** The one and only instance of this class */
	static private KCallContextAnalysis instance = null;
   /**
	   * User getInstance() to get an instance of this class.
	   */
	  private KCallContextAnalysis() {}  
	  /* (non-Javadoc)
	   * @see analyses.Analysis#createPostProcessor()
	   */
	  public Analyzer createPostProcessor()
	  {
	    return new ObfuscationAnalyzer();
	  }
	  
	  /* (non-Javadoc)
	   * @see analyses.Analysis#createDomainFactory()
	   */
	  public DomainFactory createDomainFactory()
	  {
		 return KCallFactory.getInstance();
		  
	  }

	  /* (non-Javadoc)
	   * @see analyses.Analysis#createWorklist()
	   */
	  public Worklist createWorklist()
	  {
      return new ContextSensitiveWorklist();
	  }
	  
	  /**
	   * Returns the one and only instance of this class.
	   * @return an instace of this class.
	   */
	  static public Analysis getInstance()
	  {
	    if(null == instance)
	      instance = new KCallContextAnalysis();
	    return instance;
	  }

	  /* (non-Javadoc)
	   * @see analyses.Analysis#getAnnotationProvider()
	   */
	  public AnnotationProvider getAnnotationProvider()
	  {
	    return new ObfuscationAnnotationProvider();
	  }

	  /* (non-Javadoc)
	   * @see analyses.Analysis#getTitle()
	   */
	  public String getTitle()
	  {
	    return TITLE;
	  }

    /* (non-Javadoc)
     * @see analyses.AbstractAnalysis#getInstructionHistoryList()
     */
    public InstructionHistoryList getInstructionHistoryList()
    {
      return new ContextAwareInstructionHistoryList();
    }

    /* (non-Javadoc)
     * @see analyses.AbstractAnalysis#getInstructionInterpreter()
     */
    public InstructionInterpreter getInstructionInterpreter()
    {
      return new KCallInterpreter();
    }
    
	  /* (non-Javadoc)
	   * @see java.lang.Object#toString()
	   */
	  public String toString()
	  {
	    return getTitle();
	  }
}
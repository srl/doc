package analyses;

import instructionInterpreters.InstructionInterpreter;
import instructionInterpreters.ContextInsensitiveInterpreter;
import ui.programViewer.NullAnnotationProvider;
import x86Interpreter.DefaultInstructionHistoryList;
import x86Interpreter.InstructionHistoryList;
import analyzer.Analyzer;
import analyzer.AnnotationProvider;

/**
 * Provides a basic implementation of an <code>Analysis</code> that can
 * be used as a starting point for other <code>Analysis</code>
 * implementations. 
 */
public abstract class AbstractAnalysis implements Analysis
{
  /**
   * Returns an <code>Analysis</code> that this <code>Analysis</code>
   * depends on.  This implementation returns <code>null</code>. 
   * Extensions of this class should override this method if there is
   * a dependency.
   * @return <code>Analysis</code> that this <code>Analysis</code> 
   *    depends on.
   */
  public Analysis getDependency()
  {
    return null;
  }

  /**
   * Returns a default history list that determines if an instruction
   * has been interpreted by checking to see if it has a non-null state.
   */
  public InstructionHistoryList getInstructionHistoryList()
  {
    return new DefaultInstructionHistoryList();
  }
  
  /**
   * Returns an <code>Analyzer</code> that performs any needed 
   * pre-processing.  This implementation returns a 
   * <code>NullAnalyzer</code>  Extensions of this class should
   * override this method if any pre-processnig is needed.
   * @return a <code>NullAnalyzer</code>
   */
  public Analyzer createPreProcessor()
  {
    return new NullAnalyzer();
  }

  /**
   * Returns an <code>Analyzer</code> that performs any needed 
   * post-processing.  This implementation returns a 
   * <code>NullAnalyzer</code>  Extensions of this class should
   * override this method if any post-processnig is needed.
   * @return a <code>NullAnalyzer</code>
   */
  public Analyzer createPostProcessor()
  {
    return new NullAnalyzer();
  }

  /**
   * Returns an <code>AnnotationProvider</code> that performs any 
   * needed post-processing.  This implementation returns a 
   * <code>NullAnnotationProvider</code>  Extensions of this class should
   * override this method if any annotations are needed.
   * @return a <code>NullAnnotationProvider</code>
   */
  public AnnotationProvider getAnnotationProvider()
  {
    return new NullAnnotationProvider();
  }
  
  /**
   * Returns an <code>InstructionInterpreter</code> used to interpret
   * each instruction.  This implementation returns the
   * <code>StandardInstructionInterpreter</code>. Extensions of this
   * class should override this method if a different 
   * <code>InstructionInterpreter</code> is needed.
   * @return <code>StandardInstructionInterpreter</code>
   */
  public InstructionInterpreter getInstructionInterpreter()
  {
    return ContextInsensitiveInterpreter.getInstance();
  }
}

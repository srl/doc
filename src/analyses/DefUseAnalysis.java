package analyses;

import x86Interpreter.ForwardBlockAnalysisWorklist;
import x86Interpreter.Worklist;
import defUseAnalysisDomain.DefUseDomainFactory;
import domain.DomainFactory;

/**
 * Performs a live register analysis for each block in the program.
 */
public class DefUseAnalysis extends AbstractAnalysis implements Analysis
{
  /** Human-readible title of this <code>Analysis</code> */
  private static final String TITLE = "Def-Use Analysis";
  
  /** The one and only instance */
  static private DefUseAnalysis instance = null;
  
  /**
   * Constructor.  Do not use.  Use getInstance instead. 
   */
  private DefUseAnalysis()
  {}

  /* (non-Javadoc)
   * @see analyses.Analysis#createDomainFactory()
   */
  public DomainFactory createDomainFactory()
  {
    return DefUseDomainFactory.getInstance();
  }

  /* (non-Javadoc)
   * @see analyses.Analysis#createWorklist()
   */
  public Worklist createWorklist()
  {
    return new ForwardBlockAnalysisWorklist();
  }

  /**
   * Returns the one and only instance of this class.
   * @return an instance of this class.
   */
  static public DefUseAnalysis getInstance()
  {
    if(instance == null)
      instance = new DefUseAnalysis();
    return instance;
  }

  /* (non-Javadoc)
   * @see analyses.Analysis#getTitle()
   */
  public String getTitle()
  {
    return TITLE;
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return getTitle();
  }
}

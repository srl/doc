package analyses;

import obfuscations.ObfuscationAnalyzer;
import obfuscations.ObfuscationAnnotationProvider;
import vsaAsgDomain.VsaAsgDomainFactory;
import x86Interpreter.ForwardAnalysisWorklist;
import x86Interpreter.Worklist;
import analyzer.Analyzer;
import analyzer.AnnotationProvider;
import domain.DomainFactory;

/**
 * Performs interpertation using Value-Set Analysis and the
 * Abstract Stack Graph.  Analyzes the results to find obfuscations.
 * Implemented as Singleton.
 */
public class ContextInsensitiveAnalysis extends AbstractAnalysis implements Analysis
{
  /** Human-readible title of this <code>Analysis</code>. */
  private static final String TITLE = "Context-Insensitive Obfuscation Analysis";
  
  /** The one and only instance of this class */
  static private ContextInsensitiveAnalysis instance = null;
  
  /**
   * User getInstance() to get an instance of this class.
   */
  private ContextInsensitiveAnalysis() {}  
  
  /* (non-Javadoc)
   * @see analyses.Analysis#createPostProcessor()
   */
  public Analyzer createPostProcessor()
  {
    return new ObfuscationAnalyzer();
  }
  
  /* (non-Javadoc)
   * @see analyses.Analysis#createDomainFactory()
   */
  public DomainFactory createDomainFactory()
  {
    return VsaAsgDomainFactory.getInstance();
  }

  /* (non-Javadoc)
   * @see analyses.Analysis#createWorklist()
   */
  public Worklist createWorklist()
  {
    return new ForwardAnalysisWorklist();
  }

  /**
   * Returns the one and only instance of this class.
   * @return an instace of this class.
   */
  static public Analysis getInstance()
  {
    if(null == instance)
      instance = new ContextInsensitiveAnalysis();
    return instance;
  }

  /* (non-Javadoc)
   * @see analyses.Analysis#getAnnotationProvider()
   */
  public AnnotationProvider getAnnotationProvider()
  {
    return new ObfuscationAnnotationProvider();
  }

  /* (non-Javadoc)
   * @see analyses.Analysis#getTitle()
   */
  public String getTitle()
  {
    return TITLE;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return getTitle();
  }
}

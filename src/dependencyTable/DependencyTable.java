package dependencyTable;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Maintains a record of dependencies for a collection of <code>Objects</code>.
 * An <code>Object</code> can be retrieved from the table based on its 
 * number of dependencies and priority.  The <code>Object</code> chosen is the 
 * <code>Object</code> with the lowest number of dependencies and the 
 * highest priority.
 */
public class DependencyTable  
{
	/**
	 * Stores a list of <code>Item</code>s 
	 * @see DependencyTable.Item
	 */
	private List<Item> dependencyTable = new LinkedList<Item>();
  
  /**
   * Identifies whether this list is currently sorted.
   * To improve performance, the list is only sorted when
   * absolutely necessary (such as just before a search is performed).
   * If an operation requires the list to be sorted, remember to call the
   * sort() function. Likewise, if you modify the order of the list,
   * call the setNotSorted() method to indicate that the list is no 
   * longer sorted.
   */
  private boolean isSorted = true;
  
	/**
	 * Adds an object to this table.  An <code>Item</code>
   * is created that references this object and is initialized 
   * to its default values.  If the object already exists, 
   * an <code>IllegalArgumentException</code> is thrown.
   * The object must implement the <code>Comparable</code>
   * interface to facilate sorting.
	 * @see Item
	 */
	public void add(Comparable obj)
	{
		Item item;
		item = find(obj);
		if(item == null)
    {
			dependencyTable.add(new Item(obj));
      setNotSorted();
    }
		else
			throw new IllegalArgumentException();
	}
	
	/**
	 * Adds an <code>Item</code> to this table.  
   * The <code>Item</code> is assumed to be initialized.
	 */
	public void add(Item item)
	{
		dependencyTable.add(item);
    setNotSorted();
	}	
	
  /**
   * Returns the <code>Item</code> that represents the 
   * input parameter <code>obj</code>. Returns <code>null</code> 
   * if no such <code>Item</code> exists.
   * @param obj representation of the <code>Item</code> to find.  
   *            <code>Item.getObject().equals(obj)</code> must
   *            be <code>true</code>.
   * @return the <code>Item</code>, if found.  <code>null</code> 
   *            otherwise.
   */
  public Item find(Object obj)
  {
    Comparator comparator = new Comparator()
    {
      public int compare(Object o1, Object o2)
      {
        if(o1 instanceof Item && o2 instanceof Item)
          return ((Item)o1).getObject().compareTo(((Item)o2).getObject());
        else if(o1 instanceof Item && !(o2 instanceof Item))
          return ((Item)o1).getObject().compareTo(o2);
        else if( !(o1 instanceof Item) && o2 instanceof Item)
          return ((Comparable)o1).compareTo( ((Item)o2).getObject() );
        else
          return ((Comparable)o1).compareTo(o2);
      }
    };
    
    int index;
    sort();
    index = Collections.binarySearch(this.dependencyTable, obj, comparator);
    if(index >= 0)
      return this.dependencyTable.get(index);
    else
      return null;
  }
  
  /**
   * Retrieves the next <code>Item</code>. The <code>Item</code> returned will have
   * the following properties:<br>
   * <ul>
   *  <li>will have the lowest dependency count
   *  <li>will exist in the input parameter <code>objects</code>.
   *  <li>out of all the items satisfying the first two properites,
   *      will have the highest priority (this function may choose
   *      to ignore this rule).
   * </ul>
   * 
   * @param objects list of <code>Item</code>s of which the next <code>Item</code> should
   *        be chosen from.
   * @return the <code>Item</code> satisfying the properties stated above; 
   *        <code>null</code>, if the objects list is empty; 
   *        <code>null</code>, if this table is empty.
   * @throws <code>IllegalArgumentException</code> if any element
   *        of <code>objects</code> is not in the table.
   */
  public Item getNext(List objects)
  {
		Item 								currentItem;
  	java.util.Iterator 	iter 		= objects.iterator();
  	Item 								result 	= null;
  	
    sort();
  	while(iter.hasNext())
  	{
  		currentItem = find(iter.next());
  		if(currentItem != null)
  		{
  			if(result == null)
  				result = currentItem;
  			else if(currentItem.isBefore(result))
  				result = currentItem;
  		}
  		else
  			throw new IllegalArgumentException();
  	}
  	return result;
  }
  
  /**
   * Returns an iterator for traversing this table.
   */
  public Iterator iterator() 
  { 
    sort();
  	return new Iterator(); 
  }
  
  /**
   * Sets an internal data member to indicate that the
   * ordering of this list has changed and is no longer
   * sorted.
   */
  public void setNotSorted()
  {
    isSorted = false;
  }
  
  /**
   * Sorts this list, but only if the list is not already
   * sorted.  
   */
  public void sort()
  {
    if(!isSorted)
    {
      Collections.sort(dependencyTable);
      isSorted = true;
    }
  }
  
  /**
   * String representation
   */
  public String toString()
  {
    sort();
  	return dependencyTable.toString();
  }

  /**
   * Iterator for traversing this table.
   */
  public class Iterator
  {
   	private java.util.Iterator iter;

   	public Iterator() 					{ iter = dependencyTable.iterator(); }
  	public boolean hasNext() 		{ return iter.hasNext(); }
  	public Item next()  				{ return (Item)iter.next(); }
  }
}

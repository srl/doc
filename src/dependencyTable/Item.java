package dependencyTable;

/**
 * Each element in the <code>DependencyTable</code> is represented as an <code>Item</code>. 
 */
public class Item implements Comparable<Item>
{
	/** Number of dependencies for this item. */
	private int	count;
	
	/** The CFG node that this represents. */
	private Comparable obj;
	
	/**
	 * Priority for this.  When two objects have equal dependency count, the priority
	 * can be used to determine which node to chosen next.  By decreasing
	 * the priority of an object, you are stating that the processing
	 * of the object should be delayed until there are no other objects ready for
   * processing.
	 */
	private Priority priority; 
	
	/**
	 * Creates <code>this</code>, initializes count to zero and priority to default level.
	 */
	public Item(Comparable obj)
	{
		this.obj = obj; 
		this.count = 0;
		this.priority = new Priority();
	}
	
	/**
	 * Creates <code>this</code>, initialized to specified properties.
	 */
	public Item(Comparable obj, int count, Priority priority)
	{
		this.obj = obj;
		this.count = count;
		this.priority = priority;
	}
	
  /**
   * Compares this to <code>item</code>
   * @param item <code>Item</code> to compare to.
   * @return a negative integer, zero, or a positive integer 
   *    as this object is less than, equal to, or greater 
   *    than the specified object.
   */
  public int compareTo(Item item)
  {
    return obj.compareTo(item.obj);
  }
  
	/**
	 * Decreases the number of dependencies for <code>this</code> by one.
	 */
	public void decreaseDependencies()
	{
		count--;
	}
	
	/**
	 * Decreases the priority level by one.
	 */
	public void decreasePriority()
	{
		priority.decrease();
	}
	
	/**
	 * Returns number of dependencies
	 */
	public int getDependencies()
	{
		return count;
	}

	/**
	 * Returns the object referenced by <code>this</code>.
	 */
	public Comparable getObject()
	{
		return obj;
	}
	
	/**
	 * Increases the number of dependencies for <code>this</code> by one.
	 */
	public void increaseDependencies()
	{
		count++;
	}

	/**
	 * Increases the priority level by one.
	 */
	public void increasePriority()
	{
		priority.increase();
	}

	/**
	 * Returns true if this <code>Item</code> should be chosen before another
	 * <code>Item</code>. An <code>Item</code> is considered to be before another if 
	 * (a) its dependency count is less than the other <code>item</code>'s 
	 * dependency count, or (b) it's dependency count 
	 * is the same as the other <code>Item</code>, but its priority is higher.
	 */
	public boolean isBefore(Item item)
	{
		if(this.count < item.count)
			return true;
		else if(this.count == item.count)
			return this.priority.isGreaterThan(item.priority);
		else
			return false;
	}

	/**
	 * String representation
	 */
	public String toString()
	{
		return obj + " --> (" + count + ", " + priority + ")";
	}
}  
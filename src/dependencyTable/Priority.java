package dependencyTable;

/**
 * Maintains a priority level.  When created, it is initialized to
 * a default level.  It can later be increased or decreased by 
 * single-level increments.
 */
public class Priority 
{
  /** The priority level */
	private int level = 0;
	
	/**
	 * Decreases priority by one level.
	 */
	public void decrease()
	{
		if(level != Integer.MIN_VALUE)
			level--;
	}
	
	/**
	 * Increases priority by one level.
	 */
	public void increase()
	{
		if(level != Integer.MAX_VALUE)
			level++;
	}
	
	/**
	 * Returns <code>true</code> if this object's priority 
   * is less than <code>priority</code>
	 */
	public boolean isLessThan(Priority priority)
	{
		return level < priority.level;
	}
	
	/**
	 * Returns <code>true</code> if this object's priority 
   * is greater than <code>priority</code>
	 */
	public boolean isGreaterThan(Priority priority)
	{
		return level > priority.level;
	}

	/**
	 * String representation
	 */
	public String toString()
	{
		return Integer.toString(level);
	}
}

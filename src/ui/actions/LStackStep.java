package ui.actions;

import java.io.IOException;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;

import analyses.AllAnalyses;

import ui.programViewer.AsmFileDocumentProvider;
import ui.programViewer.ProgramViewer;
import x86InstructionSet.Instruction;
import x86InstructionSet.Program;

/**
 * Sets the currently selected instruction as the entry point of
 * the currently opened program.
 */
public class LStackStep extends Action
  implements IWorkbenchWindowActionDelegate 
{
  /** Title of this menu item. */
  private static final String TITLE = "Context Sensitive L-Stack Step";

  /**
   * Creates this.
   */
  public LStackStep()
  {
    super(TITLE);
  }
  
	/**
	 * @see IWorkbenchWindowActionDelegate#dispose()
	 */
	public void dispose() 
	{
	}

	/**
	 * @see IWorkbenchWindowActionDelegate#init(org.eclipse.ui.IWorkbenchWindow)
	 */
	public void init(IWorkbenchWindow window) 
	{
	}

  /* (non-Javadoc)
   * @see org.eclipse.jface.action.IAction#run()
   */
	public void run() {
		Boolean step = true;
		try {
			RunAnalysisAction analysisAction = 
					new RunAnalysisAction("Sensitive L-Stack Step", AllAnalyses.get()[1], step);
			analysisAction.run();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
  
	/**
	 * @see IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action) 
	{
	  run();
	}

	/**
	 * @see IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) 
	{
	}
}

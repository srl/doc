package ui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;

import ui.programViewer.AsmFileDocumentProvider;
import ui.programViewer.ProgramViewer;
import x86InstructionSet.Instruction;
import x86InstructionSet.Program;

/**
 * Sets the currently selected instruction as the entry point of
 * the currently opened program.
 */
public class SetEntryPoint extends Action
  implements IWorkbenchWindowActionDelegate 
{
  /** Title of this menu item. */
  private static final String TITLE = "Set Entry Point";

  /**
   * Creates this.
   */
  public SetEntryPoint()
  {
    super(TITLE);
  }
  
	/**
	 * @see IWorkbenchWindowActionDelegate#dispose()
	 */
	public void dispose() 
	{
	}

	/**
	 * @see IWorkbenchWindowActionDelegate#init(org.eclipse.ui.IWorkbenchWindow)
	 */
	public void init(IWorkbenchWindow window) 
	{
	}

  /* (non-Javadoc)
   * @see org.eclipse.jface.action.IAction#run()
   */
  public void run()
  {
    Program       program;
    Instruction   selectedInstruction;
    ProgramViewer viewer;
    
    viewer = (ProgramViewer)PlatformUI.getWorkbench().
      getActiveWorkbenchWindow().getActivePage().getActiveEditor();
    if(viewer == null)
      return;
    
    selectedInstruction = viewer.getSelectedInstruction();
    program = viewer.getInputProgram();
    program.setEntryPoint(selectedInstruction.getAddress());
    viewer.markEntryPoint();
    int entryIndex = program.getInstructionIndex(selectedInstruction.getAddress());
    AsmFileDocumentProvider.stepNumber = entryIndex;
  }
  
	/**
	 * @see IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action) 
	{
	  run();
	}

	/**
	 * @see IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) 
	{
	}
}

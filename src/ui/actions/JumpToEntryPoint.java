package ui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IActionDelegate;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;

import ui.programViewer.ProgramViewer;

/**
 * Moves current selection to entry point instruction.
 */
public class JumpToEntryPoint extends Action 
  implements IWorkbenchWindowActionDelegate 
{
  /** Text displayed in menu for this action */
  private static final String COMMAND_TEXT = "Jump to &Entry Point";

  /**
   * Creates this.
   */
  public JumpToEntryPoint()
  {
    super(COMMAND_TEXT);
  }
  
	/**
	 * @see IWorkbenchWindowActionDelegate#dispose()
	 */
	public void dispose() 
	{
	}

	/**
	 * @see IWorkbenchWindowActionDelegate#init(org.eclipse.ui.IWorkbenchWindow)
	 */
	public void init(IWorkbenchWindow window) 
	{
	}

  /* (non-Javadoc)
   * @see org.eclipse.jface.action.IAction#run()
   */
  public void run()
  {
    ProgramViewer   viewer;
    
    viewer = 
      (ProgramViewer)PlatformUI.getWorkbench().
        getActiveWorkbenchWindow().getActivePage().getActiveEditor();
    if(viewer == null)
      return;
    
    viewer.moveToEntryPoint();
  }
  
	/**
	 * @see IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action) 
	{
    run();
	}

	/**
	 * @see IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) 
	{
	}
}

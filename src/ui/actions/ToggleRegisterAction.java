package ui.actions;

import java.util.Collections;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;

import ui.programViewer.AsmFileDocumentProvider;
import ui.programViewer.ProgramViewer;

import domain.Register;

/**
 * Runs a given <code>Analysis</code> when activated by the user. 
 */
public class ToggleRegisterAction extends Action implements
    IWorkbenchWindowActionDelegate
{ 
	public Register register = null;
  /**
   * Creates this.
   * @param title title of this <code>action</code>.  Displayed in menu.
   * @param analysis <code>Analysis</code> that is to be run
   *    when the user activates this <code>Action</code>
   */
  public ToggleRegisterAction(String title, Register register)
  { 
    super(title);
    this.register = register;
  }

/* (non-Javadoc)
   * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#dispose()
   */
  public void dispose()
  {
  }

	@Override
	public void run(IAction action) {
		run();		
	}
	
	public void run()
	{
		ProgramViewer viewer;
		viewer = getProgramViewer();
		if(Register.regList.contains(register))
		{
			Register.regList.remove(register);
		}
		else
		{
			Register.regList.add(register);
		}
		Collections.sort(Register.regList);
		viewer.refreshSelection();
	}
	
	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void init(IWorkbenchWindow window) {
		// TODO Auto-generated method stub
		
	}
	
	  /**
	   * Returns the <code>ProgramViewer</code> editor window.  
	   * @return <code>ProgramViewer</code> editor window.  null if
	   *      there is no such window.
	   */
	  private static ProgramViewer getProgramViewer()
	  {
	    return (ProgramViewer)getActiveWorkbenchWindow().getActivePage().getActiveEditor();
	  }
	  
	  /**
	   * Returns the currently active window.
	   * @return currently active window.
	   */
	  private static IWorkbenchWindow getActiveWorkbenchWindow()
	  {
	    return PlatformUI.getWorkbench().getActiveWorkbenchWindow();
	  }


}

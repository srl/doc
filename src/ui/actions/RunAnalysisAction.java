package ui.actions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URISyntaxException;
import java.util.Iterator;
import java.util.Scanner;

import logger.Log;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.IWorkbenchWindowActionDelegate;
import org.eclipse.ui.PlatformUI;

import ui.misc.InterpretMenu;
import ui.programViewer.AsmFileDocumentProvider;
import ui.programViewer.ProgramViewer;
import x86InstructionSet.Address;
import x86InstructionSet.Instruction;
import x86InstructionSet.Program;
import x86Interpreter.ExternalFunctionLoader;
import x86Interpreter.Interpreter;
import analyses.Analysis;
import analyzer.AnalysisResult;
import detectObfuscation.Strings;
import domain.State;

/**
 * Runs a given <code>Analysis</code> when activated by the user. 
 */
public class RunAnalysisAction extends Action implements
    IWorkbenchWindowActionDelegate
{
  /** the <code>Analysis</code> to run */
  private Analysis analysis = null;
  private Boolean shouldStep = false;
  
  /**
   * Creates this.
   * @param title title of this <code>action</code>.  Displayed in menu.
   * @param analysis <code>Analysis</code> that is to be run
   *    when the user activates this <code>Action</code>
   */
  public RunAnalysisAction(String title, Analysis analysis)
  { 
    super(title);
    this.analysis = analysis;
  }
  
  public RunAnalysisAction(String title, Analysis analysis, Boolean shouldStep) throws IOException
  { 
	    super(title);
	    this.analysis = analysis;
	    this.shouldStep = shouldStep;
  }

/* (non-Javadoc)
   * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#dispose()
   */
  public void dispose()
  {
  }

  /**
   * Performs any dependent <code>Analysis</code> for ths current 
   * <code>Analysis</code>.  This requires creating a new
   * <code>RunAnalysisAction</code> and having it perform the
   * <code>Analysis</code>.  
   * <p>
   * The <code>Analysis</code> will, of course, modify 
   * the <code>State</code> of each instruction.  It is
   * the caller's responsibility to save this information if
   * it is needed.  
   * <p>
   * Once complete, this method will copy all <code>State</code>
   * information for each instruction to either 
   * <code>Instruction.setPrevAnalysisBeginState</code> or
   * <code>Instruction.setPrevAnalysisEndState</code>, 
   * depending on whether the <code>State</code> is the
   * begin or end state.  This is to allow the 
   * <code>Analysis</code> to use the <code>State</code> 
   * information from the dependent <code>Analysis</code> and
   * to update the <code>State</code> without fear of
   * overwriting the dependent <code>Analysis</code>.
   * <p>
   * It is safe to call this method even if there is no
   * dependent <code>Analysis</code> for the current 
   * <code>Analysis</code>   
   * @param program
   */
  private void doAnyDependentAnalysis(Program program)
  {
    Analysis  dependentAnalysis;
    
    dependentAnalysis = analysis.getDependency();
    if(dependentAnalysis != null)
    {
      new RunAnalysisAction(
          dependentAnalysis.getTitle(), dependentAnalysis).run();
      copyStateToPrevState(program);
    }
  }

  /**
   * Copies the <code>State</code> information for each
   * <code>Instruction</code> in the given <code>Program</code>
   * into the previous <code>State</code> field. 
   * <p>
   * Formally,<br>
   * <code>
   * For each Instruction i in program
   *   i.setPrevAnalysisBeginState( i.getBeginState )
   *   i.setPrevAnalysisEndState( i.getEndState );
   * </code>
   * 
   * @param program the program to be updated.
   */
  private void copyStateToPrevState(Program program)
  {
    Instruction           instruction; 
    Iterator<Instruction> iter;
    
    iter = program.iterator();    
    while(iter.hasNext())
    {
      instruction = (Instruction)iter.next();
      instruction.setPrevAnalysisBeginState( instruction.getBeginState() );
      instruction.setPrevAnalysisEndState( instruction.getEndState() );
    }
  }

  /**
   * Returns the <code>Shell</code> for the currently active window.
   * @return <code>Shell</code> for currently active window.
   */
  private Shell getShell()
  {
    return getActiveWorkbenchWindow().getShell();
  }

  /**
   * Returns the currently active window.
   * @return currently active window.
   */
  private static IWorkbenchWindow getActiveWorkbenchWindow()
  {
    return PlatformUI.getWorkbench().getActiveWorkbenchWindow();
  }
  
  /**
   * Returns the <code>ProgramViewer</code> editor window.  
   * @return <code>ProgramViewer</code> editor window.  null if
   *      there is no such window.
   */
  private static ProgramViewer getProgramViewer()
  {
    return (ProgramViewer)getActiveWorkbenchWindow().getActivePage().getActiveEditor();
  }

  /* (non-Javadoc)
   * @see org.eclipse.ui.IWorkbenchWindowActionDelegate#init(org.eclipse.ui.IWorkbenchWindow)
   */
  public void init(IWorkbenchWindow window)
  {
  }

  /**
   * Create a <code>ProgressMonitorDialog</code> and starts an
   * <code>Interpter</code> associated with the 
   * <code>ProgressMonitorDialog</code>.  If an error occurs
   * while interpreting, this method will handle the error and
   * display an appropriate message. The caller need not inform
   * the user about any errors, though the caller can use the
   * return value from this function to determine if an error
   * occurred and perhaps use that to decide whether to continue
   * any other analysis work.
   * @param interpreter the <code>Interpreter</code> that should be
   *          launched.
   * @return true if the <code>Interpreter</code> encountered no
   *      problems.  false if an error ocurred.   
   */
  private boolean launchInterpreter(Interpreter interpreter)
  {
    final boolean FORK = true;        // fork new process for interpreter
    final boolean CANCELABLE = true;  // user can cancel interpreter
    
    boolean errorOccurred;
    
    errorOccurred = false;
    
    try
    {
      new ProgressMonitorDialog(getShell()).run(FORK, CANCELABLE, interpreter);
    }
    catch(InvocationTargetException e)
    {
      Log.write(Log.ERROR, "An exception occurred.", e);
      errorOccurred = true;
    }
    catch(InterruptedException e)
    {
      Log.write(Log.INFO, Strings.getUserCancelledInterpretationMsg());
      errorOccurred = true;
    }
    return !errorOccurred;
  }


  
  /**
   * Performs any necessary preparation on the given
   * <code>Program</code> that needs to be done to ensure
   * the <code>Program</code> is ready for <code>Analysis</code>.
   * This method should be called before performing any
   * <code>Analysis</code> on the program.
   * @param program the <code>Program</code> to update.
   */
  private void prepareProgramForAnalysis(Program program)
  {
    program.setNewControlFlowGraph();
    removeExistingStateInformation(program);
  }

  /**
   * Removes any existing <code>State</code> inforation from
   * each <code>Instruction</code> in the given <code>Program</code>.
   * This is done by setting the begin and end state to null.
   * This should be done before performing an <code>Analysis</code>
   * to ensure the program has a clean state.
   * @param program the program to be updated.
   */
  private void removeExistingStateInformation(Program program)
  {
    Instruction           instruction; 
    Iterator<Instruction> iter;
    
    iter = program.iterator();
    while(iter.hasNext())
    {
      instruction = (Instruction)iter.next();
      instruction.setBeginState(null);
      instruction.setEndState(null);
    }
  }

  /**
   * Runs the <code>Analysis</code> given to this object
   * in the constructor. The following steps are performed:<br>
   * - Run any dependent analyses required by this analysis.<br>
   * - Do any necessary preparation on the current program
   *   before needed before running the analysis.<br>
   * - Run the analysis.<br>
   * - Update the  
   */
  public void run()
  {
	int stepNumber;     
	
    AnalysisResult  analysisResult = null;
    Interpreter     interpreter = null;
    Program         program;
    State           state;
    ProgramViewer   viewer;

    assert(analysis != null);

    viewer = getProgramViewer();
    if(viewer != null)
    {    
      program = viewer.getInputProgram();
      doAnyDependentAnalysis(program);    
      prepareProgramForAnalysis(program);
      AsmFileDocumentProvider.currentInstruction = null;
      
      state = analysis.createDomainFactory().createState();
      
      analysis.createPreProcessor().analyze(program);
      
      Instruction first = viewer.getSelectedInstructions().getFirst();
      Instruction last;
      if(this.shouldStep)
      {
		  if(AsmFileDocumentProvider.stepNumber == -1)
			  AsmFileDocumentProvider.stepNumber = 0;
		  
    	  stepNumber = AsmFileDocumentProvider.stepNumber;
		  
	      AsmFileDocumentProvider.stepNumber ++;
      }
      else
      {
    	  AsmFileDocumentProvider.stepNumber = -1;
      }
      
	  last = viewer.getSelectedInstructions().getLast();
     
	try {
		interpreter = new Interpreter(
			      program, 
			      analysis,
			      first,
			      last,
			      state, 
			      ExternalFunctionLoader.loadExternalFunctionTables() );
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (URISyntaxException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
      
      state.init();
      
      viewer.removeAllMarkers();
      if(launchInterpreter(interpreter))
      {
	      if(AsmFileDocumentProvider.currentInstruction != null)
	      {
	    	  viewer.markCurrentInstruction(AsmFileDocumentProvider.currentInstruction);
	      }
	      viewer.markInterpretedInstructions();
	      viewer.markEntryPoint();
	      viewer.markBreakPoint();
          analysisResult = analysis.createPostProcessor().analyze(program);
          analysis.getAnnotationProvider().annotate(
              viewer.getInputResource(), analysisResult, viewer);
          viewer.setSelection(AsmFileDocumentProvider.currentSelection);
      }

      
    }
  }

  /* (non-Javadoc)
   * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
   */
  public void run(IAction action)
  {
    run();
  }

  /* (non-Javadoc)
   * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
   */
  public void selectionChanged(IAction action, ISelection selection)
  {
  }
}

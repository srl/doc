/*
 * StackView.java
 * Created on Apr 10, 2005
 */
package ui.stack;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import ui.directedGraph.DirectedGraphViewer;
import ui.programViewer.InstructionSelection;
import vsaAsgDomain.BeginStateStackGraphContentProvider;
import vsaAsgDomain.EndStateStackGraphContentProvider;
import vsaAsgDomain.StackGraphLabelProvider;

/**
 * Displays a graph representation of an abstract stack.  The only nodes
 * drawn are those on the path from the top of the stack to the bottom
 * (including the top and bottom).  
 */
public class StackView extends ViewPart implements ISelectionListener
{
	static final boolean ALLOW_TRUNCATING = true;
    DirectedGraphViewer viewer;

  final String NO_CONTENT_MESSAGE = "Interpret a file and select an instruction to view " +
		"the stack contents.";
  
  /** User action that displays the begin state */
  private Action useBeginStateAction;
    
  /** User action that displays the end state */
  private Action useEndStateAction;
  
  /** 
   * Specifies which state is currently being displayed: the 
   * begin state or the end state 
   */
  private DisplayState displayedState = DisplayState.END_STATE;
  
  /** Enum that specifies the two state options: begin or end */
  enum DisplayState
  {
    BEGIN_STATE,
    END_STATE
  };
  
  /**
   * Creates all actions that are applicable to this view. 
   */
  private void createActions()
  {
    useBeginStateAction = new Action("Show Begin State") 
    {
      public void run()
      {
        displayedState = DisplayState.BEGIN_STATE;
        updateActionChecks();
        viewer.setContentProvider(new BeginStateStackGraphContentProvider());
        viewer.refresh();
      }
    };
    
    useEndStateAction = new Action("Show End State")
    {
      public void run()
      {
        displayedState = DisplayState.END_STATE;
        updateActionChecks();
        viewer.setContentProvider(new EndStateStackGraphContentProvider());
        viewer.refresh();
      }
    };    
  }

  /**
   * Creates the menu for this view. 
   */
  private void createMenu()
  {
    IMenuManager menu;
    
    menu = getViewSite().getActionBars().getMenuManager();
    menu.add(useBeginStateAction);
    menu.add(useEndStateAction);
    updateActionChecks();
  }
  
    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    public void createPartControl(Composite parent)
    {
      createStackViewer(parent);
      createActions();
      createMenu();
      createToolbar();
      getViewSite().getPage().addSelectionListener(this);
    }

    /**
     * @param parent
     */
    private void createStackViewer(Composite parent)
    {
      viewer = new DirectedGraphViewer(parent, ALLOW_TRUNCATING);
      viewer.setNoContentMessage(NO_CONTENT_MESSAGE);
      viewer.setContentProvider(new EndStateStackGraphContentProvider());
      viewer.setLabelProvider(new StackGraphLabelProvider());
    }

    /**
     * Creates the toolbar for this view. 
     */
    private void createToolbar()
    {
      IToolBarManager toolbar;
      
      toolbar = getViewSite().getActionBars().getToolBarManager();
      toolbar.add(useBeginStateAction);
      toolbar.add(useEndStateAction);
    }
    
    /**
     * Destroys this object
     */
    public void dispose()
    {
        getViewSite().getPage().removeSelectionListener(this);
        super.dispose();
    }
    
    /**
     * Updates the stack contents with new state inforamtion.
     * This class is a SelectionListener for this Page (a page contains all
     * of the views for this perspective).  The FileView notifies the Page
     * when a new instruction is selected and the Page then calls
     * this function.  This function obtains the state for the selected 
     * instruction and displays the stack contents for that state.
     */
    public void selectionChanged(IWorkbenchPart part, ISelection selection)
    {
      if (selection instanceof InstructionSelection) 
      {
        viewer.setInput( ((InstructionSelection)selection).getInstruction());

        viewer.refresh();
      }
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#setFocus()
     */
    public void setFocus()
    {
        // nothing needs to be done here.
    }

    /**
     * Adds or removes checks from the view actions based on the current
     * state of the view. 
     */
    private void updateActionChecks()
    {
      useBeginStateAction.setChecked(displayedState == DisplayState.BEGIN_STATE);  
      useEndStateAction.setChecked(displayedState == DisplayState.END_STATE);
    }
}

/*
 * InstructionSelection.java
 * Created on Apr 18, 2005
 */
package ui.programViewer;



import org.eclipse.jface.viewers.ISelection;

import x86InstructionSet.Instruction;



/**
 * An instance of this class is created and given to
 * any ISelectionChangedListeners that are listening
 * to the ProgramViewer (and perhaps other classes)
 * when an instruction is selected.
 * Each instance of InstructionSelection stores the
 * selected instruction. 
 */
public class InstructionSelection implements ISelection
{
    Instruction instruction;

    /**
     * Constructs a new InstructionSelection.
     * @param instruction
     */
    public InstructionSelection(Instruction instruction)
    {
        this.instruction = instruction;
    }
    
    /**
     * Retrieves the selected instruction.
     */
    public Instruction getInstruction()
    {
        return instruction;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ISelection#isEmpty()
     */
    public boolean isEmpty()
    {
        return false;
    }
}

/*
 * AsmFileDocumentProvider.java
 * Created on Apr 19, 2005
 */
package ui.programViewer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;

import logger.Log;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.FileDocumentProvider;
import org.eclipse.ui.part.FileEditorInput;

import parser.Parser;
import ui.outputters.DialogOutputter;
import ui.outputters.Outputter;
import ui.outputters.TpiawoOutputter;
import ui.outputters.UnmorphOutputter;
import x86InstructionSet.Address;
import x86InstructionSet.Program;
import detectObfuscation.Preferences;
import detectObfuscation.Strings;



/**
 * Converts an input object into a IDocument for display in
 * a TextViewer.
 */
public class AsmFileDocumentProvider extends FileDocumentProvider
{
    Program 		program;
    public static int stepNumber = 0;
    public static Address currentInstruction = null;
    public static InstructionSelection currentSelection = null;
        
    /**
     * Returns the program that was obtained after parsing/interpreting 
     * the input element in createDocument.  Returns null if the
     * input element could not be parsed/interpreted or if createDocument
     * was never called.
     */
    public Program getProgram()
    {
        return program;
    }
    
    /**
     * Loads and parses an assembly file 
     * (obtained from Ollydbg).  Returns a String representation
     * of the program.  After calling this function, GetProgram
     * can be called to retrieve the program as an instance of Program. 
     */
    public IDocument createDocument(Object element) throws CoreException
    {
      program = null;
      //stepNumber allows doc to remember which line it should step to.
      stepNumber = -1;
      
      
      if(element instanceof IFileEditorInput)
      {
        IDocument 			document	= new Document();
    		FileEditorInput file			= (FileEditorInput)element;
    		String 					filePath 	= file.getPath().toString();

        program = parseProgram(filePath);
    		program	= parseExternalFunctionStubs(program);

        Log.write(Log.INFO, "Entry point is at " + program.getEntryPoint() );
      // 	Log.write(Log.INFO, "Function start " + program.getStartAddress()); 
      // 	Log.write(Log.INFO, "Function end " + program.getEndAddress());
       	
        prepareOutputter();
  	    document.set(program.toString());
  	    return document;
      }
      else
        return null;
    }
    
    /**
     * Parses each .asm file in the exports directory and adds the 
     * parsed content to the input program.  Thus, the imported stubs
     * are placed in the same Program object as the program under analysis.
     * @throws PartInitException
     */
    public Program parseExternalFunctionStubs(Program program) throws PartInitException
    {
      String      currentFile	= new String(); 
      File[]      exports; 
      FileFilter  exportsFilter;         
      Parser      fileParser = Parser.getInstance();
      File        importsFolder; 
      String      importsPath;
      Program     result = program;
      IPreferenceStore 	store = PlatformUI.getPreferenceStore();		

      // this filter retrieves only .asm files.
      exportsFilter = new FileFilter() 
      { 
          public boolean accept(File pathname)
          {
              return pathname.isFile() && pathname.getName().endsWith(".asm");
          }
      };

      // grab the imports folder and retrieve all .asm files.
      importsPath = store.getString(Preferences.STUB_FILE_PATH);
      if(!importsPath.equals(""))
      {
        importsFolder = new File(importsPath);
        exports = importsFolder.listFiles(exportsFilter);
	        
        if(exports != null)
        {		
	        // for each .asm file, parse it and add it to the program.
	        try
	        {
		        for(int i = 0; i < exports.length; i++)
		        {
	            currentFile = exports[i].getPath();
	            
	            result = fileParser.parse(currentFile, result);	            
		        }
	        }
	        catch (java.io.FileNotFoundException e) 
			    {
            Log.write(Log.ERROR, Strings.getFileNotFoundMsg(currentFile));
		        throw new PartInitException(Strings.getFileNotFoundMsg(currentFile));
			    }
			    catch (ollydbgParser.ParseException e) 
			    {
            Log.write(Log.ERROR, Strings.getParseErrorMsg(e.getMessage()));
		        throw new PartInitException(Strings.getShortParseErrorMsg((e.getMessage())));
			    } 
			    catch (Exception e) 
			    {
            Log.write(Log.ERROR, Strings.getUnexpectedErrorMsg(e.getMessage()), e);
		        throw new PartInitException(Strings.getShortUnexpectedParseErrorMsg(e.getMessage()));
			    }    
        }
      }
      return result;        
    }
    
    /**
     * Parses the program specified by the input file.  Handles errors by
     * displaying error messages, thus the caller need not check for
     * errors.
     * @throws PartInitException
     */
    private Program parseProgram(String filePath) throws PartInitException
    {
  		Parser 		fileParser 	= Parser.getInstance();
  		Program		result;
  
  		try
  		{
  		    result = fileParser.parse(filePath);
  		}
	    catch (java.io.FileNotFoundException e) 
	    {
        Log.write(Log.ERROR, Strings.getFileNotFoundMsg(filePath));
        throw new PartInitException(Strings.getFileNotFoundMsg(filePath));
	    }
	    catch (ollydbgParser.ParseException e) 
	    {
        Log.write(Log.ERROR, Strings.getParseErrorMsg(e.getMessage()));
        throw new PartInitException(Strings.getParseErrorMsg((e.getMessage())));
	    } 
	    catch (Exception e) 
	    {
        Log.write(Log.ERROR, Strings.getUnexpectedParseErrorMsg(e.getMessage()), e);
        throw new PartInitException(Strings.getUnexpectedParseErrorMsg(e.getMessage()));
	    }    
	    return result;
    }

    /**
     * Performs initialization of the Outputter to prepare it for 
     * printing.
     */
    private void prepareOutputter()
    {
      int       outputterType;
      Outputter selectedOutputter;
      
      outputterType = PlatformUI.getPreferenceStore().getInt(Preferences.OUTPUTTER);
      switch(outputterType)
      {
      case Preferences.DIALOG_OUTPUTTER:
        selectedOutputter = new DialogOutputter();
        break;
        
      case Preferences.TPIAWO_OUTPUTTER:
        selectedOutputter = new TpiawoOutputter();
        break;
        
      case Preferences.UNMORPH_OUTPUTTER:
        selectedOutputter = new UnmorphOutputter();
        break;
        
      default:
        selectedOutputter = new TpiawoOutputter();
        break;
      }
      Outputter.setOutputter(selectedOutputter);
    }
}

/*
 * ProgramViewer.java
 * Created on Apr 19, 2005
 */
package ui.programViewer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;

import logger.Log;
import obfuscations.Obfuscation;
import obfuscations.ObfuscationList;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.editors.text.TextEditor;
import org.eclipse.ui.texteditor.MarkerUtilities;

import ui.controlFlow.ProgramSelection;
import ui.markerViewers.MarkerFields;
import ui.markerViewers.MarkerIds;
import x86InstructionSet.Instruction;
import x86InstructionSet.Address;
import x86InstructionSet.Program;
import x86InstructionSet.UnknownAddress;
import x86Interpreter.InstructionList;
import detectObfuscation.Strings;

/**
 * Displays the contents of a Program to the user.  The Program cannot
 * be modified, but individual instructions can be selected which trigger
 * reactions in other view windows. 
 */
public class ProgramViewer extends TextEditor implements ISelectionProvider
{
    private LinkedList<ISelectionChangedListener>	listeners = 
      new LinkedList<ISelectionChangedListener>();
    
    /**
     * Creates a new ProgramViewer.  There is not need to call this 
     * constructor directly.  The Eclipse framework will instantiate
     * a ProgramViewer when a .asm file is opened.
     *
     */
    public ProgramViewer()
    {
        super();
        setDocumentProvider(new AsmFileDocumentProvider());
    }

    /**
     * Adds a selection listener to this viewer.  Selection listeners are
     * notified when an instruction is selected.  If a view wishes to be
     * notified of instruction selections, it should call 
     * IWorkbenchSite.addSelectionListener instead of this function.
     * Example:
     * <code>
     * public void createPartControl() {
     *  ...
     *  getViewSite().getPage().addSelectionListener(this);
     * }
     * </code>
     */
    public void addSelectionChangedListener(ISelectionChangedListener listener)
    {
        if(!listeners.contains(listener))
            listeners.add(listener);
    }

    /** 
     * Initializes this control.
     */
    public void createPartControl(Composite parent)
    {
        super.createPartControl(parent);
        
        // Close any previously opened editors before opening this editor.  Multiple editors
        // are not allowed, because the state stores some of its data members as 
        // static.  If multiple editors are opened, then two states that belong to
        // completely different assembly files would get mixed up and crashes would
        // follow
        IEditorReference editors[] = getSite().getPage().getEditorReferences();
        for(int i = 0; i < editors.length; i++)
        {
            IEditorPart editor = editors[i].getEditor(false);
            if(editor != null && editor  != this)
                getSite().getPage().closeEditor(editor, false);
        }
        
        markEntryPoint();
    }
    
    /**
     * Disposes of this control. 
     */
    public void dispose()
    {
        getSite().setSelectionProvider(null);
        removeAllMarkers();
    }

    /**
     * Returns the program that is providing the input to this editor.
     */
    public Program getInputProgram()
    {
        return ((AsmFileDocumentProvider)getDocumentProvider()).getProgram();
    }
    
    /**
     * Returns the resource that is providing the input to this editor.
     */
    public IResource getInputResource()
    {
        return ((IFileEditorInput)getEditorInput()).getFile();
    }
    
    public int getLineNumber(Instruction instruction)
    {
      return getInputProgram().getInstructionIndex(instruction.getAddress());
    }
    
    /**
     * Returns an array containing all obfuscations for the input instruction.
     */
    public ObfuscationList getObfuscationsForInstruction(Instruction instruction)
    {
        ObfuscationList result = new ObfuscationList();
        if(getInputProgram() != null)
        {
	        ObfuscationList obfuscations = getInputProgram().getObfuscations();
	        
	        ObfuscationList.Iterator iter = obfuscations.iterator();
	        while(iter.hasNext())
	        {
	            Obfuscation obfuscation = iter.next();
	            if(obfuscation.getFirst().equals(instruction))
	                result.add(obfuscation);
	        }
        }
        return result;
    }

    public int getOffsetAtLine(int lineNumber)
    {
      return getSourceViewer().getTextWidget().getOffsetAtLine(lineNumber);
    }
    
    /**
     * Returns the instruction that is currently under the caret in the
     * Text widget.
     */
    public Instruction getSelectedInstruction()
    {
        if(getInputProgram() != null)
            return  getInputProgram().getInstruction( getSelectedLineNumber() );
        else
            return null;
    }
    
    /**
     * Returns a list of instructions that are currently highlighted.
     * Even if the instruction is only partially highlighted, it is
     * included in this list. If no characters are highlighted, then
     * an empty list is returned. 
     */
    public InstructionList getSelectedInstructions()
    {
      int             endLine;
      InstructionList result;
      ITextSelection  selection;
      int             startLine;
      
      result = new InstructionList();
      selection = (ITextSelection)doGetSelection();
      if(selection.getLength() > 0)
      {
      	startLine = selection.getStartLine();
      	endLine = selection.getEndLine();
      	
      	for(int i = startLine; i < endLine + 1; i++)
      		result.add( getInputProgram().getInstruction(i) );
      }
    	return result;
    }
    
    /**
     * Returns the line number (one-based) of the currently selected
     * line in the Text widget.
     */
    public int getSelectedLineNumber()
    {
        StyledText widget = getSourceViewer().getTextWidget();
        return widget.getLineAtOffset(widget.getCaretOffset());
    }
    
    /**
     * Returns the currently selection instruction in the 
     * form of an InstructionSelection.
     */
    public ISelection getSelection()
    {
        ISourceViewer viewer = getSourceViewer();
        if(viewer != null && viewer.getTextWidget() != null)
	        return new InstructionSelection(getSelectedInstruction());
        else
            return null;
    }

    /**
     * Handles changes in cursor position by notifying all listeners
     * that a instruction has been selected.
     */
    protected void handleCursorPositionChanged()
	{
	    super.handleCursorPositionChanged();
	    
	    if(getInputProgram() != null)
	    {
	    	setTargetMarkers();
		    setNextInstructionMarkers();		    
		    notifySelectionChangedListeners(new ProgramSelection(getInputProgram()));
		    notifySelectionChangedListeners(getSelection());
	    }
	}
    
    /**
     * This function is called by the Eclipse framework to determine if the
     * editor allows changes to be made to the displayed text.  
     * Since we are using this editor simply as a way to display information, 
     * we do not want the user to edit the document and thus always 
     * return false to indicate this.
     */
    public boolean isEditable()
    {
    	return false;
    }
    
    /**
     * This function is called by the Eclipse framework to determine if the user
     * can edit the input document.  Since we are using this editor simply as a
     * way to display information, we do not want the user to edit the document
     * and thus always return false to indicate this.  Specifically, by returning
     * false, we are preventing the user from accessing the shift right and
     * shift left context-menu options.
     */
    public boolean isEditorInputModifiable()
    {
    	return false;
    }    
    
    /**
     * Adds a marker icon next to the entry point instruction (on screen).
     */
    public void markEntryPoint()
    {
      int                     addressLength;
      HashMap<String, Object> attributes;
      Address                 entryPointAddress;
      Instruction             instruction; 
      int                     linenumber;
      int                     lineOffset;

    	removeMarkers(MarkerIds.ENTRY_POINT_MARKER);
    	
    	// if there is no input program (program could not be parsed maybe), then
    	// don't try to label the entry point, there is none.
    	if(getInputProgram() == null)
    		return;
    	
    	entryPointAddress = getInputProgram().getEntryPoint();
    	
    	// Make sure an entry point exists.
    	if(entryPointAddress == UnknownAddress.UNKNOWN)
    		return;

    	// get the entry point instruction and mark it.
  		instruction = getInputProgram().getInstruction(entryPointAddress);
	    try
	    {
	    	linenumber		= getInputProgram().getInstructionIndex(instruction.getAddress());
        lineOffset 		= getSourceViewer().getTextWidget().getOffsetAtLine(linenumber);
        addressLength = instruction.getAddress().toString().length();

      	attributes = new HashMap<String, Object>();
        attributes.put(MarkerFields.INSTRUCTION, instruction);
        attributes.put(IMarker.LINE_NUMBER, new Integer(linenumber));
        attributes.put(IMarker.CHAR_START, new Integer(lineOffset));
        attributes.put(IMarker.CHAR_END, new Integer(lineOffset + addressLength));
        MarkerUtilities.createMarker(getInputResource(), attributes, 
        		MarkerIds.ENTRY_POINT_MARKER);
	    }
	    catch(CoreException e)
	    {
        Log.write(Log.ERROR, Strings.getUnexpectedErrorMsg(e.getMessage()), e);
	    }
    }
    
    /**
     * Adds a marker icon next to the entry point instruction (on screen).
     */
    public void markBreakPoint()
    {
      int                     addressLength;
      HashMap<String, Object> attributes;
      Address                 breakPointAddress;
      Instruction             instruction; 
      int                     linenumber;
      int                     lineOffset;
    	
  		removeMarkers(MarkerIds.BREAK_POINT_MARKER);
      
    	// if there is no input program (program could not be parsed maybe), then
    	// don't try to label the entry point, there is none.
    	if(getInputProgram() == null)
    		return;
    	
    	for(int i=0;i<getInputProgram().getBreakPoints().size();i++)
    	{
	    	breakPointAddress = getInputProgram().getBreakPoints().get(i);
	    	
	    	// Make sure an entry point exists.
	    	if(breakPointAddress == UnknownAddress.UNKNOWN)
	    		return;
	
	    	// get the entry point instruction and mark it.
	  		instruction = getInputProgram().getInstruction(breakPointAddress);
		    try
		    {
		    	linenumber		= getInputProgram().getInstructionIndex(instruction.getAddress());
	        lineOffset 		= getSourceViewer().getTextWidget().getOffsetAtLine(linenumber);
	        addressLength = instruction.getAddress().toString().length();
	
	      	attributes = new HashMap<String, Object>();
	        attributes.put(MarkerFields.INSTRUCTION, instruction);
	        attributes.put(IMarker.LINE_NUMBER, new Integer(linenumber));
	        attributes.put(IMarker.CHAR_START, new Integer(lineOffset));
	        attributes.put(IMarker.CHAR_END, new Integer(lineOffset + addressLength));
	        MarkerUtilities.createMarker(getInputResource(), attributes, 
	        		MarkerIds.BREAK_POINT_MARKER);
		    }
		    catch(CoreException e)
		    {
	        Log.write(Log.ERROR, Strings.getUnexpectedErrorMsg(e.getMessage()), e);
		    }
    	}
    }

    
    /**
     * Returns true if a marker with the given ID exists at the given line number.
     */
    public boolean markerExists(String markerId, int lineNumber)
    {
        try
        {
            IMarker[] markers = getInputResource().findMarkers(markerId, false, 0);
            for(int i = 0; i < markers.length; i++)
            {
                if( ((Integer)markers[i].getAttribute(IMarker.LINE_NUMBER)).intValue() == 
                    lineNumber )
                {
                    return true;
                }
            }
        }
        catch(CoreException e)
        {
          Log.write(Log.ERROR, "CoreException in markerExists()", e);
          return false;
        }
        return false;
    }
    
    /**
     * Places a marker next to each instruction that has been interpreted to signify
     * to the user that the instruction has been interpreted.
     */
    public void markInterpretedInstructions()
    {
      int                       addressLength;
      HashMap<String, Object>   attributes;
      Iterator<Instruction>     iter;
    	int 			                linenumber;
    	int 			                lineOffset;

    	removeMarkers(MarkerIds.INTERPRETED_INSTRUCTION_MARKER);
    	
    	iter = getInputProgram().iterator();
    	while(iter.hasNext())
    	{
    		Instruction instruction = (Instruction)iter.next();
    		if(instruction.getEndState() != null)
    		{
    			// this instruction has a state, so it was interpretted, therefore
    			// add an interpretation marker.
    	    try
    	    {
  		    	linenumber		= getInputProgram().getInstructionIndex(instruction.getAddress());
            lineOffset 		= getSourceViewer().getTextWidget().getOffsetAtLine(linenumber);
            addressLength = instruction.getAddress().toString().length();

          	attributes = new HashMap<String, Object>();
            attributes.put(MarkerFields.INSTRUCTION, instruction);
            attributes.put(IMarker.LINE_NUMBER, new Integer(linenumber));
            attributes.put(IMarker.CHAR_START, new Integer(lineOffset));
            attributes.put(IMarker.CHAR_END, new Integer(lineOffset + addressLength));
            MarkerUtilities.createMarker(getInputResource(), attributes, 
            		MarkerIds.INTERPRETED_INSTRUCTION_MARKER);
    	    }
    	    catch(CoreException e)
    	    {
            Log.write(Log.ERROR, Strings.getUnexpectedErrorMsg(e.getMessage()), e);
    	    }
    		}
    	}    	
    }
    
    /**
     * Selects the entry point instruction.
     */
    public void moveToEntryPoint()
    {    
    	int 				addressLength;
    	Address 		entryPointAddress;
  		Instruction instruction; 
    	int 				linenumber;
    	int 				lineOffset;

    	entryPointAddress = getInputProgram().getEntryPoint();
    	
    	// Make sure an entry point exists.
    	if(entryPointAddress == UnknownAddress.UNKNOWN)
    		return;
  		instruction = getInputProgram().getInstruction(entryPointAddress);

    	linenumber		= getInputProgram().getInstructionIndex(instruction.getAddress());
      lineOffset 		= getSourceViewer().getTextWidget().getOffsetAtLine(linenumber);
      addressLength = instruction.getAddress().toString().length();

    	super.selectAndReveal(lineOffset+1, addressLength);
    }
    
    /**
     * Notifies all listeners of a the currently selection instruction.
     * @param selection an ISelection holding the currently selected instruction.
     */
    private void notifySelectionChangedListeners(ISelection selection)
    {
        SelectionChangedEvent changedEvent = new SelectionChangedEvent(this, selection); 
        Iterator iter = listeners.iterator();
        while(iter.hasNext())
            ((ISelectionChangedListener)iter.next()).selectionChanged(changedEvent);
    }
    
    /**
     * Removes all markers from the display.
     */
    public void removeAllMarkers()
    {
	    try
	    {
	        getInputResource().deleteMarkers(null, false, 1);
	    }
	    catch(CoreException e)
	    {
	    }
    }
    
    /**
     * Removes all markers of the specified type.  
     * Example:
     *   removeMarker("detectObfuscation.callObfuscationMarker");
     */
    public void removeMarkers(String markerType)
    {
	    try
	    {
	        getInputResource().deleteMarkers(markerType, false, 1);
	    }
	    catch(CoreException e)
	    {
	    }
    }
    
    /**
     * Removes a listener from this object.
     */
    public void removeSelectionChangedListener(ISelectionChangedListener listener)
    {
        listeners.remove(listener);
    }

    /**
     * Sets focus to this control.
     */
    public void setFocus()
    {
        if(getInputProgram() != null)
            getSite().setSelectionProvider(this);
    }

	/**
	 * Sets the NextInstruction Markers for the selected
	 * instruction.  These markers show which instruction
	 * is interpretted next.  Remove any previously existing
	 * NextInstruction markers.
	 */
	private void setNextInstructionMarkers() 
	{
    int                      addressLength;
	  HashMap<String, Object>  attributes;
	  Instruction              instruction;
	  int                      lineNumber;
	  int                      lineOffset;

    // remove any previously added next instruction markers.
		removeMarkers(MarkerIds.NEXT_INSTRUCTION_MARKER);
		
		// add next instruction markers for the current instruction
		
		// get next instruction(s) from control-flow graph
		InstructionList next =
			getInputProgram().getControlFlowGraph().getNext(getSelectedInstruction());
		
		// add markers at each instruction
		try
		{
		    Iterator<Instruction> iter = next.iterator();
		    while(iter.hasNext())
		    {
		        instruction = iter.next();
		        lineNumber = getInputProgram().getInstructionIndex( 
		                instruction.getAddress() ) ;
		        lineOffset = getSourceViewer().getTextWidget().getOffsetAtLine(lineNumber);
		        addressLength = instruction.getAddress().toString().length();
		        
		        if(!markerExists(MarkerIds.NEXT_INSTRUCTION_MARKER, lineNumber))
		        {
		            attributes = new HashMap<String, Object>();
		            attributes.put(IMarker.LINE_NUMBER, new Integer(lineNumber));
		            attributes.put(MarkerFields.INSTRUCTION, instruction);
		            attributes.put(IMarker.CHAR_START, new Integer(lineOffset));
		            attributes.put(IMarker.CHAR_END, new Integer(lineOffset + addressLength));
			        MarkerUtilities.createMarker(getInputResource(), attributes, 
			        		MarkerIds.NEXT_INSTRUCTION_MARKER);
		        }
		    }
		}
		catch(CoreException e)
		{
		    throw new Error("CoreException in setNextInstructionMarkers");
		}
	}
    
    /**
     * This function is not implemented and does nothing.
     */
    public void setSelection(InstructionSelection selection)
    {
    	if(selection != null)
    	{
			setTargetMarkers(selection);
			setNextInstructionMarkers(selection);		    
	    	notifySelectionChangedListeners(selection);
    	}
    }

    private void setTargetMarkers(InstructionSelection selection) {
    	int                       addressLength;
        HashMap<String, Object>   attributes;
        ObfuscationList.Iterator  iter;
        int                       lineNumber;
        int                       lineOffset;
        ObfuscationList           obfuscations;
        Obfuscation               obfuscation;

        // remove any previously added target markers.
  	    removeMarkers(MarkerIds.TARGET_MARKER);
  	    
  	    // add target markers for the current instruction
  	    // if it contains obfuscations.
  	    obfuscations = getObfuscationsForInstruction(selection.getInstruction());
  	    try
  	    {
  		    iter = obfuscations.iterator();
  		    while(iter.hasNext())
  		    {
  		        obfuscation = iter.next();
  		        if(obfuscation.getSecond() != null)
  		        {
  		            lineNumber = getInputProgram().getInstructionIndex( 
  		                    obfuscation.getSecond().getAddress() ) ;
  		            lineOffset = getSourceViewer().getTextWidget().getOffsetAtLine(lineNumber);
  		            addressLength = obfuscation.getSecond().getAddress().toString().length();
  		            
  		            if(!markerExists(MarkerIds.TARGET_MARKER, lineNumber))
  		            {
  			            attributes = new HashMap<String, Object>();
  			            attributes.put(IMarker.LINE_NUMBER, new Integer(lineNumber));
  			            attributes.put(MarkerFields.INSTRUCTION, obfuscation.getSecond());
  			            attributes.put(IMarker.CHAR_START, new Integer(lineOffset));
  			            attributes.put(IMarker.CHAR_END, new Integer(lineOffset + addressLength));
  				        MarkerUtilities.createMarker(getInputResource(), attributes, MarkerIds.TARGET_MARKER);
  		            }
  		        }
  		    }
  	    }
  	    catch(CoreException e)
  	    {
  	        throw new Error("CoreException in setTargetMarkers");
  	    }  
	}

	/**
     * Parameterized with selection.
     * This sets the markers indicating what the next instruction could
     * be.  This is called when a step or break needs to simulate a user
     * clicking on the last interpreted instruction.
     * @param selection
     */
    private void setNextInstructionMarkers(InstructionSelection selection) {
		// TODO Auto-generated method stub
    	int                      addressLength;
  	  HashMap<String, Object>  attributes;
  	  Instruction              instruction;
  	  int                      lineNumber;
  	  int                      lineOffset;

      // remove any previously added next instruction markers.
  		removeMarkers(MarkerIds.NEXT_INSTRUCTION_MARKER);
  		
  		// add next instruction markers for the current instruction
  		
  		// get next instruction(s) from control-flow graph
  		InstructionList next =
  			getInputProgram().getControlFlowGraph().getNext(selection.getInstruction());
  		
  		// add markers at each instruction
  		try
  		{
  		    Iterator<Instruction> iter = next.iterator();
  		    while(iter.hasNext())
  		    {
  		        instruction = iter.next();
  		        lineNumber = getInputProgram().getInstructionIndex( 
  		                instruction.getAddress() ) ;
  		        lineOffset = getSourceViewer().getTextWidget().getOffsetAtLine(lineNumber);
  		        addressLength = instruction.getAddress().toString().length();
  		        
  		        if(!markerExists(MarkerIds.NEXT_INSTRUCTION_MARKER, lineNumber))
  		        {
  		            attributes = new HashMap<String, Object>();
  		            attributes.put(IMarker.LINE_NUMBER, new Integer(lineNumber));
  		            attributes.put(MarkerFields.INSTRUCTION, instruction);
  		            attributes.put(IMarker.CHAR_START, new Integer(lineOffset));
  		            attributes.put(IMarker.CHAR_END, new Integer(lineOffset + addressLength));
  			        MarkerUtilities.createMarker(getInputResource(), attributes, 
  			        		MarkerIds.NEXT_INSTRUCTION_MARKER);
  		        }
  		    }
  		}
  		catch(CoreException e)
  		{
  		    throw new Error("CoreException in setNextInstructionMarkers");
  		}
  	}

	/**
     * Place Target markers for the currently selected instruction. 
     * Remove any previously existing target markers. (Target markers
     * mark the instructions associated with an obfuscation).
     */
    private void setTargetMarkers()
    {
      int                       addressLength;
      HashMap<String, Object>   attributes;
      ObfuscationList.Iterator  iter;
      int                       lineNumber;
      int                       lineOffset;
      ObfuscationList           obfuscations;
      Obfuscation               obfuscation;

      // remove any previously added target markers.
	    removeMarkers(MarkerIds.TARGET_MARKER);
	    
	    // add target markers for the current instruction
	    // if it contains obfuscations.
	    obfuscations = getObfuscationsForInstruction(getSelectedInstruction());
	    try
	    {
		    iter = obfuscations.iterator();
		    while(iter.hasNext())
		    {
		        obfuscation = iter.next();
		        if(obfuscation.getSecond() != null)
		        {
		            lineNumber = getInputProgram().getInstructionIndex( 
		                    obfuscation.getSecond().getAddress() ) ;
		            lineOffset = getSourceViewer().getTextWidget().getOffsetAtLine(lineNumber);
		            addressLength = obfuscation.getSecond().getAddress().toString().length();
		            
		            if(!markerExists(MarkerIds.TARGET_MARKER, lineNumber))
		            {
			            attributes = new HashMap<String, Object>();
			            attributes.put(IMarker.LINE_NUMBER, new Integer(lineNumber));
			            attributes.put(MarkerFields.INSTRUCTION, obfuscation.getSecond());
			            attributes.put(IMarker.CHAR_START, new Integer(lineOffset));
			            attributes.put(IMarker.CHAR_END, new Integer(lineOffset + addressLength));
				        MarkerUtilities.createMarker(getInputResource(), attributes, MarkerIds.TARGET_MARKER);
		            }
		        }
		    }
	    }
	    catch(CoreException e)
	    {
	        throw new Error("CoreException in setTargetMarkers");
	    }    
    }

	public void markCurrentInstruction(Address currentAddress) {
		  int                     addressLength;
	      HashMap<String, Object> attributes;
	      Instruction             instruction; 
	      int                     linenumber;
	      int                     lineOffset;

	    	removeMarkers(MarkerIds.CURRENT_INSTRUCTION_MARKER);
	    	
	    	// if there is no input program (program could not be parsed maybe), then
	    	// don't try to label the current instruction, there is none.
	    	if(getInputProgram() == null)
	    		return;

	    	// get the entry point instruction and mark it.
	  		instruction = getInputProgram().getInstruction(currentAddress);
		    try
		    {
		    	linenumber		= getInputProgram().getInstructionIndex(instruction.getAddress());
	        lineOffset 		= getSourceViewer().getTextWidget().getOffsetAtLine(linenumber);
	        addressLength = instruction.getAddress().toString().length();

	      	attributes = new HashMap<String, Object>();
	        attributes.put(MarkerFields.INSTRUCTION, instruction);
	        attributes.put(IMarker.LINE_NUMBER, new Integer(linenumber));
	        attributes.put(IMarker.CHAR_START, new Integer(lineOffset));
	        attributes.put(IMarker.CHAR_END, new Integer(lineOffset + addressLength));
	        MarkerUtilities.createMarker(getInputResource(), attributes, 
	        		MarkerIds.CURRENT_INSTRUCTION_MARKER);
		    }
		    catch(CoreException e)
		    {
	        Log.write(Log.ERROR, Strings.getUnexpectedErrorMsg(e.getMessage()), e);
		}
	}

	@Override
	public void setSelection(ISelection selection) {
		// TODO Auto-generated method stub
		
	}

	public void refreshSelection() {
    	InstructionSelection selection = (InstructionSelection) getSelection();

		setTargetMarkers(selection);
		setNextInstructionMarkers(selection);		    
    	notifySelectionChangedListeners(selection);
	}
}

package ui.programViewer;

import org.eclipse.core.resources.IResource;

import analyzer.AnalysisResult;
import analyzer.AnnotationProvider;

/**
 * An annotation provider that does nothing.  Intended to be
 * used in cases where no annotations are used. 
 */
public class NullAnnotationProvider implements AnnotationProvider
{
  /* (non-Javadoc)
   * @see analyzer.AnnotationProvider#annotate(org.eclipse.core.resources.IResource, analyzer.AnalysisResult, ui.programViewer.ProgramViewer)
   */
  public void annotate(IResource resource, AnalysisResult result,
      ProgramViewer viewer)
  {
  }
}

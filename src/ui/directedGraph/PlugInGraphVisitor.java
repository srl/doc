package ui.directedGraph;

import java.util.Iterator;

import org.eclipse.draw2d.CompoundBorder;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.TitleBarBorder;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;

public class PlugInGraphVisitor extends GraphVisitor {
    final Color SELETED_BACK_COLOR;
	final Color	UNSELECTED_BACK_COLOR;
	final Color	SELECTED_TEXT_COLOR;
	final Color	UNSELECTED_TEXT_COLOR;
	private boolean allowTruncating;
	
	private Point windowSizeNeeded;
	
  
    private FigureCanvas canvas;

	public PlugInGraphVisitor(Composite drawingSurface) {
		super();
		SELETED_BACK_COLOR 	= Display.getCurrent().getSystemColor(SWT.COLOR_LIST_SELECTION);
	    UNSELECTED_BACK_COLOR = Display.getCurrent().getSystemColor(SWT.COLOR_LIST_BACKGROUND);
	    SELECTED_TEXT_COLOR 	= Display.getCurrent().getSystemColor(SWT.COLOR_LIST_SELECTION_TEXT);
	    UNSELECTED_TEXT_COLOR = Display.getCurrent().getSystemColor(SWT.COLOR_LIST_FOREGROUND);
	    
        canvas = new FigureCanvas(drawingSurface);
        canvas.setContents(new Figure());
	}

	@Override
	public void addNode(IDirectedGraphLabelProvider labelProvider, Node node,
		Label label, ISelection selection) {
		Color			backColor;												// color of label background

	    Color			foreColor;												// color of label foreground
	    windowSizeNeeded = new Point(0,0);// necessary window size.
		
	    if(labelProvider.isSelected(node.data, selection))
	    {
	    	foreColor = SELECTED_TEXT_COLOR;
	    	backColor = SELETED_BACK_COLOR;
	    }
	    else
	    {
	    	foreColor = UNSELECTED_TEXT_COLOR;
	    	backColor = UNSELECTED_BACK_COLOR;
	    }
	    
	    label = createNodeLabel(labelProvider.getTitle(node.data), 
	    		labelProvider.getText(node.data),
	    		node.width, node.height, foreColor, backColor);
	    
	    Point position = new Point(node.x, node.y);
	    label.setLocation( new Point(node.x, node.y) );


	    windowSizeNeeded = graphicalNodes(windowSizeNeeded, position, label, node);
	    
	    canvas.getContents().add(label);
	}

	@Override
	public void addEdge(IDirectedGraphLabelProvider labelProvider, Edge edge) {
		PolylineConnection conn = new PolylineConnection();

      	conn.setEndpoints( edge.start, edge.end);
      	conn.setTargetDecoration(new PolylineDecoration());

      	canvas.getContents().add(conn);
	}

	@Override
	public void printNoContent() {
		System.out.println("No Content Available"); 
		
	}

	public Point graphicalNodes(Point windowSizeNeeded, Point position,
			Label label, Node node) {
		if(position.x + node.width > windowSizeNeeded.x)
	        windowSizeNeeded.x = position.x + node.width;
	    if(position.y + node.height > windowSizeNeeded.y)
	        windowSizeNeeded.y = position.y + node.height;
	    
	    return windowSizeNeeded;
	}

	@Override
	public IFigure clearDisplay() {
        IFigure surface = canvas.getContents();
        while( surface.getChildren().iterator().hasNext() )
            surface.remove( (IFigure)surface.getChildren().iterator().next() );            
        return canvas.getContents();
	}

	@Override
	public void showNoContentMessage(String noContentMessage) {
		final int BORDER_WIDTH = 2;
	    Label label;
	    int height = 0;
	    int width = 0;
      
	    label = new Label();

	      // set up body.
	      label.setText(noContentMessage);
	      label.setLocation(new Point(BORDER_WIDTH, BORDER_WIDTH));
	      label.setBackgroundColor(UNSELECTED_BACK_COLOR);
	      label.setForegroundColor(UNSELECTED_TEXT_COLOR);
			
			  height = (int)(FigureUtilities.getTextExtents(noContentMessage, canvas.getFont()).height);
			  width = (int)(FigureUtilities.getTextExtents(noContentMessage, canvas.getFont()).width);
			  label.setSize(width, height);
			  
	      canvas.getContents().add(label);
	}

	@Override
	public void setBounds() {
		canvas.getContents().setBounds( new Rectangle( new Point(0, 0) , windowSizeNeeded ) );
	}

	@Override
	public void refresh(DirectedGraph graph, Object[] nodes, DirectedGraphLayout layout, 
			String noContentMessage, IDirectedGraphLabelProvider labelProvider) {
	    if(!allowTruncating)
	    	setNodesToPreferredSize(labelProvider, graph);
    
	    if(nodes.length > 0)
    	layout.visit(graph);
	    else
	    	showNoContentMessage(noContentMessage);
	}
	
	public void setNodesToPreferredSize(IDirectedGraphLabelProvider labelProvider, DirectedGraph graph) {
		Iterator 	iter 	= graph.nodes.iterator();
		Node			node;				// node to be displayed in graph

		while(iter.hasNext())
		{
		  node = (Node)iter.next();

		  int height = 0;
		  int width = 0;
		  String text = labelProvider.getText(node.data);
		
		  height = (int)(FigureUtilities.getTextExtents(text, canvas.getFont()).height + 30);
		  width = (int)(FigureUtilities.getTextExtents(text, canvas.getFont()).width * 1.2);
		  node.height = height;
		  node.width = width;
		}
	}
	
	private Label createNodeLabel(String title, String body, int width, int height, Color textColor, Color backColor)
    {
	    Label 			label; 
	    CompoundBorder 	border;
	    TitleBarBorder	innerBorder;
        
	    label = new Label();

      // set up title.
      border = new CompoundBorder( new LineBorder(), new TitleBarBorder());
      innerBorder = ((TitleBarBorder)border.getInnerBorder());
      innerBorder.setLabel(title);
      innerBorder.setTextAlignment(PositionConstants.CENTER);
      innerBorder.setTextColor(textColor );
      innerBorder.setBackgroundColor(backColor);
      label.setBorder(border);
      
      // set up body.
      label.setText(body);
      label.setTextAlignment(PositionConstants.CENTER);
      label.setTextPlacement( PositionConstants.SOUTH );
      label.setBackgroundColor(textColor);
      if(allowTruncating)	// no tooltip needed if there is no truncating.
      	label.setToolTip(new Label( label.getText() ) );	
      label.setLabelAlignment(PositionConstants.CENTER);
      label.setSize(width, height);

      return label;        
    }

	@Override
	public void setAllowTruncating(boolean allowTruncating) {
		this.allowTruncating = allowTruncating;
	}

	@Override
	public void addCyclicEdge(IDirectedGraphLabelProvider labelProvider,
			Edge edge) {
		
		PolylineConnection l = new PolylineConnection();
	    l.removeAllPoints();
	    l.addPoint( new Point( edge.source.x + edge.source.width,
	            edge.source.y + edge.source.height / 4) );
	    l.addPoint( new Point( edge.source.x + edge.source.width + 20,
	            edge.source.y + edge.source.height / 4) );
	    l.addPoint( new Point( edge.source.x + edge.source.width + 20,
	            edge.source.y + edge.source.height / 2) );
	    l.addPoint( new Point( edge.source.x + edge.source.width,
	            edge.source.y + edge.source.height / 2) );
	    l.setTargetDecoration(new PolylineDecoration());

	    canvas.getContents().add(l);
	}

	@Override
	public void resetCollections() {
		// TODO Auto-generated method stub
		
	}
}

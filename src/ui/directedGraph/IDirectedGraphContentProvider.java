/*
 * IDirectedGraphContentProvider.java
 * Created on Apr 10, 2005
 */
package ui.directedGraph;

import org.eclipse.jface.viewers.IStructuredContentProvider;

/**
 * Interface for any content providers for the DirectedGraphViewer.
 */
public interface IDirectedGraphContentProvider extends
        IStructuredContentProvider
{
    /**
     * Returns the children of the input node.
     */
    public Object[] getChildren(Object element);
    
    /**
     * Returns true if the two input elements refer to the same
     * node.
     */
    public boolean equals(Object elementA, Object elementB);
}

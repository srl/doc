/*
 * DirectedGraphViewer.java
 * Created on Apr 10, 2005
 */
package ui.directedGraph;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.draw2d.CompoundBorder;
import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.TitleBarBorder;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.EdgeList;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.jface.viewers.ContentViewer;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;

/**
 * Converts an internal representation of a directed graph to
 * a visual representation displayed on the screen.  This
 * implementation requires the Draw2d Java package.
 */
public class DirectedGraphViewer extends ContentViewer
{

    private IDirectedGraphContentProvider	contentProvider;
    private Object												inputElement;
    private IDirectedGraphLabelProvider		labelProvider;
    private Composite 										parent;
    
    private EdgeList 			cyclicEdges;	// edges that loop back to the same node. 
    private Object[] 			edges;				// edges in the graph.
    private DirectedGraph graph;				// a graph used to create the layout of nodes/edges.
    private Object[] 			nodes;				// nodes in the graph
    
    private String  noContentMessage = new String("");	// message to display if there is no input.
    protected GraphVisitor graphVisitor;
    
    
    /**
     * Creates a new viewer that will display its graph on
     * the input drawingSurface.
     * @param allowTruncating if true, each node in the graph will be set to
     * 		the same size and any text that cannot fit in a node will be truncating.
     * 		The missing text will be visible by hovering over the node.  If false,
     * 		each node will be expanded to fit the text and no text will be
     * 		displayed when hovering over the a node.
     */
    //assuming these parameters will only be used for Plug-In implementation
    public DirectedGraphViewer(Composite drawingSurface, boolean allowTruncating)
    {
    	graphVisitor = new PlugInGraphVisitor(drawingSurface);
    	graphVisitor.setAllowTruncating(allowTruncating);
    }
    
    //assuming parameters will be skipped for Stand-Alone implementation
    public DirectedGraphViewer(GraphVisitor gVisitor)
    {    	
        graphVisitor = gVisitor;
    	graphVisitor.setAllowTruncating(true);  
    }
    
    /**
     * Draws the stack graph.  
     * @param graph			the graph structure
     * @param cyclicEdges	list containing edges that point back to 
     * 						the same node.  These are handled seperately
     * 						because the layout algorithm doesn't work with them.
     */
    private void drawGraph(DirectedGraph graph, EdgeList cyclicEdges, ISelection selection)
    {
	    Iterator 	iter 		= graph.nodes.iterator();
	    Label 		label = null; 														// each node is displayed using a label
	    Node			node = null;															// node to be displayed in graph
	    
	    // Draw nodes
	    int count = 0;
	    
	    graphVisitor.resetCollections();
	    while(iter.hasNext())
	    {
		    node = (Node)iter.next();
		    graphVisitor.addNode(labelProvider, node, label, selection);
	    }
	    
	    // draw edges
	    iter = graph.edges.iterator();
	    
	    while(iter.hasNext())
	    {
	        Edge edge = (Edge)iter.next();
	        graphVisitor.addEdge(labelProvider, edge);
	    }

	    // draw cyclic edges.
	    iter = cyclicEdges.iterator();
	    while(iter.hasNext())
	    {
		    // create a polyline connection that starts from the node,
		    // moves outward a little, then loops back to the same node.
	        node = (Node)iter.next();
	        Edge edge = new Edge( node, node );
	        graphVisitor.addCyclicEdge(labelProvider, edge);
        
	    }
	    
	    graphVisitor.setBounds();
    }
    
    /**
     * @see ContentViewer#getInput()
     */
    public Object getInput()
    {
    	return inputElement;
    }
    
    /**
     * Searches an array of Nodes for the given element.  The element is of a type
     * known only to the content provider, so the content provider is contacted
     * to determine if two elements are equal.  it is expected that each node
     * in the DirectedGraph has an object of the same type as element 
     * associated with its data field (i.e., graph.nodes(i).data should be of
     * the same type as element).  More formally,
     *   For each node in nodes
     *     if node.element == element (as according to content provider)
     *       return node 
     * @param graph   The DirectedGraph to search.
     * @param element Some element in the graph
     * @return the node that has element as its data field.  Or null if no
     * 			such node was found.
     */
    private Node findNode(Object[] nodes, Object element)
    {
        IDirectedGraphContentProvider 	provider;
        
        for(int i = 0; i < nodes.length; i++)
        {
            provider = (IDirectedGraphContentProvider)getContentProvider();
            if(provider.equals( ((Node)nodes[i]).data, element))
                return (Node)nodes[i];
        }
        return null;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.Viewer#getControl()
     */    public Control getControl()
    {
        return parent;
    }

    /**
     * Returns an array consisting of an edge parent->child relationship 
     * obtained by calling provider.getChildren.  More formally,
     *   For each element in provider.getElements(intputElement)
     *     For each child in provider.getChildren(element)
     * 	 	  add edge from element->child 
     * Note however, that no edges are ever created from a Node back to
     * itself.  In other words, no edges go from A->A.  If there exists
     * an edge that goes from A->A, it is placed in the cyclicEdges EdgeList.
     * @param nodes Array of nodes that need edges
     * @param cyclicEdges [out] will contain list of edges that travel from A->A
     */    Object[] getEdges(	Object[]	nodes, ArrayList<Node>	selfReferentialNodes)
    {
	    Edge			      edge;
	    ArrayList<Edge> edges;
	    Object[] 	      inputElements;
	    Object[]	      neighbors;
      Node            source;
      Node            target;

	    edges					= new ArrayList<Edge>();
	    inputElements = contentProvider.getElements(inputElement);
	    
      for(int i = 0; i < inputElements.length; i++)
      {
        neighbors = contentProvider.getChildren(inputElements [i]);
        for(int j = 0; j < neighbors.length; j++)
        {
          source = findNode(nodes, inputElements[i]);
          target = findNode(nodes, neighbors[j]);
          if(source == target)
           selfReferentialNodes.add(source);
          else
          {
            edge = new Edge( source, target );
            edges.add(edge);
          }
        }
      }
      return edges.toArray();
    }
    
    /**
     * Returns an array consisting of one node for each element obtained by
     * calling provider.getElements(inputElement).  This array of nodes can
     * later be used to construct a DirectedGraph.  The data element of each
     * Node is set to the corresponding element.  More formally, 
     * Nodes[i].data = provider.getElements(inputElement)[i]
     */
    Object[] getNodes()
    {
	    Object[] 	      inputElements;
	    Node			      node;			
	    ArrayList<Node> nodes;
	    
	    nodes 				= new ArrayList<Node>();
	    inputElements = contentProvider.getElements(inputElement);
	    for(int i = 0; i < inputElements .length; i++)
	    {
	        node 				= new Node();
	        node.data 	= inputElements[i];

	        node.width 	= 125;
	        nodes.add(node);
	    }        
	    return nodes.toArray();
    }

    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ISelectionProvider#getSelection()
     */    public ISelection getSelection()
    {
    	// selection is not provided by this control.
        return null;
    }
    
    /**
     * Creates a DirectedGraph consisting of the input nodes and edges.
     * 
     * Warning are suppressed for this function because it uses code contained in
     * Draw3D that doesn't contain templated parameters.  I'd rather not modify
     * that code and I don't like seeing the resulting warning messages, so I
     * disabled them for this function --mpv.
     */
    @SuppressWarnings("unchecked") DirectedGraph makeGraph(Object[] nodes, Object[] edges)
    {
        DirectedGraph graph = new DirectedGraph();
        
        for(int i = 0; i < nodes.length; i++)
        {
        	// Draw2d cannot draw graphs that are not completely connected, so
        	// remove any nodes that have no edges.  Of course, if there is
        	// only one node, then it is always a connected graph, with or
        	// without any edges.
            if( ((Node)nodes[i]).incoming.size() > 0 ||
            		((Node)nodes[i]).outgoing.size() > 0 ||
            		nodes.length == 1)        	
            	graph.nodes.add(nodes[i]);
        }
        
        for(int i = 0; i < edges.length; i++)
            graph.edges.add(edges[i]);
        
        return graph;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.Viewer#refresh()
     */
    public void refresh()
    {
      DirectedGraphLayout 	layout;			// prepares nodes in graph for display.

      graphVisitor.clearDisplay();
      
      if(inputElement != null)
      {
      	cyclicEdges	= new EdgeList();
  	    layout 			= new DirectedGraphLayout();

        nodes	= getNodes();
        edges	= getEdges(nodes, cyclicEdges);
        graph	= makeGraph(nodes, edges);
        		    
        graphVisitor.refresh(graph, nodes, layout, noContentMessage, labelProvider);
        
        drawGraph(graph, cyclicEdges, null);
        
      }
      else{
    	  graphVisitor.printNoContent();
          graphVisitor.showNoContentMessage(noContentMessage);

      }
   	  
    }
    
    /**
     * Sets the content provider for this viewer.  provider
     * must implement IDIrectedGraphContentProvider
     */    public void setContentProvider(IStructuredContentProvider provider)
    {
        if(provider instanceof IDirectedGraphContentProvider)
        {
            super.setContentProvider(provider);
            contentProvider = (IDirectedGraphContentProvider)provider;
        }
        else
            throw new Error("DirectedGraphViewer.java: Invalid content provider given to setContentProvider");
    }
    
    /**
     * Sets the input to this viewer.
     */    public void setInput(Object inputElement)
    {
        this.inputElement = inputElement;
    }

    /**
     * Sets the label provider for this viewer. provider must implement
     * IDirectedGraphLabelProvider  
     */    public void setLabelProvider(ILabelProvider provider)
    {
        if(provider instanceof IDirectedGraphLabelProvider)
        {
            super.setLabelProvider(provider);
            labelProvider = (IDirectedGraphLabelProvider)provider;
        }
        else
            throw new Error("DirectedGraphViewer.java: Invalid label provider given to setLabelProvider");
    }
    
    /**
     * Sets a message to be displayed whenever there is no input or
     * the input is simply blank.
     */
    public void setNoContentMessage(String msg)
    {
    	noContentMessage = msg;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.Viewer#setSelection(org.eclipse.jface.viewers.ISelection, boolean)
     */    public void setSelection(ISelection selection, boolean reveal)
    {
    	graphVisitor.clearDisplay();

    	if(nodes.length > 0)
      	drawGraph(graph, cyclicEdges, selection);
    	else
    		graphVisitor.showNoContentMessage(noContentMessage);
    }

	public void clearDisplay() {
		graphVisitor.clearDisplay();		
	}
}

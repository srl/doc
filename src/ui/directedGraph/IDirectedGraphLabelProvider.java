/*
 * IDirectedGraphLabelProvider.java
 * Created on Apr 11, 2005
 */
package ui.directedGraph;

import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;

/**
 * Used to provide the text for the DirectedGraphViewer.  getTitle
 * should return the title of a specific graph node and getText
 * should return the body of the graph node.
 */
public interface IDirectedGraphLabelProvider extends ILabelProvider
{
    /**
     * Returns the title of the specified graph node.
     */
    public String getTitle(Object element);

    /**
     * Returns true if the element is selected and should appear
     * different from the others.
     */
    public boolean isSelected(Object element, ISelection selection);
}

package ui.directedGraph;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.jface.viewers.ISelection;

public class TextGraphVisitor extends GraphVisitor {
	@Override
	public void printNoContent() {
	}

	@Override
	public void addEdge(IDirectedGraphLabelProvider labelProvider, Edge edge) {
		// TODO Auto-generated method stub
		String source = labelProvider.getTitle(edge.source.data);
		String target = labelProvider.getTitle(edge.target.data);
		System.out.println(source + " --> " + target);
	}

	@Override
	public IFigure clearDisplay() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void showNoContentMessage(String noContentMessage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBounds() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refresh(DirectedGraph graph, Object[] nodes,
			DirectedGraphLayout layout, String noContentMessage,
			IDirectedGraphLabelProvider labelProvider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addNode(IDirectedGraphLabelProvider labelProvider, Node node,
			Label label, ISelection selection) {
		String title = labelProvider.getTitle(node.data);
		String text = labelProvider.getText(node.data);
		System.out.println("Node: " + title + " : " + text);
	}

	@Override
	public void setAllowTruncating(boolean allowTruncating) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCyclicEdge(IDirectedGraphLabelProvider labelProvider,
			Edge edge) {
		String source = labelProvider.getTitle(edge.source.data);
		String target = labelProvider.getTitle(edge.target.data);
		System.out.println(source + " ==> " + target);
	}

	@Override
	public void resetCollections() {
		// TODO Auto-generated method stub
		
	}

}

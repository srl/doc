package ui.directedGraph;

import java.util.List;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.graphics.Color;
import org.jdom.Document;
import org.jdom.Element;


public class XMLGraphVisitor extends GraphVisitor {
	Element root = null;
    List<Element> rootlist = null;
    Element analysiselement = null;
    Element asgelement = null;
    Element icollection = null;
    List<Element> instructionlist = null;
    Element instruction = null;
    Element nodeelement = null;
    Element edgeelement = null;
    Element labelelement = null;
    Element contentselement = null;
    Element sourceelement = null;
    Element destinationelement = null;
    Element nodecollection = null;
    Element edgecollection = null;
    
    public XMLGraphVisitor(Document XMLdoc) {
    	root = XMLdoc.getRootElement();
    }

	public void addNode(String title, String text) {
		nodeelement = new Element("NODE");
	    nodecollection.addContent(nodeelement);
	    
	    labelelement = new Element("LABEL").setText(title);
	    contentselement = new Element("CONTENTS").setText(text);
	    nodeelement.addContent(labelelement);
	    nodeelement.addContent(contentselement);
	}

	public void addEdge(String source, String target) {
		edgeelement = new Element("EDGE");
	    edgecollection.addContent(edgeelement);
	    
	    sourceelement = new Element("SOURCE").setText(source);
	    edgeelement.addContent(sourceelement);
	    destinationelement = new Element("DESTINATION").setText(target);
	    edgeelement.addContent(destinationelement);
	}

	@Override
	public void printNoContent() {
		// this function should do nothing for the xml document
	}

	@Override
	public void addEdge(IDirectedGraphLabelProvider labelProvider, Edge edge) {
		String source = labelProvider.getText(edge.source.data);
		String target = labelProvider.getText(edge.target.data);
		
		edgeelement = new Element("EDGE");
	    edgecollection.addContent(edgeelement);
	    
	    sourceelement = new Element("SOURCE").setText(source);
	    edgeelement.addContent(sourceelement);
	    destinationelement = new Element("DESTINATION").setText(target);
	    edgeelement.addContent(destinationelement);
	}

	@Override
	public IFigure clearDisplay() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void showNoContentMessage(String noContentMessage) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBounds() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refresh(DirectedGraph graph, Object[] nodes,
			DirectedGraphLayout layout, String noContentMessage,
			IDirectedGraphLabelProvider labelProvider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addNode(IDirectedGraphLabelProvider labelProvider, Node node,
		Label label, ISelection selection) {
		String title = labelProvider.getTitle(node.data);
		String text = labelProvider.getText(node.data);
		
		nodeelement = new Element("NODE");
	    nodecollection.addContent(nodeelement);
	    
	    labelelement = new Element("LABEL").setText(title);
	    contentselement = new Element("CONTENTS").setText(text);
	    nodeelement.addContent(labelelement);
	    nodeelement.addContent(contentselement);	
	}

	@Override
	public void setAllowTruncating(boolean allowTruncating) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void addCyclicEdge(IDirectedGraphLabelProvider labelProvider,
			Edge edge) {
		String source = labelProvider.getText(edge.source.data);
		String target = labelProvider.getText(edge.target.data);
		
		edgeelement = new Element("EDGE");
	    edgecollection.addContent(edgeelement);
	    
	    sourceelement = new Element("SOURCE").setText(source);
	    edgeelement.addContent(sourceelement);
	    destinationelement = new Element("DESTINATION").setText(target);
	    edgeelement.addContent(destinationelement);
	}

	@Override
	public void resetCollections() {
	    rootlist = root.getChildren();
	    analysiselement = rootlist.get(rootlist.size()-1);
	    asgelement = analysiselement.getChild("ABSTRACTSTACKGRAPH");
	    icollection = asgelement.getChild("INSTRUCTIONCOLLECTION");
	    instructionlist = icollection.getChildren();
	    instruction = instructionlist.get(instructionlist.size()-1);      
	    nodecollection = new Element("NODECOLLECTION");
	    edgecollection = new Element("EDGECOLLECTION");
	    instruction.addContent(nodecollection);
	    instruction.addContent(edgecollection);
	}
}

package ui.directedGraph;

import org.eclipse.draw2d.FigureCanvas;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.graph.DirectedGraph;
import org.eclipse.draw2d.graph.DirectedGraphLayout;
import org.eclipse.draw2d.graph.Edge;
import org.eclipse.draw2d.graph.Node;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.graphics.Color;


public abstract class GraphVisitor {

	public abstract void addEdge(IDirectedGraphLabelProvider labelProvider, Edge edge);
	
	public abstract void printNoContent();

	public abstract IFigure clearDisplay();

	public abstract void showNoContentMessage(String noContentMessage);

	public abstract void setBounds();

	public abstract void refresh(DirectedGraph graph, Object[] nodes, 
			DirectedGraphLayout layout, String noContentMessage, IDirectedGraphLabelProvider labelProvider);

	public abstract void addNode(IDirectedGraphLabelProvider labelProvider, Node node,
			Label label, ISelection selection);

	public abstract void setAllowTruncating(boolean allowTruncating);

	public abstract void addCyclicEdge(IDirectedGraphLabelProvider labelProvider,
			Edge edge);

	public abstract void resetCollections();
}

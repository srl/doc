package ui.controlFlow;


import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


import org.eclipse.jface.viewers.Viewer;

import controlFlowGraph.ControlFlowGraph;
import controlFlowGraph.ControlFlowGraphNode;
import controlFlowGraph.ControlFlowGraphNodeList;

import ui.directedGraph.IDirectedGraphContentProvider;
import x86InstructionSet.Program;

/**
 * Provides the content necessary to display the control-flow graph.
 *
 */
public class ControlFlowGraphContentProvider implements IDirectedGraphContentProvider
{
    /* (non-Javadoc)
     * @see detectObfuscation.IDirectedGraphContentProvider#getChildren(java.lang.Object)
     */
    public Object[] getChildren(Object elementIn)
    {
        if(elementIn instanceof ControlFlowGraphNode)
        {
            ControlFlowGraphNode node = (ControlFlowGraphNode)elementIn;

      			while(node.getSuccessors().size() == 1 && 
      					node.getSuccessors().iterator().next().getPredecessors().size() == 1)
      				node = node.getSuccessors().iterator().next();
            
            return node.getSuccessors().toArray();
        }
        else
            throw new Error("ControlFlowGraphContentProvider: Invalid object passed to getChildren.");
    }

    /* (non-Javadoc)
     * @see detectObfuscation.IDirectedGraphContentProvider#equals(java.lang.Object, java.lang.Object)
     */
    public boolean equals(Object elementA, Object elementB)
    {
        return ((ControlFlowGraphNode)elementA).getInstruction().equals(
        		((ControlFlowGraphNode)elementB).getInstruction());
    }

    /**
     * Returns all nodes in the control flow graph.
     * @param inputElement program whose control-flow graph is to be displayed.
     */
    public Object[] getElements(Object inputElement)
    {
    	ControlFlowGraph 	              cfg;
      ControlFlowGraphNode            item;
      ControlFlowGraphNodeList.Iterator iter;
    	Program 					              program;
      ArrayList<ControlFlowGraphNode> result;
      List<ControlFlowGraphNode>      visited;
    	Queue<ControlFlowGraphNode> 		worklist = new LinkedList<ControlFlowGraphNode>();
    	
    	program = (Program)inputElement;
    	cfg = program.getControlFlowGraph();
    	
    	// if the entry-point is not in the CFG (for whatever reason), return
    	// no CFG nodes.
    	if(cfg.getNode(program.getInstruction(program.getEntryPoint())) == null)
    		return new Object[0];

    	worklist.add( program.getControlFlowGraph().getNode(
    					program.getInstruction( program.getEntryPoint() )));
    	
    	visited = new LinkedList<ControlFlowGraphNode>();
    	result = new ArrayList<ControlFlowGraphNode>();
    	
    	while(!worklist.isEmpty())
    	{
    		item = (ControlFlowGraphNode)worklist.remove();
    		if(!visited.contains(item))
    		{
    			result.add(item);
    			visited.add(item);
    			
    			while(item.getSuccessors().size() == 1 && 
    					item.getSuccessors().iterator().next().getPredecessors().size() == 1)
    				item = item.getSuccessors().iterator().next();

    			iter  = item.getSuccessors().iterator();
    			while(iter.hasNext())
    				worklist.add(iter.next());
    		}
    	}    	
    	return result.toArray();
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IContentProvider#dispose()
     */
    public void dispose()
    {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
     */
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {
        // this function is only needed if the input element can be modified.
        // that is not the case here.
    }

}

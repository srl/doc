package ui.controlFlow;


import org.eclipse.jface.viewers.ISelection;

import x86InstructionSet.Program;


/**
 * An instance of this class is created and given to
 * any ISelectionChangedListeners that are listening
 * to the ProgramViewer (and perhaps other classes)
 * after a program is interpretted.
 * Each instance of ProgramSelection stores the
 * interpretted program.  Receivers of this object
 * can display relevent information about the 
 * program, such a entry point address,
 * control flow graph, etc.
 */
public class ProgramSelection implements ISelection
{
    Program program;

    /**
     * Constructs this
     */
    public ProgramSelection(Program program)
    {
        this.program = program;
    }
    
    /**
     * Retrieves the selected (and interpretted) program.
     */
    public Program getProgram()
    {
        return program;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ISelection#isEmpty()
     */
    public boolean isEmpty()
    {
        return false;
    }
}

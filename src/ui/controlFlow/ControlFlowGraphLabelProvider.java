package ui.controlFlow;


import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.graphics.Image;

import controlFlowGraph.ControlFlowGraphNode;

import ui.programViewer.InstructionSelection;

/**
 * Content provider for retrieving abstrack stack contents.  For use
 * with the DirectedGraphViewer.
 */
public class ControlFlowGraphLabelProvider implements ui.directedGraph.IDirectedGraphLabelProvider
{

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ILabelProvider#getImage(java.lang.Object)
     */
    public Image getImage(Object element)
    {
        return null;
    }
    
    /**
     * Returns the title of element.
     */
    public String getTitle(Object element)
    {
    	return ((ControlFlowGraphNode)element).getInstruction().getAddress().toString();
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ILabelProvider#getText(java.lang.Object)
     */
    public String getText(Object element)
    {
    	ControlFlowGraphNode 	node 		= (ControlFlowGraphNode)element;
    	StringBuffer 					result 	= new StringBuffer();
    	
    	result.append(node.getInstruction().toString());
    	
			while(node.getSuccessors().size() == 1 && 
					node.getSuccessors().iterator().next().getPredecessors().size() == 1)
			{
				node = node.getSuccessors().iterator().next();
				result.append("\n" + node.getInstruction().toString());
			}

			return result.toString();
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
     */
    public void addListener(ILabelProviderListener listener)
    {

    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
     */
    public void dispose()
    {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
     */
    public boolean isLabelProperty(Object element, String property)
    {
        return false;
    }

    /**
     * @see ui.directedGraph.IDirectedGraphLabelProvider#isSelected(Object, ISelection)
     */
    public boolean isSelected(Object element, ISelection selection)
    {
    	ControlFlowGraphNode	node;

    	if(selection instanceof InstructionSelection)
    	{
    		node = (ControlFlowGraphNode)element;
    		if(node.getInstruction().equals( ((InstructionSelection)selection).getInstruction() ))
    			return true;
    		while(node.getSuccessors().size() == 1 && 
    				node.getSuccessors().iterator().next().getPredecessors().size() == 1)
    		{
    			node = node.getSuccessors().iterator().next();
      		if(node.getInstruction().equals( ((InstructionSelection)selection).getInstruction() ))
      			return true;
    		}
    	}
    	return false;
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
     */
    public void removeListener(ILabelProviderListener listener)
    {
    }
}

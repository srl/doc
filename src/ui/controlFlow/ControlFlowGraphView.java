package ui.controlFlow;


import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import controlFlowGraph.ControlFlowGraph;
import ui.directedGraph.DirectedGraphViewer;
import ui.programViewer.InstructionSelection;

/**
 * Displays a control-flow graph.
 */
public class ControlFlowGraphView extends ViewPart implements ISelectionListener
{
	final static boolean ALLOW_TRUNCATING = false;
    DirectedGraphViewer viewer;

  final String NO_CONTENT_MESSAGE = "Interpret a file and select an instruction to view " +
		"the control-flow graph.";
    
  private ControlFlowGraph currentCfg = null;	// the cfg currently displayed. 
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    public void createPartControl(Composite parent)
    {
        viewer = new DirectedGraphViewer(parent, ALLOW_TRUNCATING);
        viewer.setNoContentMessage(NO_CONTENT_MESSAGE);
        viewer.setContentProvider(new ControlFlowGraphContentProvider());
        viewer.setLabelProvider(new ControlFlowGraphLabelProvider());
        getViewSite().getPage().addSelectionListener(this);
    }

    /**
     * Destroys this object
     */
    public void dispose()
    {
        getViewSite().getPage().removeSelectionListener(this);
        super.dispose();
    }
    
    /**
     * Updates the flow graph with a new graph.
     * This class is a SelectionListener for this Page (a page contains all
     * of the views for this perspective).  The FileView notifies the Page
     * when something new is selected and the Page then calls
     * this function.  This function obtains and displays the control flow graph 
     * for the selected program.
     */
    public void selectionChanged(IWorkbenchPart part, ISelection selection)
    {
      ControlFlowGraph newCfg;

      //This section was modified to update for stepping and break points
  	  if (selection instanceof ProgramSelection)
  	  {
  	      newCfg   		= ((ProgramSelection)selection).getProgram().getControlFlowGraph();
  	  	  currentCfg = (ControlFlowGraph)newCfg.clone();
  	      viewer.setInput( ((ProgramSelection)selection).getProgram() );
  	      System.out.println("ProgramSelection is "+((ProgramSelection)selection).getProgram());
  	  }
      viewer.refresh();
      if(selection instanceof InstructionSelection)
      {
          viewer.clearDisplay();
          viewer.setSelection(selection);
      }
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#setFocus()
     */
    public void setFocus()
    {
        // nothing needs to be done here.
    }
}

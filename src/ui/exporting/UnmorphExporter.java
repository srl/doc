package ui.exporting;

import java.io.File;
import java.io.FileWriter;
import java.util.Iterator;

import logger.Log;
import x86InstructionSet.Instruction;
import x86InstructionSet.Program;

/**
 * Exports the program along with state information to a file.  The program output
 * format is designed to be used as input to the unmorph program.  This
 * exporter is intended to be used only with the live variable analysis domain.
 */
public class UnmorphExporter implements Exporter
{
  /* (non-Javadoc)
   * @see ui.exporting.Exporter#export(x86InstructionSet.Program, java.lang.String)
   */
  public boolean export(Program program, String filename)
  {
    File output;
    FileWriter fileWriter;
    
    output = new File(filename);
    
    try
    {
      fileWriter = new FileWriter(output);

      StringBuffer result = new StringBuffer();
      Iterator<Instruction> iter;
      Instruction  instruction;
      
      iter = program.iterator();
      if(iter.hasNext())
      {
        instruction = iter.next();
        result.append(instruction.getBeginState() + " " + instruction);
        while(iter.hasNext())
        {
          instruction = iter.next();
          result.append("\r\n" + instruction.getBeginState() + " " + instruction);
        }
      }
      fileWriter.write(result.toString());
      
      fileWriter.close();
    }
    catch(Exception e)
    {
      Log.write(Log.ERROR, "Execption occured", e);
    }
    
    return false;
  }
}

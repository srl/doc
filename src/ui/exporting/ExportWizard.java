package ui.exporting;

import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

import ui.programViewer.ProgramViewer;
import x86InstructionSet.Program;

/**
 * Wizard allowing user option to export analysis data
 * in availalbe formats.
 */
public class ExportWizard extends Wizard implements IExportWizard
{
  private ExportPage exportPage;
  private IWorkbench workbench;
  
  /* (non-Javadoc)
   * @see org.eclipse.jface.wizard.IWizard#addPages()
   */
  public void addPages()
  {
    exportPage = new ExportPage("exportPage");
    addPage(exportPage);
  }
  
  /**
   * Retrieves the currently active program (if there is one) from the
   * workbench. Returns null if there is no active program.
   * @return currently active program.
   */
  private Program getActiveProgram()
  {
    ProgramViewer viewer;
    
    viewer = (ProgramViewer)workbench.getActiveWorkbenchWindow().getActivePage().getActiveEditor();
    
    return viewer.getInputProgram();
  }
  
  /* (non-Javadoc)
   * @see org.eclipse.ui.IWorkbenchWizard#init(org.eclipse.ui.IWorkbench, org.eclipse.jface.viewers.IStructuredSelection)
   */
  public void init(IWorkbench workbench, IStructuredSelection selection)
  {
    this.workbench = workbench;
  }
  
  /* (non-Javadoc)
   * @see org.eclipse.jface.wizard.IWizard#performFinish()
   */
  public boolean performFinish()
  {
    Exporter exporter = new UnmorphExporter();
    exporter.export(getActiveProgram(), exportPage.getFilename());
    return true;
  }
}

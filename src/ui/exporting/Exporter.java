package ui.exporting;

import x86InstructionSet.Program;

/**
 * Exports a program and/or state information to a file.
 */
public interface Exporter
{
  /**
   * Exports the input program and/or state information to the specified file. 
   * If a file with the input filename already exists, the file is overwritten.
   * @param program program to export. 
   * @param filename the name of the file to export the data to.
   * @return true if successful, false if an error occurs.
   */
  public boolean export(Program program, String filename);
}

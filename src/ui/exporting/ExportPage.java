package ui.exporting;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import detectObfuscation.Preferences;

/**
 * Visual dialog page containing export options.
 */
public class ExportPage extends WizardPage implements Listener
{
  private Text  fileText;
  private List  formatList;
  
  /**
   * Creates a new wizard page with the given name, and
   * with no title or image.
   *
   * @param pageName the name of the page
   */
  protected ExportPage(String pageName) 
  {
      super(pageName, "Export Analysis Results", null);
      setDescription("Configure export output.");
  }

  /**
   * Creates a new wizard page with the given name, title, and image.
   *
   * @param pageName the name of the page
   * @param title the title for this wizard page,
   *   or <code>null</code> if none
   * @param titleImage the image descriptor for the title of this wizard page,
   *   or <code>null</code> if none
   */
  protected ExportPage(String pageName, String title,
          ImageDescriptor titleImage) 
  {
    super(pageName, title, titleImage);
  }

  /* (non-Javadoc)
   * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
   */
  public void createControl(Composite parent)
  {
    Composite   composite;
    Label       fileLabel;
    Label       formatLabel;
    FormLayout  layout;
    
    FormData  formatListData;
    FormData  fileLabelData;
    FormData  fileTextData;
    
    layout = new FormLayout();
    layout.marginLeft = 5;
    layout.marginRight= 5;
    layout.spacing = 10;

    composite = new Composite(parent, SWT.NONE);
    composite.setLayout(layout);
    
    formatLabel = new Label(composite, SWT.NONE);
    formatLabel.setText("Export Format:");
    
    formatList = new List(composite, SWT.BORDER);
    formatList.setItems(new String[] {"Unmorph Format"});
    formatList.select(0);
    formatListData = new FormData();
    formatListData.left = new FormAttachment(formatLabel);
    formatList.setLayoutData(formatListData);
    
    fileLabel = new Label(composite, SWT.NONE);
    fileLabel.setText("Filename:");
    fileLabelData = new FormData();
    fileLabelData.top = new FormAttachment(formatLabel);
    fileLabel.setLayoutData(fileLabelData);
    
    fileText = new Text(composite, SWT.BORDER);
    fileTextData = new FormData();
    fileTextData.left = new FormAttachment(fileLabel);
    fileTextData.top = new FormAttachment(formatLabel);
    fileText.setLayoutData(fileTextData);
    fileText.addListener(SWT.Modify, this);
    
    setControl(composite);
  }
  
  /**
   * Returns the user-selected export format.
   * @return export format.
   */
  public int getExportFormat()
  {
    int format;
    
    switch(formatList.getSelectionIndex())
    {
    case 1:
      format = Preferences.UNMORPH_EXPORTER;
      break;
      
    default:
      format = Preferences.UNMORPH_EXPORTER;
      break;
    }
    
    return format;
  }
  
  /**
   * Returns the destination file name obtained from the user.
   * @return destination file name.
   */
  public String getFilename()
  {
    return fileText.getText();
  }

  /**
   * TODO: document function
   * @param event
   */
  public void handleEvent (Event event)
  {
    /* TODO: Add event handler code
    if (e.widget == b) 
    {
      String s[] = {"*.asm"};
      Shell shell = new Shell();
      fd = new FileDialog(shell, SWT.APPLICATION_MODAL);
      fd.setFilterExtensions(s);
      if(fd.open() != null)
      {
        fileText.setText(fd.getFilterPath()+ File.separator + fd.getFileName());
        if(this.getProjectName()!= null) setPageComplete(true);
      }
    }
    */
  }
}

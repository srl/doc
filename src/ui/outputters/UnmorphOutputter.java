package ui.outputters;

import logger.Log;
import ollydbgParser.StringUtils;
import x86InstructionSet.AdcInstruction;
import x86InstructionSet.AddInstruction;
import x86InstructionSet.Address;
import x86InstructionSet.AddressOperand;
import x86InstructionSet.AndInstruction;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.CdqInstruction;
import x86InstructionSet.CldInstruction;
import x86InstructionSet.CmpInstruction;
import x86InstructionSet.CwdInstruction;
import x86InstructionSet.DecInstruction;
import x86InstructionSet.DivInstruction;
import x86InstructionSet.EnterInstruction;
import x86InstructionSet.ExpressionOperand;
import x86InstructionSet.ExternalFunctionOperand;
import x86InstructionSet.HltInstruction;
import x86InstructionSet.IdivInstruction;
import x86InstructionSet.ImmediateOperand;
import x86InstructionSet.ImulInstruction1op;
import x86InstructionSet.ImulInstruction2op;
import x86InstructionSet.ImulInstruction3op;
import x86InstructionSet.InInstruction;
import x86InstructionSet.IncInstruction;
import x86InstructionSet.IndirectOperand;
import x86InstructionSet.JaJnbeInstruction;
import x86InstructionSet.JaeInstruction;
import x86InstructionSet.JbInstruction;
import x86InstructionSet.JbeInstruction;
import x86InstructionSet.JeInstruction;
import x86InstructionSet.JecxzInstruction;
import x86InstructionSet.JgInstruction;
import x86InstructionSet.JgeInstruction;
import x86InstructionSet.JlInstruction;
import x86InstructionSet.JleInstruction;
import x86InstructionSet.JmpInstruction;
import x86InstructionSet.JnbInstruction;
import x86InstructionSet.JneInstruction;
import x86InstructionSet.JnsInstruction;
import x86InstructionSet.JnzInstruction;
import x86InstructionSet.JsInstruction;
import x86InstructionSet.LeaInstruction;
import x86InstructionSet.LeaveInstruction;
import x86InstructionSet.LockInstruction;
import x86InstructionSet.LodsInstruction;
import x86InstructionSet.LoopdInstruction;
import x86InstructionSet.Memory16Operand;
import x86InstructionSet.Memory32Operand;
import x86InstructionSet.Memory8Operand;
import x86InstructionSet.MovInstruction;
import x86InstructionSet.MovsInstruction;
import x86InstructionSet.MovsxInstruction;
import x86InstructionSet.MovzxInstruction;
import x86InstructionSet.MulInstruction;
import x86InstructionSet.NegInstruction;
import x86InstructionSet.NopInstruction;
import x86InstructionSet.NotInstruction;
import x86InstructionSet.OrInstruction;
import x86InstructionSet.PopInstruction;
import x86InstructionSet.PopadInstruction;
import x86InstructionSet.PopfdInstruction;
import x86InstructionSet.PushInstruction;
import x86InstructionSet.PushadInstruction;
import x86InstructionSet.PushfdInstruction;
import x86InstructionSet.Register16Operand;
import x86InstructionSet.Register32Operand;
import x86InstructionSet.Register8Operand;
import x86InstructionSet.RepMovsInstruction;
import x86InstructionSet.RepStosInstruction;
import x86InstructionSet.RepneScasInstruction;
import x86InstructionSet.RepzMovsInstruction;
import x86InstructionSet.RepzStosInstruction;
import x86InstructionSet.RetInstruction;
import x86InstructionSet.RolInstruction;
import x86InstructionSet.RorInstruction;
import x86InstructionSet.SarInstruction;
import x86InstructionSet.SbbInstruction;
import x86InstructionSet.SegmentRegisterOperand;
import x86InstructionSet.SetaeSetnbInstruction;
import x86InstructionSet.SetbSetnaeInstruction;
import x86InstructionSet.SetbeSetnaInstruction;
import x86InstructionSet.SeteSetzInstruction;
import x86InstructionSet.SetgSetnleInstruction;
import x86InstructionSet.SetgeSetnlInstruction;
import x86InstructionSet.SetlSetngeInstruction;
import x86InstructionSet.SetleSetngInstruction;
import x86InstructionSet.SetneSetnzInstruction;
import x86InstructionSet.ShlInstruction;
import x86InstructionSet.ShrInstruction;
import x86InstructionSet.StdInstruction;
import x86InstructionSet.StosInstruction;
import x86InstructionSet.SubInstruction;
import x86InstructionSet.TestInstruction;
import x86InstructionSet.UndefInstruction;
import x86InstructionSet.UnknownAddress;
import x86InstructionSet.XchgInstruction;
import x86InstructionSet.XorInstruction;

/**
 * Converts <code>Instruction</code> to a text format that can be 
 * used as input to the Unmorph program.
 */
public class UnmorphOutputter extends Outputter
{
  /** 
   * Text to be printed just before each instruction. This is used
   * to provide a seperatation between the printed instruction from 
   * the line numbers.
   */
  protected final static String LEADING_TEXT = " ";

  /** Text that seperates each operand.  Typically, a comma. */
  protected final static String OPERAND_SEPERATOR = ",";
  
  /**
   * desired width of the opcode column. used by subclasses for printing.
   */
  protected final static int OPCODE_WIDTH = 7;
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.Address)
   */
  public String toString(Address address)
  {
    return "0x" + Long.toHexString(address.toLong());
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTAddInstruction)
   */
  @Override
  public String toString(AdcInstruction instruction)
  {
    return StringUtils.padRight("adc", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTAddInstruction)
   */
  @Override
  public String toString(AddInstruction instruction)
  {
    return StringUtils.padRight("add", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTAddInstruction)
   */
  @Override
  public String toString(AndInstruction instruction)
  {
    return  StringUtils.padRight("and", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      instruction.getSrcOperand();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTAddressOperand)
   */
  @Override
  public String toString(AddressOperand operand)
  {
    return operand.getAddress().toString();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCallInstruction)
   */
  @Override
  public String toString(CallInstruction instruction)
  {
    return StringUtils.padRight("call", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCdqInstruction)
   */
  @Override
  public String toString(CdqInstruction instruction)
  {
    return "cdq";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCldInstruction)
   */
  @Override
  public String toString(CldInstruction instruction)
  {
    return "cld";  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCmpInstruction)
   */
  @Override
  public String toString(CmpInstruction instruction)
  {
    return StringUtils.padRight("cmp", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + OPERAND_SEPERATOR + 
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCwdInstruction)
   */
  @Override
  public String toString(CwdInstruction instruction)
  {
    return "cwd";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTDecInstruction)
   */
  @Override
  public String toString(DecInstruction instruction)
  {
    return StringUtils.padRight("dec", OPCODE_WIDTH) + 
      instruction.getDstOperand();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTDivInstruction)
   */
  @Override
  public String toString(DivInstruction instruction)
  {
    return StringUtils.padRight("div", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTEnterInstruction)
   */
  @Override
  public String toString(EnterInstruction instruction)
  {
    return StringUtils.padRight("enter", OPCODE_WIDTH) + 
      instruction.getSizeOperand() + OPERAND_SEPERATOR + 
      instruction.getNestingLevelOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTExpressionOperand)
   */
  @Override
  public String toString(ExpressionOperand instruction)
  {
    // TODO This method is left empty for now, because the
    // the way the expression class is designed needs improvement.
    // Currently, this function is not used.
    return null;
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTExternalFunctionOperand)
   */
  @Override
  public String toString(ExternalFunctionOperand instruction)
  {
    return instruction.getName();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTHltInstruction)
   */
  public String toString(HltInstruction instruction)
  {
    return "hlt";
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTIdivInstruction)
   */
  @Override
  public String toString(IdivInstruction instruction)
  {
    return StringUtils.padRight("idiv", OPCODE_WIDTH) + 
      instruction.getDstOperand();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTImmediateOperand)
   */
  @Override
  public String toString(ImmediateOperand instruction)
  {
    // TODO This function is not yet implemented, because 
    // the design of the ASTImmediateOperand class needs 
    // improvement.  This function is never used.
    return null;
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTImulInstruction_1op)
   */
  @Override
  public String toString(ImulInstruction1op instruction)
  {
    return StringUtils.padRight("imul", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTImulInstruction_2op)
   */
  @Override
  public String toString(ImulInstruction2op instruction)
  {
    return StringUtils.padRight("imul", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTImulInstruction_3op)
   */
  @Override
  public String toString(ImulInstruction3op instruction)
  {
    return StringUtils.padRight("imul", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +
      instruction.getSrc1Operand() + OPERAND_SEPERATOR + 
      instruction.getSrc2Operand();    
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTInInstruction)
   */
  @Override
  public String toString(InInstruction instruction)
  {
    return StringUtils.padRight("in", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTIncInstruction)
   */
  @Override
  public String toString(IncInstruction instruction)
  {
    return StringUtils.padRight("inc", OPCODE_WIDTH) + 
      instruction.getDstOperand(); 
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTIndirectOperand)
   */
  @Override
  public String toString(IndirectOperand instruction)
  {
    return instruction.getOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJaJnbeInstruction)
   */
  @Override
  public String toString(JaJnbeInstruction instruction)
  {
    return StringUtils.padRight("ja", OPCODE_WIDTH) + 
      instruction.getTargetOperand();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJaJnbeInstruction)
   */
  @Override
  public String toString(JaeInstruction instruction)
  {
    return StringUtils.padRight("jae", OPCODE_WIDTH) + 
      instruction.getTargetOperand();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJbeInstruction)
   */
  @Override
  public String toString(JbeInstruction instruction)
  {
    return StringUtils.padRight("jbe", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJbInstruction)
   */
  @Override
  public String toString(JbInstruction instruction)
  {
    return StringUtils.padRight("jb", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJecxzInstruction)
   */
  @Override
  public String toString(JecxzInstruction instruction)
  {
    return StringUtils.padRight("jecxz", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJeInstruction)
   */
  @Override
  public String toString(JeInstruction instruction)
  {
    return StringUtils.padRight("je", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJgeInstruction)
   */
  @Override
  public String toString(JgeInstruction instruction)
  {
    return StringUtils.padRight("jge", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJgInstruction)
   */
  @Override
  public String toString(JgInstruction instruction)
  {
    return StringUtils.padRight("jg", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJleInstruction)
   */
  @Override
  public String toString(JleInstruction instruction)
  {
    return StringUtils.padRight("jle", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJlInstruction)
   */
  @Override
  public String toString(JlInstruction instruction)
  {
    return StringUtils.padRight("jl", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJmpInstruction)
   */
  @Override
  public String toString(JmpInstruction instruction)
  {
    return StringUtils.padRight("jmp", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJnbInstruction)
   */
  @Override
  public String toString(JnbInstruction instruction)
  {
    return StringUtils.padRight("jnb", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.JnbInstruction)
   */
  @Override
  public String toString(JneInstruction instruction)
  {
    return StringUtils.padRight("jne", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJnsInstruction)
   */
  @Override
  public String toString(JnsInstruction instruction)
  {
    return StringUtils.padRight("jns", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJnzInstruction)
   */
  public String toString(JnzInstruction instruction)
  {
    return StringUtils.padRight("jnz", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJsInstruction)
   */
  @Override
  public String toString(JsInstruction instruction)
  {
    return StringUtils.padRight("js", OPCODE_WIDTH) + 
      instruction.getTargetOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLeaInstruction)
   */
  @Override
  public String toString(LeaInstruction instruction)
  {
    return StringUtils.padRight("lea", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLeaveInstruction)
   */
  @Override
  public String toString(LeaveInstruction instruction)
  {
    return "leave";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLockInstruction)
   */
  public String toString(LockInstruction instruction)
  {
    return StringUtils.padRight("lock", OPCODE_WIDTH) +
      instruction.getChildInstruction();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLodsInstruction)
   */
  @Override
  public String toString(LodsInstruction instruction)
  {
    return StringUtils.padRight("lods", OPCODE_WIDTH) + 
      instruction.getDstOperand() + ", " +
      instruction.getSrcOperand(); 
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLoopdInstruction)
   */
  @Override
  public String toString(LoopdInstruction instruction)
  {
    return StringUtils.padRight("loopd", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMemory16Operand)
   */
  @Override
  public String toString(Memory16Operand instruction)
  {
    StringBuffer result = new StringBuffer();
    result.append("WORD PTR ");
    result.append("[" + instruction.getOperand() + "]");
    return result.toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMemory32Operand)
   */
  @Override
  public String toString(Memory32Operand instruction)
  {
    StringBuffer result = new StringBuffer();
    result.append("DWORD PTR ");
    result.append("[" + instruction.getOperand()+ "]");
    return result.toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMemory8Operand)
   */
  @Override
  public String toString(Memory8Operand instruction)
  {
    StringBuffer result = new StringBuffer();
    result.append("BYTE PTR ");
    result.append("[" + instruction.getOperand() + "]");
    return result.toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMovInstruction)
   */
  @Override
  public String toString(MovInstruction instruction)
  {
    return StringUtils.padRight("mov", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMovsInstruction)
   */
  @Override
  public String toString(MovsInstruction instruction)
  {
    return StringUtils.padRight("movs", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMovsxInstruction)
   */
  @Override
  public String toString(MovsxInstruction instruction)
  {
    return StringUtils.padRight("movsx", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMovzxInstruction)
   */
  @Override
  public String toString(MovzxInstruction instruction)
  {
    return StringUtils.padRight("movzx", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMulInstruction)
   */
  @Override
  public String toString(MulInstruction instruction)
  {
    return StringUtils.padRight("mul", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTNegInstruction)
   */
  @Override
  public String toString(NegInstruction instruction)
  {
    return StringUtils.padRight("neg", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTNopInstruction)
   */
  @Override
  public String toString(NopInstruction instruction)
  {
    return "nop";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTNotInstruction)
   */
  @Override
  public String toString(NotInstruction instruction)
  {
    return StringUtils.padRight("not ", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTOrInstruction)
   */
  @Override
  public String toString(OrInstruction instruction)
  {
    return StringUtils.padRight("or", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTPopInstruction)
   */
  @Override
  public String toString(PopInstruction instruction)
  {
    return StringUtils.padRight("pop", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.PopadInstruction)
   */
  public String toString(PopadInstruction instruction)
  {
    return "popad";
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.PopfdInstruction)
   */
  public String toString(PopfdInstruction instruction)
  {
    return "popfd";
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.PushadInstruction)
   */
  public String toString(PushadInstruction instruction)
  {
    return "pushad";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTPushfdInstruction)
   */
  @Override
  public String toString(PushfdInstruction instruction)
  {
    return "pushfd";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTPushInstruction)
   */
  @Override
  public String toString(PushInstruction instruction)
  {
    return StringUtils.padRight("push", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRegister16Operand)
   */
  @Override
  public String toString(Register16Operand instruction)
  {
    switch(instruction.getRegister())
    {
      case AX: return "ax";
      case BX: return "bx";
      case CX: return "cx";
      case DX: return "dx";
      case BP: return "bp";
      case DI: return "di";
      case SI: return "si";
      case SP: return "sp";
      default:
        Log.write(Log.ERROR, "UnmorphOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");
        throw new Error("UnmorphOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");
    }
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRegister32Operand)
   */
  @Override
  public String toString(Register32Operand instruction)
  {
    switch(instruction.getRegister())
    {
      case EAX: return "eax";
      case EBX: return "ebx";
      case ECX: return "ecx";
      case EDX: return "edx";
      case EBP: return "ebp";
      case EDI: return "edi";
      case ESI: return "esi";
      case ESP: return "esp";
      default:
        Log.write(Log.ERROR, "UnmorphOutputter.toString(ASTRegister32Operand: Unknown register type encountered.");
        throw new Error("UnmorphOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");             
    }
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRegister8Operand)
   */
  @Override
  public String toString(Register8Operand instruction)
  {
    switch(instruction.getRegister())
    {
      case AL: return "al";
      case AH: return "ah";
      
      case BL: return "bl";
      case BH: return "bh";

      case CL: return "cl";
      case CH: return "ch";

      case DL: return "dl";
      case DH: return "dh";

      default:
        Log.write(Log.ERROR, "UnmorphOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");
        throw new Error("UnmorphOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");
    }
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRepMovsInstruction)
   */
  @Override
  public String toString(RepMovsInstruction instruction)
  {
    return StringUtils.padRight("rep movs ", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRepneScasInstruction)
   */
  @Override
  public String toString(RepneScasInstruction instruction)
  {
    return StringUtils.padRight("repne scas ", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRepStosInstruction)
   */
  @Override
  public String toString(RepStosInstruction instruction)
  {
    return StringUtils.padRight("rep stos ", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRepStosInstruction)
   */
  @Override
  public String toString(RepzMovsInstruction instruction)
  {
    return StringUtils.padRight("repz movs ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + OPERAND_SEPERATOR +
      instruction.getSrcOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.RepzMovsInstruction)
   */
  @Override
  public String toString(RepzStosInstruction instruction)
  {
    return StringUtils.padRight("repz stos ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + OPERAND_SEPERATOR +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRetInstruction)
   */
  @Override
  public String toString(RetInstruction instruction)
  {
    return StringUtils.padRight("ret", OPCODE_WIDTH) + 
    ( (instruction.getDstOperand() != null) ?  instruction.getDstOperand().toString() : "" );
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRolInstruction)
   */
  @Override
  public String toString(RolInstruction instruction)
  {
    return StringUtils.padRight("rol ", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRorInstruction)
   */
  @Override
  public String toString(RorInstruction instruction)
  {
    return StringUtils.padRight("ror ", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSarInstruction)
   */
  @Override
  public String toString(SarInstruction instruction)
  {
    return StringUtils.padRight("sar", OPCODE_WIDTH) + 
      instruction.getDstOperand() +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSbbInstruction)
   */
  @Override
  public String toString(SbbInstruction instruction)
  {
    return  
       
      StringUtils.padRight("sbb ", OPCODE_WIDTH) + 
      instruction.getDstOperand() +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSegmentRegisterOperand)
   */
  @Override
  public String toString(SegmentRegisterOperand instruction)
  {
    // Segement registers are not used in Unmorph, so print nothing.
    return "";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetaeSetnbInstruction)
   */
  @Override
  public String toString(SetaeSetnbInstruction instruction)
  {
    return StringUtils.padRight("setae", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetbeSetnaInstruction)
   */
  @Override
  public String toString(SetbeSetnaInstruction instruction)
  {
    return StringUtils.padRight("setbe", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetbSetnaeInstruction)
   */
  @Override
  public String toString(SetbSetnaeInstruction instruction)
  {
    return StringUtils.padRight("setb", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSeteSetzInstruction)
   */
  @Override
  public String toString(SeteSetzInstruction instruction)
  {
    return StringUtils.padRight("sete", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetgeSetnlInstruction)
   */
  @Override
  public String toString(SetgeSetnlInstruction instruction)
  {
    return StringUtils.padRight("setge", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetgSetnleInstruction)
   */
  public String toString(SetgSetnleInstruction instruction)
  {
    return StringUtils.padRight("setg", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetleSetngInstruction)
   */
  @Override
  public String toString(SetleSetngInstruction instruction)
  {
    return StringUtils.padRight("setle", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetlSetngeInstruction)
   */
  @Override
  public String toString(SetlSetngeInstruction instruction)
  {
    return StringUtils.padRight("setl", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetneSetnzInstruction)
   */
  @Override
  public String toString(SetneSetnzInstruction instruction)
  {
    return StringUtils.padRight("setne", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTShlInstruction)
   */
  @Override
  public String toString(ShlInstruction instruction)
  {
    return StringUtils.padRight("shl", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      (instruction.getSrcOperand() != null ? instruction.getSrcOperand() : "");
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTShrInstruction)
   */
  @Override
  public String toString(ShrInstruction instruction)
  {
    return StringUtils.padRight("shr", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      (instruction.getSrcOperand() != null ? instruction.getSrcOperand() : "");
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTStdInstruction)
   */
  @Override
  public String toString(StdInstruction instruction)
  {
    return "std";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTStosInstruction)
   */
  @Override
  public String toString(StosInstruction instruction)
  {
    return StringUtils.padRight("stos ", OPCODE_WIDTH) + 
      instruction.getDstOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSubInstruction)
   */
  @Override
  public String toString(SubInstruction instruction)
  {
    return StringUtils.padRight("sub", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTTestInstruction)
   */
  @Override
  public String toString(TestInstruction instruction)
  {
    return StringUtils.padRight("test ", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTUndefInstruction)
   */
  @Override
  public String toString(UndefInstruction instruction)
  {
    // Unmorph doesn't use UNDEF operataions--they were created
    // specifically for this project--so ignore them.
    return "";     
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTXchgInstruction)
   */
  @Override
  public String toString(XchgInstruction instruction)
  {
    return  
       
      StringUtils.padRight("xchg ", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR + 
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTXorInstruction)
   */
  @Override
  public String toString(XorInstruction instruction)
  {
    return StringUtils.padRight("xor", OPCODE_WIDTH) + 
      instruction.getDstOperand() + OPERAND_SEPERATOR +  
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.UnknownAddress)
   */
  public String toString(UnknownAddress address)
  {
    return "0x????????";
  }
}

package ui.outputters;

import logger.Log;
import ollydbgParser.StringUtils;
import x86InstructionSet.AdcInstruction;
import x86InstructionSet.AddInstruction;
import x86InstructionSet.Address;
import x86InstructionSet.AddressOperand;
import x86InstructionSet.AndInstruction;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.CdqInstruction;
import x86InstructionSet.CldInstruction;
import x86InstructionSet.CmpInstruction;
import x86InstructionSet.CwdInstruction;
import x86InstructionSet.DecInstruction;
import x86InstructionSet.DivInstruction;
import x86InstructionSet.EnterInstruction;
import x86InstructionSet.ExpressionOperand;
import x86InstructionSet.ExternalFunctionOperand;
import x86InstructionSet.HltInstruction;
import x86InstructionSet.IdivInstruction;
import x86InstructionSet.ImmediateOperand;
import x86InstructionSet.ImulInstruction1op;
import x86InstructionSet.ImulInstruction2op;
import x86InstructionSet.ImulInstruction3op;
import x86InstructionSet.InInstruction;
import x86InstructionSet.IncInstruction;
import x86InstructionSet.IndirectOperand;
import x86InstructionSet.JaJnbeInstruction;
import x86InstructionSet.JaeInstruction;
import x86InstructionSet.JbInstruction;
import x86InstructionSet.JbeInstruction;
import x86InstructionSet.JeInstruction;
import x86InstructionSet.JecxzInstruction;
import x86InstructionSet.JgInstruction;
import x86InstructionSet.JgeInstruction;
import x86InstructionSet.JlInstruction;
import x86InstructionSet.JleInstruction;
import x86InstructionSet.JmpInstruction;
import x86InstructionSet.JnbInstruction;
import x86InstructionSet.JneInstruction;
import x86InstructionSet.JnsInstruction;
import x86InstructionSet.JnzInstruction;
import x86InstructionSet.JsInstruction;
import x86InstructionSet.LeaInstruction;
import x86InstructionSet.LeaveInstruction;
import x86InstructionSet.LockInstruction;
import x86InstructionSet.LodsInstruction;
import x86InstructionSet.LoopdInstruction;
import x86InstructionSet.Memory16Operand;
import x86InstructionSet.Memory32Operand;
import x86InstructionSet.Memory8Operand;
import x86InstructionSet.MovInstruction;
import x86InstructionSet.MovsInstruction;
import x86InstructionSet.MovsxInstruction;
import x86InstructionSet.MovzxInstruction;
import x86InstructionSet.MulInstruction;
import x86InstructionSet.NegInstruction;
import x86InstructionSet.NopInstruction;
import x86InstructionSet.NotInstruction;
import x86InstructionSet.OrInstruction;
import x86InstructionSet.PopInstruction;
import x86InstructionSet.PopadInstruction;
import x86InstructionSet.PopfdInstruction;
import x86InstructionSet.PushInstruction;
import x86InstructionSet.PushadInstruction;
import x86InstructionSet.PushfdInstruction;
import x86InstructionSet.Register16Operand;
import x86InstructionSet.Register32Operand;
import x86InstructionSet.Register8Operand;
import x86InstructionSet.RepMovsInstruction;
import x86InstructionSet.RepStosInstruction;
import x86InstructionSet.RepneScasInstruction;
import x86InstructionSet.RepzMovsInstruction;
import x86InstructionSet.RepzStosInstruction;
import x86InstructionSet.RetInstruction;
import x86InstructionSet.RolInstruction;
import x86InstructionSet.RorInstruction;
import x86InstructionSet.SarInstruction;
import x86InstructionSet.SbbInstruction;
import x86InstructionSet.SegmentRegisterOperand;
import x86InstructionSet.SetaeSetnbInstruction;
import x86InstructionSet.SetbSetnaeInstruction;
import x86InstructionSet.SetbeSetnaInstruction;
import x86InstructionSet.SeteSetzInstruction;
import x86InstructionSet.SetgSetnleInstruction;
import x86InstructionSet.SetgeSetnlInstruction;
import x86InstructionSet.SetlSetngeInstruction;
import x86InstructionSet.SetleSetngInstruction;
import x86InstructionSet.SetneSetnzInstruction;
import x86InstructionSet.ShlInstruction;
import x86InstructionSet.ShrInstruction;
import x86InstructionSet.StdInstruction;
import x86InstructionSet.StosInstruction;
import x86InstructionSet.SubInstruction;
import x86InstructionSet.TestInstruction;
import x86InstructionSet.UndefInstruction;
import x86InstructionSet.UnknownAddress;
import x86InstructionSet.XchgInstruction;
import x86InstructionSet.XorInstruction;

/**
 * Converts program <code>Instruction</code>s to text.
 * Intended for use as the default <code>Outputter</code>.
 */
public class TpiawoOutputter extends Outputter
{
  /** 
   * the String to place between the address and the rest of 
   * the instruction
   */
  protected final static String ADDRESS_SEPERATOR = " ";

  /** 
   * Text to be printed just before each instruction. This is used
   * to provide a seperatation between the printed instruction from 
   * the line numbers.
   */
  protected final static String LEADING_TEXT = " ";
  
  /**
   * desired width of the opcode column. used by subclasses for printing.
   */
  protected final static int OPCODE_WIDTH = 6;
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.Address)
   */
  public String toString(Address address)
  {
    return Long.toHexString(address.toLong());
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTAdcInstruction)
   */
  public String toString(AdcInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR +
      StringUtils.padRight("ADC", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrcOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTAddInstruction)
   */
  @Override
  public String toString(AddInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR +
      StringUtils.padRight("ADD", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTAddInstruction)
   */
  @Override
  public String toString(AndInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR +
      StringUtils.padRight("AND", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " + 
      instruction.getSrcOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTAddressOperand)
   */
  @Override
  public String toString(AddressOperand operand)
  {
    return operand.getAddress().toString();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCallInstruction)
   */
  @Override
  public String toString(CallInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("CALL", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCdqInstruction)
   */
  @Override
  public String toString(CdqInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      "CDQ";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCldInstruction)
   */
  @Override
  public String toString(CldInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      "CLD";  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCmpInstruction)
   */
  @Override
  public String toString(CmpInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("CMP", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " + 
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTCwdInstruction)
   */
  @Override
  public String toString(CwdInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      "CWD";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTDecInstruction)
   */
  @Override
  public String toString(DecInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("DEC", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTDivInstruction)
   */
  @Override
  public String toString(DivInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("DIV", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTEnterInstruction)
   */
  @Override
  public String toString(EnterInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("ENTER", OPCODE_WIDTH) + 
      instruction.getSizeOperand().toString() + ", " + 
      instruction.getNestingLevelOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTExpressionOperand)
   */
  @Override
  public String toString(ExpressionOperand instruction)
  {
    // TODO This method is left empty for now, because the
    // the way the expression class is designed needs improvement.
    // Currently, this function is not used.
    return null;
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTExternalFunctionOperand)
   */
  @Override
  public String toString(ExternalFunctionOperand instruction)
  {
    return instruction.getName();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTHltInstruction)
   */
  public String toString(HltInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("HLT", OPCODE_WIDTH);
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTIdivInstruction)
   */
  @Override
  public String toString(IdivInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("IDIV", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTImmediateOperand)
   */
  @Override
  public String toString(ImmediateOperand instruction)
  {
    // TODO This function is not yet implemented, because 
    // the design of the ASTImmediateOperand class needs 
    // improvement.  This function is never used.
    return null;
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTImulInstruction_1op)
   */
  @Override
  public String toString(ImulInstruction1op instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("IMUL", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTImulInstruction_2op)
   */
  @Override
  public String toString(ImulInstruction2op instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("IMUL", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTImulInstruction_3op)
   */
  @Override
  public String toString(ImulInstruction3op instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("IMUL", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrc1Operand().toString() + ", " + 
      instruction.getSrc2Operand().toString();    
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTInInstruction)
   */
  @Override
  public String toString(InInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("IN", OPCODE_WIDTH) + 
      instruction.getDstOperand() + ", " +
      instruction.getSrcOperand();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTIncInstruction)
   */
  @Override
  public String toString(IncInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("INC", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString(); 
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTIndirectOperand)
   */
  @Override
  public String toString(IndirectOperand instruction)
  {
    return instruction.getOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJaJnbeInstruction)
   */
  @Override
  public String toString(JaJnbeInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JA", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.JaJnbeInstruction)
   */
  @Override
  public String toString(JaeInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JAE", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJbeInstruction)
   */
  @Override
  public String toString(JbeInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JBE", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJbInstruction)
   */
  @Override
  public String toString(JbInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JB", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJecxzInstruction)
   */
  @Override
  public String toString(JecxzInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JECXZ", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJeInstruction)
   */
  @Override
  public String toString(JeInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JE", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJgeInstruction)
   */
  @Override
  public String toString(JgeInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JGE", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJgInstruction)
   */
  @Override
  public String toString(JgInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JG", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJleInstruction)
   */
  @Override
  public String toString(JleInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JLE", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJlInstruction)
   */
  @Override
  public String toString(JlInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JL", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJmpInstruction)
   */
  @Override
  public String toString(JmpInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JMP", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJnbInstruction)
   */
  @Override
  public String toString(JnbInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JNB", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJnbInstruction)
   */
  @Override
  public String toString(JneInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JNE", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJnsInstruction)
   */
  @Override
  public String toString(JnsInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JNS", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJnzInstruction)
   */
  public String toString(JnzInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JNZ", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTJsInstruction)
   */
  @Override
  public String toString(JsInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("JS", OPCODE_WIDTH) + 
      instruction.getTargetOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLeaInstruction)
   */
  @Override
  public String toString(LeaInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("LEA", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " + 
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLeaveInstruction)
   */
  @Override
  public String toString(LeaveInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      "LEAVE";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLockInstruction)
   */
  public String toString(LockInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("LOCK", OPCODE_WIDTH) +
      instruction.getChildInstruction();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLodsInstruction)
   */
  @Override
  public String toString(LodsInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("LODS", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString(); 
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTLoopdInstruction)
   */
  @Override
  public String toString(LoopdInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("LOOPD ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMemory16Operand)
   */
  @Override
  public String toString(Memory16Operand instruction)
  {
    StringBuffer result = new StringBuffer();
    result.append("word ptr ");
    switch(instruction.getSegmentRegister())
    {
    case CS:
      result.append("cs:");
      break;

    case DS:
      result.append("ds:");
      break;

    case ES:
      result.append("es:");
      break;

    case FS:
      result.append("fs:");
      break;

    case GS:
      result.append("gs:");
      break;

    case SS:
      result.append("ss:");
      break;
    }
    
    result.append("[" + instruction.getOperand().toString() + "]");
    return result.toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMemory32Operand)
   */
  @Override
  public String toString(Memory32Operand instruction)
  {
    StringBuffer result = new StringBuffer();
    result.append("dword ptr ");
    switch(instruction.getSegmentRegister())
    {
    case CS:
      result.append("cs:");
      break;

    case DS:
      result.append("ds:");
      break;

    case ES:
      result.append("es:");
      break;

    case FS:
      result.append("fs:");
      break;

    case GS:
      result.append("gs:");
      break;

    case SS:
      result.append("ss:");
      break;
    }
    
    result.append("[" + instruction.getOperand().toString() + "]");
    return result.toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMemory8Operand)
   */
  @Override
  public String toString(Memory8Operand instruction)
  {
    StringBuffer result = new StringBuffer();
    result.append("byte ptr ");
    switch(instruction.getSegmentRegister())
    {
    case CS:
      result.append("cs:");
      break;

    case DS:
      result.append("ds:");
      break;

    case ES:
      result.append("es:");
      break;

    case FS:
      result.append("fs:");
      break;

    case GS:
      result.append("gs:");
      break;

    case SS:
      result.append("ss:");
      break;
    }
    
    result.append("[" + instruction.getOperand().toString() + "]");
    return result.toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMovInstruction)
   */
  @Override
  public String toString(MovInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("MOV", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " + 
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMovsInstruction)
   */
  @Override
  public String toString(MovsInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("MOVS", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " + 
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMovsxInstruction)
   */
  @Override
  public String toString(MovsxInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("MOVSX", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMovzxInstruction)
   */
  @Override
  public String toString(MovzxInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("MOVZX", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTMulInstruction)
   */
  @Override
  public String toString(MulInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("MUL", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTNegInstruction)
   */
  @Override
  public String toString(NegInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("NEG ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTNopInstruction)
   */
  @Override
  public String toString(NopInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      "NOP";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTNotInstruction)
   */
  @Override
  public String toString(NotInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("NOT ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTOrInstruction)
   */
  @Override
  public String toString(OrInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("OR", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " + 
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTPopInstruction)
   */
  @Override
  public String toString(PopInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("POP", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.PopadInstruction)
   */
  public String toString(PopadInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() +
      ADDRESS_SEPERATOR +
      "POPAD";
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.PopfdInstruction)
   */
  public String toString(PopfdInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR +
      "POPFD";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.PushadInstruction)
   */
  public String toString(PushadInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() +
      ADDRESS_SEPERATOR + "PUSHAD";
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTPushfdInstruction)
   */
  @Override
  public String toString(PushfdInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      "PUSHFD";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTPushInstruction)
   */
  @Override
  public String toString(PushInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("PUSH", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRegister16Operand)
   */
  @Override
  public String toString(Register16Operand instruction)
  {
    switch(instruction.getRegister())
    {
      case AX: return "ax";
      case BX: return "bx";
      case CX: return "cx";
      case DX: return "dx";
      case BP: return "bp";
      case DI: return "di";
      case SI: return "si";
      case SP: return "sp";
      default:
        Log.write(Log.ERROR, "TpiawoOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");
        throw new Error("TpiawoOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");
    }

  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRegister32Operand)
   */
  @Override
  public String toString(Register32Operand instruction)
  {
    switch(instruction.getRegister())
    {
      case EAX: return "eax";
      case EBX: return "ebx";
      case ECX: return "ecx";
      case EDX: return "edx";
      case EBP: return "ebp";
      case EDI: return "edi";
      case ESI: return "esi";
      case ESP: return "esp";
      default:
        Log.write(Log.ERROR, "TpiawoOutputter.toString(ASTRegister32Operand: Unknown register type encountered.");
        throw new Error("TpiawoOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");             
    }
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRegister8Operand)
   */
  @Override
  public String toString(Register8Operand instruction)
  {
    switch(instruction.getRegister())
    {
      case AL: return "al";
      case AH: return "ah";
      
      case BL: return "bl";
      case BH: return "bh";

      case CL: return "cl";
      case CH: return "ch";

      case DL: return "dl";
      case DH: return "dh";

      default:
        Log.write(Log.ERROR, "TpiawoOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");
        throw new Error("TpiawoOutputter.toString(ASTRegister16Operand: Unknown register type encountered.");
    }
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRepMovsInstruction)
   */
  @Override
  public String toString(RepMovsInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("REP MOVS ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRepneScasInstruction)
   */
  @Override
  public String toString(RepneScasInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("REPNE SCAS ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRepStosInstruction)
   */
  @Override
  public String toString(RepStosInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("REP STOS ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRepStosInstruction)
   */
  @Override
  public String toString(RepzMovsInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("REPZ MOVS ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86InstructionSet.RepzStosInstruction)
   */
  @Override
  public String toString(RepzStosInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("REPS STOS ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRetInstruction)
   */
  @Override
  public String toString(RetInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("RET", OPCODE_WIDTH) + 
    ( (instruction.getDstOperand() != null) ?  instruction.getDstOperand().toString() : "" );
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRolInstruction)
   */
  @Override
  public String toString(RolInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("ROL ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTRorInstruction)
   */
  @Override
  public String toString(RorInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("ROR ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSarInstruction)
   */
  @Override
  public String toString(SarInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SAR", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSbbInstruction)
   */
  @Override
  public String toString(SbbInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SBB ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() +
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSegmentRegisterOperand)
   */
  @Override
  public String toString(SegmentRegisterOperand instruction)
  {
    switch(instruction.getRegister())
    {
      case CS: return "cs";
      case DS: return "ds";
      case ES: return "es";
      case FS: return "fs";
      case GS: return "gs";
      case SS: return "ss";
      default: 
        Log.write(Log.ERROR, "TpiawoOutputter.toString(ASTSegmentRegisterOperand): Invalid register.");
        throw new Error("TpiawoOutputter.toString(ASTSegmentRegisterOperand): Invalid register.");             
    }
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetaeSetnbInstruction)
   */
  @Override
  public String toString(SetaeSetnbInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SETAE", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetbeSetnaInstruction)
   */
  @Override
  public String toString(SetbeSetnaInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SETBE", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetbSetnaeInstruction)
   */
  @Override
  public String toString(SetbSetnaeInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SETB", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSeteSetzInstruction)
   */
  @Override
  public String toString(SeteSetzInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SETE", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetgeSetnlInstruction)
   */
  @Override
  public String toString(SetgeSetnlInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SETGE", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetgSetnleInstruction)
   */
  public String toString(SetgSetnleInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SETG", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }
  
  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetleSetngInstruction)
   */
  @Override
  public String toString(SetleSetngInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SETLE", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetlSetngeInstruction)
   */
  @Override
  public String toString(SetlSetngeInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SETL", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSetneSetnzInstruction)
   */
  @Override
  public String toString(SetneSetnzInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SETNE", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTShlInstruction)
   */
  @Override
  public String toString(ShlInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SHL", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() +
      (instruction.getSrcOperand() != null ? instruction.getSrcOperand().toString() : "");
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTShrInstruction)
   */
  @Override
  public String toString(ShrInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SHR", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() +
      (instruction.getSrcOperand() != null ? instruction.getSrcOperand().toString() : "");
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTStdInstruction)
   */
  @Override
  public String toString(StdInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      "STD";
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTStosInstruction)
   */
  @Override
  public String toString(StosInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("STOS ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTSubInstruction)
   */
  @Override
  public String toString(SubInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("SUB", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " + 
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTTestInstruction)
   */
  @Override
  public String toString(TestInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("TEST ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " + 
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTUndefInstruction)
   */
  @Override
  public String toString(UndefInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("UNDEF", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString();  
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTXchgInstruction)
   */
  @Override
  public String toString(XchgInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("XCHG ", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " + 
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.ASTXorInstruction)
   */
  @Override
  public String toString(XorInstruction instruction)
  {
    return LEADING_TEXT + instruction.getAddress() + 
      ADDRESS_SEPERATOR + 
      StringUtils.padRight("XOR", OPCODE_WIDTH) + 
      instruction.getDstOperand().toString() + ", " +  
      instruction.getSrcOperand().toString();
  }

  /* (non-Javadoc)
   * @see ui.outputters.Outputter#toString(x86Parser.UnknownAddress)
   */
  public String toString(UnknownAddress address)
  {
    return "????????h";
  }
}

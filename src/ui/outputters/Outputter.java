package ui.outputters;

import x86InstructionSet.AdcInstruction;
import x86InstructionSet.AddInstruction;
import x86InstructionSet.Address;
import x86InstructionSet.AddressOperand;
import x86InstructionSet.AndInstruction;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.CdqInstruction;
import x86InstructionSet.CldInstruction;
import x86InstructionSet.CmpInstruction;
import x86InstructionSet.CwdInstruction;
import x86InstructionSet.DecInstruction;
import x86InstructionSet.DivInstruction;
import x86InstructionSet.EnterInstruction;
import x86InstructionSet.ExpressionOperand;
import x86InstructionSet.ExternalFunctionOperand;
import x86InstructionSet.HltInstruction;
import x86InstructionSet.IdivInstruction;
import x86InstructionSet.ImmediateOperand;
import x86InstructionSet.ImulInstruction1op;
import x86InstructionSet.ImulInstruction2op;
import x86InstructionSet.ImulInstruction3op;
import x86InstructionSet.InInstruction;
import x86InstructionSet.IncInstruction;
import x86InstructionSet.IndirectOperand;
import x86InstructionSet.JaJnbeInstruction;
import x86InstructionSet.JaeInstruction;
import x86InstructionSet.JbInstruction;
import x86InstructionSet.JbeInstruction;
import x86InstructionSet.JeInstruction;
import x86InstructionSet.JecxzInstruction;
import x86InstructionSet.JgInstruction;
import x86InstructionSet.JgeInstruction;
import x86InstructionSet.JlInstruction;
import x86InstructionSet.JleInstruction;
import x86InstructionSet.JmpInstruction;
import x86InstructionSet.JnbInstruction;
import x86InstructionSet.JneInstruction;
import x86InstructionSet.JnsInstruction;
import x86InstructionSet.JnzInstruction;
import x86InstructionSet.JsInstruction;
import x86InstructionSet.LeaInstruction;
import x86InstructionSet.LeaveInstruction;
import x86InstructionSet.LockInstruction;
import x86InstructionSet.LodsInstruction;
import x86InstructionSet.LoopdInstruction;
import x86InstructionSet.Memory16Operand;
import x86InstructionSet.Memory32Operand;
import x86InstructionSet.Memory8Operand;
import x86InstructionSet.MovInstruction;
import x86InstructionSet.MovsInstruction;
import x86InstructionSet.MovsxInstruction;
import x86InstructionSet.MovzxInstruction;
import x86InstructionSet.MulInstruction;
import x86InstructionSet.NegInstruction;
import x86InstructionSet.NopInstruction;
import x86InstructionSet.NotInstruction;
import x86InstructionSet.OrInstruction;
import x86InstructionSet.PopInstruction;
import x86InstructionSet.PopadInstruction;
import x86InstructionSet.PopfdInstruction;
import x86InstructionSet.PushInstruction;
import x86InstructionSet.PushadInstruction;
import x86InstructionSet.PushfdInstruction;
import x86InstructionSet.Register16Operand;
import x86InstructionSet.Register32Operand;
import x86InstructionSet.Register8Operand;
import x86InstructionSet.RepMovsInstruction;
import x86InstructionSet.RepStosInstruction;
import x86InstructionSet.RepneScasInstruction;
import x86InstructionSet.RepzMovsInstruction;
import x86InstructionSet.RepzStosInstruction;
import x86InstructionSet.RetInstruction;
import x86InstructionSet.RolInstruction;
import x86InstructionSet.RorInstruction;
import x86InstructionSet.SarInstruction;
import x86InstructionSet.SbbInstruction;
import x86InstructionSet.SegmentRegisterOperand;
import x86InstructionSet.SetaeSetnbInstruction;
import x86InstructionSet.SetbSetnaeInstruction;
import x86InstructionSet.SetbeSetnaInstruction;
import x86InstructionSet.SeteSetzInstruction;
import x86InstructionSet.SetgSetnleInstruction;
import x86InstructionSet.SetgeSetnlInstruction;
import x86InstructionSet.SetlSetngeInstruction;
import x86InstructionSet.SetleSetngInstruction;
import x86InstructionSet.SetneSetnzInstruction;
import x86InstructionSet.ShlInstruction;
import x86InstructionSet.ShrInstruction;
import x86InstructionSet.StdInstruction;
import x86InstructionSet.StosInstruction;
import x86InstructionSet.SubInstruction;
import x86InstructionSet.TestInstruction;
import x86InstructionSet.UndefInstruction;
import x86InstructionSet.UnknownAddress;
import x86InstructionSet.XchgInstruction;
import x86InstructionSet.XorInstruction;


/**
 * Defines how each <code>Instruction</code> is translated into
 * text.  The user should subclass this class in order
 * to define a new <code>Instruction</code> output format. Users
 * can specify which <code>Outputter</code> to use by calling the
 * <code>setOutputter</code> method. If this method is not called,
 * the default <code>outputter</code> will be used, which is
 * <code>TpiawoOutputter</code>. 
 */
public abstract class Outputter 
{
  /** The currently selected outputter */
  static private Outputter instance = null;
  
  /** The defaultOutputter is used if no other Outputter is specified. */
  static private Outputter defaultOutputter = new TpiawoOutputter();
  
  /**
   * Returns a reference to the currently specified <code>Outputter</code>.
   * If no <code>Outputter</code> has been specified, the default
   * <code>Outputter</code> is used.  
   * @return the currently specified <code>Outputter</code>
   */
  static public Outputter getInstance()
  {
    if(null == instance)
      instance = defaultOutputter;
    return instance;
  }
  
  /**
   * Specifies which <code>Outputter</code> should be used from here
   * on. The <code>Outputter</code> can be changed again at a 
   * later time by calling this function again.
   * @param outputter the output to use. Must not be null.
   */
  static public void setOutputter(Outputter outputter)
  {
    assert(outputter != null);
    instance = outputter;
  }

  /**
   * @param address
   * @return string representation.
   */
  abstract public String toString(Address address);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(AdcInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(AddInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(AndInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(AddressOperand instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(CallInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(CdqInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(CldInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(CmpInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(CwdInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(DecInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(DivInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(EnterInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(ExpressionOperand instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(ExternalFunctionOperand instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(HltInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(IdivInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(ImmediateOperand instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(ImulInstruction1op instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(ImulInstruction2op instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(ImulInstruction3op instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(InInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(IncInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(IndirectOperand instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JaeInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JaJnbeInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JbeInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JbInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JecxzInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JeInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JgeInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JgInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JleInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JlInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JmpInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JnbInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JneInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JnsInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JnzInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(JsInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(LeaInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(LeaveInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(LockInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(LodsInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(LoopdInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(Memory16Operand instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(Memory32Operand instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(Memory8Operand instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(MovInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(MovsInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(MovsxInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(MovzxInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(MulInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(NegInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(NopInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(NotInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(OrInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(PopInstruction instruction);

  /**
   * Convert a PopfdInstruction to a string.
   * @param instruction instruction to be converted
   * @return string representation.
   */
  abstract public String toString(PopfdInstruction instruction);

  /**
   * Returns string representation of a POPAD instruction.
   * @param instruction instruction to convert to string
   * @return string representation
   */
  abstract public String toString(PopadInstruction instruction);

  /**
   * Returns a string representing the <code>Pushad</code> instruction.
   * @param instruction instruction to be converted to a string.
   * @return string representing input instruction.
   */
  abstract public String toString(PushadInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(PushfdInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(PushInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(Register16Operand instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(Register32Operand instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(Register8Operand instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(RepMovsInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(RepneScasInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(RepStosInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(RepzMovsInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(RepzStosInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(RetInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(RolInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(RorInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SarInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SbbInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SegmentRegisterOperand instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SetaeSetnbInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SetbeSetnaInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SetbSetnaeInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SeteSetzInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SetgeSetnlInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SetgSetnleInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SetleSetngInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SetlSetngeInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SetneSetnzInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(ShlInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(ShrInstruction instruction);
  
  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(StdInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(StosInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(SubInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(TestInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(UndefInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(XchgInstruction instruction);

  /**
   * @param instruction
   * @return string representation.
   */
  abstract public String toString(XorInstruction instruction);

  /**
   * @param address
   * @return string representation.
   */
  abstract public String toString(UnknownAddress address);
}

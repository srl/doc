/*
 * RegisterContentProvider.java
 * Created on Apr 8, 2005
 */
package ui.registers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import kCallContextAnalysisDomain.KCallContext;
import lCallContextAnalysisDomain.LCallContext;
import lStackContextAnalysisDomain.LStackContext;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import csAnalysisDomain.csAnalysisDomain;

import vsaAsgDomain.VsaAsgState;
import x86InstructionSet.Instruction;
import x86Interpreter.Interpreter;
import domain.Register;
import domain.State;

/**
 * Provides the content for the RegistersView. It is expected that
 * this class will be subclassed to provide support for providing the
 * content from the begin state and the end state, depending on which
 * subclassed <code>RegisterContentProvider</code> is being used.
 */
public abstract class RegisterContentProvider implements IStructuredContentProvider 
{
    /**
     * Returns an array for use in a TableView.  Each element in the array
     * is a Register-Value pair that is later converted into a String by
     * the RegisterLabelProvider.
     */
	public Object[] getElements(Object input)
    {
    
      ArrayList<RegisterValueContextPair> regContext =  new ArrayList<RegisterValueContextPair>();;
      State state = getState(input);
                  
      String address;
      if(state != null)
        {
          if ( state instanceof csAnalysisDomain) {
        	
        	  String stackString;
        	  regContext = new ArrayList<RegisterValueContextPair>();
        	  csAnalysisDomain context = (csAnalysisDomain)state;
        	  
        	  Map contextmap = context.getMapping();
			  Iterator <String> contextSet = contextmap.keySet().iterator();
			  while (contextSet.hasNext()){
				  stackString = contextSet.next();
				  ArrayList<Register> regList = Register.regList;
				  for (int ri = 0; ri < regList.size(); ri++){
					  Register reg = regList.get(ri);
					  regContext.add(new RegisterValueContextPair(reg, ((State) contextmap.get(stackString)).getRegisterValue(reg), stackString));
				  }
			  }   
        	  return regContext.toArray();
              
              
          }   else if ( state instanceof VsaAsgState) {
			  ArrayList<Register> regList = Register.regList;
				  for (int ri = 0; ri < regList.size(); ri++) {
					  Register reg = regList.get(ri);
					  regContext.add(new RegisterValuePair(reg, state.getRegisterValue(reg)));
				  }
      	  
              return regContext.toArray();
          }
        	         
        }
      
      return regContext.toArray();
        
    }
    
    /**
     * Returns the State that should be used by this content provider. 
     * @param instruction the instruction that contains the register we are interested in.
     * @return the State to use.
     */
    protected abstract State getState(Object instruction);
        
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IContentProvider#dispose()
     */
    public void dispose()
    {        
    }
    
    /**
     * Since the input to this content provider isn't expected to be modified,
     * this function does nothing.
     */
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {
    }
}

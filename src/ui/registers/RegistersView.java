/*
 * RegistersView.java
 * Created on Apr 5, 2005
 */
package ui.registers;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;

import ui.programViewer.InstructionSelection;

/**
 * Displays the registers contents for a given state.
 */
public class RegistersView extends ViewPart implements ISelectionListener
{
  /** The viewer widget */
  private TableViewer tableviewer;
    
  /** User action that displays the begin state */
  private Action useBeginStateAction;
    
  /** User action that displays the end state */
  private Action useEndStateAction;
  
  /** 
   * Specifies which state is currently being displayed: the 
   * begin state or the end state 
   */
  private DisplayState displayedState = DisplayState.END_STATE;
  
  /** Enum that specifies the two state options: begin or end */
  enum DisplayState
  {
    BEGIN_STATE,
    END_STATE
  };
  

  /**
   * Creates all actions that are applicable to this view. 
   */
  private void createActions()
  {
    useBeginStateAction = new Action("Show Begin State") 
    {
      public void run()
      {
        displayedState = DisplayState.BEGIN_STATE;
        updateActionChecks();
        tableviewer.setContentProvider(new BeginStateRegisterContentProvider());
        tableviewer.refresh();
      }
    };
    
    useEndStateAction = new Action("Show End State")
    {
      public void run()
      {
        displayedState = DisplayState.END_STATE;
        updateActionChecks();
        tableviewer.setContentProvider(new EndStateRegisterContentProvider());
        tableviewer.refresh();
      }
    };    
  }

  /**
   * Creates the menu for this view. 
   */
  private void createMenu()
  {
    IMenuManager menu;
    
    menu = getViewSite().getActionBars().getMenuManager();
    menu.add(useBeginStateAction);
    menu.add(useEndStateAction);
    updateActionChecks();
  }

  /**
   * Creates the initial layout of the registers table.
   */
  public void createPartControl(Composite parent)
  {
    createRegisterTable(parent);
    createActions();
    createMenu();
    createToolbar();
    
    getViewSite().getPage().addSelectionListener(this);
  }

  /**
   * Creates the toolbar for this view. 
   */
  private void createToolbar()
  {
    IToolBarManager toolbar;
    
    toolbar = getViewSite().getActionBars().getToolBarManager();
    toolbar.add(useBeginStateAction);
    toolbar.add(useEndStateAction);
  }

  /**
   * Creates the table that is responsible for displaying register contents.
   * @param parent the parent of this view.
   */
  private void createRegisterTable(Composite parent)
  {
    tableviewer = new TableViewer(parent);
    Table table = (Table)tableviewer.getControl();
    
    table.setHeaderVisible(true);
    
    TableColumn column = new TableColumn( table, 0);
    column.setText("Register");
    column.setWidth(80);
    
    column = new TableColumn( table, 0);
    column.setText("Value");
    column.setWidth(130);
    
    column = new TableColumn( table, 0);
    column.setText("Context");
    column.setWidth(130);
    
    tableviewer.setContentProvider(new EndStateRegisterContentProvider());
    tableviewer.setLabelProvider(new RegisterLabelProvider());
  }
    
  /**
   * Destroys this object
   */
  public void dispose()
  {
    getViewSite().getPage().removeSelectionListener(this);
    super.dispose();
  }
    
  /**
   * Updates the register contents with new state inforamtion.
   * This class is a SelectionListener for this Page (a page contains all
   * of the views for this perspective).  The FileView notifies the Page
   * when a new instruction is selected and the Page then calls
   * this function.  This function obtains the state for the selected 
   * instruction and displays the register contents for that state.
   */
  public void selectionChanged(IWorkbenchPart part, ISelection selection)
  {
    if (selection instanceof InstructionSelection) 
    {
      tableviewer.setInput( ((InstructionSelection)selection).getInstruction() );
    }
  }
        
  /**
   * Sets focus to the appropriate control in this view.
   */
  public void setFocus()
  {
    tableviewer.getControl().setFocus();
  }
    
  /**
   * Assigns new input for this view.
   * @param input
   */
  public void setInput(Object input)
  {
    tableviewer.setInput(input);
  }

  /**
   * Adds or removes checks from the view actions based on the current
   * state of the view. 
   */
  private void updateActionChecks()
  {
    useBeginStateAction.setChecked(displayedState == DisplayState.BEGIN_STATE);  
    useEndStateAction.setChecked(displayedState == DisplayState.END_STATE);
  }
}

package ui.registers;

import x86InstructionSet.Instruction;
import domain.State;

/**
 * Provides the register view with the register contents for the begin state 
 * of an instruction. 
 */
public class StateRegisterContentProvider extends RegisterContentProvider
{
  /* (non-Javadoc)
   * @see ui.registers.RegisterContentProvider#getRegisterValue(x86InstructionSet.Instruction, domain.Register)
   */
  protected State getState(Object obj)
  {
	  State state = (State) obj;
    // clone the state before returning the value to prevent accidental changes
    // to the state.  Some state may change internally by calls to getRegisterValue.
    if(state != null)
      return (State) state.clone();
    else
      return null;
  }
}

/*
 * RegisterValuePair.java
 * Created on Apr 8, 2005
 */
package ui.registers;

import domain.Register;
import domain.Value;

/**
 * A pairing of Register and Value. Intended for use by the RegisterContentProvider and 
 * RegisterLabelProvider classes.  The RegisterContentProvider class prepares an
 * array of RegisterValuePairs and the RegisterLabelProvider retrieves a specific
 * item from the array and converts to a String.
 */
public class RegisterValuePair extends RegisterValueContextPair
{
    public RegisterValuePair(Register register, Value value)
    {
    	// ARUN: Context is empty, to avoid confustion with
    	// "Insensitive" used in other places.
        super(register, value, "");
    }
}

package ui.registers;

import domain.Register;
import domain.Value;

/**
 * A triple of Register, Value and Context. Intended for use by the RegisterContentProvider and 
 * RegisterLabelProvider classes.  The RegisterContentProvider class prepares an
 * array of RegisterValueContextPairs and the RegisterLabelProvider retrieves a specific
 * item from the array and converts to a String.
 */
public class RegisterValueContextPair {

	 public RegisterValueContextPair(Register register, Value value, String stackString)
	    {
	        this.register = register;
	        this.value = value;
	        this.string = stackString;
	    }
	 public Boolean equals(RegisterValueContextPair other) {
		 return (register.equals(other.register))
		       && value.equals(other.value)
		       && (string.equals(other.string));
	 }
	 
	    public Register register;
	    public Value value;
	    public String string;
	
}

/*
 * RegisterLabelProvider.java
 * Created on Apr 8, 2005
 */
package ui.registers;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

/**
 * Provides the strings used to display the register contents.
 */
public class RegisterLabelProvider implements ITableLabelProvider
{

    /**
     * Does nothing.  
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
     */
    public Image getColumnImage(Object element, int columnIndex)
    {
        return null;
    }

    /**
     * Converts a RegisterValueContextPair into the appropriate text.  Used
     * by TableViewer.
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
     */
    public String getColumnText(Object element, int columnIndex)
    {
        if (element instanceof RegisterValueContextPair)
        {
        	RegisterValueContextPair pair = (RegisterValueContextPair)element;
            if(columnIndex == 0)
	        {
	        	switch(pair.register)
	            {
		            case EAX:	return "EAX";
	            	case EBP:	return "EBP";
	            	case EBX:	return "EBX";
	            	case ECX:	return "ECX";
	            	case EDI:	return "EDI";
	            	case EDX:	return "EDX";
	            	case ESI:	return "ESI";
	            	case ESP:	return "ESP";
	            	case CS:	return "CS";
	            	case DS:	return "DS";
	            	case ES:	return "ES";
	            	case FS:	return "FS";
	            	case GS:	return "GS";
	            	case SS:	return "SS";
	            	case AL:	return "AL";
	            	case AX: 	return "AX";
	            	case AH:	return "AH";
	            	case BX:	return "BX";
	            	case BL:	return "BL";
	            	case BH:	return "BH";
	            	case CX:	return "CX";
	            	case CL:	return "CL";
	            	case CH:	return "CH";
	            	case DX: 	return "DX";
	            	case DL:	return "DL";
	            	case DH:	return "DH";
	            	case SP:	return "SP";
	            	case BP:	return "BP";
	            	case SI:	return "SI";
	            	case DI: 	return "DI";
	            	//add other registers
	            	default:
	            	    throw new Error("RegisterLabelProvider.getColumnText: Unknown register");
	            }
	        }
	        else if(columnIndex == 1)
	        {
	            return pair.value.toString();
	        }
            
            //TODO:
	        else if (columnIndex == 2)
	        {
	        	return pair.string;
	        }
	                   
	        else
	            throw new Error("Incorrect columnIndex passed to getColumnText");
        	
        }
        
        else
            throw new Error("Incorrect object type pased to getColumnText");
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
     */
    public void addListener(ILabelProviderListener listener)
    {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
     */
    public void dispose()
    {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
     */
    public boolean isLabelProperty(Object element, String property)
    {
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
     */
    public void removeListener(ILabelProviderListener listener)
    {
    }
    
      
}

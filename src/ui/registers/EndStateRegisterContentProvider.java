package ui.registers;

import x86InstructionSet.Instruction;
import domain.State;

/**
 * Provides the register view with the register contents for the end state 
 * of an instruction. 
 */
public class EndStateRegisterContentProvider extends RegisterContentProvider
{
  /* (non-Javadoc)
   * @see ui.registers.RegisterContentProvider#getRegisterValue(x86InstructionSet.Instruction, domain.Register)
   */
  protected State getState(Object obj)
  {
	  Instruction instruction = (Instruction) obj;
    // clone the state before returning the value to prevent accidental changes
    // to the state.  Some state may change internally by calls to getRegisterValue.
    if(instruction.getEndState() != null)
      return (State)instruction.getEndState().clone();
    else
      return null;
  }
}

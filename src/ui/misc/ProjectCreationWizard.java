/*
 * Created on Augest 7, 2004
 */

package ui.misc;

/*******************************************************************************
 * 
 * Copyright (C) 2004, 2005, University of Louisiana at Lafayette
 * Author: Eric Uday Kumar 
 * 
 *******************************************************************************/

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.jface.viewers.IStructuredSelection;


/**
 * Project creation wizard for Detect Obfuscation.
 * @author Eric Uday Kumar
 */
public class ProjectCreationWizard extends Wizard implements INewWizard {
	private IStructuredSelection selection;
	private IWorkbench workbench;
	private ProjectCreationPage mainPage;

	public void init(IWorkbench workbench, IStructuredSelection selection) {
		this.workbench = workbench;
		this.selection = selection;
		setWindowTitle("Obfuscation Detection Project");
	}

	public void addPages() {
		mainPage = new ProjectCreationPage(workbench, selection);
		addPage((IWizardPage) mainPage);
	}
	
	public boolean canFinish() {
		return mainPage.checkFinish();
	}

	public boolean performFinish() {
		if (mainPage.finish()) return true;
		else return false;
	}
}

/*
 * ViewIds.java
 * Created on May 12, 2005
 */
package ui.misc;

/**
 * Contains IDs of the various views in this project.
 * These IDs are defined in <code>plugin.xml</code>
 */
public class ViewIds
{
    public static final String MATCHING_INSTRUCTION_VIEW  = "MatchingInstructionsView";
    public static final String MESSAGES_VIEW              = "MessageView";
    public static final String OBFUSCATED_CALLS_VIEW      = "ObfuscatedCallsView";
    public static final String OBFUSCATED_RETURNS_VIEW    = "ObfuscatedReturnsView";
    public static final String REGISTERS_VIEW             = "RegistersView";
    public static final String STACK_VIEW                 = "StackView";
    public static final String CONTROL_FLOW_VIEW		  = "ControlFlowGraphView";
    public static final String VALID_CALL_RET_SITES_VIEW  = "ValidCallRetSitesView";
    public static final String PROGRAM_VIEWER_VIEW        = "ProgramViewer";
}

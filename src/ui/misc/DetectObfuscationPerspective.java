/*
 * Created on June 7, 2004
 */

package ui.misc;

/*******************************************************************************
 * 
 * Copyright (C) 2004, 2005, University of Louisiana at Lafayette
 * Author: Eric Uday Kumar 
 * 
 *******************************************************************************/

import org.eclipse.ui.IPerspectiveFactory; 
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IFolderLayout;


/**
 * Perspective for Detect Obfuscation plugin
 * @author Eric Uday Kumar
 */
public class DetectObfuscationPerspective implements IPerspectiveFactory{
	
	public void createInitialLayout(IPageLayout layout){
	defineActions(layout);
	defineLayout(layout);
	}

	public void defineActions(IPageLayout layout) {
		// Add "new wizards".
		layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.folder");
		layout.addNewWizardShortcut("org.eclipse.ui.wizards.new.file");

		// Add "show views".
		layout.addShowViewShortcut(IPageLayout.ID_RES_NAV);
	}

  /**
   * Creates the initial control layout for this perspective.
   */
	public void defineLayout(IPageLayout layout) 
	{
    final String MESSAGES_FOLDER  = "Messages";
    final String NAVIGATE_FOLDER	= "Navigate";
    final String REGISTERS_FOLDER	= "Registers";
    final String STACK_FOLDER		= "Stack";

    String editorArea = layout.getEditorArea();
		
		IFolderLayout navigateFolder = layout.createFolder(NAVIGATE_FOLDER, IPageLayout.LEFT, 
                                                 .2f, editorArea);
		navigateFolder.addView(IPageLayout.ID_RES_NAV);
		
    IFolderLayout registersFolder = layout.createFolder(REGISTERS_FOLDER, IPageLayout.RIGHT, 
            .75f, editorArea);
    registersFolder.addView(ViewIds.REGISTERS_VIEW);

    IFolderLayout stackFolder = layout.createFolder(STACK_FOLDER, IPageLayout.BOTTOM, 
            .40f, REGISTERS_FOLDER);
    stackFolder.addView(ViewIds.STACK_VIEW);
    
    IFolderLayout messagesFolder = layout.createFolder(MESSAGES_FOLDER, IPageLayout.BOTTOM,
            .70f, editorArea);
    messagesFolder.addView(ViewIds.VALID_CALL_RET_SITES_VIEW);        
    messagesFolder.addView(ViewIds.OBFUSCATED_CALLS_VIEW);
    messagesFolder.addView(ViewIds.OBFUSCATED_RETURNS_VIEW);
    messagesFolder.addView(ViewIds.MATCHING_INSTRUCTION_VIEW);
    messagesFolder.addView(ViewIds.CONTROL_FLOW_VIEW);
	}
}

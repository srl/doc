/*
 * Created on June 7, 2004
 */

package ui.misc;

/*******************************************************************************
 * 
 * Copyright (C) 2004, 2005, University of Louisiana at Lafayette
 * Author: Eric Uday Kumar 
 * 
 *******************************************************************************/

import java.io.*;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.dialogs.WizardNewProjectCreationPage;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.GridData; 
import org.eclipse.swt.widgets.*;
import org.eclipse.swt.SWT;
import org.eclipse.core.runtime.IPath;
import java.nio.channels.FileChannel;

/**
 * Project creation page for Detect Obfuscation.
 * @author Eric Uday Kumar
 */
public class ProjectCreationPage extends WizardNewProjectCreationPage implements Listener{
	private Text 	fileText;		// file name, path included
	private Button b;
	private FileDialog fd;
	private IPath currentProjPath;
	
    private Listener fileModifyListener = new Listener() {
        public void handleEvent(Event e) {
            boolean valid = validatePage();
            setPageComplete(valid);
        }
    };	
	
	ProjectCreationPage(IWorkbench workbench, IStructuredSelection selection) {
		super("Basic New Project Page");
		this.setTitle("Create New Project");
	}
	
	public void createControl(Composite parent) {
		super.createControl(parent);
		Composite composite = (Composite)getControl();
		
		// create the desired layout for this wizard page
		Group group = new Group(composite, SWT.NONE);
		GridLayout gl = new GridLayout();
		gl.numColumns = 3;
		group.setLayout(gl);
		group.setText("Select asm file"); 
		Label label1 = new Label (group, SWT.NONE);
		label1.setText("Browse for ASM file:");				
		fileText = new Text(group, SWT.BORDER);
		fileText.addListener(SWT.Modify, fileModifyListener );
		b = new Button(group, SWT.PUSH);
		b.addListener(SWT.Selection, this);
		b.setText("&Browse...");
		
		//create the widgets  and their grid data objects
		GridData data1 = new GridData();
		label1.setLayoutData(data1);
		GridData data2 = new GridData(GridData.FILL_HORIZONTAL);
		fileText.setLayoutData(data2);
		GridData data3 = new GridData();
		b.setLayoutData(data3);
		GridData gd = new GridData(GridData.FILL_HORIZONTAL);
		group.setLayoutData(gd);
	}
	
	public void handleEvent(Event e) {
		if (e.widget == b) {
			String s[] = {"*.asm"};
			Shell shell = new Shell();
			fd = new FileDialog(shell, SWT.APPLICATION_MODAL);
			fd.setFilterExtensions(s);
			if(fd.open() != null)
			{
				fileText.setText(fd.getFilterPath()+ File.separator + fd.getFileName());
				if(this.getProjectName()!= null) setPageComplete(true);
			}
		}
	}
	
	public boolean finish(){
		String 				filename;	// name of input file, without path.
		IProject 			handle = getProjectHandle();
		IProgressMonitor	monitor = new NullProgressMonitor();
		
		filename = new File(fileText.getText()).getName();	
		try {
			handle.create(monitor);
			handle.open(monitor);
			currentProjPath = getLocationPath().append(getProjectName());
		
			try {
				// Create channel on the source
                FileChannel srcChannel = new FileInputStream(fileText.getText()).getChannel();

                // Create channel on the destination
				FileChannel dstChannel = new FileOutputStream(currentProjPath.toOSString()+ 
				                                              File.separator + filename).getChannel();
                
    
				// Copy file contents from source to destination
				dstChannel.transferFrom(srcChannel, 0, srcChannel.size());
    
				// Close the channels
				srcChannel.close();
				dstChannel.close();
			} 	catch (IOException e) {
                e.printStackTrace();
			}
				
			handle.refreshLocal(1, monitor);
			return true;
		} 	catch (Exception e) {
				System.out.println("Problem with progress monitor" + e.getMessage());
				return false;
			}
	}

	public boolean checkFinish() 
	{
		if( this.getProjectName() == null || this.getProjectName() == "" ||
				fileText.getText() == "" || fileText.getText() == null ) 
		{
			return false;
		}
		else {	
			return true;
		}
	}	
}

/*
 * LoggingPreferencePage.java
 * Created on May 30, 2005
 */
package ui.misc;

import logger.Log;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import detectObfuscation.Preferences;

/**
 * Displays the preference options for the DetectObfuscation plugin.  Right now, all
 * perferences are shown on the same page, the DefaultPreferencePage.  Later, as more
 * options are added, it may be necessary to split the options onto multiple pages.
 */
public class DefaultPreferencePage extends PreferencePage implements IWorkbenchPreferencePage
{
  /** Width of margins inside a <code>Group</code> composite. */
  private final int GROUP_LAYOUT_MARGIN_WIDTH = 5;
  /** Height of margins inside a <code>Group</code> composite. */
  private final int GROUP_LAYOUT_MARGIN_HEIGHT = 5;
  /** Spacing between controls inside a <code>Group</code> composite. */
  private final int GROUP_LAYOUT_SPACING = 10;
  
  /** The text control that stores the user-inputed stubs path. */
  private Text stubsPathText;

  /** The list control that stores the user-inputed output format. */
  private List outputterList;
  
  /** Array containing names of each format option to be displayed to the user. */
  private final String OUTPUT_FORMAT_NAMES[] =
  {
      "Default Format",
      "Unmorph Input Format",
      "Dialog Format"
  };
  /** Index into OUTPUT_FORMAT_NAMES array */
  private final int TPIAWO_FORMAT_INDEX = 0;
  private final int UNMORPH_INPUT_FORMAT_INDEX = 1;
  private final int DIALOG_FORMAT_INDEX = 2;
  private final int NUM_OF_FORMATS = 3;
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.preference.PreferencePage#createContents(org.eclipse.swt.widgets.Composite)
     */
    protected Control createContents(Composite parent)
    {
        Composite composite = new Composite(parent, SWT.NONE);
        composite.setLayout(new RowLayout(SWT.VERTICAL));
        
        createUiGroup(composite);
        createStubsGroup(composite);

        return composite;
    }

    /**
     * Creates all controls related to the Stubs group.
     * @param parent the parent control.
     */ 
    private void createStubsGroup(Composite parent)
    {
      Group       stubsGroup;
      FormLayout  stubsLayout;
      Label       stubsPathLabel;
      FormData    stubsPathLabelData;
      
      final String  STUB_GROUP_TEXT       = "Stub DLLs";
      final String  STUB_PATH_LABEL_TEXT  = "Stub &DLLs Path:";
      final int     STUB_PATH_LABEL_WIDTH = 300;

      // Create a group for stub location options
      stubsLayout = new FormLayout();
      stubsLayout.marginWidth   = GROUP_LAYOUT_MARGIN_WIDTH;
      stubsLayout.marginHeight  = GROUP_LAYOUT_MARGIN_HEIGHT;
      stubsLayout.spacing       = GROUP_LAYOUT_SPACING;
      stubsGroup = new Group(parent, SWT.NONE);
      stubsGroup.setText(STUB_GROUP_TEXT);
      stubsGroup.setLayout(stubsLayout);

      // add option to input path of DLL stubs
      stubsPathLabel = new Label(stubsGroup, SWT.NONE);
      stubsPathLabel.setText(STUB_PATH_LABEL_TEXT);

      stubsPathLabelData = new FormData();
      stubsPathLabelData.left = new FormAttachment(stubsPathLabel);
      stubsPathLabelData.width = STUB_PATH_LABEL_WIDTH;
      stubsPathText = new Text(stubsGroup, SWT.BORDER);
      stubsPathText.setText(PlatformUI.getPreferenceStore().getString(Preferences.STUB_FILE_PATH));
      stubsPathText.setLayoutData(stubsPathLabelData);
    }

    /**
     * @param composite
     */
    private void createUiGroup(Composite composite)
    {
      final String  OUTPUTTER_LABEL_TEXT  = "&Output Format:";
      final int     OUTPUTTER_LIST_WIDTH  = 200;
      final String  UI_GROUP_TEXT         = "User Interface";
      
      Label       outputterLabel; 
      FormData    outputterListData;
      Group       uiGroup;
      FormLayout  uiLayout;
      
      // Create a group for user-interface options.
      uiLayout = new FormLayout();
      uiLayout.marginWidth  = GROUP_LAYOUT_MARGIN_WIDTH;
      uiLayout.marginHeight = GROUP_LAYOUT_MARGIN_HEIGHT;
      uiLayout.spacing      = GROUP_LAYOUT_SPACING;
      uiGroup = new Group(composite, SWT.NONE);
      uiGroup.setText(UI_GROUP_TEXT);
      uiGroup.setLayout(uiLayout);
      
      // add option to specify ui outputter.
      outputterLabel = new Label(uiGroup, SWT.NONE);
      outputterLabel.setText(OUTPUTTER_LABEL_TEXT);
      
      outputterListData = new FormData();
      outputterListData.left = new FormAttachment(outputterLabel);
      outputterListData.width = OUTPUTTER_LIST_WIDTH;
      outputterList = new List(uiGroup, SWT.SINGLE | SWT.BORDER);
      outputterList.setLayoutData(outputterListData);
      // populate the list.
      for(int i = 0; i < NUM_OF_FORMATS; i++)
        outputterList.add(OUTPUT_FORMAT_NAMES[i]);
      // highlight the correct format, based on preference store settings.
      int highlightIndex;
      int outputType = PlatformUI.getPreferenceStore().getInt(Preferences.OUTPUTTER);
      switch(outputType)
      {
      case Preferences.TPIAWO_OUTPUTTER:
        highlightIndex = TPIAWO_FORMAT_INDEX;
        break;
        
      case Preferences.UNMORPH_OUTPUTTER:
        highlightIndex = UNMORPH_INPUT_FORMAT_INDEX;
        break;
        
      case Preferences.DIALOG_OUTPUTTER:
        highlightIndex = DIALOG_FORMAT_INDEX;
        break;
        
      default:
        Log.write(Log.WARNING, "Invalid output format selected in preference store.  Resetting to default format");
        highlightIndex = TPIAWO_FORMAT_INDEX;
        break;
      }
      outputterList.select(highlightIndex);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
     */
    public void init(IWorkbench workbench)
    {
        DetectObfuscationPlugin myPlugin = DetectObfuscationPlugin.getDefault();
        if(myPlugin instanceof AbstractUIPlugin)
        {
            this.setPreferenceStore(PlatformUI.getPreferenceStore());
        }
    }
    
    /**
     * Resets each control to the default value for that preference.
     */
    protected void performDefaults()
    {
    	stubsPathText.setText(PlatformUI.getPreferenceStore().getDefaultString(Preferences.STUB_FILE_PATH));
      outputterList.setSelection(TPIAWO_FORMAT_INDEX);
    }     
    
    /**
     * Saves currently selected preferences to the preference store.
     */
    public boolean performOk()
    {
      IPreferenceStore store = PlatformUI.getPreferenceStore();
      
      // save stubs path.
      store.setValue(Preferences.STUB_FILE_PATH, stubsPathText.getText());
      
      // save output format.
      int formatIndex = outputterList.getSelectionIndex();
      int selectedOutputter;
      switch(formatIndex)
      {
      case TPIAWO_FORMAT_INDEX:
        selectedOutputter = Preferences.TPIAWO_OUTPUTTER;
        break;
        
      case UNMORPH_INPUT_FORMAT_INDEX:
        selectedOutputter = Preferences.UNMORPH_OUTPUTTER;
        break;
        
      case DIALOG_FORMAT_INDEX:
        selectedOutputter = Preferences.DIALOG_OUTPUTTER;
        break;
        
      default:
        Log.write(Log.ERROR, "Invalid format type selected.  Resetting to default.");
        selectedOutputter = Preferences.TPIAWO_OUTPUTTER;
        break;
      }
      store.setValue(Preferences.OUTPUTTER, selectedOutputter);
      
      return true;
    }
}

package ui.misc;

import java.io.IOException;
import org.eclipse.jface.action.MenuManager;
import domain.Register;
import ui.actions.ToggleRegisterAction;

/**
 * Maintains the Interpret Menu. 
 */
public class RegisterMenu extends MenuManager
{
  /** Title of this menu.  The Title is displayed on the top menu bar. */
  private static final String TITLE = "Registers";

  /**
   * Creates this. Initializes menu with title and actions. 
 * @throws IOException 
   */
  public RegisterMenu()
  {
    super(TITLE);
    Register reg[] = Register.values();
    
    for(Register register : reg)
    {
      add(new ToggleRegisterAction(register.toString(), register));
    }
  }
}

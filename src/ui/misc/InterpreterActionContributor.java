package ui.misc;

import java.io.IOException;

import org.eclipse.jface.action.IMenuManager;
import org.eclipse.ui.texteditor.BasicTextEditorActionContributor;

/**
 * Dynamically creates the interpret menu commands. 
 */
public class InterpreterActionContributor extends BasicTextEditorActionContributor
{
  /* (non-Javadoc)
   * @see org.eclipse.ui.part.EditorActionBarContributor#contributeToMenu(org.eclipse.jface.action.IMenuManager)
   */
  public void contributeToMenu(IMenuManager menu)
  {
    final String MENU_LOCATION = "additions";
    
    super.contributeToMenu(menu);

	menu.insertAfter(MENU_LOCATION, new InterpretMenu());
	menu.insertAfter(MENU_LOCATION, new RegisterMenu());
  }
}

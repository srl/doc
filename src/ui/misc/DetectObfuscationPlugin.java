/*
 * Created on June 7, 2004
 */

package ui.misc;


/*******************************************************************************
 * 
 * Copyright (C) 2004, 2005, University of Louisiana at Lafayette
 * Author: Eric Uday Kumar 
 * 
 *******************************************************************************/

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPluginDescriptor;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.plugin.AbstractUIPlugin;

import detectObfuscation.Preferences;

/**
 * The main plugin class for Detect Obfuscation.
 * @author Eric Uday Kumar
 */
public class DetectObfuscationPlugin extends AbstractUIPlugin {
	//The shared instance.
	private static DetectObfuscationPlugin plugin;
	//Resource bundle.
	private ResourceBundle resourceBundle;

	PropertyChangeSupport listener = new PropertyChangeSupport(this);
	
	/**
	 * The constructor.
	 */
	public DetectObfuscationPlugin(IPluginDescriptor descriptor) 
  {
	  // TODO: remove deprecated code
		super(descriptor);
		plugin = this;
		try {
			resourceBundle= ResourceBundle.getBundle("detectObfuscation.DetectObfuscationPluginResources");
		} catch (MissingResourceException x) {
			resourceBundle = null;
		}
	}

	/**
	 * Returns the shared instance.
	 */
	public static DetectObfuscationPlugin getDefault() {
		return plugin;
	}

	/**
	 * Returns the workspace instance.
	 */
	public static IWorkspace getWorkspace() {
		return ResourcesPlugin.getWorkspace();
	}

	/**
	 * Returns the string from the plugin's resource bundle,
	 * or 'key' if not found.
	 */
	public static String getResourceString(String key) {
		ResourceBundle bundle= DetectObfuscationPlugin.getDefault().getResourceBundle();
		try {
			return bundle.getString(key);
		} catch (MissingResourceException e) {
			return key;
		}
	}

	/**
	 * Returns the plugin's resource bundle,
	 */
	public ResourceBundle getResourceBundle() {
		return resourceBundle;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener view) {
		listener.addPropertyChangeListener(view);
	}

	public void firePropertyChange(String prop, Object oldO, Object newO) {
		if (this == null) return;
		listener.firePropertyChange( prop, oldO, newO);
	}

	/**
	 * Initializes preferences to their default values, if default values exist.
	 */
	public void initializeDefaultPreferences(IPreferenceStore store)
	{
    store.setDefault(Preferences.OUTPUTTER, Preferences.OUTPUTTER_DEFAULT);  
	  store.setDefault(Preferences.STUB_FILE_PATH, Preferences.STUB_DLL_PATH_DEFAULT);
	}
}

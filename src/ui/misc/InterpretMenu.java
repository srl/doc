package ui.misc;

import java.io.IOException;

import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;

import ui.actions.JumpToEntryPoint;
import ui.actions.RunAnalysisAction;
import ui.actions.SetBreakPoint;
import ui.actions.SetEntryPoint;
import analyses.AllAnalyses;
import analyses.Analysis;

/**
 * Maintains the Interpret Menu. 
 */
public class InterpretMenu extends MenuManager
{
  /** Title of this menu.  The Title is displayed on the top menu bar. */
  private static final String TITLE = "Interpreter";

  /**
   * Creates this. Initializes menu with title and actions. 
 * @throws IOException 
   */
  public InterpretMenu()
  {
    super(TITLE);
    
    for(Analysis analysis : AllAnalyses.get())
    {
      add(new RunAnalysisAction(analysis.getTitle(), analysis));
    }
    /*
    add(new InterpretAction());
    add(new InterpretSelectionAction());
    add(new BlockLiveRegisterAnalysisAction());
    */
    add(new Separator());
    add(new JumpToEntryPoint());
    add(new SetEntryPoint());
  }
}

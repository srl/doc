/*
 * ValidCallRetSitesView.java
 * Created on May 12, 2005
 */
package ui.markerViewers;

import org.eclipse.swt.widgets.Composite;



/**
 * Displays a list of all valid call-return sites.
 */
public class ValidCallRetSitesView extends MarkerView
{
    public void createPartControl(Composite parent)
    {
        setMarkerFilter(MarkerIds.CALLRETSITE_MARKER);
        super.createPartControl(parent);
    }
}

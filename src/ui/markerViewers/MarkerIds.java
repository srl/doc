/*
 * MarkerIds.java
 * Created on Apr 21, 2005
 */
package ui.markerViewers;

/**
 * Contains constants for each marker id. These constants
 * are defined in <code>plugin.xml</code>
 */
public class MarkerIds
{
    public final static String CALLOBFUSCATION_MARKER		= "detectObfuscation.callObfuscationMarker";
    public final static String CALLRETSITE_MARKER 			= "detectObfuscation.validCallRetMarker";
    public final static String ENTRY_POINT_MARKER				= "detectObfuscation.entrypointMarker";
    public final static String BREAK_POINT_MARKER				= "detectObfuscation.breakpointMarker";
    public final static String NEXT_INSTRUCTION_MARKER	= "detectObfuscation.nextInstructionMarker";
    public final static String RETOBFUSCATION_MARKER		= "detectObfuscation.retObfuscationMarker";
    public final static String TARGET_MARKER 						=	"detectObfuscation.targetMarker";
    public final static String INTERPRETED_INSTRUCTION_MARKER	= 
    	"detectObfuscation.interpretedInstructionMarker";
	public static final String CURRENT_INSTRUCTION_MARKER = "detectObfuscation.currentInstructionMarker";
}

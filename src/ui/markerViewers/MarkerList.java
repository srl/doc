/*
 * MarkerDeltaList.java
 * Created on May 12, 2005
 */
package ui.markerViewers;

import org.eclipse.core.resources.IMarker;

/**
 * Maintains a list of IMarkers.  Intended for use with
 * MarkerContentProvider.
 */
public class MarkerList
{
    private IMarker[] markers;
    
    public MarkerList(IMarker markers[])
    {
        this.markers = markers;
    }
    
    public IMarker[] getMarkers()
    {
        return markers;
    }
}

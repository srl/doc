/*
 * ObfuscatedReturnsView.java
 * Created on May 12, 2005
 */
package ui.markerViewers;

import org.eclipse.swt.widgets.Composite;



/**
 * Displays a list of all obfuscated returns.
 */
public class ObfuscatedReturnsView extends MarkerView
{
    public void createPartControl(Composite parent)
    {
        setMarkerFilter(MarkerIds.RETOBFUSCATION_MARKER);
        super.createPartControl(parent);
    }
}

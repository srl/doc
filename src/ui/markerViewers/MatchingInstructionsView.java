/*
 * ValidCallRetSitesView.java
 * Created on May 12, 2005
 */
package ui.markerViewers;

import org.eclipse.swt.widgets.Composite;



/**
 * Displays all markers for associated instructions.
 */
public class MatchingInstructionsView extends MarkerView
{
    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    public void createPartControl(Composite parent)
    {
        setMarkerFilter(MarkerIds.TARGET_MARKER);
        super.createPartControl(parent);
    }
}

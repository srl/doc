/*
 * ObfuscatedCallsView.java
 * Created on May 12, 2005
 */
package ui.markerViewers;

import org.eclipse.swt.widgets.Composite;



/**
 * Displays a list of obfuscated calls.
 */
public class ObfuscatedCallsView extends MarkerView
{
    public void createPartControl(Composite parent)
    {
        setMarkerFilter(MarkerIds.CALLOBFUSCATION_MARKER);
        super.createPartControl(parent);
    }
}

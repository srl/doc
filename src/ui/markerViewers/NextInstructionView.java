package ui.markerViewers;

import org.eclipse.swt.widgets.Composite;

/**
 * View that displays Next markers. (markers that
 * indicate the next executed instruction).
 */
public class NextInstructionView extends MarkerView
{
    public void createPartControl(Composite parent)
    {
        setMarkerFilter(MarkerIds.NEXT_INSTRUCTION_MARKER);
        super.createPartControl(parent);
    }
}

/*
 * MarkerFields.java
 * Created on May 12, 2005
 */
package ui.markerViewers;

import org.eclipse.core.resources.IMarker;

/**
 * Contains constants for each marker field.  These fields are
 * defined in <code>plugin.xml</code>
 */
public class MarkerFields
{
    public static final String LINE_NUMBER = IMarker.LINE_NUMBER;
    public static final String INSTRUCTION = "instruction";
}

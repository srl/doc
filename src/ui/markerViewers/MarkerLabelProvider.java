/*
 * MarkerLabelProvider.java
 * Created on May 12, 2005
 */
package ui.markerViewers;



import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;

import x86InstructionSet.Instruction;




/**
 * Provides labels for a table viewer.  Intended for use
 * by the MarkerView.
 */
public class MarkerLabelProvider implements ITableLabelProvider
{
    final static int COLUMN_LINE_NUMBER = 0;
    final static int COLUMN_ADDRESS		= 1;
    final static int COLUMN_INSTRUCTION = 2;

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnImage(java.lang.Object, int)
     */
    public Image getColumnImage(Object element, int columnIndex)
    {
        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ITableLabelProvider#getColumnText(java.lang.Object, int)
     */
    public String getColumnText(Object element, int columnIndex)
    {
        if(element instanceof IMarker)
        {
            IMarker marker = (IMarker)element;
            
            switch(columnIndex)
            {
            case COLUMN_LINE_NUMBER:
                return Integer.toString( marker.getAttribute(IMarker.LINE_NUMBER, 0) + 1 );
                
            case COLUMN_ADDRESS:
            	try
				{
            		Instruction instruction = (Instruction)marker.getAttribute(MarkerFields.INSTRUCTION);
            		return instruction.getAddress().toString();
				}
            	catch(CoreException e)
				{
            		return "CoreException. Address could not be retrieved.";
				}

            case COLUMN_INSTRUCTION:
            	try
				{
            		Instruction instruction = (Instruction)marker.getAttribute(MarkerFields.INSTRUCTION);
            		return instruction.toString();
				}
            	catch(CoreException e)
				{
            		return "CoreException. Instruction could not be retrieved.";
				}
                
            default:
                throw new Error("This line should never execute.");
            }
        }

        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
     */
    public void addListener(ILabelProviderListener listener)
    {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
     */
    public void dispose()
    {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
     */
    public boolean isLabelProperty(Object element, String property)
    {
        return false;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
     */
    public void removeListener(ILabelProviderListener listener)
    {
    }

}

/*
 * MarkerView.java
 * Created on May 12, 2005
 */
package ui.markerViewers;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.actions.SelectionProviderAction;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.views.markers.internal.ActionOpenMarker;


/**
 * Displays a list of markers (similar to the BookmarkView, ProblemView, etc. in eclipse)
 */
public class MarkerView extends ViewPart implements ISelectionChangedListener, IResourceChangeListener
{
	private SelectionProviderAction openAction;
    private TableViewer				viewer;
    private	String					markerFilter;

 
    
    /**
     * Creates the actions that this MarkerView may invoke.
     */
	private void createActions()
	{
		openAction = new ActionOpenMarker(this, viewer); 
	}

    /**
     * Initializes this.  
     * Note to subclasses: Call setMarkerFilter before calling
     * createPartControl!
     */
    public void createPartControl(Composite parent)
    {
    	
        viewer = new TableViewer(parent, SWT.READ_ONLY | SWT.FULL_SELECTION);
        
        Table table = viewer.getTable();
        
        table.setHeaderVisible(true);
        table.setLinesVisible(true);

        TableColumn column = new TableColumn(table, 0);
        column.setText("Line");
        column.setWidth(40);
        
        column = new TableColumn(table, 0);
        column.setText("Address");
        column.setWidth(80);

        column = new TableColumn(table, 0);
        column.setText("Instruction");
        column.setWidth(400);        
        
        viewer.setContentProvider(new MarkerContentProvider());
        viewer.setLabelProvider(new MarkerLabelProvider());
        viewer.addSelectionChangedListener(this);
        
        ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
        createActions();
        refresh();
    }
    
    /**
     * Disposes of this.
     */
    public void dispose()
    {
        openAction.dispose();
    }
    
    /**
     * Updates the displayed markers.
     * Called by the Eclipse framework when a marker is added/removed
     * to a resourced (and called for other modifications as well).
     */
	public void resourceChanged(IResourceChangeEvent event) 
	{
	    // Before refreshing, make sure that we have a filter.
	    if(markerFilter != null)
	        refresh();
	}
    
	/**
	 * Refreshes the list of markers.
	 */
    public void refresh()
    {
		try
		{
			IMarker[] markers = 
			    ResourcesPlugin.getWorkspace().getRoot().findMarkers(markerFilter, true, 
			        IResource.DEPTH_INFINITE);
			viewer.setInput(new MarkerList(markers));			
		}
		catch(CoreException e)
		{
		    viewer.setInput(new IMarker[] {});
		}
    }

    /**
     * Highlights the selected instruction in the main editor window..  
     */
	public void selectionChanged(SelectionChangedEvent selection) 
	{
	    openAction.run();
	}

	/**
	 * sets focus to this control
	 */
    public void setFocus()
    {
    }

    /**
     * Informs this view of which marker should be displayed.  All
     * children of this marker will also be displayed.  This function
     * is intended to be called by any subclasses. If it is not called,
     * no markers will be displayed.
     * @parem filter String id of the marker to be displayed.
     */
    protected void setMarkerFilter(String filter)
    {
        markerFilter = filter;
    }
}

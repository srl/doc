/*
 * MessageView.java
 * Created on May 11, 2005
 */
package ui.messageViewer;


import org.eclipse.jface.text.Document;
import org.eclipse.jface.text.TextViewer;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.swt.SWT;

/**
 * Provides an on-screen area for informative/error messages intended for the user.
 * This class may not be around for much longer; users should use logger.Log for
 * reported messages instead.
 * @deprecated
 */
public class MessageView extends ViewPart
{
    private static Document document = new Document();
    private TextViewer viewer;
    
    /**
     * Adds a message after the previously added message (if any).
     */
    public static void addMessage(String msg)
    {
        document.set(document.get() + msg + "\r");
    }
    
    /**
     * Erases all messages from the display.
     *
     */
    public static void clear()
    {
        document.set("");
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
     */
    public void createPartControl(Composite parent)
    {
        viewer = new TextViewer(parent, SWT.V_SCROLL | SWT.READ_ONLY);
        viewer.setDocument(document);
    }

    /* (non-Javadoc)
     * @see org.eclipse.ui.IWorkbenchPart#setFocus()
     */
    public void setFocus()
    {
    }
}

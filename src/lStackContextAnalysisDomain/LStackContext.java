package lStackContextAnalysisDomain;

/**
 * @author Davidson Boccardo 
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.Vector;

import csAnalysisDomain.csAnalysisDomain;

import obfuscations.CallObfuscation;

import logger.Log;
import vsaAsgDomain.IReducedIntervalCongruence;
import vsaAsgDomain.IStackLocationSet;
import vsaAsgDomain.StackLocation;
import vsaAsgDomain.VsaAsgState;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.Instruction;
import x86InstructionSet.Operand;
import x86InstructionSet.RetInstruction;
import x86Interpreter.Interpreter;
import domain.Register;
import domain.Value;

/**
 * Maintains the state of registers and memory for each stackString.  The Context 
 * is capable of managing register values and values placed on the stack, however 
 * arbitrary memory locations, such as the heap, are not stored. (future work) 
 */

public class LStackContext extends csAnalysisDomain
{
	/**
	 * Creates a context with a initial stack string and an initial
	 * empty state.  The object created by this constructor is not initialized.
	 * Must call init() before using this object.  The reason for this is 
	 * because we must delay initialization until the interpreter has had a 
	 * chance to assign the Interpreter.activeState static variable.
	 */
	public LStackContext()
	{
	}
	
	 /**
     * Returns the addresses that are currently on the 
     * top of the stack associated with the current stack-string, i.e. the return address. 
     * Does not modify the stack pointer (esp). 
     */
	
	
	public AddressList getReturnAddressAsList()
    {
		AddressList 								result = new AddressList();
      	IReducedIntervalCongruence 					ric;
        IReducedIntervalCongruence.Iterator 		ricIter;
        IStackLocationSet.Iterator 					stackiter;
        IStackLocationSet 							stackTops;
        
        IStackLocationSet.Iterator 					stackiter2;
        stackTops = ((vsaAsgDomain.Value)getRegisterValue(Register.ESP)).getStackLocations();
        
       	for(stackiter = stackTops.iterator(); stackiter.hasNext(); )
            {
       		// Iterate over all the stack tops
       		// Find the one that corresponds to the last index in the stack string
       		// Use the values in the memory, and create return addresses.
            	        	
            	int trace = contextString.lastIndexOf("-");
            	String cont = contextString.substring(trace+1, contextString.length());
            	
//            	to treat cases where the context is added in the same instruction more than one time, i.e., sub esp, n
                if (cont.contains("|")) cont = cont.substring(0, cont.length()-2);
            	
            	int length = cont.length();
            	if (cont.endsWith("+")) cont = cont.substring(0, length-1);
            	// Added by Arun
            	if (cont.contains("T")) cont = "0";
            	int check = Integer.parseInt( cont );
       		 	
       		 	StackLocation temp2 = stackiter.next();
       		 	
       		 	if (temp2.getCreator() == null) continue;
       		 	
       		 	int temp = temp2.getCreator().getIndex()+1;
       		 	
       		 	if( temp == check ) {
       		 	    // found the stack top matching the stack string
       		 		ric = getMemoryTable().getValue( temp2.getMemoryArea() ).getRIC();
       		 
       		 		if(ric.size() <= 5)
       		 		{
       		 			ricIter = ric.iterator();
       		 			while(ricIter.hasNext())
       		 			{
       		 				long returnAddress = ricIter.next();
       		 				if(!result.has(new Address(returnAddress)))
       		 					result.add( new Address(returnAddress));
       		 			}
       		 		}
       		 		else
       		 		{
       		 			Log.write(Log.WARNING, "One of the stack tops contains more than 5 values. " +
       		 			" It will not be added to the list of return addresses.");
       		 		}
       		 	}
       		} 
                            
        return result;
    }
	
	 /**
     * Removes & returns the value at the top of the stack. and chop the last element
     * of the stack-string
     */
	
	public Value pop() {
		VsaAsgState state;
		Value value;
		int n = 1; // how many strings are chopped from the stack-string
		
		Operand numBytesOperand;

		Value numBytes;

		if ( Interpreter.getActiveInstruction() instanceof RetInstruction )
		{
			numBytesOperand = ((RetInstruction)Interpreter.getActiveInstruction()).getDstOperand();
			
			if (numBytesOperand == null) n = 1; else { //caso se the ret instruction is only 4 bytes
			
				numBytes = (Value) numBytesOperand.interpret(Interpreter.getActiveState());
				IReducedIntervalCongruence ric = ((vsaAsgDomain.Value)numBytes).getRIC();

				if( ric.isDefined())
				{
					assert ric.size() == 1;	
					n = (int) ric.getMinimumValue();
				}

				n = n/4 + 1; // 1 stack-location = 4 bytes
			}

		}
		
		String stackString = getContextString();
		
		if (!(stackString.endsWith("+"))) {
			
			popSuffix(n);
			state = (VsaAsgState) get(stackString);
			value = (Value) state.pop();
			updateAddMapping(getContextString(), state );
			assert map.containsKey( getContextString() );
			return value;
		
		} 	else { //check if POP instructions
			
			state = (VsaAsgState) get(stackString);
			value = (Value) state.pop();
			
			
			
			
	        return value;
			
		}
		
	}
    
	
	
	 /**
     * Pushes a value onto the stack & add the address to the stack-string
     */
	
	public void push(Value value) 
	{
		VsaAsgState state;
		Instruction instruction;
		StackLocation newStackLocation;	// stack location added by the push operation.	
						
		state = (VsaAsgState)get(getContextString()).clone();
		
		state.push(value);
		
		newStackLocation = getStackTop(state);
		
		instruction = newStackLocation.getCreator();
		
		pushSuffix(Integer.toString(instruction.getIndex()+1)); // getIndex() returns the line of stack-location-1
		updateAddMapping(getContextString(), state);
		
	}

	/**
     * Updates the stack-string appending some program-location (label) on it. 
     */

	public String pushSuffix(String id)
	{
		
		if (contextString.contains(id)){
			
			contextString = contextString.substring(0, contextString.indexOf(id));
			contextString += id + "+"; 
		}
		else {
			contextString += "-" + id; 
		}
		
		return contextString;
				
	}

	/**
     * Updates the stack-string removing n program-locations. This n is determined by
     * the number of bytes. 
     */
	
	public String popSuffix(int bytes){
		
		   
		int lenght = contextString.length(); 
						
		while ((bytes != 0) && (lenght > 1)){
			
			while (contextString != null) {
				
			contextString = contextString.substring(0, lenght-1);
			lenght--;
			if (contextString.endsWith("-")) break;
										
			}
				
			contextString = contextString.substring(0, lenght-1);
			lenght--;
			bytes--;
		}
		
		return contextString;
	}
	
	/**
     * Updates the stack-string removing n program-locations. This n is determined by
     * the number of bytes. 
     */
	
	public ArrayList<String> findContexts(){
		
		ArrayList<String> vector = new ArrayList<String>();
		IStackLocationSet.Iterator 					stackiter;
		IStackLocationSet.Iterator 					stackiter2;
        IStackLocationSet 							stackTops;
        StackLocation                               stackTop = null;
        int StackTopIndex;
        IStackLocationSet                           stackTopPredecessors;
        StackLocation								stackTopPredecessor;
       
        stackTops = ((vsaAsgDomain.Value)getRegisterValue(Register.ESP)).getStackLocations();
                                
        int trace = contextString.lastIndexOf("-");
    	String contTop = contextString.substring(trace+1, contextString.length()-1);
    	
//    	 to treat cases where the context is added in the same instruction more than one time, i.e., sub esp, n
        if (contTop.contains("|")) contTop = contTop.substring(0, contTop.length()-2);
        
		stackiter 				= stackTops.iterator();
		
		while (stackiter.hasNext())
		{
			stackTop 				= stackiter.next();
			if (stackTop.getCreator() == null) continue;
			int check               = stackTop.getCreator().getIndex()+1;
			if (check == Integer.parseInt(contTop)) break;
		}
			
		stackTopPredecessors 	= stackTop.getPredecessors();
		stackiter 				= stackTopPredecessors.iterator();
		
		while (stackiter.hasNext()){
			String stackStringTemp = contextString;
			String stackStringAccum = ""; 
	        String stackStringAccum2 = ""; 	
	        
	        stackTopPredecessor 	= stackiter.next();
	        if ((stackTopPredecessor.getCreator().getIndex()+1) < (stackTop.getCreator().getIndex()+1)) continue;
			
	        IStackLocationSet path	= getAbstractStackGraph().getNodesOnPathToB( stackTop, stackTopPredecessor );
	    	        
			for(stackiter2 = path.iterator(); stackiter2.hasNext(); )
	        {
				StackTopIndex = stackiter2.next().getCreator().getIndex()+1;
				if (stackStringTemp.contains(Integer.toString(StackTopIndex))) continue;
				stackStringAccum +=  StackTopIndex + "-";
						 
	        }
			
			while (stackStringAccum.length() > 0){
				if (stackStringAccum.endsWith("-")){
					stackStringAccum2 += stackStringAccum.substring(stackStringAccum.length()-1, stackStringAccum.length());
					stackStringAccum = stackStringAccum.substring(0, stackStringAccum.length()-1);
				}
				int locationOfTrace = stackStringAccum.lastIndexOf("-");
				stackStringAccum2 += stackStringAccum.substring(locationOfTrace+1, stackStringAccum.length());
				stackStringAccum = stackStringAccum.substring(0, locationOfTrace+1);
			}
						
			stackStringTemp += stackStringAccum2;
			if (!(vector.contains(stackStringTemp))) vector.add(stackStringTemp);
			
			stackStringTemp = contextString.substring(0, contextString.length()-1);
			stackStringAccum= "";
			stackStringAccum2= "";
			
			
			for(stackiter2 = path.iterator(); stackiter2.hasNext(); )
	        {
				StackTopIndex = stackiter2.next().getCreator().getIndex()+1;
				if (stackStringTemp.contains(Integer.toString(StackTopIndex))) continue;
				stackStringAccum +=  StackTopIndex + "-";
	        }
			
			while (stackStringAccum.length() > 0){
				if (stackStringAccum.endsWith("-")){
					stackStringAccum2 += stackStringAccum.substring(stackStringAccum.length()-1, stackStringAccum.length());
					stackStringAccum = stackStringAccum.substring(0, stackStringAccum.length()-1);
				}
				int locationOfTrace = stackStringAccum.lastIndexOf("-");
				stackStringAccum2 += stackStringAccum.substring(locationOfTrace+1, stackStringAccum.length());
				stackStringAccum = stackStringAccum.substring(0, locationOfTrace+1);
			}
			
			stackStringTemp += stackStringAccum2;
			if (!(vector.contains(stackStringTemp))) vector.add(stackStringTemp);
		}
		
		
		
		return vector;
		
	
	}
	
	// Pop a context from the l-context, and change ESP to point to the 
	// corresponding top of stack. The string has the syntax
	//   T[-tx]*, where, where tx has the form 'nn' or 'nn+' or 'nn|ll' or 'nn|ll+'
	public void changeTop() {
	
	    String 	contTop;
        System.out.println("changeTop - context String:<" + contextString + ">");
        int trace = contextString.lastIndexOf("-");
        
        if (contextString.endsWith("+")) contTop = contextString.substring(trace+1, contextString.length()-1);
        else contTop = contextString.substring(trace+1, contextString.length());
        
        // to treat cases where the context is added in the same instruction more than one time,
        // i.e., sub esp, n
        // BUGGY: the "length-2" accounts for stripping a single digit after "|". It fails if there are 
        // multiple digits. Use indexOf instead.
        if (contTop.contains("|")) contTop = contTop.substring(0, contTop.length()-2);
        
        // ARUN: Refactored code. Integer.parseInt was generating error
        // for tse-example.asm. contTop was "T"
        // The hack removes the parse error, but then leads to non-termination,
        // AFTER exiting the while loop.
        
		int contTopVal = -1;
		try {
			contTopVal = Integer.parseInt(contTop);
		} catch (NumberFormatException ex){
			System.out.println("Stack string not an integer "+ contTop + "\n"+ ex.getMessage());
		}
   		
	 	IStackLocationSet.Iterator 					stackiter;
	    IStackLocationSet 							stackTops;
        StackLocation                               stackTop = null;
        stackTops = ((vsaAsgDomain.Value)getRegisterValue(Register.ESP)).getStackLocations();
		stackiter 				= stackTops.iterator();
	
		// following ESP, find the next top of stack corresponding to the contTop
		while (stackiter.hasNext())
		{
			if (contTopVal == -1) { System.out.print("."); System.out.flush();}
			stackTop 				= stackiter.next();
			if (stackTop.getCreator() == null) continue;
			// Using index+1, could lead to error. May be better off having a function
			// getCreator().getID().
			int check               = stackTop.getCreator().getIndex()+1;
			if (check == contTopVal) break;
		}
		if (contTopVal == -1) System.out.println("+");
		// what if there is no next top of stack corresponding to contTop
		// happens ESP is pointing to bottom, such as when stack is 'over popped'.
		// then stackTop is null (and which may be causing issues).
		Value value = new vsaAsgDomain.Value(stackTop);
		setRegisterValue(Register.ESP, value) ;
		
	}
	
}

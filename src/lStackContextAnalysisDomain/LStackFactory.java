package lStackContextAnalysisDomain;

import vsaAsgDomain.ReducedIntervalCongruence;
import domain.DomainFactory;
import domain.State;
import domain.Value;

public class LStackFactory extends DomainFactory {

	//	 The on and only instance of this.
	static private DomainFactory instance = null;
	
	private LStackFactory() {};
	
	
	@Override
	public State createState() 
	{
		return new LStackContext();
	}

	@Override
	public Value createValue() {
		return new vsaAsgDomain.Value(); 
	}

	@Override
	public Value createValue(int init) {
		return new vsaAsgDomain.Value(init);
	}

	@Override
	public Value createValue(int start, int end) {
		vsaAsgDomain.Value result = new vsaAsgDomain.Value(
				new ReducedIntervalCongruence(1, start, end, 0) );
		return result;
	}
	
	/**
	 * Returns the one and only instance of VsaAsgDomainFactory
	 */
	static public DomainFactory getInstance()
	{
		if(instance == null)
			instance = new LStackFactory();
		return instance;
	}

}

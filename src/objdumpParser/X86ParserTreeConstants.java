package objdumpParser;  
/* Generated By:JJTree: Do not edit this line. .\X86ParserTreeConstants.java */

public interface X86ParserTreeConstants
{
  public int JJTVOID = 0;
  public int JJTPROGRAM = 1;
  public int JJTADCINSTRUCTION = 2;
  public int JJTADDINSTRUCTION = 3;
  public int JJTANDINSTRUCTION = 4;
  public int JJTCALLINSTRUCTION = 5;
  public int JJTCDQINSTRUCTION = 6;
  public int JJTCLDINSTRUCTION = 7;
  public int JJTCMPINSTRUCTION = 8;
  public int JJTCWDINSTRUCTION = 9;
  public int JJTDECINSTRUCTION = 10;
  public int JJTDIVINSTRUCTION = 11;
  public int JJTENTERINSTRUCTION = 12;
  public int JJTHLTINSTRUCTION = 13;
  public int JJTIDIVINSTRUCTION = 14;
  public int JJTIMULINSTRUCTION_1OP = 15;
  public int JJTIMULINSTRUCTION_2OP = 16;
  public int JJTIMULINSTRUCTION_3OP = 17;
  public int JJTININSTRUCTION = 18;
  public int JJTINCINSTRUCTION = 19;
  public int JJTJAEINSTRUCTION = 20;
  public int JJTJAJNBEINSTRUCTION = 21;
  public int JJTJBEINSTRUCTION = 22;
  public int JJTJECXZINSTRUCTION = 23;
  public int JJTJBINSTRUCTION = 24;
  public int JJTJEINSTRUCTION = 25;
  public int JJTJGEINSTRUCTION = 26;
  public int JJTJGINSTRUCTION = 27;
  public int JJTJLEINSTRUCTION = 28;
  public int JJTJLINSTRUCTION = 29;
  public int JJTJMPINSTRUCTION = 30;
  public int JJTJNBINSTRUCTION = 31;
  public int JJTJNEINSTRUCTION = 32;
  public int JJTJNSINSTRUCTION = 33;
  public int JJTJNZINSTRUCTION = 34;
  public int JJTJSINSTRUCTION = 35;
  public int JJTLEAINSTRUCTION = 36;
  public int JJTLEAVEINSTRUCTION = 37;
  public int JJTLOCKINSTRUCTION = 38;
  public int JJTLODSINSTRUCTION = 39;
  public int JJTLOOPDINSTRUCTION = 40;
  public int JJTMOVINSTRUCTION = 41;
  public int JJTMOVSINSTRUCTION = 42;
  public int JJTMOVSXINSTRUCTION = 43;
  public int JJTMOVZXINSTRUCTION = 44;
  public int JJTMULINSTRUCTION = 45;
  public int JJTNEGINSTRUCTION = 46;
  public int JJTNOPINSTRUCTION = 47;
  public int JJTNOTINSTRUCTION = 48;
  public int JJTORINSTRUCTION = 49;
  public int JJTPOPINSTRUCTION = 50;
  public int JJTPUSHFDINSTRUCTION = 51;
  public int JJTPUSHINSTRUCTION = 52;
  public int JJTREPMOVSINSTRUCTION = 53;
  public int JJTREPZMOVSINSTRUCTION = 54;
  public int JJTREPZSTOSINSTRUCTION = 55;
  public int JJTREPNESCASINSTRUCTION = 56;
  public int JJTREPSTOSINSTRUCTION = 57;
  public int JJTRETINSTRUCTION = 58;
  public int JJTROLINSTRUCTION = 59;
  public int JJTRORINSTRUCTION = 60;
  public int JJTSARINSTRUCTION = 61;
  public int JJTSBBINSTRUCTION = 62;
  public int JJTSETAESETNBINSTRUCTION = 63;
  public int JJTSETBSETNAEINSTRUCTION = 64;
  public int JJTSETBESETNAINSTRUCTION = 65;
  public int JJTSETESETZINSTRUCTION = 66;
  public int JJTSETNESETNZINSTRUCTION = 67;
  public int JJTSETLSETNGEINSTRUCTION = 68;
  public int JJTSETGESETNLINSTRUCTION = 69;
  public int JJTSETLESETNGINSTRUCTION = 70;
  public int JJTSETGSETNLEINSTRUCTION = 71;
  public int JJTSHLINSTRUCTION = 72;
  public int JJTSHRINSTRUCTION = 73;
  public int JJTSTDINSTRUCTION = 74;
  public int JJTSTOSINSTRUCTION = 75;
  public int JJTSUBINSTRUCTION = 76;
  public int JJTTESTINSTRUCTION = 77;
  public int JJTUNDEFINSTRUCTION = 78;
  public int JJTXCHGINSTRUCTION = 79;
  public int JJTXORINSTRUCTION = 80;
  public int JJTREGISTER32OPERAND = 81;
  public int JJTREGISTER16OPERAND = 82;
  public int JJTREGISTER8OPERAND = 83;
  public int JJTSEGMENTREGISTEROPERAND = 84;
  public int JJTADDRESSOPERAND = 85;
  public int JJTIMMEDIATEOPERAND = 86;
  public int JJTLEAOPERAND = 87;
  public int JJTMEMORY32OPERAND = 88;
  public int JJTMEMORY16OPERAND = 89;
  public int JJTMEMORY8OPERAND = 90;
  public int JJTINDIRECTOPERAND = 91;
  public int JJTEXPRESSIONOPERAND = 92;
  public int JJTEXTERNALFUNCTIONOPERAND = 93;


  public String[] jjtNodeName = {
    "void",
    "Program",
    "AdcInstruction",
    "AddInstruction",
    "AndInstruction",
    "CallInstruction",
    "CdqInstruction",
    "CldInstruction",
    "CmpInstruction",
    "CwdInstruction",
    "DecInstruction",
    "DivInstruction",
    "EnterInstruction",
    "HltInstruction",
    "IdivInstruction",
    "ImulInstruction_1op",
    "ImulInstruction_2op",
    "ImulInstruction_3op",
    "InInstruction",
    "IncInstruction",
    "JaeInstruction",
    "JaJnbeInstruction",
    "JbeInstruction",
    "JecxzInstruction",
    "JbInstruction",
    "JeInstruction",
    "JgeInstruction",
    "JgInstruction",
    "JleInstruction",
    "JlInstruction",
    "JmpInstruction",
    "JnbInstruction",
    "JneInstruction",
    "JnsInstruction",
    "JnzInstruction",
    "JsInstruction",
    "LeaInstruction",
    "LeaveInstruction",
    "LockInstruction",
    "LodsInstruction",
    "LoopdInstruction",
    "MovInstruction",
    "MovsInstruction",
    "MovsxInstruction",
    "MovzxInstruction",
    "MulInstruction",
    "NegInstruction",
    "NopInstruction",
    "NotInstruction",
    "OrInstruction",
    "PopInstruction",
    "PushfdInstruction",
    "PushInstruction",
    "RepMovsInstruction",
    "RepzMovsInstruction",
    "RepzStosInstruction",
    "RepneScasInstruction",
    "RepStosInstruction",
    "RetInstruction",
    "RolInstruction",
    "RorInstruction",
    "SarInstruction",
    "SbbInstruction",
    "SetaeSetnbInstruction",
    "SetbSetnaeInstruction",
    "SetbeSetnaInstruction",
    "SeteSetzInstruction",
    "SetneSetnzInstruction",
    "SetlSetngeInstruction",
    "SetgeSetnlInstruction",
    "SetleSetngInstruction",
    "SetgSetnleInstruction",
    "ShlInstruction",
    "ShrInstruction",
    "StdInstruction",
    "StosInstruction",
    "SubInstruction",
    "TestInstruction",
    "UndefInstruction",
    "XchgInstruction",
    "XorInstruction",
    "Register32Operand",
    "Register16Operand",
    "Register8Operand",
    "SegmentRegisterOperand",
    "AddressOperand",
    "ImmediateOperand",
    "LeaOperand",
    "Memory32Operand",
    "Memory16Operand",
    "Memory8Operand",
    "IndirectOperand",
    "ExpressionOperand",
    "ExternalFunctionOperand",
  };
}

/*
 * Created on May 17, 2004
 */

package objdumpParser;

/*******************************************************************************
 * 
 * Copyright (C) 2004, 2005, University of Louisiana at Lafayette
 * Author: Eric Uday Kumar 
 * 
 *******************************************************************************/

import x86InstructionSet.Address;
import x86InstructionSet.Program;
import x86InstructionSet.UnknownAddress;
import domain.State;

/**
 * Defines an inteface that all nodes in the AST that represent instructions
 * inherit from.  
 */
public abstract class Instruction extends SimpleNode implements x86InstructionSet.Instruction
{
  /** 
   * Address of this instruction.  This field is made public ONLY for use by the parser.  jjTree
   * requires access to internal fields of classes instantiated by it.  Users of this class
   * should not accesss this field directory, instead use the <code>getAddress()</code> function. 
   */
	public	Address	address = UnknownAddress.UNKNOWN;
  
	private		State		incomingState;
	private 	State   outgoingState;
  
  /** Stores state obtained from previously performed analysis. */
  private State prevAnalysisBeginState  = null;
  private State prevAnalysisEndState    = null;
  
	private		Program	program;	// the program that this instruction belongs to.
	
	/**
	 * desired width of the opcode column. used by subclasses for printing.
   * TODO: Remove
	 */
	protected	final static int OPCODE_WIDTH = 6; 

	/**
	 * Creates this
	 */
	public Instruction(int id)
	{
		super(id);
	}
	
	/**
	 * Creates this
	 */
	public Instruction(X86Parser p, int id) 
	{
		super(p, id);
	}
	
	/**
	 * @see Comparable#compareTo	 
	 */
	public int compareTo(x86InstructionSet.Instruction instruction)
	{
    return this.address.compareTo( instruction.getAddress() );
    /*
    if( !(instruction instanceof Instruction) )
      throw new IllegalArgumentException();
    
    if(this.id < ((unmorphParser.Instruction)instruction).id)
      return -1;
    else if(this.id > ((unmorphParser.Instruction)instruction).id)
      return 1;
    else
      return 0;*/
	}
	 
	/**
	 * @see Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj)
	{
		return address.equals( ((Instruction)obj).address ); 
	}
	
    /**
     * Returns the address of this instruction.
     */
	public Address getAddress()
	{
		return address;
	}
	
	/*
	 * Returns the state associated with the incoming edge of this instruction.
	 */
	public State getBeginState()
	{
		return incomingState;
	}

	/**
	 * Returns the zero-based index of this instruction. 
	 */
	public int getIndex()
	{
		return program.getInstructionIndex(getAddress());
	}
	
	/**
	 * Returns the state associated with the outgoing edge of this instruction.
	 */
	public State getEndState()
	{
		return outgoingState;
	}

  /* (non-Javadoc)
   * @see x86InstructionSet.Instruction#getPrevAnalysisBeginState()
   */
  public State getPrevAnalysisBeginState()
  {
    return prevAnalysisBeginState;
  }
  
  /* (non-Javadoc)
   * @see x86InstructionSet.Instruction#getPrevAnalysisEndState()
   */
  public State getPrevAnalysisEndState()
  {
    return prevAnalysisEndState;
  }
  
	/**
	 * Associates a state with the incoming edge of this instruction.
	 */
	public void setBeginState(State state)
	{
		incomingState = state;
	}
	
	/**
	 * Associates a state with the outgoing edge of this instruction.
	 */
	public void setEndState(State state)
	{
		outgoingState = state;
	}
	
  /* (non-Javadoc)
   * @see x86InstructionSet.Instruction#setPrevAnalysisBeginState(domain.State)
   */
  public void setPrevAnalysisBeginState(State state)
  {
    prevAnalysisBeginState = state;
  }
  
  /* (non-Javadoc)
   * @see x86InstructionSet.Instruction#setPrevAnalysisEndState(domain.State)
   */
  public void setPrevAnalysisEndState(State state)
  {
    prevAnalysisEndState = state;
  }
  
	/**
	 * Specifies the program that contains this instruction. 
	 */
	public void setProgram(Program program)
	{
		this.program = program;
	}
}
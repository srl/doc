package objdumpParser;  

import java.util.Map;

import instructionInterpreters.InstructionInterpreter;
import ui.outputters.Outputter;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.JneInstruction;
import x86InstructionSet.Operand;
import x86Interpreter.Interpreter;
import domain.State;
import domain.Value;

/* Generated By:JJTree: Do not edit this line. ASTJneInstruction.java */

/**
 *
 */
public class ASTJneInstruction extends Instruction implements JneInstruction
{
  /**
   * @param id
   */
  public ASTJneInstruction(int id) 
  {
    super(id);
  }

  /**
   * @param p
   * @param id
   */
  public ASTJneInstruction(X86Parser p, int id) 
  {
    super(p, id);
  }

  /* (non-Javadoc)
   * @see ollydbgParser.JnbInstruction#getNextQuickly(ollydbgParser.Address)
   */
  public AddressList getNextQuickly(Address nextAddress)
  {
    Value.Iterator  iter;
    Value           jumpTarget; 
    AddressList     result = new AddressList(nextAddress);
    
    if(jjtGetChild(0) instanceof ASTAddressOperand)
    {
      jumpTarget = ((Operand)jjtGetChild(0)).interpret(
          Interpreter.getActiveAnalysis().createDomainFactory().createState() );
      for(iter = jumpTarget.iterator(); iter.hasNext(); )
        result.add(new Address(iter.next()));
    }
    return result;
  }

  /* (non-Javadoc)
   * @see ollydbgParser.JnbInstruction#getTargetOperand()
   */
  public Operand getTargetOperand()
  {
    return (Operand)jjtGetChild(0);
  }
  
  /* (non-Javadoc)
   * @see ollydbgParser.JnbInstruction#interpret(domain.State, ollydbgParser.Address)
   */
  public Map<Address, State> interpret(InstructionInterpreter interpreter, 
      State state, Address nextAddress)
  {
    return interpreter.interpret(this, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return Outputter.getInstance().toString(this);
  }  
}

package objdumpParser;  

import java.util.Map;

import instructionInterpreters.InstructionInterpreter;
import ui.outputters.Outputter;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import domain.State;

/* Generated By:JJTree: Do not edit this line. ASTLeaveInstruction.java */

public class ASTLeaveInstruction  extends Instruction implements x86InstructionSet.LeaveInstruction 
{
  public ASTLeaveInstruction(int id) {
    super(id);
  }

  public ASTLeaveInstruction(X86Parser p, int id) {
    super(p, id);
  }

  /* (non-Javadoc)
   * @see x86InstructionSet.Instruction#getNextQuickly(x86InstructionSet.InstructionAddress)
   */
  public AddressList getNextQuickly(Address nextAddress) 
  {
    return new AddressList(nextAddress);
  }

  /* (non-Javadoc)
   * @see ollydbgParser.LeaveInstruction#interpret(domain.State, ollydbgParser.Address)
   */
  public Map<Address, State> interpret(InstructionInterpreter interpreter, 
      State state, Address nextInstruction)
  {
    return interpreter.interpret(this, state, nextInstruction);
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  /* (non-Javadoc)
   * @see ollydbgParser.LeaveInstruction#toString()
   */
  public String toString()
  {
    return Outputter.getInstance().toString(this);
  }
}

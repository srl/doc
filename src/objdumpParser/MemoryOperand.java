/*
 * MemoryOperand.java
 * Created on May 24, 2005
 */
package objdumpParser;

import x86InstructionSet.Operand;
import domain.State;

/**
 * Interface for nodes in the syntax tree that are intended
 * to hold memory operands.
 */
public interface MemoryOperand extends Operand
{
    /**
     * Returns the address placed in this operand, but doesn't
     * dereference the address.  In other words, if this operand is the target
     * of a MOV instruction, then the value returned is the address to be
     * MOVed to.
     */
    public domain.Value getTarget(State state);
}

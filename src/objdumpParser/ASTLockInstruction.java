package objdumpParser;  

import java.util.Map;

import instructionInterpreters.InstructionInterpreter;
import ui.outputters.Outputter;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import domain.State;

/* Generated By:JJTree: Do not edit this line. ASTLockInstruction.java */

public class ASTLockInstruction extends Instruction implements x86InstructionSet.LockInstruction 
{
  public ASTLockInstruction(int id) {
    super(id);
  }

  public ASTLockInstruction(X86Parser p, int id) {
    super(p, id);
  }

  /* (non-Javadoc)
   * @see x86InstructionSet.Instruction#getNextQuickly(x86InstructionSet.InstructionAddress)
   */
  public AddressList getNextQuickly(Address nextAddress) 
  {
    return new AddressList(nextAddress);
  }

  /* (non-Javadoc)
   * @see ollydbgParser.LockInstruction#getChildInstruction()
   */
  public Instruction getChildInstruction()
  {
    return (Instruction)jjtGetChild(0); 
  }
  
  /* (non-Javadoc)
   * @see x86Parser.Instruction#interpret(domain.State, x86Parser.Address)
   */
  public Map<Address, State> interpret(InstructionInterpreter interpreter, 
      State state, Address nextAddress)
  {
    return getChildInstruction().interpret(interpreter, state, nextAddress);
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  /* (non-Javadoc)
   * @see ollydbgParser.LockInstruction#toString()
   */
  public String toString()
  {
    return Outputter.getInstance().toString(this);
  }
}

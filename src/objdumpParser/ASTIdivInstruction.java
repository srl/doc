package objdumpParser;  

import java.util.Map;

import instructionInterpreters.InstructionInterpreter;
import ui.outputters.Outputter;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.Operand;
import domain.State;

/* Generated By:JJTree: Do not edit this line. ASTIdivInstruction.java */

/**
 * 
 */
public class ASTIdivInstruction  extends Instruction implements x86InstructionSet.IdivInstruction
{
  /**
   * @param id
   */
  public ASTIdivInstruction(int id) {
    super(id);
  }

  /**
   * @param p
   * @param id
   */
  public ASTIdivInstruction(X86Parser p, int id) {
    super(p, id);
  }

  /* (non-Javadoc)
   * @see x86InstructionSet.Instruction#getNextQuickly(x86InstructionSet.InstructionAddress)
   */
  public AddressList getNextQuickly(Address nextAddress) 
  {
    return new AddressList(nextAddress);
  }

  /* (non-Javadoc)
   * @see ollydbgParser.IdivInstruction#getDstOperand()
   */
  public Operand getDstOperand()
  {
    return (Operand)jjtGetChild(0);
  }
  
  /* (non-Javadoc)
   * @see x86InstructionSet.Instruction#interpret(instructionInterpreters.InstructionInterpreter, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(InstructionInterpreter interpreter, 
      State state, Address nextInstruction)
  {
    return interpreter.interpret(this, state, nextInstruction);
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return Outputter.getInstance().toString(this);
  }
}

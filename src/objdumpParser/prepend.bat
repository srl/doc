@echo off
rem Prepends the input file with the input text.
rem Will not prepend if the file alrady contains the text.
rem param 1 - input file
rem param 2 - input text

rem Make sure the input text is not already in the file.
find %2 < %1 > nul
if errorlevel 1 goto DoPrepend
goto End

:DoPrepend
echo %~2 > prepend.txt 
copy prepend.txt+%1 tmp.txt /Y > nul
copy tmp.txt %1 /Y > nul

del prepend.txt
del tmp.txt	

:End
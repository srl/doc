/*
 * StringUtils.java
 * Created on Apr 19, 2005
 */
package objdumpParser;

/**
 * Provides various utility functions for operating on
 * Strings.
 */
public class StringUtils
{
  /**
   * Returns a string consisting of the original <code>input</code>
   * string duplicated <code>count</code> times.
   * @param original the string to be duplicated.
   * @param count number of times to duplicate the string.
   * @return the input string duplicate <code>count</code> times.
   */
  public static String duplicate(String original, int count)
  {
    StringBuffer result = new StringBuffer();
    for(int i = 0; i < count; i++)
      result.append(original);
    return result.toString();
  }
  
    /**
     * Pads the right side of input string with space. 
     * @param original input string to be padded.
     * @param width desired with of the output string.
     * @return The original string with 
     * 	(width - original.length()) spaces added. 
     */
    public static String padRight(final String original, int width)
    {
        StringBuffer 	result 		 = new StringBuffer(original);
        int				spacesNeeded = width - original.length();
        for(int i = 0; i < spacesNeeded; i++)
            result.append(" ");
        return result.toString();
    }
}

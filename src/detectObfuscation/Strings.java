/*
 * Messages.java
 * Created on May 11, 2005
 */
package detectObfuscation;

/**
 * Contains various String constants for use in the user interface and other
 * parts of the program (instead of hard-coding the strings).
 */
public class Strings
{
  /** 
   * Returns user-interface message to be displayed when
   * a file cannot be found.
   */
  static public String getFileNotFoundMsg(String filename)
  { return "The file '" + filename + "' could not be found."; }
    
  /**
   * Returns user-interface message to be displayed when
   * an error occurs during parsing.
   */
  static public String getParseErrorMsg(String parseError)
 	{ return parseError; }
  
  /**
   * Returns a short version of the user-interface message to be
   * displayed when an error occurs during parsing.
   */
  static public String getShortParseErrorMsg(String parseError)
  { return "An error occurred during parsing.  See Messages view for more information."; }
    
  /** Message to be displayed when no entry point exists in the program */
  static final public String NO_ENTRY_POINT 	= "The selected program does not have an entry point.  Interpretation can not be performed.";
  
  /** The DetectObfuscation plugin id. */
  static final public String PLUGIN_ID     	  = "edu.louisiana.cacs.DetectObfuscation";

  /**
   * Returns message to be displayed when an unexpected error occurs
   * while parsing.
   */
  static public String getUnexpectedParseErrorMsg(String errMsg)
  { return "An unexpected error has occurred while parsing the input program." + "\r" + errMsg; }
  
  /**
   * Returns a shorter version of the message to be displayed when
   * an unexpected error occurs while parsing.
   */
  static public String getShortUnexpectedParseErrorMsg(String errMsg)
  { return "An unexpected error has occurred while parsing the input program.  See Messages view for more information."; }
    
  /**
   * Returns the message to be displayed when an unexpected error
   * occurs during interpretation.
   */
  static public String getUnexpectedInterpretationErrorMsg(String errMsg)
  { return "An unexpected error has occurred while interpreting the input program." + "\r" + errMsg; }
  
  /**
   * Returns a shorter version of the message to be displayed when an 
   * unexpected error occurs during interpretation.
   */
  static public String getShortUnexpectedInterpretationErrorMsg(String errMsg)
  { return "An unexpected error has occurred while interpreting the input program.  See Messages view for more information."; }
    
  /**
   * Return the message to be displayed when an unexpected error occurs.
   */
  static public String getUnexpectedErrorMsg(String errMsg)
  { return "An unexpected error has occurred." + "\r" + errMsg; }
  
  /**
   * Returns a short version of the message to be displayed when
   * an unexpected error occurs.
   */
  static public String getShortUnexpectedErrorMsg(String errMsg)
  { return "An unexpected error has occurred.  See Messages view for more inforamtion."; }

  /**
   * Returns the message to be displayed when the user cancels the
   * interpretation operation.
   */
  static public String getUserCancelledInterpretationMsg()
  {
    return "User cancelled the interpretation operation. Displayed results are only partial.";
  }
}

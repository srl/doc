package detectObfuscation;

import analyses.Analysis;
import ui.registers.RegisterLabelProvider;
import x86InstructionSet.Program;

public abstract class OutputVisitor {
	
	public abstract void setLabel(String label);

	public abstract void stackGraphContent(Object input);

	public abstract void registerContent(Object input);

	public abstract void beforeRegisterContents();

	public abstract void beforeStackGraph();

	public abstract void addRegister(RegisterLabelProvider rlp, Object regElement);

	public abstract void controlFlow(Program program);

	public abstract void addRegContents();

	public abstract void obfuscations(Analysis analysis, Program program);

	public abstract void printResults();

	public abstract void flush();
	
}

package detectObfuscation;

import obfuscations.ObfuscationAnalyzer;
import obfuscations.ObfuscationVisitor;
import obfuscations.TextObfuscationVisitor;
import obfuscations.XMLObfuscationVisitor;
import analyses.Analysis;
import analyzer.AnalysisResult;
import ui.registers.RegisterLabelProvider;
import x86InstructionSet.Program;

public class TextOutputVisitor extends OutputVisitor {
	String registerDelay;
	String analysisTypeDelay;
	
	public TextOutputVisitor() {
		super();
		registerDelay = "";
		//results.output = "";
	}

	@Override
	public void setLabel(String label) {
		analysisTypeDelay = label +"\r\n";
		//results.output+="Context-Insensitive Obfuscation Analysis"+"\r\n";		
	}


	@Override
	public void flush() {
		System.out.println(analysisTypeDelay);
		analysisTypeDelay = "";
	}
	
	@Override
	public void beforeStackGraph() {
		System.out.println("Abstract Stack Graph for Each Instruction\r\n");
		//results.output += "Abstract Stack Graph for Each Instruction\r\n";
	}

	@Override
	public void stackGraphContent(Object input) {
		System.out.println("\r\nInstruction = "+ input);
		//results.output+="Instruction = "+ input+"\r\n";
	}

	@Override
	public void beforeRegisterContents() {
		registerDelay = "";
		registerDelay += "Register Contents for Each Instruction\r\n";
	}
	
	@Override
	public void registerContent(Object input) {
		registerDelay +="\r\nInstruction = "+ input + "\r\n";
	}

	@Override
	public void addRegister(RegisterLabelProvider rlp, Object regElement) {
		registerDelay += (rlp.getColumnText(regElement, 0)+" ");
		registerDelay += (rlp.getColumnText(regElement, 1)+" ");
		registerDelay += (rlp.getColumnText(regElement, 2)+"\r\n");		
	}

	@Override
	public void controlFlow(Program program) {
		System.out.println("\r\nControl Flow Graph\r\n");
		System.out.println(program.getControlFlowGraph()+"\r\n");
		//results.output+="\r\n\r\nControl Flow Graph\r\n";  
  	    //results.output+=program.getControlFlowGraph()+"\r\n";
	}

	@Override
	public void addRegContents() {
		System.out.println(registerDelay);
		//results.output += registerDelay;
	}

	@Override
	public void obfuscations(Analysis analysis, Program program) {
		System.out.println("\r\nDetected Obfuscations\r\n");
		//results.output+="\r\n\r\nDetected Obfuscations\r\n";
		
		//This eventually causes ObfuscationAnalyzer to output any
        //detected obfuscations.
        ObfuscationVisitor obfuscationVisitor = null;
      	obfuscationVisitor = new TextObfuscationVisitor();
        ObfuscationAnalyzer obfuscationAnalyzer = (ObfuscationAnalyzer) analysis.createPostProcessor();
        obfuscationAnalyzer.setVisitor(obfuscationVisitor);
        AnalysisResult analysisResult = obfuscationAnalyzer.analyze(program);
	}

	@Override
	public void printResults() {
		//System.out.println(results.output);
	}


	
	


}

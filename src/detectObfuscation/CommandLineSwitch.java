package detectObfuscation;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Parameter;
import com.martiansoftware.jsap.QualifiedSwitch;
import com.martiansoftware.jsap.SimpleJSAP;
import com.martiansoftware.jsap.UnflaggedOption;

public class CommandLineSwitch {
	boolean xmlSwitch;
	boolean plainSwitch;
	int entryIndexSwitch;
	String entryAddressSwitch;
	boolean insensitiveSwitch;
	boolean lstackSwitch;
	boolean lcallSwitch;
	boolean kcallSwitch;
	boolean stackGraphSwitch;
	boolean regContentsSwitch;
	boolean controlFlowSwitch;
	boolean obfuscationsSwitch;
	boolean traceSwitch;
	String inputFileSwitch;
	String importPathSwitch;
	boolean showAnalysisDiff;
	
	public CommandLineSwitch(String[] args) throws JSAPException {
		//JSAP is a library that allows efficient creation of command line switches
		SimpleJSAP jsap = new SimpleJSAP("demon", 
            "Automates analysis techniques used by DOC", new Parameter[] {
                new FlaggedOption( "entryIndex", JSAP.INTEGER_PARSER, "0", JSAP.REQUIRED, 'i', "index", 
                    "The index of the instruction to start analysis on." ),
                new FlaggedOption( "entryAddress", JSAP.STRING_PARSER, "-1", JSAP.REQUIRED, 'a', "address", 
                    "The address of the instruction to start analysis on." ),
                new QualifiedSwitch( "Context Insensitive", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'n', "insensitive", 
                    "Performs Context insensitive analysis." ),
                new QualifiedSwitch( "Context Sensitive l-stack", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 's', "stack", 
                    "Performs Context Sensitive l-stack analysis." ),
                new QualifiedSwitch( "Context Sensitive l-call", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'l', "l-call", 
                    "Performs Context Sensitive l-call analysis." ),
                new QualifiedSwitch( "Context Sensitive k-call", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'k', "k-call", 
                    "Performs Context Sensitive k-call analysis." ),
                new QualifiedSwitch( "Stack Graph", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'g', "graph", 
                    "Outputs contents of Abstract Stack Graph" ),
                new QualifiedSwitch( "Register Contents", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'r', "registers", 
                    "Outputs Register Contents" ),   
                new QualifiedSwitch( "Control Flow", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'c', "control", 
                    "Outputs Control Flow Graph" ),     
                new QualifiedSwitch( "Detected Obfuscations", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'o', "obfuscations", 
                    "Outputs Detected Obfuscations" ),     
                new QualifiedSwitch( "Plain Text Output", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'p', "plain", 
                    "Outputs Results in an easily readable plain text format" ),  
                new QualifiedSwitch( "XML Output", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'x', "xml", 
                    "Outputs Results in xml format" ), 
                new QualifiedSwitch( "Toggle Trace", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'T', "trace", 
                    "Toggles Instruction Interpretation Trace" ),  
                new UnflaggedOption( "inputFile", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.REQUIRED, JSAP.GREEDY, 
                    "The assembly file to be analyzed."),
                new FlaggedOption( "importsPath", JSAP.STRING_PARSER, "Imports", JSAP.REQUIRED, 'I', "imports", 
                	"The path to the file containing imported external functions." ),
                new QualifiedSwitch( "Difference of States", JSAP.STRING_PARSER, JSAP.NO_DEFAULT, JSAP.NOT_REQUIRED, 'd', "difference", 
                "Outputs Register Contents" )
         
            }
        );
        
		//these lines of code are necessary for JSAP
        JSAPResult config = jsap.parse(args);    
        if ( jsap.messagePrinted() ) System.exit( 1 );
        String[] inputArray = config.getStringArray("inputFile");
        
        xmlSwitch = config.getBoolean("XML Output");
        plainSwitch = config.getBoolean("Plain Text Output");
    	entryIndexSwitch = config.getInt("entryIndex");
    	entryAddressSwitch = config.getString("entryAddress");
    	insensitiveSwitch = config.getBoolean("Context Insensitive");
    	lstackSwitch = config.getBoolean("Context Sensitive l-stack");
    	lcallSwitch = config.getBoolean("Context Sensitive l-call");
    	kcallSwitch = config.getBoolean("Context Sensitive k-call");
    	stackGraphSwitch = config.getBoolean("Stack Graph");
    	regContentsSwitch = config.getBoolean("Register Contents");
    	controlFlowSwitch = config.getBoolean("Control Flow");
    	obfuscationsSwitch = config.getBoolean("Detected Obfuscations");
    	traceSwitch = config.getBoolean("Toggle Trace");
    	inputFileSwitch = config.getString("inputFile");
    	importPathSwitch = config.getString("importsPath");
    	showAnalysisDiff = config.getBoolean("Difference of States");
    	
	}
	
}

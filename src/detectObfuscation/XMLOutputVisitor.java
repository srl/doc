package detectObfuscation;

import java.io.IOException;

import obfuscations.ObfuscationAnalyzer;
import obfuscations.ObfuscationVisitor;
import obfuscations.TextObfuscationVisitor;
import obfuscations.XMLObfuscationVisitor;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;

import analyses.Analysis;
import analyzer.AnalysisResult;

import ui.registers.RegisterLabelProvider;
import x86InstructionSet.Program;

public class XMLOutputVisitor extends OutputVisitor {
	XMLOutputter outp;
    Element root;
	Element analysiselement;
	Element asgelement;
    Element icollection;
    Element ricollection;
    Element rcontentselement;
    Element instructionelement;
    Element rinstructionelement;
    Element addresselement;
    Element commandelement;
    Element raddresselement;
    Element rcommandelement;
    Element registerelement;
    Element rlabelelement;
    Element rcontentelement;
    Element rcontextelement;
    Element rcollectionelement;
	Element controlelement;
    Element addresscollection;
    Element edgecollection;
    Element cfgaelement;
    Element cfgeelement;
    Element cfgsource;
    Element cfgdestination;
    Element obfcollection;
    Element obfelement;
    
  
    
	public XMLOutputVisitor(Document doc) {
		super();
		root = doc.getRootElement();
		outp = new XMLOutputter(Format.getPrettyFormat());
	}

	@Override
	public void setLabel(String label) {
		analysiselement = new Element("ANALYSIS");
    	root.addContent(analysiselement);
    	analysiselement.addContent(new Element("NAME").setText(label));
	}


	@Override
	public void beforeStackGraph() {
	  asgelement = new Element("ABSTRACTSTACKGRAPH");
      analysiselement.addContent(asgelement);
      icollection = new Element("INSTRUCTIONCOLLECTION");
      asgelement.addContent(icollection);
	}

	@Override
	public void stackGraphContent(Object input) {
		instructionelement = new Element("INSTRUCTION");
		icollection.addContent(instructionelement);
		String instructionstring = input.toString();
        while(instructionstring.contains("  " ))
        	instructionstring = instructionstring.replace("  ", " ");
    
        String[] instructionparts = instructionstring.trim().split(" ");
        String addressstring = instructionparts[0];
        String commandstring;
        if (instructionparts.length <=2) {
        	 commandstring = instructionparts[1];
        } else
        commandstring = instructionparts[1] + " " + instructionparts[2];
        addresselement = new Element("ADDRESS").setText(addressstring);
        commandelement = new Element("COMMAND").setText(commandstring);
        instructionelement.addContent(addresselement);
        instructionelement.addContent(commandelement);
        
        
	}

	@Override
	public void beforeRegisterContents() 
	{
	  rcontentselement = new Element("REGISTERCONTENTS");
	  ricollection = new Element("INSTRUCTIONCOLLECTION");
      
      rcontentselement.addContent(ricollection);
      analysiselement.addContent(rcontentselement);  
	}
	
	@Override
	public void registerContent(Object input) {
		rinstructionelement = new Element("INSTRUCTION");
        ricollection.addContent(rinstructionelement);
        
		String instructionstring = input.toString();
        while(instructionstring.contains("  " ))
        	instructionstring = instructionstring.replace("  ", " ");
    
        String[] instructionparts = instructionstring.trim().split(" ");
        String addressstring = instructionparts[0];
        String commandstring;
        if (instructionparts.length <=2) {
        	 commandstring = instructionparts[1];
        } else
        commandstring = instructionparts[1] + " " + instructionparts[2];

        addresselement = new Element("ADDRESS").setText(addressstring);
        commandelement = new Element("COMMAND").setText(commandstring);
        rinstructionelement.addContent(addresselement);
        rinstructionelement.addContent(commandelement);
		
        rcollectionelement = new Element("REGISTERCOLLECTION");
  	    rinstructionelement.addContent(rcollectionelement);
        
	}

	@Override
	public void addRegister(RegisterLabelProvider rlp, Object regElement) {
      registerelement = new Element("REGISTER");
	  rcollectionelement.addContent(registerelement);
  	  	
  	  rlabelelement = new Element("LABEL").setText(rlp.getColumnText(regElement, 0));
	  rcontentelement = new Element("CONTENTS").setText(rlp.getColumnText(regElement, 1));
	  rcontextelement = new Element("CONTEXT").setText(rlp.getColumnText(regElement, 2));
	  registerelement.addContent(rlabelelement);
	  registerelement.addContent(rcontentelement);
	  registerelement.addContent(rcontextelement);
	}

	@Override
	public void controlFlow(Program program) {
	  controlelement = new Element("CONTROLFLOWGRAPH");
  	  analysiselement.addContent(controlelement);
  	  addresscollection = new Element("ADDRESSCOLLECTION");
  	  edgecollection = new Element("EDGECOLLECTION");
  	  cfgaelement = null;
  	  cfgeelement = null;
  	  cfgsource = null;
  	  cfgdestination = null;
  	  controlelement.addContent(addresscollection);
  	  controlelement.addContent(edgecollection);
	
	  String cfg = program.getControlFlowGraph().toString();
	  String[] cfgparts = cfg.split("\n");
	  String[] cfgadds = cfgparts[0].replace("[", "").replace("]", "").split(", ");
	  for(String a: cfgadds)
	  {
	  	cfgaelement = new Element("ADDRESS").setText(a);
	  	addresscollection.addContent(cfgaelement);
	  }
	  for(int c=1;c<cfgparts.length;c++)
	  {
	      cfgeelement = new Element("EDGE");
	      edgecollection.addContent(cfgeelement);
	  	  String[] edgeparts = cfgparts[c].split(" --> ");
	  	  cfgsource = new Element("SOURCE").setText(edgeparts[0]);
	  	  cfgdestination = new Element("DESTINATION").setText(edgeparts[1]);
	  	  cfgeelement.addContent(cfgsource);
	  	  cfgeelement.addContent(cfgdestination);
	  }
	}

	@Override
	public void addRegContents() {
		// TODO Auto-generated method stub
	}

	@Override
	public void obfuscations(Analysis analysis, Program program) {
  	  obfelement = new Element("DETECTEDOBFUSCATIONS");
	  analysiselement.addContent(obfelement);
	  obfcollection = new Element("OBFUSCATIONCOLLECTION");
	  obfelement.addContent(obfcollection);
	  
	  //This eventually causes ObfuscationAnalyzer to output any
      //detected obfuscations.
      ObfuscationVisitor obfuscationVisitor = null;
      obfuscationVisitor = new XMLObfuscationVisitor(root.getDocument());
      ObfuscationAnalyzer obfuscationAnalyzer = (ObfuscationAnalyzer) analysis.createPostProcessor();
      obfuscationAnalyzer.setVisitor(obfuscationVisitor);
      AnalysisResult analysisResult = obfuscationAnalyzer.analyze(program);
	}

	@Override
	public void printResults() {
		try {
			outp.output(root.getDocument(), System.out);
		} catch (IOException e) {
			e.printStackTrace();
		}
 		
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}
	
	
    
}

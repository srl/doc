/*
 * Preferences.java
 * Created on May 30, 2005
 */
package detectObfuscation;

/**
 * Stores constants related to the plugin's preferences.
 */
public class Preferences
{
  // PREFERENCE CONSTANTS
  /** identifies the outputter to use. */
  public static final String OUTPUTTER = "outputter";
  /** identifies the path of the stub DLLs. */
  public static final String STUB_FILE_PATH = "stub_file_path";
    
  // POSSIBLE VALUES
  public static final int TPIAWO_OUTPUTTER  = 1;
  public static final int UNMORPH_OUTPUTTER = 2;
  public static final int DIALOG_OUTPUTTER  = 3;
  
  // POSSIBLE EXPORTER TYPES
  public static final int UNMORPH_EXPORTER = 1;

  // DEFAULT VALUES
  /** Default value for the outputter. */
  public static final int OUTPUTTER_DEFAULT = TPIAWO_OUTPUTTER;
  /** Default value for the path of the stub DLLs. */
  public static final String STUB_DLL_PATH_DEFAULT = "";
}

/**
 * This class allows DOC to be run as a stand-alone java application.
 * It's named demon because it takes control of DOC's resources.
 * 
 * I will keep a journal of changes made to DOC here:
 * 
 * Changed getExportsPathFromPreferenceStore (line 61) in ExternalFunctionLoader
 * to recognize when demon is being run and to look for a directory called 
 * Imports in the same directory as the demon.jar file.
 * 
 * Changed the values returned by RegisterLabelProvider so that each
 * label contains both the register name, value, and context.
 * 
 * Created a new class called StandAloneGraphViewer that is just a
 * modified version of DirectedGraphViewer to output The ASG in string
 * form to the console for each instruction.
 * 
 * Added code to Interpreter that sends output from the error log to a
 * file called results.txt
 * This is for comparing the 4 different types of analysis
 * 
 * Added code to Demon that outputs the name of the file being
 * analyzed to results.txt
 * 
 * Removed a few lines from demon that seemed to do nothing
 * 
 * Added in flexible command line options by using JSAP
 * These command lines allow users to choose the analysis
 * types, output contents, and output formats.
 * 
 * Added in XML output format by using JDOM
 * 
 * JSAP and JDOM are downloaded libraries that had to be added
 * to the class path.
 * 
 * Re-factored demon and some of the classes it uses with the 
 * visitor patter in order to reduce redundant code and 
 * improve readability.  
 * 
 * In summary classes modified or created to work with demon include:
 * demon
 *   OutputVisitor
 *     XMLOutputVisitor
 *     TextOutputVisitor
 * results
 * ObfuscationAnalyzer
 *   ObfuscationVisitor
 *     XMLObfuscationVisitor
 *     TextObfuscationVisitor
 *     PlugInObfuscationVisitor
 * StandAloneGraphViewer
 *   GraphVisitor
 *     XMLGraphVisitor
 *     TextGraphVisitor
 *     PlugInGraphVisitor
 * Trace
 * ExternalFunctionLoader
 * 
 * 
 * 
 * @author Luke Deshotels
 */

package detectObfuscation;

import java.io.*;
import java.util.*;

import logger.Trace;
import obfuscations.ObfuscationAnalyzer;
import obfuscations.ObfuscationVisitor;
import obfuscations.TextObfuscationVisitor;
import obfuscations.XMLObfuscationVisitor;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;

import analyses.Analysis;
import analyses.LCallContextAnalysis;
import analyses.LStackContextAnalysis;
import analyses.KCallContextAnalysis;
import analyses.ContextInsensitiveAnalysis;
import analyzer.AnalysisResult;

import parser.ParseException;
import parser.Parser;
import domain.State;

import ui.registers.EndStateRegisterContentProvider;
import ui.registers.RegisterLabelProvider;
import x86InstructionSet.*;
import x86Interpreter.ExternalFunctionLoader;
import x86Interpreter.Interpreter;
import x86Interpreter.NoEntryPointException;
import ui.directedGraph.DirectedGraphViewer;
import ui.directedGraph.GraphVisitor;
import ui.directedGraph.PlugInGraphVisitor;
import ui.directedGraph.TextGraphVisitor;
import ui.directedGraph.XMLGraphVisitor;
import vsaAsgDomain.EndStateStackGraphContentProvider;
import vsaAsgDomain.StackGraphLabelProvider;

import com.martiansoftware.jsap.*;

import org.jdom.output.XMLOutputter;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import java.net.URISyntaxException;
import logger.Trace;

public class Main {
	/**
	 * @param args
	 * @throws ParseException 
	 * @throws InterruptedException 
	 * @throws NoEntryPointException 
	 * @throws IOException 
	 * @throws JSAPException 
	 * @throws CoreException 
	 * @throws URISyntaxException 
	 */
	
	// Default arguments -- useful for testing in Eclipse Environment
	private static String default_args[] = {
		// "-x,   // xml
		// "-n",  // insensitive analysis
		 "-s",  // l-stack analysis
		// "-l",  // l-call analysis
		// "-k",  // k-call analysis
		// "-dT", // difference Trace
		 "-g",  // output ASG
		// "/home/cajunbot/DOC/tests/2obfcalls-1.asm"
		// "/home/cajunbot/DOC/tests/2calls.asm"
		//"/home/cajunbot/DOC/tests/tse-example.asm"
		// "C:\\Users\\Arun\\Projects\\workspace-collection\\TSE-examples\\tse-example.asm"
		"C:\\Users\\Arun\\Projects\\workspace-collection\\TSE-examples\\2calls.asm"
				 
	};
	public static void main(String[] args) throws ParseException, NoEntryPointException, InterruptedException, IOException, JSAPException, CoreException, URISyntaxException {
       
		// For testing and debugging only
		if (args.length == 0) args = default_args;

		//Assign command line arguments to easily accessible variables.
		CommandLineSwitch commandLineSwitch = new CommandLineSwitch(args);
		
        //The entry point for analysis is determined by a command line switch.
        //There is a significant difference between the entyAddress and entryIndex switches.
        //Each has its own command line switch that expects either an index or 
        //an address accordingly.  By default the system will use an entryIndex of 0
        int entryPoint = commandLineSwitch.entryIndexSwitch;             
                  		
		//convert data at filePath into a Program object
		Parser 		fileParser 	= Parser.getInstance();
  		String filePath = commandLineSwitch.inputFileSwitch;
  		
    	//outputs name of file being analyzed to results.txt
 		//The file results.txt will contain data related to DOC's performance.
  		//This includes time, instructions analyzed, etc. This data is output in
  		//Interpreter.
  		BufferedWriter out = new BufferedWriter(new FileWriter("results.txt", true));
  		out = new BufferedWriter(new FileWriter("results.txt", true));
  	  	out.write(filePath+",");
  	  	out.close();

  		//This list iterates through each of the four analysis types and uses command
  		//line switches to determine which are run and which are skipped.
  		Boolean[] analysisTypes = {
  				commandLineSwitch.insensitiveSwitch, 
  				commandLineSwitch.lstackSwitch,
  				commandLineSwitch.lcallSwitch, 
  				commandLineSwitch.kcallSwitch
  		};
  		
 	    //Determines how output is produced by assigning OutputVisitor
        //to either a child based on plain text output or one for XML output
        OutputVisitor outputVisitor = null;
        //XMLdoc will become the XML document for output if an XML
        //document is requested at command line
        Document XMLdoc = null;
		Element root = new Element("ANALYSISCOLLECTION");
		XMLdoc = new Document(root);
        //Determines how output is handled by the DirectedGraphViewer when
        //it is passed one of three types of GraphVisitor.  If no GraphViewer
        //is passed the default constructor uses a PlugInGraphViewer
        GraphVisitor graphVisitor = null;
        	
	      if(commandLineSwitch.xmlSwitch){
	        	outputVisitor = new XMLOutputVisitor(XMLdoc);
	        	graphVisitor = new XMLGraphVisitor(XMLdoc);
	      } else {
	        // Plain text output is default
	        // if(commandLineSwitch.plainSwitch){
	        	outputVisitor = new TextOutputVisitor();
	        	graphVisitor = new TextGraphVisitor();
	      }

  		//Analysis starts here.
  		//This loop causes demon to use all four analysis techniques
  		//and outputs which analyses is currently being used.
  		for(int i =0;i<analysisTypes.length;i++){
	    	if(!analysisTypes[i])
	    		continue;

  			Analysis analysis = selectAnalysis(i);
  			if (analysis == null) continue;
  	    	
		    //This seems to be another way to track DOC by outputting
		    //analysis results in a readable format.
		    if(commandLineSwitch.traceSwitch) Trace.toggle();
		    
		    //sets the GraphVisitor type for Trace to avoid mixed output types
		    //note that Trace only works with the plain text GraphVisitor.
		    Trace.setGraphVisitor(new TextGraphVisitor());

	  		Program		program;
	    	program = fileParser.parse(filePath);
			    	
		    //if the entry address has been specified with a switch then it is used
		    //to find the correct entry point.  Otherwise the entryIndex switch or the
		    //default of 0 is used.
	        if(!(commandLineSwitch.entryAddressSwitch.equals("-1"))){
	        	Address entryAddress = new Address(commandLineSwitch.entryAddressSwitch);
	        	entryPoint = program.getInstructionIndex(entryAddress);
	        }
	        
	  	    Interpreter     interpreter;
	  	    State           state;
		    
		    state = analysis.createDomainFactory().createState();
		        //ExternalFunctionLoader is passed the importPathSwitch to avoid needing the
	        //importPath as a global variable.
			interpreter = new Interpreter(
			          program, 
			          analysis, 
			          program.getInstruction(entryPoint),
			          null,
			          state, 
			          ExternalFunctionLoader.loadExternalFunctionTables(commandLineSwitch.importPathSwitch));
		    
		    state.init();
		    
	  	    //monitor is necessary to trick DOC into working as if
	  	    //the plug-in interface were being used.
		    IProgressMonitor	monitor = new NullProgressMonitor();  

	
	    	//interpreter.interpret returns an annotated version of the program
	    	//with register values for each instruction
	    	//This creates the control flow graph too

		    interpreter.interpret(monitor);
		            		 
			outputVisitor.setLabel(analysis.getTitle());
		    outputVisitor.flush();
		    
		      if(commandLineSwitch.regContentsSwitch)  {
		    	  outputRegisterContents(outputVisitor, program);
		      }
		      
		    /// ABSTRACT STACK GRAPH ///////////
	    	
	 					      
	        if(commandLineSwitch.stackGraphSwitch) 	  {
	     	   outputAbstractStackGraph(outputVisitor, graphVisitor, program);
	          }

	        ////////////////////////////////////////
	        
		      if (commandLineSwitch.showAnalysisDiff){
			   outputRegisterDiffs(program);
		      }
		      
		      /////////////////////////////////////////////////
		      //Adds the control flow graph to the output.
		      if(commandLineSwitch.controlFlowSwitch)
		          outputVisitor.controlFlow(program);
		      
		      //Outputs any detected obfuscations
		      if(commandLineSwitch.obfuscationsSwitch)
		    	  outputVisitor.obfuscations(analysis, program);  		
		     
	    }
  		outputVisitor.printResults();
  		
	  }


	private static Analysis selectAnalysis(int i) {
		Analysis analysis = null;
		   	
		switch(i){
		case 0:
			analysis = ContextInsensitiveAnalysis.getInstance();
			
			break;
		case 1:
			analysis = LStackContextAnalysis.getInstance();		    	
			break;
		case 2:
			analysis = LCallContextAnalysis.getInstance();
			break;
		case 3:
			analysis = KCallContextAnalysis.getInstance();
			break;
		}
		return analysis;
	}
	
	
	private static void outputRegisterDiffs(Program program) {
		//makes an iterator that steps through each instruction of the program
			Iterator<Instruction> iter;
		    iter = program.iterator();     
		    while(iter.hasNext())
		    {
		    	Instruction  instruction = iter.next();
		   		Trace.enable();
		   		System.out.println(instruction);
		   		Trace.traceRegisterDiffs(instruction);
		   	  }
	}
	private static void outputAbstractStackGraph(OutputVisitor outputVisitor,
			GraphVisitor graphVisitor, Program program) {
		//viewer is essential for output of the abstract stack graph
		  	  DirectedGraphViewer   viewer = null;
		    //This section deals with the abstract stack graph.
		    
		    viewer = new DirectedGraphViewer(graphVisitor);
		    viewer.setNoContentMessage("NO_CONTENT_MESSAGE");
		    viewer.setContentProvider(new EndStateStackGraphContentProvider());
		    viewer.setLabelProvider(new StackGraphLabelProvider());

			  //outputs labels designating a section
			  //for stack graph contents
		   outputVisitor.beforeStackGraph();
		  
		   //makes an iterator that steps through each instruction of the program
		   Iterator<Instruction> iter;
		    iter = program.iterator();    
		  
		  while(iter.hasNext())
		  {
			  Instruction  instruction = iter.next();
		   		outputVisitor.stackGraphContent(instruction);
		   		  viewer.setInput(instruction);
		   		  //refreshing the viewer causes stack graph
		   		  //contents to be output
		   		  viewer.refresh();
		   }
	}
	private static void outputRegisterContents(OutputVisitor outputVisitor,
			Program program) {
		//outputs labels designating a section
		  //for register contents
		  outputVisitor.beforeRegisterContents();
		  Iterator<Instruction> iter;
		  iter = program.iterator();    
		  while(iter.hasNext())
		  {
			  Instruction  instruction = iter.next();
    	   
			  outputVisitor.registerContent(instruction);

		      //rcp provides the register data for each instruction
		      EndStateRegisterContentProvider rcp = new EndStateRegisterContentProvider();
		      //rcp seems to be made of rlp elements
		      RegisterLabelProvider rlp = new RegisterLabelProvider();
		      
		      //adds the instruction itself
		      Object[] regElementArray = rcp.getElements(instruction);

		      for(Object regElement: regElementArray)
		          outputVisitor.addRegister(rlp, regElement);
		  
		  }
		  //Outputs the Register Contents data held in result.
			  outputVisitor.addRegContents();
	} 
}

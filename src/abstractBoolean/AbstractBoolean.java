package abstractBoolean;

/**
 * Represents a boolean value used during abstract interpretation.  
 * The abstract boolean values form a lattice consisting of
 * {Top, True, False, Bottom}
 */
public interface AbstractBoolean 
{
	// Possible values for an AbstractBoolean.
	public static final TopBoolean     Top     = new TopBoolean();
	public static final FalseBoolean   False   = new FalseBoolean();
	public static final TrueBoolean    True    = new TrueBoolean();
	public static final BottomBoolean  Bottom  = new BottomBoolean();
	
	/**
	 * Performs the join operation on two <code>AbstractBoolean</code>s.
	 * @return the join of this <code>AbstractBoolean</code> with <code>rhs</code>.
	 */	
	public AbstractBoolean join(AbstractBoolean rhs);
}

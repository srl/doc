/*
 * True.java
 * Created on Jul 20, 2005
 */
package abstractBoolean;

/**
 * One of the possible values for an abstract boolean.  This
 * value represents True only.
 */
public class TrueBoolean implements AbstractBoolean 
{
	/**
	 * @see AbstractBoolean#join
	 */
	public AbstractBoolean join(AbstractBoolean rhs)
	{ 		
		if(rhs == AbstractBoolean.Top)
			return AbstractBoolean.Top;
	
		else if(rhs == AbstractBoolean.True)
			return AbstractBoolean.True;
	
		else if(rhs == AbstractBoolean.False)
			return AbstractBoolean.Top;
	
		else // rhs == AbstractBoolean.Bottom
			return AbstractBoolean.True;
	}

	/**
	 * String representation
	 */
	public String toString()
	{
		return "{True}";
	}
}

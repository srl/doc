/*
 * Bottom.java
 * Created on Jul 20, 2005
 */
package abstractBoolean;

/**
 * One of the possible values for an abstract boolean.  This
 * value represents Bottom only.
 */
public class BottomBoolean implements AbstractBoolean 
{
	/**
	 * @see AbstractBoolean#join
	 */
	public AbstractBoolean join(AbstractBoolean rhs)
	{
		return rhs;
	}

	/**
	 * String representation
	 */
	public String toString()
	{
		return "{}";
	}
}

/*
 * Top.java
 * Created on Jul 20, 2005
 */
package abstractBoolean;

/**
 * One of the possible values for an abstract boolean. This
 * value represents both True and False.  This means that, at 
 * this point in the abstract interpretation, it is possible
 * for the value to be True and it's possible for the value
 * to be False.  
 */
public class TopBoolean implements AbstractBoolean
{
	/**
	 * @see AbstractBoolean#join
	 */
	public AbstractBoolean join(AbstractBoolean rhs)
	{
		return AbstractBoolean.Top;
	}

	/**
	 * String representation
	 */
	public String toString()
	{
		return "{True, False}";
	}
}

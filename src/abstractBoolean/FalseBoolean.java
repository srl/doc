/*
 * False.java
 * Created on Jul 20, 2005
 */
package abstractBoolean;

/**
 * One of the possible values for an abstract boolean.  This
 * value represents False only.
 */
public class FalseBoolean implements AbstractBoolean 
{
	/**
	 * @see AbstractBoolean#join
	 */
	public AbstractBoolean join(AbstractBoolean rhs)
	{ 
		if(rhs == AbstractBoolean.Top)
			return AbstractBoolean.Top;
		
		else if(rhs == AbstractBoolean.True)
			return AbstractBoolean.Top;
		
		else if(rhs == AbstractBoolean.False)
			return AbstractBoolean.False;
		
		else // rhs == AbstractBoolean.Bottom
			return AbstractBoolean.False;
	}

	/**
	 * String representation
	 */
	public String toString()
	{
		return "{False}";
	}
}

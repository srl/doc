/*
 * DefinedStackLocationSet.java
 * Created on Mar 14, 2005
 */
package vsaAsgDomain;

import abstractBoolean.AbstractBoolean;
import java.util.LinkedList;

import vsaAsgDomain.IStackLocationSet.Iterator;


/**
 * Represents a StackLocationSet that has been initialized.
 * 
 * @see vsaAsgDomain.IStackLocationSet
 */
public class StackLocationSet implements IStackLocationSet
{
    /**
     * Contains the StackLocations in this set.
     */
    private LinkedList<StackLocation> data = new LinkedList<StackLocation>();

    
    /**
     * Creates a new DefinedStackLocationSet belonging to the 
     * specified state.
     */
    public StackLocationSet()
    {
    }
    
    /**
     * Adds this set with a StackLocationSet 
     * @return this + rhs 
     */
    public Value add(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        IStackLocationSet.Iterator	iter	= iterator();
        IStackLocationSet.Iterator 	rhsIter = rhs.iterator();
        Value 						result 	= new Value();
        
        while(iter.hasNext())
        {
            StackLocation lhs = iter.next();
            while(rhsIter.hasNext())
            {
                Value r = lhs.add(rhsIter.next());
                if(result.isDefined() && r.isDefined())
                {
                    result.merge(r);
                }
                else if(r.isDefined() && !result.isDefined())
                    result = r;
            }
        }
        
        return result;
    }

    /**
     * Adds this set with a ReducedIntervalCongruence
     * @return this + rhs 
     */
    public Value add(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        IStackLocationSet.Iterator iter 	= iterator();
        Value result					= new Value();
        
        while(iter.hasNext())
        {
            Value r = iter.next().add(rhs);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;
        }

        return result;
    }

    /**
     * Adds a reference to a StackLocation to this set.  Duplicates are removed.
     */
    public void addToSet(final StackLocation node)
    {
      if(!has(node))
        data.add(node);
    }
    
    /**
     * Removes all elements from this set.
     */
    public void clear()
    {
        data.clear();
    }
    
    /**
     * Returns a copy of this object.
     */
    public Object clone()
    {
        try
        {
	        StackLocationSet copy = (StackLocationSet)super.clone();
	        copy.data = (LinkedList)data.clone();
	        return copy;
        }
        catch(CloneNotSupportedException e)
        {
            throw new Error("This should never happen.");
        }
    }
    
    /**
     * Returns true if <code>this</code> contains the exact same 
     * <code>StackLocation</code>s as <code>obj</code>.
     */
    public boolean equals(Object obj)
    {
    	IStackLocationSet 					rhs = (IStackLocationSet)obj;
    	IStackLocationSet.Iterator 	iter;
    	StackLocation 							currentNode; 
    	
    	if(size() == rhs.size())
    	{
	    	iter = iterator();
	    	while(iter.hasNext())
	    	{
	    		currentNode = iter.next();
	    		if(!rhs.has(currentNode))
	    			return false;
	    	}
	    	return true;
    	}

    	return false;
    }
    
    /**
     * Divides this set by an RIC.  
     * @return this / rhs
     */
    public Value divide(final IReducedIntervalCongruence rhs) throws UndefinedOperationException 
    {
        IStackLocationSet.Iterator	iter	= iterator();
        Value 						result 	= new Value();
        
        while(iter.hasNext())
            result.merge( iter.next().divide(rhs) );
        
        return result;
    }

    /**
     * Divides this set by a StackLocationSet. 
     * @return this / rhs 
     */
    public Value divide(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        IStackLocationSet.Iterator	iter	= iterator();
        IStackLocationSet.Iterator 	rhsIter = rhs.iterator();
        Value 						result 	= new Value();
        
        while(iter.hasNext())
            while(rhsIter.hasNext())
                result.merge( iter.next().divide(rhsIter.next() ) );
        
        return result;
    }

    /**
     * Determines if this set already contains a particular
     * StackLocation.
     * @return true if item is in this set.
     * @see vsaAsgDomain.IStackLocationSet#has(StackLocation)
     */
    public boolean has(final StackLocation item)
    {
        IStackLocationSet.Iterator iter = iterator();
        while(iter.hasNext())
            if(iter.next() == item)
                return true;
         return false;
    }
    
    /**
     * Returns true if this StackLocationSet has been initialized.  Since this is
     * a DefinedStackLocationSet, the result is always true.
     */
    public boolean isDefined() 
    { 
        return true; 
    };
    
    /**
     * @see IStackLocationSet#isEqualTo(IStackLocationSet)
     */
    public AbstractBoolean isEqualTo(IStackLocationSet rhs)
    {
    	if(!rhs.isDefined())
    		return AbstractBoolean.Top;

    	// if both sets are empty, then they are guaranteed to be equal.
    	if(this.size() == 0 && rhs.size() == 0)
    		return AbstractBoolean.True;
    	
    	// if both sets contain only one item and that item is the same,
    	// then pick any item from set A and any item from set B, they are
    	// guaranteed to be the same item.
    	if(this.size() == 1 && rhs.size() == 1)
    	{
    		if(this.iterator().next() == rhs.iterator().next())
    			return AbstractBoolean.True;
    	}
    	
    	// if the sets contain more than one item and there is at least one
    	// item that is in both sets, then if you pick a single item from each
    	// set, there is a chance it might be the same or it might not be the same item.
    	// For this case, return Top to signify that they might be equal.
    	if(this.intersection(rhs).size() > 0)
    		return AbstractBoolean.Top;
    	
    	// If there are no items in both sets, then it is guaranteed to be false.  That is,
    	// if you pick an item from set A and from set B, they are guaranteed to be different.
    	return AbstractBoolean.False;
    }
    
    /**
     * @see IStackLocationSet#intersection(IStackLocationSet)
     */
    public IStackLocationSet intersection(IStackLocationSet rhs)
    {
    	StackLocationSet	result = new StackLocationSet();
    	IStackLocationSet.Iterator iterThis;
    	IStackLocationSet.Iterator iterRhs;
    	
    	if(!rhs.isDefined())
    		return (IStackLocationSet)this.clone();
    	
    	iterThis = this.iterator();
    	iterRhs = rhs.iterator();
    	while(iterThis.hasNext() & iterRhs.hasNext())
    	{
			StackLocation stackLocation = iterThis.next();
    		while(iterRhs.hasNext())
    		{
    			if(stackLocation == iterRhs.next())
    				result.addToSet(stackLocation);
    		}
    		iterRhs = rhs.iterator();
    	}
    	return result;
    }
    
    /**
     * Merges another StackLocationSet with this one.
     * @return this union rhs 
     */
    public IStackLocationSet merge(final IStackLocationSet rhs)
    {
        IStackLocationSet.Iterator iter;
        IStackLocationSet	merged = new StackLocationSet();
        
        // ARUN: THIS IS EXPENSIVE. ITS GENERATING A 
        // NEW StackLocationSet, RATHER MERGING INTO THE
        // CURRENT ONE.
        iter = iterator();
        while(iter.hasNext())
            merged.addToSet(iter.next());
        
        iter = rhs.iterator();
        while(iter.hasNext())
            merged.addToSet(iter.next());
        
        return merged;
    }
    
    /**
     * Multiplies this set by a StackLocationSet
     * @return this * rhs  
     */
    public Value multiply(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        IStackLocationSet.Iterator	iter	= iterator();
        IStackLocationSet.Iterator 	rhsIter = rhs.iterator();
        Value 						result 	= new Value();
        
        while(iter.hasNext())
            while(rhsIter.hasNext())
                result.merge( iter.next().multiply(rhsIter.next() ) );
        
        return result;
    }

    /**
     * Returns true if this stacklocation set overapproximates rhs.
     */
    public boolean overapproximates(final IStackLocationSet rhs)
    {
        if(size() >= rhs.size())
        {
            IStackLocationSet.Iterator iter = rhs.iterator();
            while(iter.hasNext())
                if( !has(iter.next()) )
                    return false;
            return true;
        }
        return false;
    }    
    
    /**
     * Multiplies this set by a ReducedIntervalCongruence
     * @return this * rhs
     */
    public Value multiply(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        IStackLocationSet.Iterator	iter	= iterator();
        Value 						result 	= new Value();
        
        while(iter.hasNext())
            result.merge( iter.next().multiply(rhs) );
        
        return result;
    }

    /**
     * Returns true if this > rhs.  A StackLocationSet A is considered 
     * greater than an RIC B if there is at least one member in A that 
     * is greater than at least one member in B. 
     */
    public boolean isGreaterThan(final IReducedIntervalCongruence rhs)
    {
        return true;
    }
    
    /**
     * Returns true if this > rhs.  A StackLocationSet A is considered 
     * greater than another StackLocationSet B if there is at least one 
     * member in A that is greater than at least one member in B. 
     */
    public boolean isGreaterThan(final IStackLocationSet rhs)
    {
        return true;
    }
    
    /**
     * Returns true if this < rhs.  A StackLocationSet A is considered 
     * less than an RIC B if there is at least one member in A that 
     * is less than at least one member in B. 
     */
    public boolean isLessThan(final IReducedIntervalCongruence rhs)
    {
        return true;
    }
    
    /**
     * Returns true if this < rhs.  A StackLocationSet A is considered 
     * less than another StackLocationSet B if there is at least one 
     * member in A that is less than at least one member in B. 
     */
    public boolean isLessThan(final IStackLocationSet rhs)
    {
        return true;
    }
    
    /**
     * Returns an iterator for traversing every node in this set.
     */
    public IStackLocationSet.Iterator iterator()
    {
        return new Iterator();
    }
    
    public class Iterator implements IStackLocationSet.Iterator
    {
        public Iterator()  						{ m_iter = data.iterator();}
        public boolean hasNext()        		{ return m_iter.hasNext();}
        public boolean hasPrevious()        	{ return ((Iterator) m_iter).hasPrevious();}
        public StackLocation next()      		{ return (StackLocation)m_iter.next();}
        public StackLocation previous()      	{ return (StackLocation)((Iterator) m_iter).previous();}
        public void remove()        			{ m_iter.remove();}
        private java.util.Iterator	m_iter;
    }
    
    /**
     * Returns the number of StackLocations in this set.
     */
    public int size()
    {
        return data.size();
    }
    
    /**
     * Subtracts a StackLocationSet from this set.
     * @return this - rhs
     */
    public Value subtract(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        IStackLocationSet.Iterator	iter	= iterator();
        IStackLocationSet.Iterator 	rhsIter = rhs.iterator();
        Value 						result 	= new Value();
        
        while(iter.hasNext())
        {
            StackLocation lhs = iter.next();
            while(rhsIter.hasNext())
                result.merge( lhs.subtract(rhsIter.next() ) );
        }
        
        return result;
    }
    

    /**
     * Subtracts a ReducedIntervalCongruence from this set.
     * @return this - rhs
     */
    public Value subtract(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        IStackLocationSet.Iterator	iter	= iterator();
        Value 						result 	= new Value();
        
        while(iter.hasNext())
        {
            Value r = iter.next().subtract(rhs);
            if(result.isDefined())
            {
                if(r.isDefined())
                    result.merge(r);
            }
            else
                result = r;
        }
        
        return result;
    }

    /**
     * Returns an array containing each stack-location in this set.
     */
    public Object[] toArray()
    {
        return data.toArray();
    }
    
    /**
     * Returns a string representation of this StackLocationSet
     */
    public String toString()
    {
      String result;
      
      result = new String("{");
      IStackLocationSet.Iterator iter = iterator();
      if(iter.hasNext())
      {
        result += iter.next().getDisplayName();
        while(iter.hasNext())
          result += ", " + iter.next().getDisplayName();
      }
      result += "}";
      return result;
    }
    
    /**
     * Widens this stackLocationSet
     * @return the widened set.
     */
    public IStackLocationSet widen(final IStackLocationSet rhs)
    {
//      IStackLocationSet	result = (StackLocationSet)clone();
//      IStackLocationSet.Iterator thisIter = iterator();
//      
//      while(thisIter.hasNext())
//      {
//      	StackLocation thisNode = thisIter.next();
//        IStackLocationSet.Iterator rhsIter = rhs.iterator();
//      	while(rhsIter.hasNext())
//      	{
//      		result = result.merge(thisNode.widen(rhsIter.next()));
//      	}
//      }
//      return result;	
        StackLocationSet	result = (StackLocationSet)clone();
        IStackLocationSet.Iterator iter = rhs.iterator();
        while(iter.hasNext())
            result.addToSet(iter.next());
        return result;	
    }
    
    /**
     * Returns the xor of this and rhs.
     */
    public Value xor(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        // A stack-location xor'd with an RIC?...we don't know how to do that.
        if(rhs.isDefined())
            throw new UndefinedOperationException();
        
        return new Value();
    }

    /**
     * Returns the xor of this and rhs.
     */
    public Value xor(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        // A stack-location set xor'd with another stack-location set?...we don't know how to do that.
        if(rhs.isDefined())
        	throw new UndefinedOperationException();
        
        return new Value();
    }
    
    /**
     * Returns the or of this and rhs.
     */
    public Value or(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        // A stack-location or'd with an RIC?...we don't know how to do that.
        if(rhs.isDefined())
            throw new UndefinedOperationException();
        
        return new Value();
    }

    /**
     * Returns the or of this and rhs.
     */
    public Value or(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        // A stack-location set or'd with another stack-location set?...we don't know how to do that.
        if(rhs.isDefined())
        	throw new UndefinedOperationException();
        
        return new Value();
    }
    
    /**
     * Returns the and of this and rhs.
     */
    public Value and(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        // A stack-location and'd with an RIC?...we don't know how to do that.
        if(rhs.isDefined())
            throw new UndefinedOperationException();
        
        return new Value();
    }

    /**
     * Returns the or of this and rhs.
     */
    public Value and(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        // A stack-location set and'd with another stack-location set?...we don't know how to do that.
        if(rhs.isDefined())
        	throw new UndefinedOperationException();
        
        return new Value();
    }
}

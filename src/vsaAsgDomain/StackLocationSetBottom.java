package vsaAsgDomain;

import abstractBoolean.AbstractBoolean;

public class StackLocationSetBottom implements IStackLocationSet 
{
	/**
	 * @see IStackLocationSet#add(IStackLocationSet)
	 */
	public Value add(IStackLocationSet rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#add(IReducedIntervalCongruence)
	 */
	public Value add(IReducedIntervalCongruence rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#addToSet(StackLocation)
	 */
	public void addToSet(StackLocation node) 
	{
		throw new Error("Operation not supported");
	}

	/**
	 * @see IStackLocationSet#clear()
	 */
	public void clear() 
	{
	}

	/**
	 * @see IStackLocationSet#clone()
	 */
	public Object clone()
	{
		//try
		try
		{
			return super.clone();
		}
		catch(CloneNotSupportedException e)
		{
			throw new Error("This should never happen");
		}
	}
	
	 /**
     * @see IStackLocationSet#equals(IStackLocationSet rhs)
     */
    public boolean equals(final Object rhs)
    {
    	return rhs instanceof StackLocationSetBottom;
    }
    
	
	/**
	 * @see IStackLocationSet#divide(IStackLocationSet)
	 */
	public Value divide(IStackLocationSet rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#divide(IReducedIntervalCongruence)
	 */
	public Value divide(IReducedIntervalCongruence rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#has(StackLocation)
	 */
	public boolean has(StackLocation item) 
	{
		return false;
	}

	/**
	 * @see IStackLocationSet#intersection(IStackLocationSet)
	 */
	public IStackLocationSet intersection(IStackLocationSet rhs) 
	{
		return new StackLocationSet();
	}

	/**
	 * @see IStackLocationSet#isDefined()
	 */
	public boolean isDefined() 
	{
		return false;
	}

	/**
	 * @see IStackLocationSet#isEqualTo(IStackLocationSet)
	 */
	public AbstractBoolean isEqualTo(IStackLocationSet rhs) 
	{
		if(rhs instanceof StackLocationSetBottom)
			return AbstractBoolean.True;
		else
			return AbstractBoolean.False;
	}

	/**
	 * @see IStackLocationSet#isGreaterThan(IStackLocationSet)
	 */
	public boolean isGreaterThan(IStackLocationSet rhs) 
	{
		return false;
	}

	/**
	 * @see IStackLocationSet#isGreaterThan(IReducedIntervalCongruence)
	 */
	public boolean isGreaterThan(IReducedIntervalCongruence rhs) 
	{
		return false;
	}

	/**
	 * @see IStackLocationSet#isLessThan(IStackLocationSet)
	 */
	public boolean isLessThan(IStackLocationSet rhs) 
	{
		return false;
	}

	/**
	 * @see IStackLocationSet#isLessThan(IReducedIntervalCongruence)
	 */
	public boolean isLessThan(IReducedIntervalCongruence rhs) 
	{
		return false;
	}

	/**
	 * @see IStackLocationSet#merge(IStackLocationSet)
	 */
	public IStackLocationSet merge(IStackLocationSet rhs) 
	{
		return (IStackLocationSet)rhs.clone();
	}

	/**
	 * @see IStackLocationSet#multiply(IStackLocationSet)
	 */
	public Value multiply(IStackLocationSet rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#multiply(IReducedIntervalCongruence)
	 */
	public Value multiply(IReducedIntervalCongruence rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#overapproximates(IStackLocationSet)
	 */
	public boolean overapproximates(IStackLocationSet rhs) 
	{
		return rhs instanceof StackLocationSetBottom || rhs.size()==0;
	}

	/**
	 * @see IStackLocationSet#iterator()
	 */
	public IStackLocationSet.Iterator iterator() 
	{
		return new Iterator();
	}
	
	public class Iterator implements IStackLocationSet.Iterator
	{
		public boolean hasNext() { return false; }
		public boolean hasPrevious() { return false; }
		public void    remove()  { throw new Error("Operation not supported"); }
		public StackLocation next() { throw new Error("operation not supported"); }
		public StackLocation previous() { throw new Error("operation not supported"); }
	}

	/**
	 * @see IStackLocationSet#size()
	 */
	public int size() 
	{
		return 0;
	}

	/**
	 * @see IStackLocationSet#subtract(IStackLocationSet)
	 */
	public Value subtract(IStackLocationSet rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#subtract(IReducedIntervalCongruence)
	 */
	public Value subtract(IReducedIntervalCongruence rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#toArray()
	 */
	public Object[] toArray() 
	{
		return new StackLocation[0];
	}

  /**
   * String representation.
   */
	//TODO: fix the hack bottom to top
  public String toString()
  {
  	return "{}";
  }
	
	/**
	 * @see IStackLocationSet#widen(IStackLocationSet)
	 */
	public IStackLocationSet widen(IStackLocationSet rhs) 
	{
		return (IStackLocationSet)rhs.clone();
	}

	/**
	 * @see IStackLocationSet#xor(IReducedIntervalCongruence)
	 */
	public Value xor(IReducedIntervalCongruence rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#xor(IStackLocationSet)
	 */
	public Value xor(IStackLocationSet rhs) throws UndefinedOperationException 
	{
		return new Value();
	}
	
	/**
	 * @see IStackLocationSet#or(IReducedIntervalCongruence)
	 */
	public Value or(IReducedIntervalCongruence rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#or(IStackLocationSet)
	 */
	public Value or(IStackLocationSet rhs) throws UndefinedOperationException 
	{
		return new Value();
	}
	
	/**
	 * @see IStackLocationSet#or(IReducedIntervalCongruence)
	 */
	public Value and(IReducedIntervalCongruence rhs) throws UndefinedOperationException 
	{
		return new Value();
	}

	/**
	 * @see IStackLocationSet#or(IStackLocationSet)
	 */
	public Value and(IStackLocationSet rhs) throws UndefinedOperationException 
	{
		return new Value();
	}
}

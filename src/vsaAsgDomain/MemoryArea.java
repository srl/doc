/*
 * MemoryArea.java
 * Created on Apr 14, 2005
 */
package vsaAsgDomain;

/**
 * Represents a piece of storage.  Registers, stack-locations, and any other place
 * for storing values is a MemoryArea.
 */
public class MemoryArea implements Cloneable
{
    /**
     * Creates a new unitialized memory area.
     */
    public MemoryArea()
    {
    }
}

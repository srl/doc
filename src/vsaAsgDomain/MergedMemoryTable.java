package vsaAsgDomain;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Maintains MemoryAra-Value pairs, and also merges the values contains
 * in two IMemoryTables.
 */
public class MergedMemoryTable implements IMemoryTable 
{
	protected IMemoryTable			leftparent 	= null;
  protected IMemoryTable 			rightparent = null;
  protected HashMap<MemoryArea, Value>  values 			= new HashMap<MemoryArea, Value>();
  
  private		List<MemoryArea>		memoryAreas;   

	/**
	 * Creates this, with left and right parents initialized.  The left
	 * and right parents specify the two IMemoryTables that this table
	 * merges.  Although they are called left and right, the actual
	 * order of the parameters is not important.t
	 */
	public MergedMemoryTable(IMemoryTable leftparent, IMemoryTable rightparent, List<MemoryArea> memoryAreas)
	{
		this.leftparent = leftparent;
		this.rightparent = rightparent;
		this.memoryAreas = memoryAreas;
	}

	/**
	 * @see IMemoryTable#addValue(Value)
	 */
	public MemoryArea addValue(Value value) 
	{
    	MemoryArea area = new MemoryArea();
    	memoryAreas.add(area);
    	values.put(area, value);
    	return area;
	}

	/**
	 * @see IMemoryTable#clear()
	 */
	public void clear() 
	{
		leftparent = new RootMemoryTable();
		rightparent = new RootMemoryTable();
		values.clear();
	}

	/**
	 * @see IMemoryTable#clone()
	 */
	public Object clone()
	{
    	MemoryTable clone = new MemoryTable(this, new ValueLookupTable(), memoryAreas);
    	return clone;
	}
	
	/**
	 * @see IMemoryTable#getFullMap()
	 */
	public Map<MemoryArea, Value> getFullMap() 
	{
    	Map<MemoryArea, Value> result = leftparent.getFullMap();
    	result.putAll(rightparent.getFullMap());
    	result.putAll(values);
    	return result;		
	}

	/**
	 * @see IMemoryTable#getValue(MemoryArea)
	 */
	public Value getValue(MemoryArea source) 
	{
		IMemoryTable currentTable = this;
		Value result = null;

		while(result == null)
		{
			if(currentTable instanceof RootMemoryTable)
			{
				result = new Value();
				result.setToBottom();
			}			
			else if(currentTable instanceof MemoryTable)
			{
				result = (Value) ((MemoryTable)currentTable).values.get(source);
				if(result == null)
					currentTable = ((MemoryTable)currentTable).parent;
			}
	
			else if(currentTable instanceof MergedMemoryTable)
			{
				result = (Value) ((MergedMemoryTable)currentTable).values.get(source);
				if(result == null)
				{
					// if result not available here, 
					// return merger from both parents.
					Value leftresult = ((MergedMemoryTable)currentTable).leftparent.getValue(source);
					Value rightresult = ((MergedMemoryTable)currentTable).rightparent.getValue(source);
					result = (Value)leftresult.clone();
					result.merge(rightresult);
				}
			}
		}
		
  	return result;
	}

	/**
	 * @see IMemoryTable#iterator()
	 */
	public IMemoryTable.Iterator iterator() 
	{
		return new Iterator();
	}

	/**
	 * Provides methods for traversing the MemoryAreas and Values in this
   * MemoryTable.
   */
	public class Iterator implements IMemoryTable.Iterator
	{
		/**
		 * Used to traverse this MemoryTable as a set.
		 */
		private java.util.Iterator	iter 		= null;
		
		/**
		 * The HashMap is converted to a set before iterating.
		 */
		private Set valueSet 	= null;
		
		public Iterator()
		{
			// convert the map to a set so that we can use an Iterator.
			// TODO: remove
			valueSet = getFullMap().entrySet();
			iter = valueSet.iterator();
			//iter = memoryAreas.iterator();
		}
		
		public boolean hasNext()
		{
			return iter.hasNext();
		}
		
		public MemoryAreaValuePair next()
		{
			// TODO: Remove
			Map.Entry item = (Map.Entry)iter.next();
			return new MemoryAreaValuePair( (MemoryArea)item.getKey(), getValue((MemoryArea)item.getKey()));
			//MemoryArea 	area	= (MemoryArea)iter.next();
			//Value				value	= getValue(area);
			//return new MemoryAreaValuePair(area, value);
		}
	} 
    
    /**
     * @see IMemoryTable#union(IMemoryTable)
     */
	public IMemoryTable union(IMemoryTable rhs) 
	{
		return new MergedMemoryTable(this, rhs, memoryAreas);
	}

	/**
	 * @see IMemoryTable#overapproximates(IMemoryTable)
	 */
	public boolean overapproximates(IMemoryTable rhs) 
	{
    	boolean result = true;
    	
   		IMemoryTable.Iterator iter = rhs.iterator();
   		while(iter.hasNext() && result==true)
   		{
   			MemoryAreaValuePair pair = iter.next();
   			MemoryArea memoryarea = pair.getMemoryArea();
   			Value rhsValue = pair.getValue();
   			Value thisValue = getValue(memoryarea);
			if( !thisValue.overapproximates(rhsValue) )
			{
				result = false;
			}
   		}
   	
	   	return result;
	}

	/**
	 * @see IMemoryTable#setValue(MemoryArea, Value)
	 */
	public void setValue(MemoryArea dest, Value value) 
	{        
    	values.put(dest, value);
	}

	/**
	 * @see IMemoryTable#size()
	 */
	public int size() 
	{
		return getFullMap().size();
	}

	/**
	 * String representation
	 */
	public String toString()
	{
    	IMemoryTable.Iterator iter 	= iterator();
    	IMemoryTable.MemoryAreaValuePair pair;
    	StringBuffer result 	= new StringBuffer();

    	if(iter.hasNext())
    	{
    		pair = iter.next();
    		result.append(pair.getMemoryArea() + " --> " + pair.getValue());

    		while(iter.hasNext())
	    	{
        		pair = iter.next();
        		result.append("\r\n" + pair.getMemoryArea() + " --> " + pair.getValue());
	    	}
    	}
    	
    	return result.toString();
	}
	
	/**
	 * @see IMemoryTable#widen(IMemoryTable)
	 */
	public IMemoryTable widen(IMemoryTable rhs) 
	{
    	IMemoryTable result = (IMemoryTable)this.clone();
    	
    	IMemoryTable.Iterator iter = rhs.iterator();
    	
    	while(iter.hasNext())
    	{
    		MemoryAreaValuePair pair = iter.next();
    		
    		Value thisValue = (Value)getValue(pair.getMemoryArea()).clone();
    		Value rhsValue = pair.getValue();
    		thisValue.widen(rhsValue);
    		result.setValue(pair.getMemoryArea(), thisValue); 
    	}
    			
    	return result;
	}
}

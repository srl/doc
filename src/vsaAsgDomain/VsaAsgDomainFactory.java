package vsaAsgDomain;

import domain.DomainFactory;
import domain.State;

/**
 * Creates objects targeted for the Vsa-Asg domain.
 */
public class VsaAsgDomainFactory extends DomainFactory 
{
	// The on and only instance of this.
	static private DomainFactory instance = null;
	
	private VsaAsgDomainFactory() {};
	
	/**
	 * @see DomainFactory#createState()
	 */
	public State createState() 
	{
		return new VsaAsgState();
	}

	/**
	 * @see DomainFactory#createValue()
	 */
	public domain.Value createValue() 
	{
		return new vsaAsgDomain.Value();
	}

	/**
	 * @see DomainFactory#createValue(int)
	 */
	public domain.Value createValue(int init)
	{
		return new vsaAsgDomain.Value(init);
	}

	/**
	 * @see DomainFactory#createValue(int, int)
	 */
	public domain.Value createValue(int start, int end)
	{
		vsaAsgDomain.Value result = new vsaAsgDomain.Value(
				new ReducedIntervalCongruence(1, start, end, 0) );
		return result;
	}
	
	/**
	 * Returns the one and only instance of VsaAsgDomainFactory
	 */
	static public DomainFactory getInstance()
	{
		if(instance == null)
			instance = new VsaAsgDomainFactory();
		return instance;
	}
}

/*
 * NodeChain.java
 * Created on May 18, 2005
 */
package vsaAsgDomain;

import java.util.LinkedList;
import java.util.List;

/**
 * Represents a chain of nodes in an AbstractStackGraph.  Intended to be
 * used by the interpreter to assist in correctly appending to the
 * graph when encountering loops in the CFG.  
 * @see NodeCreatorTable
 */
public class NodeChain
{
    /**
     * The bottom of the chain.
     */
    private StackLocation bottom;

    /**
     * The top of the chain.
     */
    private StackLocation top;	
    
    /**
     * Contains all nodes in this chain, including top & bottom.
     */
    private List<StackLocation> nodes = new LinkedList<StackLocation>(); 
    
    /**
     * Creates a new node chain that is not unitialized.
     */
    public NodeChain()
    {
    }
    
    /**
     * Creates a new node chain.  If the chain is only one node long, then
     * top should be the same node as bottom.
     * @param top the top node of the chain.
     * @param bottom the bottom node of the chain.
     */
    public NodeChain(final StackLocation top, final StackLocation bottom)
    {
        if(top == null || bottom == null)
            throw new IllegalArgumentException("top/bottom must not be null");
           
        this.top = top;
        this.bottom = bottom;
        
        add(top);
        add(bottom);
    }

    /**
     * Adds a node to this chain.  Duplicates are 
     * quietly discarded.
     */
    public void add(StackLocation node)
    {
    	if(!contains(node))
    		nodes.add(node);
    }
    
    /**
     * Returns true if this chain contains the input element.
     */
    public boolean contains(StackLocation node)
    {
    	return nodes.contains(node);
    }
    
    /**
     * Returns the node that is at the bottom of this chain of nodes.
     */
    public StackLocation getBottom()
    {
        return bottom;
    }
    
    /**
     * Returns the node that is at the top of this chain of nodes.
     */
    public StackLocation getTop()
    {
        return top;
    }
    
    /**
     * Returns an iterator for this chain.
     */
    public java.util.Iterator iterator()
    {
    	return nodes.iterator();
    }

    /**
     * Assigns a node to the bottom of the chain.
     * The previous bottom is NOT removed from this chain.
     */
    public void setBottom(StackLocation node)
    {
        bottom = node;
        add(node);
    }
    
    /**
     * Assigns a node to the top of the chain.
     * The previous top is NOT removed from this chain.
     */
    public void setTop(StackLocation node)
    {
        top = node;
        add(top);
    }

    /**
     * Returns the number of nodes in this chain.
     */
    public int size()
    {
    	return nodes.size();
    }

    /**
     * String representation
     */
    public String toString()
    {
    	return nodes.toString();
    }
}

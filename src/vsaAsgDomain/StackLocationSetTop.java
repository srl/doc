/*
 * UndefinedStackLocationSet.java
 * Created on Mar 16, 2005
 */
package vsaAsgDomain;

import abstractBoolean.AbstractBoolean;


/**
 * Represents a StackLocationSet that has not yet been given a value.
 * <p>
 * The UndefinedStackLocation may not support all of the operations
 * supported by the DefinedStackLocation.  Operations not supported
 * throw an Error.
 * 
 * @see vsaAsgDomain.IStackLocationSet
 */
public class StackLocationSetTop implements IStackLocationSet
{
    /**
     * Creates a new UndefinedStackLocationSet.
     */
    public StackLocationSetTop()
    {
    }
    
    /**
     * Adds this set with a stack location set.  
     * Because this set is undefined, the result is always an undefined Value. 
     * @return this + rhs 
     */
    public Value add(final IStackLocationSet rhs)
    {
        return new Value();
    }
 
    /**
     * Adds this set with an RIC.  Because this set is undefined,
     * the result is always an undefined Value. 
     * @return this + rhs
     */
    public Value add(final IReducedIntervalCongruence rhs)
    {
        return new Value();
    }
    
    /**
     * Adds a StackLocation to this set.  Duplicates are not added.
     * @param node StackLocation to be added to this set.
     */
    public void addToSet(final StackLocation node)
    { throw new Error("Cannot add to an undefined set."); }
    
    /**
     * Removes all elements from this set.
     */
    public void clear() {};
    
    /**
     * Returns a copy of this object.
     */
    public Object clone()
    {
        try
        {
            StackLocationSetTop copy = (StackLocationSetTop)super.clone();
            return copy;
        }
        catch(CloneNotSupportedException e)
        {
            throw new Error("This should never happen");
        }
    }
    
    /**
     * Divides this set by a stack location set.
     * Because this set is undefined, the result is always an undefined value.
     * @return this / rhs 
     */
    public Value divide(final IStackLocationSet rhs)
    {
        return new Value();
    }

    /**
     * Divides this set by a defined RIC.
     * Because this set is undefined, the result is always an undefined value.
     * @return this / rhs
     */
    public Value divide(final IReducedIntervalCongruence rhs)
    {
        return new Value();
    }
    
    /**
     * Determines if this set equals rhs.
     * @param rhs set to compare this set to .
     * @return true if the two sets are equal.
     */
    public boolean equals(Object rhs)
    {
    	/**
    	 * TODO: COMMENT: is equal of rhs is not defined. 
    	 * That doesn't sound right
    	 */
        return !((IStackLocationSet)rhs).isDefined();    
    }
    
    /**
     * Determines if this set already contains a particular
     * StackLocation.  Since this is an UndefinedStackLocation, 
     * the result is always false.
     * @return true if item is in this set.
     */
    public boolean has(final StackLocation item)
    {
        return false;
    }
    
    /**
     * Returns true if this StackLocation is defined.  Since this
     * is an UndefinedStackLocation, this function always returns false. 
     */
    public boolean isDefined() 
    { 
        return false; 
    }
    
    /**
     * Merges another StackLocationSet with this one.  
     * @return a StackLocationSet containing all values in this
     * 		set along with the values in rhs, duplicates removed.
     */
    public IStackLocationSet merge(final IStackLocationSet rhs)
    {
        return (IStackLocationSet)rhs.clone();
    }
    
    /**
     * @see IStackLocationSet
     */
    public AbstractBoolean isEqualTo(IStackLocationSet rhs)
    {
    	return AbstractBoolean.Top;    		
    }
    
    /**
     * Multiplies this StackLocationSet by another StackLocationSet.
     * Because this set is undefined, the result is always an undefined value.
     * @return this * rhs 
     */
    public Value multiply(final IStackLocationSet rhs)
    {
        return new Value();
    }

    /**
     * Multiplies this StackLocationSet by a RIC.
     * Because this set is undefined, the result is always an undefined value. 
     * @return this * rhs
     */
    public Value multiply(final IReducedIntervalCongruence rhs)
    {
        return new Value();
    }

    /**
     * Returns true if this stacklocation set overapproximates rhs.
     */
    public boolean overapproximates(final IStackLocationSet rhs)
    {
        return true;
    }
   
    /**
     * Returns true if this > rhs.  A StackLocationSet A is considered 
     * greater than an RIC B if there is at least one member in A that 
     * is greater than at least one member in B. 
     */
    public boolean isGreaterThan(final IReducedIntervalCongruence rhs)
    {
        return true;
    }

    /**
     * Returns true if this > rhs.  A StackLocationSet A is considered 
     * greater than another StackLocationSet B if there is at least one 
     * member in A that is greater than at least one member in B. 
     */
    public boolean isGreaterThan(final IStackLocationSet rhs)
    {
        return true;
    }
    
    /**
     * Returns true if this < rhs.  A StackLocationSet A is considered 
     * less than an RIC B if there is at least one member in A that 
     * is less than at least one member in B. 
     */
    public boolean isLessThan(final IReducedIntervalCongruence rhs)
    {
        return true;
    }

    /**
     * Returns true if this < rhs.  A StackLocationSet A is considered 
     * less than another StackLocationSet B if there is at least one 
     * member in A that is less than at least one member in B. 
     */
    public boolean isLessThan(final IStackLocationSet rhs)
    {
        return true;
    }
    
    /**
     * @see IStackLocationSet#intersection(IStackLocationSet)
     */
    public IStackLocationSet intersection(IStackLocationSet rhs)
    {
    	StackLocationSet result = new StackLocationSet();
    	
    	IStackLocationSet.Iterator iter = rhs.iterator();
    	while(iter.hasNext())
    		result.addToSet(iter.next());

    	return result;
    }
    
    /**
     * Returns an iterator that can be used to traverse the
     * Stacklocations in this set.
     */
    public IStackLocationSet.Iterator iterator() { return new Iterator(); }
    public class Iterator implements IStackLocationSet.Iterator
    {
        public boolean hasNext()  		{ return false; }
        public boolean hasPrevious()  		{ return false; }
        public StackLocation next()		{ throw new Error("Should never happen"); }
        public StackLocation previous()		{ throw new Error("Should never happen"); }
        public void remove()			{ throw new Error("Should never happen"); }
    }
    
    /**
     * Returns the number of elements in this set.  This number is always 
     * zero for an UndefinedStackLocationSet.
     */
    public int size()
    {
        return 0;
    }

    /**
     * Subtracts a StackLocationSet from this StackLocationSet.  
     * Because this set is undefined, the result is always an undefined value.
     * @return this - rhs
     */
    public Value subtract(final IStackLocationSet rhs)
    {
        return new Value();
    }

    /**
     * Subtracts an RIC from this StackLocationSet.  
     * Because this set is undefined, the result is always an undefined value.
     * @return this - rhs
     */
    public Value subtract(final IReducedIntervalCongruence rhs)
    {
        return new Value();
    }

    /**
     * Returns an array containing all stack-locations in this set.
     */
    public Object[] toArray()
    {
        return new StackLocation[0];
    }
    
    /**
     * Returns a string representation of this StackLocationSet
     */
    public String toString()
    {
        return "T";
    }

    /**
     * Widens this stack location set.
     * @return the widened set.
     */
    public IStackLocationSet widen(final IStackLocationSet rhs)
    {
        return (IStackLocationSet)this.clone();
    }

    /**
     * Returns the xor of this and rhs.
     */
    public Value xor(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        return new Value();
    }

    /**
     * Returns the xor of this and rhs.
     */
    public Value xor(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        return new Value();
    }
    
    /**
     * Returns the or of this and rhs.
     */
    public Value or(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        return new Value();
    }

    /**
     * Returns the or of this and rhs.
     */
    public Value or(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        return new Value();
    }
    
    /**
     * Returns the and of this and rhs.
     */
    public Value and(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        return new Value();
    }

    /**
     * Returns the and of this and rhs.
     */
    public Value and(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        return new Value();
    }
}
/*
 * MemoryTable.java
 * Created on Apr 14, 2005
 */
package vsaAsgDomain;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A table of MemoryArea->Value mappings.
 */
public class MemoryTable implements Cloneable, IMemoryTable
{
    private			ValueLookupTable	cache;		
    protected 	IMemoryTable 			parent 	= new RootMemoryTable();
    protected 	HashMap<MemoryArea, Value> values 	= new HashMap<MemoryArea, Value>();
    
    private 		List<MemoryArea>	memoryAreas;
    
    /**
     * Creates a new MemoryTable with the previous memory table in 
     * the chain set to RootMemoryTable.
     */
    public MemoryTable()
    {
    	cache = new ValueLookupTable();
    	memoryAreas = new LinkedList<MemoryArea>();
    }
    
    /**
     * Creates a new MemoryTable with the previous memory table in 
     * the chain set to parent.
     */
    public MemoryTable(IMemoryTable parent, ValueLookupTable cache, List<MemoryArea> memoryAreas)
    {
    	this.parent = parent;
    	this.cache  = cache;
    	this.memoryAreas = memoryAreas;
    }
    
    /**
     * @see IMemoryTable#addValue(Value)
     */
    public MemoryArea addValue(Value value)
    {
    	MemoryArea area = new MemoryArea();
    	
    	memoryAreas.add(area);
    	values.put(area, value);
    	cache.set(area, value);
    	return area;
    }
     
    /**
     * @see IMemoryTable#clear()
     */
    public void clear()
    {
    	parent = new RootMemoryTable();
    	values.clear();
    }
    
    /**
     * Returns a clone of this MemoryTable.
     */
    public Object clone()
    {
    	MemoryTable clone = new MemoryTable();
    	clone.memoryAreas = memoryAreas;
    	clone.parent = this;
    	clone.cache  = this.cache;							// give the clone our cache object.
    	this.cache   = new ValueLookupTable();	// we'll create a new one for us. 
    	return clone;
    }
         
    /**
     * @see IMemoryTable#getFullMap()
     */
    public Map<MemoryArea, Value> getFullMap()
    {
      Map<MemoryArea, Value> result;
      
      result = new HashMap<MemoryArea, Value>();
      for(MemoryArea m : memoryAreas)
        result.put(m, getValue(m));

    	return result;
    }
    
	/**
	 * @see IMemoryTable#getValue(MemoryArea)
	 */
	public Value getValue(MemoryArea source)
	{
		IMemoryTable 	currentTable = this;
		Value 			result 		 = null;
		
		// First, search the cache for this value.
		result = cache.get(source);
		if(result != null)
			return result;
		
		// if it is not in our cache, then search our parent MemoryTables
		while(result == null)
		{
			if(currentTable instanceof RootMemoryTable)
			{
				result = new Value();
				result.setToBottom();
			}			
			else if(currentTable instanceof MemoryTable)
			{
				result = (Value) ((MemoryTable)currentTable).values.get(source);
				if(result == null)
					currentTable = ((MemoryTable)currentTable).parent;
			}
	
			else if(currentTable instanceof MergedMemoryTable)
			{
				result = (Value) ((MergedMemoryTable)currentTable).values.get(source);
				if(result == null)
				{
					// if result not available here, 
					// return merger from both parents.
					Value leftresult = ((MergedMemoryTable)currentTable).leftparent.getValue(source);
					Value rightresult = ((MergedMemoryTable)currentTable).rightparent.getValue(source);
					result = (Value)leftresult.clone();
					result.merge(rightresult);
				}
			}
		}
		
		cache.set(source, result);
		return result;    	
	}
    
    /**
     * @see IMemoryTable#iterator()
     */
    public IMemoryTable.Iterator iterator()
    {
    	return new Iterator();
    }
    
    /**
     * Provides methods for traversing the MemoryAreas and Values in this
     * MemoryTable.
     */
    public class Iterator implements IMemoryTable.Iterator
    {
    	/**
    	 * Used to traverse this MemoryTable as a set.
    	 */
    	private java.util.Iterator	iter 		= null;
    	
    	/**
    	 * The HashMap is converted to a set before iterating.
    	 */
    	private Set valueSet 	= null;
    	
    	public Iterator()
    	{
    		// convert the map to a set so that we can use an Iterator.
    		valueSet = getFullMap().entrySet();
    		iter = valueSet.iterator();
    		
    		//iter = memoryAreas.iterator();
    	}
    	
    	public boolean hasNext()
    	{
    		return iter.hasNext();
    	}
    	
    	public MemoryAreaValuePair next()
    	{
    		// TODO: Remove
    		Map.Entry item = (Map.Entry)iter.next();
			  return new MemoryAreaValuePair( (MemoryArea)item.getKey(), (Value)item.getValue());
    		//MemoryArea 	area 	= (MemoryArea)iter.next();
    		//Value				value = getValue(area);
    		//return new MemoryAreaValuePair(area, value);
    	}
    }
    
    /**
     * @see IMemoryTable#union(IMemoryTable)
     */
    public IMemoryTable union(IMemoryTable rhs)
    {
    	IMemoryTable result = (IMemoryTable)this.clone();
    	
    	IMemoryTable.Iterator iter = rhs.iterator();
    	
    	while(iter.hasNext())
    	{
    		MemoryAreaValuePair pair = iter.next();
    		
    		Value thisValue = (Value)getValue(pair.getMemoryArea()).clone();
    		Value rhsValue = pair.getValue();
    		if (thisValue.isDefined())	thisValue.merge(rhsValue);
    		else thisValue = (Value) rhsValue.clone();
    		result.setValue(pair.getMemoryArea(), thisValue); 
    	}
    			
    	return result;
    	
    	
    	
    	
    	//return new MergedMemoryTable(this, rhs, memoryAreas);
    }
    
    /**
     * @see IMemoryTable#overapproximates(IMemoryTable)
     */
    public boolean overapproximates(final IMemoryTable rhs)
    {
    	boolean result = true;
    	
   		IMemoryTable.Iterator iter = rhs.iterator();
   		while(iter.hasNext() && result==true)
   		{
   			MemoryAreaValuePair pair = iter.next();
   			MemoryArea memoryarea = pair.getMemoryArea();
   			Value rhsValue = pair.getValue();
   			Value thisValue = getValue(memoryarea);
   			if( !thisValue.overapproximates(rhsValue) )
   				result = false;
   		}
   	
	   	return result;
    }
    
    /**
     * @see IMemoryTable#setValue(MemoryArea, Value)
     */
    public void setValue(MemoryArea dest, Value value)
    {
    	values.put(dest, value);
    	cache.set(dest, value);
    }

    /**
     * @see IMemoryTable#size()
     */
    public int size()
    {
    	return getFullMap().size();
    }

    /**
     * Returns a string representation of this memory table.
     */
    public String toString()
    {
    	IMemoryTable.Iterator iter 	= iterator();
    	IMemoryTable.MemoryAreaValuePair pair;
    	StringBuffer result 	= new StringBuffer();

    	if(iter.hasNext())
    	{
    		pair = iter.next();
    		result.append(pair.getMemoryArea() + " --> " + pair.getValue());

    		while(iter.hasNext())
	    	{
        		pair = iter.next();
        		result.append("\r\n" + pair.getMemoryArea() + " --> " + pair.getValue());
	    	}
    	}
    	
    	return result.toString();
    }

    /**
     * @see IMemoryTable#widen(IMemoryTable)
     */
    public IMemoryTable widen(final IMemoryTable rhs)
    {
    	IMemoryTable result = (IMemoryTable)this.clone();
    	
    	IMemoryTable.Iterator iter = rhs.iterator();
    	
    	while(iter.hasNext())
    	{
    		MemoryAreaValuePair pair = iter.next();
    		
    		Value thisValue = (Value)getValue(pair.getMemoryArea()).clone();
    		Value rhsValue = pair.getValue();
    		if (thisValue.isDefined())	thisValue.widen(rhsValue);
    		else thisValue = (Value) rhsValue.clone();
    		result.setValue(pair.getMemoryArea(), thisValue); 
    	}
    			
    	return result;
    }
}

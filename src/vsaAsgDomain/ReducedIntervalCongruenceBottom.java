package vsaAsgDomain;

import abstractBoolean.AbstractBoolean;

/**
 * Represents an RIC that does not hold any value.  This is intended to be
 * used for values that have never been initialized.  For instance, the
 * MemoryTable may return this in cases where a request was made for a 
 * MemoryArea that does not exist.
 */
public class ReducedIntervalCongruenceBottom implements
		IReducedIntervalCongruence 
{
	/**
	 * @see IReducedIntervalCongruence#add(IReducedIntervalCongruence)
	 */
	public Value add(final IReducedIntervalCongruence rhs)
	{
		return new Value(rhs);
	}
	
	/**
	 * @see IReducedIntervalCongruence#add(IStackLocationSet)
	 */
	public Value add(final IStackLocationSet rhs) throws UndefinedOperationException
	{
		return new Value(rhs);
	}
	
    /**
     * Returns a clone of this RIC.
     */
    public Object clone()
    {
    	try
    	{
    		return super.clone();
    	}
    	catch(CloneNotSupportedException e)
    	{
    		throw new Error("This should never happen");
    	}
    }
	
    /**
     * @see IReducedIntervalCongruence#divide(IReducedIntervalCongruence)
     */
    public Value divide(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
    	return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#divide(IStackLocationSet)
     */
    public Value divide(final IStackLocationSet rhs) throws UndefinedOperationException
    {
    	return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#equals(IReducedIntervalCongruence)
     */
    public boolean equals(final IReducedIntervalCongruence rhs)
    {
    	return rhs instanceof ReducedIntervalCongruenceBottom;
    }
    
    /**
     * @see IReducedIntervalCongruence#getStride()
     */
    public int getStride()
    {
    	throw new Error("ReducedIntervalCongruenceBottom does not support getStride");
    }
    
    /**
     * @see IReducedIntervalCongruence#getRangeMin()
     */
    public long getRangeMin()
    {
    	throw new Error("ReducedIntervalCongruenceBotom does not support getRangeMin");
    }
    
    /**
     * @see IReducedIntervalCongruence#getRangeMax()
     */
    public long getRangeMax()
    {
    	throw new Error("ReducedIntervalCongruenceBottom does not support getRangeMax");
    }
    
    /**
     * @see IReducedIntervalCongruence#getBase()
     */
    public long getBase()
    {
    	throw new Error("ReducedIntervalCongruenceBottom does not support getBase");
    }
    
    /**
     * @see IReducedIntervalCongruence#getMaximumValue()
     */
    public long getMaximumValue()
    {
    	throw new Error("ReducedIntervalCongruenceBottom does not support getMaximumValue");
    }
    
    /**
     * @see IReducedIntervalCongruence#getMinimumValue()
     */
    public long getMinimumValue()
    {
    	throw new Error("ReducedIntervalCongruenceBottom does not support getMinimumValue");
    }
    
    /**
     * @see IReducedIntervalCongruence#isDefined()
     */
    public boolean isDefined()
    {
    	return false;
    }
    
    /**
     * @see IReducedIntervalCongruence#isEqualTo(IReducedIntervalCongruence)
     */
    public AbstractBoolean isEqualTo(IReducedIntervalCongruence rhs)
    {
    	if(rhs instanceof ReducedIntervalCongruenceBottom)
    		return AbstractBoolean.True;
    	else
    		return AbstractBoolean.False;
    }
    
    /**
     * @see IReducedIntervalCongruence#isGreaterThan(IReducedIntervalCongruence)
     */
    public AbstractBoolean isGreaterThan(final IReducedIntervalCongruence rhs)
    {
    	return AbstractBoolean.False;
    }
    
    /**
     * @see IReducedIntervalCongruence#isGreaterThan(IStackLocationSet)
     */
    public boolean isGreaterThan(final IStackLocationSet rhs)
    {
    	return false;
    }
    
    /**
     * @see IReducedIntervalCongruence#isLessThan(IReducedIntervalCongruence)
     */
    public AbstractBoolean isLessThan(final IReducedIntervalCongruence rhs)
    {
    	return AbstractBoolean.False;
    }
    
    /**
     * @see IReducedIntervalCongruence#isLessThan(IStackLocationSet)
     */
    public boolean isLessThan(final IStackLocationSet rhs)
    {
    	return false;
    }
    
    /**
     * @see IReducedIntervalCongruence#isSingleNumber()
     */
    public boolean isSingleNumber()
    {
    	return false;
    }
    
    /**
     * @see IReducedIntervalCongruence#iterator()
     */
    public IReducedIntervalCongruence.Iterator iterator()
    {
    	return new Iterator();
    }
    
    public class Iterator implements IReducedIntervalCongruence.Iterator
    {
    	public boolean hasNext() { return false; }
    	public long    next()    { throw new Error("There is not next."); }
    }
    
    /**
     * @see IReducedIntervalCongruence#merge(IReducedIntervalCongruence)
     */
    public IReducedIntervalCongruence merge(final IReducedIntervalCongruence rhs)
    {
    	return (IReducedIntervalCongruence)rhs.clone();
    }
    
    /**
     * @see IReducedIntervalCongruence#multiply(IReducedIntervalCongruence)
     */
    public Value multiply(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
    	return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#multiply(IStackLocationSet)
     */
    public Value multiply(final IStackLocationSet rhs) throws UndefinedOperationException
    {
    	return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#organize()
     */
    public void organize()
    {
    }
    	
    /**
     * @see IReducedIntervalCongruence#overapproximates(IReducedIntervalCongruence)
     */
    public boolean overapproximates(final IReducedIntervalCongruence rhs)
    {
    	return rhs instanceof ReducedIntervalCongruenceBottom;
    }
   
    /**
     * @see IReducedIntervalCongruence#set(long)
     */
    public void set(long value)
    {
    }
    
    /**
     * @see IReducedIntervalCongruence#set(long, int, long, long)
     */
    public void set(long base, int stride, long intervalmin, long intervalmax)
    {
    }
    
    /**
     * @see IReducedIntervalCongruence#size()
     */
    public long size()
    {
    	return 0;
    }
    
    /**
     * @see IReducedIntervalCongruence#subtract(IReducedIntervalCongruence)
     */
    public Value subtract(final IReducedIntervalCongruence rhs)
    {
    	return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#subtract(IStackLocationSet)
     */
    public Value subtract(final IStackLocationSet rhs) throws UndefinedOperationException
    {
    	return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#toString()
     */
    
    //TODO: HACK: change Top to Bottom
    public String toString()
    {
    	return "[]";
    }
    
    /**
     * @see IReducedIntervalCongruence#widen(IReducedIntervalCongruence)
     */
    public IReducedIntervalCongruence widen(final IReducedIntervalCongruence rhs)
    {
    	return (IReducedIntervalCongruence)rhs.clone();
    }
    
    /**
     * @see IReducedIntervalCongruence#xor(IReducedIntervalCongruence)
     */
    public Value xor(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
    	//XOR, OR, AND all can't have immediate values on the left side,
    	//so I only have to worry about the right hand side (rhs).    	
    	
    	long allZeroes = 0;
		if(rhs.isSingleNumber() && rhs.getMinimumValue() == allZeroes)
    	{
    		return new Value(this);
    	}
    	
        
    	if(rhs.equals(this))
    	{
    		return new Value(allZeroes);
    	}
    	
        return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#xor(IStackLocationSet)
     */
    public Value xor(final IStackLocationSet rhs) throws UndefinedOperationException
    {
    	return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#or(IReducedIntervalCongruence)
     */
    public Value or(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
    	if(rhs.equals(this))
    	{
    		return new Value(this);
    	}
    	
    	long allZeroes = 0;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allZeroes)
    	{
    		return new Value(this);
    	}
    	
    	//XOR, OR, AND all can't have immediate values on the left side,
    	//so I only have to worry about the right hand side (rhs).    	
    	long allOnes = 4294967295L;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allOnes)
    	{
    		Value value = new Value();
    		value.setRIC(rhs);
    		return value;
    	}
    	
    	return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#or(IStackLocationSet)
     */
    public Value or(final IStackLocationSet rhs) throws UndefinedOperationException
    {
    	return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#and(IReducedIntervalCongruence)
     */
    public Value and(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
    	if(rhs.equals(this))
    	{
    		return new Value(this);
    	}
    	
    	long allOnes = 4294967295L;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allOnes)
    	{
    		return new Value(this);
    	}
    	
    	//XOR, OR, AND all can't have immediate values on the left side,
    	//so I only have to worry about the right hand side (rhs).    	
    	long allZeroes = 0;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allZeroes)
    	{
    		Value value = new Value();
    		value.setRIC(rhs);
    		return value;
    	}
        return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#and(IStackLocationSet)
     */
    public Value and(final IStackLocationSet rhs) throws UndefinedOperationException
    {
    	return new Value();
    }
}
/*
 * StackLocation.java
 * Created on Mar 11, 2005
 */
package vsaAsgDomain;


import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


import x86InstructionSet.Instruction;
import x86Interpreter.Interpreter;

import logger.Log;
import domain.Register;

/**
 * Represents a single node in the AbstrackStackGraph.  Each node stores a 
 * Value, which may be unitialized at any time.  Methods are provided for 
 * updating and performing arithmetic operations on StackLocations.
 */
public class StackLocation
{
  /**
   * Each stack-location holds 4 bytes worth of data.
   */
  private int NODE_SIZE_IN_BYTES = 4;
  
  private Instruction				creator;
  private int								id;
  private static int				lastIdUsed = -1;
  private IStackLocationSet	predecessors;
  private IStackLocationSet	successors;
  private MemoryArea				memoryArea;
  private int								nodeIndex = 0;	// index of this node in comparison to
  																					// other nodes created by the same instruction

    /**
     * Creates a new StackLocation
     */
    public StackLocation()
    {
        predecessors	= new StackLocationSet();
        successors 		= new StackLocationSet();
        id 						= ++lastIdUsed;
        
        memoryArea 		= ((VsaAsgState)Interpreter.getActiveState()).getMemoryTable().addValue(
                new Value());
    }
    
    /**
     * Adds this stack location to an ric.
     * Addition of an RIC and a stack-location outputs a set of stacklocations
     * obtained by traversing the abstract graph, starting from this 
     * stack-location, and stopping after n/4 nodes have been traversed, 
     * where n is a number included in ric. This is equivalent to adding 
     * some number to a stack address and getting some other stack address 
     * as output.
     * @param rhs Number of bytes to add to this stack-location.  For every
     * 			4 bytes added, 1 new stack-location is created.
     * @return this + rhs
     */
    public Value add(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        Value				result = new Value();
        
        if(!rhs.isDefined())
            throw new UndefinedOperationException();

        // make sure every number in the RIC is a multiple of four.  If it is
        // not, then we can't perform the addition.
        IReducedIntervalCongruence.Iterator iter = rhs.iterator();
        while(iter.hasNext())
        {
            if( !isMultipleOf( iter.next(), NODE_SIZE_IN_BYTES ) )
                throw new UndefinedOperationException();
        }
        
        // perform the addition for each individual number in the RIC
        // and then merge the results.
        iter = rhs.iterator();
        while(iter.hasNext())
        {
            long operand = iter.next();
            
            if( operand >= 0)
            {
                operand /= NODE_SIZE_IN_BYTES;
                Value r = new Value(getNthSuccessors(operand) );
                if(result.isDefined() && r.isDefined())
                {
                    result.merge(r);
                }
                else if(r.isDefined() && !result.isDefined())
                    result = r;
            }
            else
            {
                IReducedIntervalCongruence ric = new ReducedIntervalCongruence();
                ric.set( -operand );
                Value r = new Value(subtract(ric).getStackLocations());
                if(result.isDefined() && r.isDefined())
                {
                    result.merge(r);
                }
                else if(r.isDefined() && !result.isDefined())
                    result = r;
            }
        }
        return result;
    }

    /**
     * Adds a stack location to this stack location.  
     * @return this + rhs
     */
    public Value add(final StackLocation rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }

    /**
     * Adds this stack location with each member in a set of stack locations.
     * @return this + rhs
     */
    public Value add(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }
    
    /**
     * Adds a predecessor to the set of predecessors for this node.
     */
    public void addPredecessor(final StackLocation predecessor)
    {
        predecessors.addToSet(predecessor);
    }

    /**
     * Adds a successor to the set of successors for this node.
     */
    public void addSuccessor(final StackLocation successor)
    {
        successors.addToSet(successor);
    }

    /**
     * Divides a StackLocation by an RIC.
     * @return this / rhs  
     */
    public Value divide(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }

    /**
     * Divides this StackLocation by a StackLocationSet.
     * @return this / rhs
     */
    public Value divide(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }

    /**
     * Divides this StackLocation by an a StackLocation.
     * @return this / rhs  
     */
    public Value divide(final StackLocation rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }
    
    /**
     * Returns true if this StackLocation equals rhs.
     */
    public boolean equals(final StackLocation rhs)
    {
        return id == rhs.id;
    }
    
    /**
     * Returns the instruction that caused this node to be created.
     * @return Address of creator.
     */
    public Instruction getCreator()
    {
        return creator;
    }
    
    /**
     * Returns an ID that can be used to distinquish
     * this stack-location from another stack-location.
     */
    public int getId()
    {
    	return id;
    }
    
    /**
	 * Return the name of this stack-location.  A name is
	 * used purely to assist the user visually, and serves
	 * no purpose internally.
  	 */
    public String getDisplayName()
    {
    	if(creator == null)
    		return "BOTTOM";
    	else
    	{
    		String name;
    		if(nodeIndex <= 1)
    			name = creator.getAddress().toString();
    			// return Integer.toString(creator.getIndex() + 1);
    		else
    			name = creator.getAddress().toString() + " - " + nodeIndex;
    			// return Integer.toString(creator.getIndex() + 1) + " - " + nodeIndex;
    		name = "[" + creator.toString() + "]";
    		return name;
    	}
    }
    
    /**
     * Returns the set of all StackLocations that immediately precede this one.
     */
    public IStackLocationSet getPredecessors()
    {
        return predecessors;
    }
    
    /**
     * Returns the set of all StackLocations that immediately succeed this one. 
     */
    public IStackLocationSet getSuccessors()
    {
        return successors;
    }
    
    /**
     * Returns the value associated with this node.
     */
    public MemoryArea getMemoryArea()
    {
        return memoryArea;
    }

    /**
     * Multiplies a StackLocation by an RIC. 
     * @return this * rhs
     */
    public Value multiply(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }

    /**
     * Multiplies a StackLocation by a StackLocationSet  
     */
    public Value multiply(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }
    
    /**
     * Multiplies a StackLocation by another StackLocation  
     */
    public Value multiply(final StackLocation rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }
   
    /**
     * Resets the internal id tracker back to it's initial value.  
     */
    public static void resetId()
    {
        lastIdUsed = -1;
    }
    
    /**
     * Sets the instruction that caused this StackLocation to be
     * created.
     */
    public void setCreator(Instruction creator)
    {
    	this.creator = creator;
    }
    
    /**
     * Index of this node in comparison to other nodes created by the 
     * same instruction. If an instruction causes three nodes to be
     * created, then the index values 1, 2, and 3 will be used.
     */
    public void setNodeIndex(int index)
    {
    	nodeIndex = index;
    }
    
    /**
     * Subtracts a ReducedIntervalCongruence from this StackLocation
     * @return this - rhs
     */
    public Value subtract(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        if(!rhs.isDefined())
            throw new UndefinedOperationException();
        
        VsaAsgState state = (VsaAsgState)Interpreter.getActiveState();
                
        // result = Value that resulted from the subtraction.
        Value result = new Value();
        
        // make sure every number in the RIC is a multiple of four.  If it is
        // not, then we can't perform the subtraction.
        IReducedIntervalCongruence.Iterator ricIter = rhs.iterator();
        while(ricIter .hasNext())
        {
            if( !isMultipleOf( ricIter .next(), NODE_SIZE_IN_BYTES ) )
                throw new UndefinedOperationException();
        }
        
        // Since we must reuse the previously created nodes, we connect edges from
        // the bottom node that was previously created to the parameter 'start'.
        NodeChain chain;
        chain = state.getAbstractStackGraph().getNodesCreatedBy(Interpreter.getActiveInstruction());
        if(chain != null)
        {
        	/* THE NODE WAS PREVIOUSLY VISITED. */
        	if(chain.size() != (rhs.getMinimumValue() / NODE_SIZE_IN_BYTES))
        	{
            final String WARNING_MSG = "subtract() - Subtracting from stack-location, 2nd time for this " +
                "instruction.  The amount being subtracted has changed and I don't know how to " +
                "handle this situation. Resetting abstract stack graph.";
            Log.write(Log.WARNING, WARNING_MSG);

            return new Value(state.getAbstractStackGraph().getBottom());
        	}
       
          // if this node does not already have an edge from the bottom
          // stack node of the current active instruction, then add an edge.
          if( !chain.getBottom().getSuccessors().has(this) &&
                  !this.getPredecessors().has(chain.getBottom()) )
          {
            state.getAbstractStackGraph().addEdge(chain.getBottom(), this);
          }
            
          // now set the resulting value equal to the top nodes that were previously created.
          result.set(new StackLocationSet());
          result.getStackLocations().addToSet(chain.getTop());
          return result;
       }
        
        /*********  THE NODE WAS PREVIOUSLY NOT VISITED. ************ */
        IStackLocationSet nodesInPathToTop = stackNodesInPathToTop(state);
        
        /*
         * For each number represented by the RIC, perform the subtraction and merge
         * the results together.  For each subtraction, we make sure that we only 
         * travel up along nodes that could take us to the stack top (getNthSuccessor
         * does this for us). 
         */
        IReducedIntervalCongruence.Iterator operandIter = rhs.iterator();
        while(operandIter.hasNext())
        {
            long operand = operandIter.next();
            if(operand >= 0)
            {
                operand /= NODE_SIZE_IN_BYTES;
                Value r = new Value(getNthPredecessors(operand, nodesInPathToTop));
                if(result.isDefined() && r.isDefined())
                {
                    result.merge(r);
                }
                else if(r.isDefined() && !result.isDefined())
                    result = r;
            }   
            else 
            {
                IReducedIntervalCongruence ric = new ReducedIntervalCongruence();
                ric.set(-operand);
                Value r = add(ric);
                if(result.isDefined() && r.isDefined())
                    result.merge(r);
                else if(r.isDefined() && !result.isDefined())
                    result = r;
            }
        }
        return result;
    }

	private IStackLocationSet stackNodesInPathToTop(VsaAsgState state) {
		/* nodesInPathToTop holds all StackLocations that occur on the path
         * from this StackLocation to the top of the stack (stored in register esp).
         * When we subtract from a StackLocation, we are traveling up the stack
         * toward the stack top, but we only want to travel on nodes that are
         * on the path to the top, because if its not on this path, then its
         * not really part of the stack at this point in time.  
         */
        IStackLocationSet nodesInPathToTop = new StackLocationSet();

        // tops = Set of StackLocations that are at the top of the stack.
        IStackLocationSet tops = ((Value)Interpreter.getActiveState().getRegisterValue(Register.ESP)).
        					getStackLocations();
        tops = (IStackLocationSet)tops.clone();

        /* For each node that is the top of the stack, compute the path from this
         * node to the top and merge them all together into one set (nodesInPathToTop).
         * When done, nodesInPathToTop will contain all of the nodes that could
         * possibly lead to one of the stack tops. 
         */
        IStackLocationSet.Iterator iter = tops.iterator();
        while(iter.hasNext())
        {
            // This piece of code below was added later to handle situations where
            // there are two tops of the stack, and one of the tops is above the other
            // in the stack graph.  In this case, we choose only the lower node to
            // represent the top of the stack.  In other words, if there is a node
            // that is one of the tops and it is above another node that is one
            // of the tops, then the higher node is removed from the set of tops
            // and thus removed from furthur consideration.
	        StackLocation left = iter.next();

	        IStackLocationSet.Iterator iter2 = tops.iterator();
            while(iter2.hasNext())
            {
		        StackLocation right = iter2.next();
		        if(left != right)
		            if(state.getAbstractStackGraph().getNodesOnPathToB(left, right).size() > 0)
		            {
		                iter.remove();
		                break;
		            }
            }
        }        

        iter = tops.iterator();
        while(iter.hasNext())
        {   
        	nodesInPathToTop = nodesInPathToTop.merge( 
                    state.getAbstractStackGraph().getNodesOnPathToB(iter.next(), this));
        }
		return nodesInPathToTop;
	}
    
    /**
     * Subtracts a StackLocationSet from this StackLocation
     * @return this - rhs
     */
    public Value subtract(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }

    /**
     * Subtracts a StackLocation from this StackLocation.  This equates
     * to subtracting two addresses.
     * @return this - rhs
     */
    public Value subtract(final StackLocation rhs) throws UndefinedOperationException
    {
    	int heightOfThis = 0;		// height of this stack-location, relative to bottom	
    	int heightOfRhs = 0;		// height of rhs, relative to bottom
    	VsaAsgState state;			// the current state
    	
    	state = (VsaAsgState)Interpreter.getActiveState();
    	
    	heightOfThis 	= state.getAbstractStackGraph().getNodeHeight(this);
    	heightOfRhs		= state.getAbstractStackGraph().getNodeHeight(rhs);
    	
    	// Note: the return value is being negated below, because the node with
    	// the large height actually has the smaller address in memory.  
    	return new Value( -(heightOfThis - heightOfRhs) * NODE_SIZE_IN_BYTES );
    }
        
    /**
     * Returns the set of predecessors that are a distance of n nodes in 
     * front of this StackLocation.
     * @param n must be >= 0.
     * @return the resulting set of stack locations.
     */
    private IStackLocationSet getNthSuccessors(long n)
    {
        IStackLocationSet result 		= new StackLocationSet();
        
        if(n < 0 )
            throw new IllegalArgumentException();
        
        if(n == 0)
        {
            result.addToSet(this);
        }
        else
        {
            IStackLocationSet.Iterator iter = getSuccessors().iterator();

            // if there are no successors, then this is bottom of the stack,
            // so add it to the result set and exit.
            if(!iter.hasNext())
            {
                result.addToSet(this);
                return result;
            }
            // otherwise, get the n-1 successors for each successor of this node.
            while(iter.hasNext())
                result = result.merge(iter.next().getNthSuccessors(n-1));
        }
        return result;
    }

    /**
     * Returns the set of successors that are a distance of n nodes behind this
     * StackLocation.  Nodes that are not contained in the acceptableNodes set
     * are not traversed.  If acceptableNodes is empty, then n new nodes are added
     * to the graph.  The acceptableNodes argument is provided, because when 
     * traveling up the graph, you will most likely only want to travel on nodes
     * that lead to the top of the stack.  This argument makes that possible.
     * @param n must be >= 0
     * @param acceptableNodes set of nodes to be traversed.
     */
    private IStackLocationSet getNthPredecessors(long n,
            								 IStackLocationSet acceptableNodes)
    {
      class Element
      {
        public long count;
        public StackLocation node;
        public Element(StackLocation node, long count)
        {
          this.node = node;
          this.count = count;
        }
        public boolean equals(Object obj)
        {
          Element element = (Element)obj;
          return (count == element.count) && (node == element.node);
        }
        public String toString()
        {
          return "(" + node + ", " + count + ")";
        }
      }

      Queue<Element>	worklist = new LinkedList<Element>();
    	IStackLocationSet result = new StackLocationSet();
    	List<Element> visited = new LinkedList<Element>();
    	
    	
    	worklist.add(new Element(this, n));
    	while(!worklist.isEmpty())
    	{
    		Element element = (Element)worklist.remove();
    		if(visited.contains( element))
    			continue;
    		visited.add(element);

	      // Since we must reuse the previously created nodes, we connect edges from
	      // the bottom node that was previously created to the parameter 'start'.
	      NodeChain chain;
	      VsaAsgState state2222 = (VsaAsgState)Interpreter.getActiveState();
	      
	      Instruction activeInstruction = Interpreter.getActiveInstruction();
	      
		chain = state2222.getAbstractStackGraph().getNodesCreatedBy(activeInstruction);
	      if(chain != null)
	      {
	      	java.util.Iterator iter = chain.iterator();
	      	while(iter.hasNext())
	      	{
	      		acceptableNodes.addToSet( (StackLocation)iter.next() );
	      	}
	      }			
        
	      // if we have reached nth successor...
	      if(element.count == 0)
	      {
	        // ... then check if this node is one of the acceptable nodes. 
	        // If it is, add it to the set of results.
	        if(acceptableNodes.has(element.node))
	            result.addToSet(element.node);
	      }
	      else
	      {
	        IStackLocationSet.Iterator iter = element.node.getPredecessors().iterator();
	        boolean anyAcceptableNodes = false;
	
	        // Check if there are any successors that are considered OK to traverse. 
	        // It is OK to traverse if it is in the set acceptableNodes.
	        while(iter.hasNext())
	        {
	          if(acceptableNodes.has(iter.next()))
	          {
	            anyAcceptableNodes = true;
	            break;
	          }
	        }
	        IStackLocationSet top;
          
	        // If there are predecessors that occur in acceptableNodes...
	        top = ((Value)Interpreter.getActiveState().getRegisterValue(Register.ESP)).getStackLocations();
	        if(anyAcceptableNodes)
	        {
	          // ...for each predecessor
	          iter = ((IStackLocationSet)element.node.getPredecessors().clone()).iterator();
	          while(iter.hasNext())
	          {
	            // get the n-1 predecessor of each predecessor to this node.
	          	StackLocation next = iter.next();
	          	if(acceptableNodes.has(next))
	          	{
		          	if( top.has(element.node) && next == element.node )
		          	{
		        			NodeChain nodes =
		        				((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().getNodesCreatedBy(
		        					activeInstruction);
		        			if(nodes != null)
		        			{
			    					((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().addEdge(
			    							nodes.getBottom(), element.node);
			    					worklist.add(new Element(nodes.getBottom(), element.count-1));
		        			}
		        			else
		        			{
		    	          VsaAsgState state = (VsaAsgState)Interpreter.getActiveState();
		    	          result.addToSet( state.getAbstractStackGraph().addNodes(element.node, element.count, 
		    	                  activeInstruction ) );
		        			}
		          	}
		          	else
		          		worklist.add(new Element(next, element.count-1));
	          	}
	          }
	        }
	        else
	        {
	    			NodeChain nodes =
	    				((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().getNodesCreatedBy(
	    					activeInstruction);
	    			if(nodes != null)
	    			{
	    				if(nodes.size() == element.count)
	    				{
	    					((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().addEdge(
	    							nodes.getBottom(), element.node);
	    					//result = result.merge( nodes.getBottom().getNthPredecessors(n-1, acceptableNodes));
	    					worklist.add(new Element(nodes.getBottom(), element.count-1));
	    				}
	    				else if(element.node == nodes.getTop())
	    				{
	  	          VsaAsgState state = (VsaAsgState)Interpreter.getActiveState();
	  	          result.addToSet( state.getAbstractStackGraph().addNodes(element.node, element.count, 
	  	                  activeInstruction ) );
	    				}
	    				else if(nodes.size() > element.count)
	    				{
	    					((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().addEdge(
	    							nodes.getBottom(), element.node);
	    					worklist.add(new Element(nodes.getBottom(), element.count-1));
	    				}
	    				else // nodes.size() < n
	    				{
	    					((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().addEdge(
	    							nodes.getBottom(), element.node);
	    					worklist.add(new Element(nodes.getBottom(), element.count-1));
	    				}
	    			}
	    			else
	    			{
		          VsaAsgState state = (VsaAsgState)Interpreter.getActiveState();
		          result.addToSet( state.getAbstractStackGraph().addNodes(element.node, element.count, 
		                  activeInstruction ) );
	    			}
	        }
	      }
    	}
    	
    	return result;
//      IStackLocationSet result = new StackLocationSet();
//      IStackLocationSet top;
//
//			if (logger.isDebugEnabled()) 
//			{
//				logger.debug("getNthPredecessors() - Looking at " + this + ", " + n + " to go.");
//			}
//			
//      // Since we must reuse the previously created nodes, we connect edges from
//      // the bottom node that was previously created to the parameter 'start'.
//      NodeChain chain;
//      VsaAsgState state2222 = (VsaAsgState)Interpreter.getActiveState();
//      chain = state2222.getAbstractStackGraph().getNodesCreatedBy(Interpreter.getActiveInstruction());
//      if(chain != null)
//      {
//      	java.util.Iterator iter = chain.iterator();
//      	while(iter.hasNext())
//      	{
//      		acceptableNodes.addToSet( (StackLocation)iter.next() );
//      	}
//      }			
//        
//      // if we have reached nth successor...
//      if(n == 0)
//      {
//        // ... then check if this node is one of the acceptable nodes. 
//        // If it is, add it to the set of results.
//        if(acceptableNodes.has(this))
//            result.addToSet(this);
//      	return result;
//      }
//      else
//      {
//        IStackLocationSet.Iterator iter = getPredecessors().iterator();
//        boolean anyAcceptableNodes = false;
//
//        // Check if there are any successors that are considered OK to traverse. 
//        // It is OK to traverse if it is in the set acceptableNodes.
//        while(iter.hasNext())
//        {
//          if(acceptableNodes.has(iter.next()))
//          {
//            anyAcceptableNodes = true;
//            break;
//          }
//        }
//            
//        // If there are predecessors that occur in acceptableNodes...
//        top = ((Value)Interpreter.getActiveState().getRegisterValue(State.REGISTER_ESP)).getStackLocations();
//        if(anyAcceptableNodes)
//        {
//          // ...for each predecessor
//          iter = ((IStackLocationSet)getPredecessors().clone()).iterator();
//          while(iter.hasNext())
//          {
//            // get the n-1 predecessor of each predecessor to this node.
//          	StackLocation next = iter.next();
//          	if( top.has(this) && next == this )
//          	{
//        			NodeChain nodes =
//        				((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().getNodesCreatedBy(
//        					Interpreter.getActiveInstruction());
//        			if(nodes != null)
//        			{
//	    					((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().addEdge(
//	    							nodes.getBottom(), this);
//	    					result = result.merge( nodes.getBottom().getNthPredecessors(n-1, acceptableNodes));
//        			}
//        			else
//        			{
//    	          VsaAsgState state = (VsaAsgState)Interpreter.getActiveState();
//    	          result.addToSet( state.getAbstractStackGraph().addNodes(this, n, 
//    	                  Interpreter.getActiveInstruction() ) );
//        			}
//          	}
//          	else
//          		result = result.merge( next.getNthPredecessors(n-1, acceptableNodes));
//          }
//        }
//        else
//        {
//    			if (logger.isDebugEnabled()) 
//    			{
//    				logger.debug("getNthPredecessors() - " + n + " new nodes added.");
//    			}
//          // ... then add n nodes to the graph.
//    			
//
//    			NodeChain nodes =
//    				((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().getNodesCreatedBy(
//    					Interpreter.getActiveInstruction());
//    			if(nodes != null)
//    			{
//    				if(nodes.size() == n)
//    				{
//    					((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().addEdge(
//    							nodes.getBottom(), this);
//    					result = result.merge( nodes.getBottom().getNthPredecessors(n-1, acceptableNodes));
//    				}
//    				else if(this == nodes.getTop())
//    				{
//  	          VsaAsgState state = (VsaAsgState)Interpreter.getActiveState();
//  	          result.addToSet( state.getAbstractStackGraph().addNodes(this, n, 
//  	                  Interpreter.getActiveInstruction() ) );
//    				}
//    				else if(nodes.size() > n)
//    				{
//    					((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().addEdge(
//    							nodes.getBottom(), this);
//    					result = result.merge( nodes.getBottom().getNthPredecessors(n-1, acceptableNodes));
//    				}
//    				else // nodes.size() < n
//    				{
//    					((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().addEdge(
//    							nodes.getBottom(), this);
//    					result = result.merge( nodes.getBottom().getNthPredecessors(n-1, acceptableNodes));
//    				}
//    			}
//    			else
//    			{
//	          VsaAsgState state = (VsaAsgState)Interpreter.getActiveState();
//	          result.addToSet( state.getAbstractStackGraph().addNodes(this, n, 
//	                  Interpreter.getActiveInstruction() ) );
//    			}
//        }
//      }
//			if (logger.isDebugEnabled()) 
//			{
//				logger.debug("getNthPredecessors() - Exiting.");
//			}
//      return result;
    }

    /**
     * Returns true if a is a multiple of b
     */
    private boolean isMultipleOf(long a, long b)
    {
        return (a % b) == 0;
    }

    /**
     * string representation
     */
    public String toString()
    {
        return "N" + id;
    }

    /**
     * Returns the result of widing this relative to rhs.  Widening of 
     * a stack-location 'A' relative to a stack-location 'B' 
     * involves finding the difference
     * between A and B and repeatedly adding that difference to A.  For
     * example, if the difference between A and B is 8 (bytes), then
     * the result of widening would include the nodes 
     * {A, B, A+8, A+16, A+24, A+32, ...}.  
     * 
     * Widening of a stack-location A is only performed 
     * when there exists a path from A to Bottom that 
     * includes B.  Thus, widening is not performed if B is on a 
     * completely different branch than A or if B is at a lower
     * memory address than A (unless there is a back-edge).  
     * In each of these two cases, the interpretation is allowed to 
     * loop until the state becomes stable.  In such a case, the
     * result returned by this function would be {A, B}.
     * The reason for this choice of implementation is purely
     * for simplicity.  The approach used here will hopefully
     * be efficient enough and will keep the code simple.
     */
    public IStackLocationSet widen(StackLocation rhs)
    {
    	IReducedIntervalCongruence 	difference;		// difference between this and rhs.
			IStackLocationSet 					result 		= new StackLocationSet();
			IStackLocationSet 					newResult = new StackLocationSet();

			newResult.addToSet(this);
			newResult.addToSet(rhs);

			// If rhs is not on a path from this to stack bottom, then exit.
			if( !((VsaAsgState)Interpreter.getActiveState()).getAbstractStackGraph().isOnPathToBottom(rhs, this) )
				return newResult;

			try
			{
				difference = rhs.subtract(this).getRIC();

				while(!result.equals(newResult))
				{
					result = (IStackLocationSet)newResult.clone();
					newResult = newResult.merge( result.add(difference).getStackLocations() );
				}
			}
			catch(UndefinedOperationException e)
			{
        Log.write(Log.ERROR, "UndefinedOperationException should never happen here.", e);
				throw new Error("This should never happen.");
			}

			return result;
			
//    	try
//    	{
//    		difference = this.subtract(rhs);
//    		
//    		domain.Value.Iterator iter;
//    		iter = difference.iterator();
//    		while(iter.hasNext())
//    		{
//    			float d;
//    			d = iter.next();
//    			StackLocationSet visited;
//    			visited = new StackLocationSet();
//    			Queue worklist;
//    			worklist = new LinkedList();
//    			worklist.add(this);
//    			while(!worklist.isEmpty())
//    			{
//    				StackLocation s = (StackLocation)worklist.remove();
//    				visited.addToSet(s);
//    				StackLocationSet R;
//    				R = s.add(d);
//    			}
//    		}
//
//    		Queue worklist;
//    		worklist = new LinkedList();
//    		List visited;
//    		visited = new LinkedList();
//    		
//    		worklist.add(this);
//    		// add this worklist
//    		while(!worklist.isEmpty())
//    		{
//    			// i = worklist.get
//    			StackLocation i;
//    			i = (StackLocation)worklist.remove();
//
//    			IReducedIntervalCongruence c;
//    			c = difference.getRIC();
//    			
//    			// e = i - c
//    			IStackLocationSet e;
//    			e = i.subtract(c).getStackLocations();
//    			
//    			// add e to result
//    			result.merge(e);
//    			
//    			// add each item in e to worklist, if not visited
//    			IStackLocationSet.Iterator iter;
//    			iter = e.iterator();
//    			while(iter.hasNext())
//    			{
//    				StackLocation s;
//    				s = iter.next();
//    				if(!visited.contains(s))
//    					visited.add(s);
//    			}
//    			// if item in e is bottom, then don't add to worklist
//    		}
//    	}
//    	catch(UndefinedOperationException e)
//    	{
//				if (logger.isDebugEnabled()) 
//					logger.debug("widen() - UndefinedOperationException encountered.");
//    		throw new Error("This should never happen.");
//    	}
//    	return null;
    }
}

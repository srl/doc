/*
 * UndefinedReducedIntervalCongruence.java
 * Created on Mar 13, 2005
 */
package vsaAsgDomain;

import abstractBoolean.AbstractBoolean;

/**
 * Represents a ReducedIntervalCongruence that may contain ALL possible
 * values that an RIC can hold.
 * <p>
 * The UndefinedReducedIntervalCongruence may not support all of the operations
 * supported by the DefinedReducedIntervalCongruence.  Operations not supported
 * throw an Error.
 * 
 * @see vsaAsgDomain.IReducedIntervalCongruence
 */
public class ReducedIntervalCongruenceTop implements IReducedIntervalCongruence
{
    /**
     * Creates a new UndefinedReducedIntervalCongruence belonging to the 
     * specified state.
     */
    public ReducedIntervalCongruenceTop()
    {
    }
    
    /**
     * Adds this RIC to another RIC.  Since this RIC is undefined, the result
     * is always undefined.
     * @return this + rhs
     */
    public Value add(final IReducedIntervalCongruence rhs)
    {
        return new Value();
    }
    
    /**
     * Adds this set with an RIC.  Since this RIC is undefined, the result
     * is always undefined.
     * @return this + rhs
     */
    public Value add(final IStackLocationSet rhs)
    {
        return new Value();
    }
    
    /**
     * Returns a copy of this.
     */
    public Object clone()
    {
        try
        {
	        ReducedIntervalCongruenceTop copy = (ReducedIntervalCongruenceTop)super.clone();
	        return copy;
        }
        catch (CloneNotSupportedException e)
        {
        	throw new Error("This should never happen.");
        }
    }

    /**
     * Divides this RIC by another RIC.  Since this RIC is undefined, the result
     * is always undefined.
     * @return this / rhs
     */
    public Value divide(final IReducedIntervalCongruence rhs)
    {
        return new Value();
    }
    
    /**
     * Divide this set by a stack location set.  Since this RIC is undefined, 
     * the result is always undefined.
     * @return this / rhs
     */
    public Value divide(final IStackLocationSet rhs)
    {
        return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#equals(IReducedIntervalCongruence)
     */
    public boolean equals(final IReducedIntervalCongruence rhs)
    {
        return !rhs.isDefined();
    }
  
    /**
     * getStride, getRangeMin, getRangeMax, and getBase are not defined for an
     * UndefinedReducedIntervalCongruence.
     */
    public int getStride() 		{ throw new Error("Undefined RIC has no stride."); }
    public long getRangeMin()	{ throw new Error("Undefined RIC has no range."); }
    public long getRangeMax()	{ throw new Error("Undefined RIC has no range."); }
    public long getBase()		{ throw new Error("Undefined RIC has no base."); }
    
    /**
     * Returns the maximum value that this RIC represents.  
     */
    public long getMaximumValue()
    {
        return IReducedIntervalCongruence.POS_INFINITY;
    }

    /**
     * Returns the minimum value that this RIC represents. 
     */
    public long getMinimumValue()
    {
        return IReducedIntervalCongruence.NEG_INFINITY;
    }
    
    /**
     * Returns true if this value is defined.  Since this is an 
     * UndefinedReducedIntegralCongruence, the result is always false.
     */
    public boolean isDefined() { return false; };
    
    /**
     * Returns true if this > rhs.  An RIC A is considered greater than
     * a StackLocationSet B if there is at least one member in A that is
     * greater than at least one member in B. 
     */
    public boolean isGreaterThan(final IStackLocationSet rhs)
    {
        return true;
    }
    
    /**
     * Returns abstractBoolean.True if this RIC is guaranteed to be equal
     * to rhs, abstractBoolean.False if this RIC is guaranteed not to be
     * equal to rhs, and abstractBoolean.Top if it is possible for this
     * RIC to be equal to rhs.
     */
    public AbstractBoolean isEqualTo(IReducedIntervalCongruence rhs)
    {
    	return AbstractBoolean.Top;
    }
    
    /**
     * Returns abstractBoolean.True if this RIC is guaranteed to be greater
     * than rhs, abstractBoolean.False if this RIC is guaranteed to not be
     * greater than rhs, and abstractBoolean.Top otherwise.
     */
    public AbstractBoolean isGreaterThan(final IReducedIntervalCongruence rhs)
    {
        return AbstractBoolean.Top;
    }

    /**
     * Returns true if this > rhs.  An RIC A is considered less than
     * a StackLocationSet B if there is at least one member in A that is
     * less than at least one member in B. 
     */
    public boolean isLessThan(final IStackLocationSet rhs)
    {
        return true;
    }
    
    /**
     * Returns abstractBoolean.Top if this RIC is guaranteed to be less than
     * rhs, abstractBoolean.False if this RIC is guaranteed to not be less
     * than rhs, abstractBolean.Top if this RIC can be less than rhs, but is not
     * guaranteed to be.
     */
    public abstractBoolean.AbstractBoolean isLessThan(final IReducedIntervalCongruence rhs)
    {
        return AbstractBoolean.Top;
    }

    /**
     * Returns true if this RIC represents a single number.  Since this RIC is undefined,
     * it always returns false.
     */
    public boolean isSingleNumber()
    {
        return false;
    }
        
    /**
     * Returns an iterator that can be used to traverse the numbers 
     * represented by this RIC.
     */
    public IReducedIntervalCongruence.Iterator iterator() { return new Iterator(); }
    public class Iterator implements IReducedIntervalCongruence.Iterator
    {
        public Iterator() 			{  }
        public boolean 	hasNext() 	{ return false; }
        public long		next() 		{ throw new Error("Should never happen"); }
    }
    
    /**
     * Merges the values in rhs with the values in this RIC.
     * @return this union rhs
     */
    public IReducedIntervalCongruence merge(final IReducedIntervalCongruence rhs)
    {
   		return new ReducedIntervalCongruenceTop();
    }
    
    /**
     * Multiplies this RIC with another RIC. Since this RIC is undefined, the result
     * is always undefined.
     * @return this * rhs
     */
    public Value multiply(final IReducedIntervalCongruence rhs)
    {
        return new Value();
    }
    
    /**
     * Multiplies this set with a StackLocationSet. Since this RIC is undefined, 
     * the result is always undefined.
     * @return this * rhs
     */
    public Value multiply(final IStackLocationSet rhs)
    {
        return new Value();
    }
    
    /**
     * @see IReducedIntervalCongruence#organize()  
     */
    public void organize()	{}
    
    /**
     * Returns true if this RIC overapproximates rhs.
     */
    public boolean overapproximates(final IReducedIntervalCongruence rhs)
    {
        return true;
    }
    
    /**
     * setStride, setRange, and setBase are not defined 
     * for an UndefinedReducedIntervalCongruence
     */ 
    public void set(long base, int stride, long intervalmin, long intervalmax)
    {
    	throw new Error("Undefined RIC has no base, stride, or range.");
    }
    
    /**
     * Assigns this to a constant value.  This function is not
     * defined for an UndefinedReducedIntervalCongruence 
     */
    public void set(long value)
    {
        throw new Error("Cannot set properties of an undefined value.  " +
                "Create a DefinedReducedIntervalCongruence.");
    }
    
    /**
     * Returns the number of elemnts represented by this RIC.
     */
    public long size()
    {
        return 0; 
    }
    
    /**
     * Subtracts an RIC from this RIC.  Since this RIC is undefined, 
     * the result is always undefined.
     * @return this - rhs
     */
    public Value subtract(final IReducedIntervalCongruence rhs)
    {
        return new Value();
    }
    
    /**
     * Subtracts a stack location set from this RIC. Since this RIC is undefined, 
     * the result is always undefined.
     * @return this - rhs
     */
    public Value subtract(final IStackLocationSet rhs)
    {
        return new Value();
    }
    
    /**
     * Returns a string representation of this RIC.  
     */
    public String toString()
    {
        return "T";
    }

    public IReducedIntervalCongruence widen(final IReducedIntervalCongruence rhs)
    {
        return new ReducedIntervalCongruenceTop();
    }

    /**
     * Returns the xor of this and rhs.
     */
    public Value xor(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {     
        long allZeroes = 0;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allZeroes)
    	{
    		return new Value(this);
    	}
        
    	if(rhs.equals(this))
    	{
    		return new Value(allZeroes);
    	}
    	
    	return new Value();
    }

    /**
     * Returns the xor of this and rhs.
     */
    public Value xor(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        return new Value();
    }
    
    /**
     * Returns the or of this and rhs.
     */
    public Value or(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
    	if(rhs.equals(this))
    	{
    		return new Value(this);
    	}
    	
    	long allZeroes = 0;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allZeroes)
    	{
    		return new Value(this);
    	}
    	
    	//XOR, OR, AND all can't have immediate values on the left side,
    	//so I only have to worry about the right hand side (rhs).    	
    	long allOnes = 4294967295L;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allOnes)
    	{
    		Value value = new Value();
    		value.setRIC(rhs);
    		return value;
    	}
        return new Value();
    }

    /**
     * Returns the or of this and rhs.
     */
    public Value or(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        return new Value();
    }
    
    /**
     * Returns the and of this and rhs.
     */
    public Value and(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
    	if(rhs.equals(this))
    	{
    		return new Value(this);
    	}
    	
    	long allOnes = 4294967295L;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allOnes)
    	{
    		return new Value(this);
    	}
    	
    	//XOR, OR, AND all can't have immediate values on the left side,
    	//so I only have to worry about the right hand side (rhs).    	
    	long allZeroes = 0;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allZeroes)
    	{
    		Value value = new Value();
    		value.setRIC(rhs);
    		return value;
    	}
        return new Value();
    }

    /**
     * Returns the and of this and rhs.
     */
    public Value and(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        return new Value();
    }
}

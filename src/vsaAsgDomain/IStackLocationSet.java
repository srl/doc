/*
 * StackLocationSet.java
 * Created on Mar 11, 2005
 */
package vsaAsgDomain;

import abstractBoolean.AbstractBoolean;

/**
 * Defines an interface for maintaining and iterating a set of StackLocations.
 */
public interface IStackLocationSet extends Cloneable
{
    /**
     * Adds this set with a StackLocationSet   
     * @return this + rhs
     * @throws UndefinedOperationException
     */
    public Value add(final IStackLocationSet rhs) throws UndefinedOperationException;

    /**
     * Adds this set with a ReducedIntervalCongruence 
     * @return this + rhs
     */
    public Value add(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
    /**
     * Adds a StackLocation to this set.  Duplicates are removed.
     */
    public void addToSet(final StackLocation node);
    
    /**
     * Removes all elements from this set.
     */
    public void clear();
    
    /**
     * Returns a copy of this object.
     */
    public Object clone();    
    
    /**
     * Divides this StackLocationSet by another StackLocationSet
     * @return this / rhs
     * @throws UndefinedOperationException
     */
    public Value divide(final IStackLocationSet rhs) throws UndefinedOperationException;
    
    /**
     * Divides this StackLocationSet by a ReducedIntervalCongruence
     * @return this / rhs
     * @throws UndefinedOperationException
     */
    public Value divide(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
    /**
     * Returns true if this StackLocationSet equals rhs. This function uses a different 
     * definition of equality than the function isEqualTo.  isEqualTo is intended to be 
     * used during the abstract interpretation, whereas equals is used to determine if 
     * two StackLocationSets hold the same data. The function equals would state that 
     * the sets {N0, N1, N2} and {N0, N1, N2} are equal, but isEqualTo would return Top, 
     * indicating that the two might be equal. 
     */
    public boolean equals(Object rhs);    
    
    /**
     * Determines if this set already contains a particular
     * StackLocation.  This is determined by seeing if rhs contains
     * the same set of references to StackLocations as this set contains, 
     * not by comparing the contents of StackLocations.
     * @return true if item is in this set.
     */
    public boolean has(final StackLocation item);
    
    /**
     * Returns a set of stack-locations containing the intersection of
     * this set and rhs.
     */
    public IStackLocationSet intersection(IStackLocationSet rhs);
    
    /**
     * Returns true if this StackLocationSet is defined, false otherwise.  In the
     * design of the VSA-ASG approach, a Value may contain a StackLocationSet, or
     * the StackLocationSet might be undefined.  This function provides inforamtion
     * about whether this set is defined. 
     */
    public boolean isDefined();
    
    /**
     * Determines if two StackLocationSets are equal.
     * @return AbstractBoolean.True if it is guaranteed that any element from this
     *    is equal to any element from rhs, AbstractBoolean.False if it is guaranteed
     *    that any element from this is not equal to any element form rhs, or 
     *    AbstractBoolean.Top if it is possible that an element from this is equal to
     *    an element from rhs.
     */
    public AbstractBoolean isEqualTo(IStackLocationSet rhs);
    
    /**
     * Returns true if this > rhs.  A StackLocationSet A is considered 
     * greater than another StackLocationSet B if there is at least one 
     * member in A that is greater than at least one member in B. 
     */
    public boolean isGreaterThan(final IStackLocationSet rhs);

    /**
     * Returns true if this > rhs.  A StackLocationSet A is considered 
     * greater than an RIC B if there is at least one member in A that 
     * is greater than at least one member in B. 
     */
    public boolean isGreaterThan(final IReducedIntervalCongruence rhs);
    
    /**
     * Returns true if this > rhs.  A StackLocationSet A is considered 
     * less than another StackLocationSet B if there is at least one 
     * member in A that is less than at least one member in B. 
     */
    public boolean isLessThan(final IStackLocationSet rhs);

    /**
     * Returns true if this > rhs.  A StackLocationSet A is considered 
     * less than an RIC B if there is at least one member in A that 
     * is less than at least one member in B. 
     */
    public boolean isLessThan(final IReducedIntervalCongruence rhs);
    
    /**
     * Merges another StackLocationSet with this one.  Duplicates removed.
     * @return this union rhs.
     */
    public IStackLocationSet merge(final IStackLocationSet rhs);
    
    /**
     * Multiplies this StackLocationSet set by another StackLocationSet
     * @return this * rhs
     * @throws UndefinedOperationException
     */
    public Value multiply(final IStackLocationSet rhs) throws UndefinedOperationException;
    
    /**
     * Returns true if this stacklocation set overapproximates rhs.
     */
    public boolean overapproximates(final IStackLocationSet rhs);
    
    /**
     * Multiplies this StackLocationSet by a ReducedIntervalCongruence
     * @return this * rhs
     * @throws UndefinedOperationException
     */
    public Value multiply(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
    /**
     * Returns an iterator that can be used to traverse each StackLocation 
     * in this set.
     */
    public Iterator iterator();
    public interface Iterator
    {
        public boolean hasNext();
        public boolean hasPrevious();
        public StackLocation previous();
        public StackLocation next();
        public void remove();
    }
    
    /**
     * Returns the number of StackLocations in this set.
     */
    public int size();
    
    /**
     * Subtracts a StackLocationSet from this StackLocationSet.  This is computed by
     * subtracting each StackLocation in rhs from each StackLocation in this set.  
     * @return this - rhs.
     * @throws UndefinedOperationException
     */
    public Value subtract(final IStackLocationSet rhs) throws UndefinedOperationException;
    
    /**
     * Subtracts a ReducedIntervalCongruence from this StackLocationSet.
     * @return this - rhs 
     */
    public Value subtract(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
    /**
     * Returns an array containing each stack-location in this set.
     */
    public Object[] toArray();
    
    /**
     * Widens this StackLocationSet.
     * @return the widened StackLocationSet
     */
    public IStackLocationSet widen(final IStackLocationSet rhs);
    
    /**
     * Returns the xor of this and rhs.
     * @return this XOR rhs
     */
    public Value xor(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
    /**
     * Returns the xor of this and rhs.
     * @return this XOR rhs
     */
    public Value xor(final IStackLocationSet rhs) throws UndefinedOperationException;

	/**
     * Returns the or of this and rhs.
     * @return this OR rhs
     */
    public Value or(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
    /**
     * Returns the or of this and rhs.
     * @return this OR rhs
     */
    public Value or(final IStackLocationSet rhs) throws UndefinedOperationException;

	/**
     * Returns the and of this and rhs.
     * @return this AND rhs
     */
    public Value and(final IStackLocationSet rhs) throws UndefinedOperationException;

	/**
     * Returns the and of this and rhs.
     * @return this AND rhs
     */
    public Value and(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
}
/*
 * StackGraphContentProvider.java
 * Created on Apr 10, 2005
 */
package vsaAsgDomain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import lCallContextAnalysisDomain.LCallContext;
import lStackContextAnalysisDomain.LStackContext;

import org.eclipse.jface.viewers.Viewer;
import vsaAsgDomain.VsaAsgState;
import csAnalysisDomain.csAnalysisDomain;

import ui.directedGraph.IDirectedGraphContentProvider;
import ui.registers.RegisterValueContextPair;
import x86InstructionSet.Instruction;
import x86Interpreter.Interpreter;
import domain.Register;
import domain.State;


/**
 * Content provider for retrieving abstrack stack contents.  For use
 * with the DirectedGraphViewer.
 */
public abstract class StackGraphContentProvider implements IDirectedGraphContentProvider
{
    /* (non-Javadoc)
     * @see detectObfuscation.IDirectedGraphContentProvider#getChildren(java.lang.Object)
     */
    public Object[] getChildren(Object elementIn)
    {
        StackGraphContentElement	element;
        int							i;
        IStackLocationSet.Iterator	iter;
        StackGraphContentElement[]	result;
        
        if(elementIn instanceof vsaAsgDomain.StackGraphContentElement)
        {
            element = (StackGraphContentElement)elementIn;
            StackLocation node = element.node;
            
            result = new StackGraphContentElement[node.getSuccessors().size()];
            iter = node.getSuccessors().iterator();
            i = 0;
            while(iter.hasNext())
                result[i++] = new StackGraphContentElement(element.state, iter.next());
            return result;
        }
        else
            throw new Error("StackGraphContentProvider: Invalid object passed to getChildren.");
    }

    /* (non-Javadoc)
     * @see detectObfuscation.IDirectedGraphContentProvider#equals(java.lang.Object, java.lang.Object)
     */
    public boolean equals(Object elementA, Object elementB)
    {
        StackGraphContentElement lhs = (StackGraphContentElement)elementA;
        StackGraphContentElement rhs = (StackGraphContentElement)elementB;
        return lhs.node.getId() == rhs.node.getId();
    }

    /**
     * Returns all stack-locations--state pairs on the path from the top of the 
     * stack to the bottom.
     */
    public Object[] getElements(Object inputElement)
    {
        StackLocation 				      currentNode;
        int							            i;
        IStackLocationSet.Iterator	iter;
        StackGraphContentElement[]	result  = null;
        State		                state;
                
        Instruction					instruction;
        IStackLocationSet           visited; 
        IStackLocationSet           worklist;
        IStackLocationSet.Iterator	worklistIter;
        
        state  = getState(inputElement);
             
            if(state != null)
            {
            	if ( state instanceof VsaAsgState ){
            		
            		
            		VsaAsgState context = (VsaAsgState)state;             	
            		        		
            		visited 	= new StackLocationSet();
                    worklist 	= new StackLocationSet();

                    // Here is an outline of this algorithm:
                    //   Worklist <- Tops of stack
                    //   for each item in worklist
                    //	   Visited <- Visited U item
                    //     Worklist <- Worklist / item
                    //     Worklist <- Worklist U children(item)
                    //   Place each item in visited in StackGraphContentElement array.
                                  	  
          			
                    Map contextmap = context.getMapping();
         			Iterator <Entry <String, VsaAsgState>> contextSet = contextmap.entrySet().iterator();
         			
         			// GET ALL THE TOP OF STACKS FROM ALL THE CONTEXTS
         			// OF THE CURRENT STATE
         			
         			while (contextSet.hasNext()){
         				  Entry<String, VsaAsgState> entry = contextSet.next();
         				  VsaAsgState substate = entry.getValue();
         				 if (!(substate == null))
							  worklist = worklist.merge(getStackTops(substate));
         			  }
       			
         			// GET ALL NODES TRANSITIVELY REACHABLE FROM TOPS OF STACK.
         			
        			while(worklist.size() > 0)
                    {
                        worklistIter = worklist.iterator();
                        currentNode  = worklistIter.next();
                        worklistIter.remove();
                        if(!visited.has(currentNode))
        	            {
        	                visited.addToSet(currentNode);
        	                worklist = worklist.merge(currentNode.getSuccessors());
        	            }
                    }
                    
        			// PUT ALL OF THE EDGE NODES IN A CONTAINER
        			// AND RETURN
                    result = new StackGraphContentElement[visited.size()];
                    iter = visited.iterator();
                    i = 0;
                    while(iter.hasNext())
                        result[i++] = new StackGraphContentElement(context, iter.next());	
        			   		       			
        			return result;
            	
            	}  else return new Object[0];
            }
        
        return new Object[0];
    }
    
    /**
     * Returns a StackLocationSet containing all of the stack-location that are the
     * stack top.
     * @param stateIn
     * @return
     */
    private IStackLocationSet getStackTops(domain.State stateIn)
    {
        VsaAsgState   state;
        domain.Value	top;
        
        state	= (VsaAsgState)stateIn;
        top		= state.getRegisterValue(Register.ESP);
        return ((Value)top).getStackLocations();        
    }

    /**
     * Returns the appropriate state to use for this content provider.
     * @param instruction the selected instruction to retrieve state from.
     * @return the state to use.
     */
    protected abstract domain.State getState(Object instruction);

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IContentProvider#dispose()
     */
    public void dispose()
    {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IContentProvider#inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object, java.lang.Object)
     */
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput)
    {
        // this function is only needed if the input element can be modified.
        // that is not the case here.
    }
}

/*
 * NodeCreatorTable.java
 * Created on May 18, 2005
 */
package vsaAsgDomain;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import x86InstructionSet.Instruction;



/**
 * Stores a mapping from Instruction to NodeChain, where given
 * an Instruction, returns a NodeChain contains the AbstractStackGraph
 * nodes that were created by the Instruction.
 */
public class NodeCreatorTable implements Cloneable
{
  /**
   * A pair consisting of Instruction X NodeChain.  Used to assist
   * in storing the Instruction-NodeChain pairs.
   */
  private class InstructionNodeChainPair
  {
    public InstructionNodeChainPair(Instruction instruction, NodeChain nodes)
    {
        this.instruction 	= instruction;
        this.nodes			= nodes;
    }
    
    public Instruction 	instruction;
    public NodeChain	nodes;
  }
    
  /**
   * The list of Instruction-NodeChain pairs.
   */
  private List<InstructionNodeChainPair> pairs = new LinkedList<InstructionNodeChainPair>();
    
  /**
   * Adds an Instruction/NodeChain pair to this table.  The instruction
   * is assumed to be responsible for the creation of the createdNodes.
   * Duplicates are not allowed.  An Error is thrown is an attempt is
   * made to add a pair that is already in this table.
   */
  public void add(final Instruction instruction, final NodeChain createdNodes)
  {
  	if(get(instruction) != null)
  		throw new Error("Cannot add duplicate entry in NodeCreatorTable.");
  	
    pairs.add( new InstructionNodeChainPair(instruction, createdNodes) );
  }

  /**
   * Returns a clone of this.
   */
  public Object clone()
  {
    try
    {
      NodeCreatorTable clone = (NodeCreatorTable)super.clone();
      clone.pairs = pairs;	// shallow copy, but that's ok here.
      return clone;
    }
    catch(CloneNotSupportedException e)
    {
      throw new Error("This should never happen");
    }
  }
    
  /**
   * Returns an the chain of nodes that were created 
   * by the input instruction.
   * Returns null if no such instruction exists in the table.
   */
  public NodeChain get(final Instruction instruction)
  {
    Iterator iter = pairs.iterator();
    while(iter.hasNext())
    {
      InstructionNodeChainPair pair = (InstructionNodeChainPair)iter.next();
      if(pair.instruction == instruction)
        return pair.nodes;
    }
    return null;
  }
}

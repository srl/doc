package vsaAsgDomain;

import x86InstructionSet.Instruction;

/**
 * Provides stack contents from the End state of an instruction. 
 */
public class EndStateStackGraphContentProvider extends
    StackGraphContentProvider
{
  /* (non-Javadoc)
   * @see vsaAsgDomain.StackGraphContentProvider#getState(x86InstructionSet.Instruction)
   */
  @Override
  protected domain.State getState(Object obj)
  {
	 Instruction instruction = (Instruction) obj;
     return instruction.getEndState();
  }
}

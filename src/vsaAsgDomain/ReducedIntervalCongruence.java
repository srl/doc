/*
 * DefinedReducedIntervalCongruence.java
 * Created on Mar 13, 2005
 */
package vsaAsgDomain;

import abstractBoolean.AbstractBoolean;

/**
 * Represents a ReducedIntervalCongruence that has been initialized.
 * 
 * @see ReducedIntervalCongruence
 */
public class ReducedIntervalCongruence implements IReducedIntervalCongruence
{
    /**
     * An ric is defined by m_stride[m_rangeMin, m_rangeMax] + m_base
     */
    private int		m_stride	= 0;
    private long	m_rangeMin	= 0;
    private long	m_rangeMax	= 0;
    private long	m_base		= 0;

    
    /**
     * Creates a new DefinedReducedIntervalCongruence 
     */
    public ReducedIntervalCongruence()
    { 
    }
    
    /**
     * Creates a new DefinedReducedIntervalCongruence initialized to the
     * specified values
     */
    public ReducedIntervalCongruence(int stride, int rangeMin, int rangeMax, int base)
    {
        m_stride = stride;
        m_rangeMin = rangeMin;
        m_rangeMax = rangeMax;
        m_base = base;
    }
    
    /**
     * Adds this RIC to another RIC.
     * @return this + rhs
     */
    public Value add(final IReducedIntervalCongruence rhs)
    {
        IReducedIntervalCongruence result;
        
        
        if(!rhs.isDefined())
        {
            // if rhs is not defined, then the result should be an undefined Value 
            return new Value();
        }
        
        else if(rhs.isSingleNumber())
        {
            // if rhs contains only a single number, then the addition is simple. 
            // Just shift the base of this RIC by rhs.
            result = (IReducedIntervalCongruence)this.clone();
            result.set(m_base + rhs.getMinimumValue(), result.getStride(), 
            		result.getRangeMin(), result.getRangeMax());
            
            return new Value(result);
        }
        
        else if(this.isSingleNumber())
        {
            // if this ric represents a single number, then the addition requires adjusting
            // the base of rhs.
            result = (IReducedIntervalCongruence)rhs.clone();
            result.set(rhs.getBase() + getMinimumValue(), result.getStride(), 
            		result.getRangeMin(), result.getRangeMax());
            
            return new Value(result);
        }
        
        else if(m_stride == rhs.getStride())
        {
            // if the strides are the same, then the equation we use is as follows.
            // a[b,c]+d + a[b',c']+d'  =   a[b+b', c+c'] + d + d'
            result = new ReducedIntervalCongruence();
            result.set(m_base + rhs.getBase(), m_stride, m_rangeMin + rhs.getRangeMin(),
            		m_rangeMax + rhs.getRangeMax() );
            return new Value(result);            
        }
        
        else
        {
            // in all other cases, we create an RIC that is actually a range
            // starting from the lowest possible number in the two RICs to the
            // highest possible number in the two RICs.
	        result = new ReducedIntervalCongruence();
	        result.set(0, 1, getMinimumValue() + rhs.getMinimumValue(), 
	        		getMaximumValue() + rhs.getMaximumValue() );
	        return new Value(result);
        }
    }
    
    /**
     * Adds this set with an RIC.
     * @return the resulting Value.
     */
    public Value add(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        return rhs.add(this);
    }
    
    /**
     * Returns a copy of this ReducedIntervalCongruence.
     */
    public Object clone()
    {
        try
        {
	        ReducedIntervalCongruence copy = (ReducedIntervalCongruence)super.clone();
	        copy.m_stride 	= m_stride;
	        copy.m_rangeMin = m_rangeMin;
	        copy.m_rangeMax = m_rangeMax;
	        copy.m_base 	= m_base;
	        return copy;
        }
        catch (CloneNotSupportedException e)
        {
        	throw new Error("This should never happen.");
        }
    }

    /**
     * Divides this RIC by an RIC.
     * @return the resuling Value.
     */
    public Value divide(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }
    
    /**
     * Divide this ReducedIntervalCongruence by a StackLocationSet
     * @return the resulting Value.
     */
    public Value divide(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }
    
    /**
     * @see IReducedIntervalCongruence#equals(IReducedIntervalCongruence)
     */
    public boolean equals(final IReducedIntervalCongruence rhs)
    {
    	boolean isEqual = false;
    	
        if(!rhs.isDefined())
            return false;
        
    	organize();
    	rhs.organize();

        isEqual = m_stride == rhs.getStride() && 
        		m_rangeMin == rhs.getRangeMin() &&
        		m_rangeMax == rhs.getRangeMax() &&
        		m_base == rhs.getBase();
        
        return isEqual;
    }
    
    public int getStride() 		{ return m_stride; }
    public long getRangeMin()	{ organize(); return m_rangeMin; };
    public long getRangeMax()	{ organize(); return m_rangeMax; };
    public long getBase()		{ return m_base; };

    /**
     * Returns the maximum value that this RIC represents.
     */
    public long getMaximumValue()
    {
      long result;

      if(m_rangeMax >= POS_INFINITY)
            return POS_INFINITY;
      
      result = m_stride * m_rangeMax + m_base;
      if(result >= POS_INFINITY)
      	return POS_INFINITY;
        
      return result; 
    }

    /**
     * Returns the minimum value that this RIC represents.
     */
    public long getMinimumValue()
    {
    	long result;
       
    	if(m_rangeMin <= NEG_INFINITY)
    		return NEG_INFINITY;
    	
    	result = m_stride * m_rangeMin + m_base;
    	if(result <= NEG_INFINITY)
    		return NEG_INFINITY;
    	
    	return result;
    }
    
    /**
     * Returns true if this RIC has been initialized.  Since this is a 
     * DefinedReducedIntervalCongruence, the result is always true.
     */
    public boolean isDefined() { return true; };

    /**
     * Returns AbstractBoolean.True if this RIC is guaranteed to be equal
     * to rhs, AbstractBoolean.False if this RIC is guaranteed not to be
     * equal to rhs, and AbstractBoolean.Top if it is possible for this
     * RIC to be equal to rhs.
     */
    public AbstractBoolean isEqualTo(IReducedIntervalCongruence rhs)
    {
    	if(this.isSingleNumber() && rhs.isSingleNumber() && 
    			this.getMinimumValue() == rhs.getMinimumValue())
    		return AbstractBoolean.True;
    	
    	if(this.getMaximumValue() < rhs.getMinimumValue() ||
    			this.getMinimumValue() > rhs.getMaximumValue())
    		return AbstractBoolean.False;
    	
    	return AbstractBoolean.Top;
    }
    
    /**
     * Returns abstractBoolean.True if this RIC is guaranteed to be greater
     * than rhs, abstractBoolean.False if this RIC is guaranteed to not be
     * greater than rhs, and abstractBoolean.Top otherwise.
     */
    public AbstractBoolean isGreaterThan(final IReducedIntervalCongruence rhs)
    {
    	if(rhs.isDefined())
    	{
    		if(this.getMinimumValue() > rhs.getMaximumValue())
    			return AbstractBoolean.True;
    		if(this.getMaximumValue() <= rhs.getMinimumValue())
    			return AbstractBoolean.False;
    	}
    	
    	return AbstractBoolean.Top;
    }

    /**
     * Returns true if this > rhs.  An RIC A is considered greater than
     * a StackLocationSet B if there is at least one member in A that is
     * greater than at least one member in B. 
     */
    public boolean isGreaterThan(final IStackLocationSet rhs)
    {
        return true;
    }
   
    /**
     * Returns true if this > rhs.  An RIC A is considered less than
     * a StackLocationSet B if there is at least one member in A that is
     * less than at least one member in B. 
     */
    public boolean isLessThan(final IStackLocationSet rhs)
    {
        return true;
    }

    /**
     * Returns abstractBoolean.Top if this RIC is guaranteed to be less than
     * rhs, abstractBoolean.False if this RIC is guaranteed to not be less
     * than rhs, abstractBolean.Top if this RIC can be less than rhs, but is not
     * guaranteed to be.
     */
    public abstractBoolean.AbstractBoolean isLessThan(final IReducedIntervalCongruence rhs)
    {
    	if(rhs.isDefined())
    	{
    		if(this.getMaximumValue() < rhs.getMinimumValue())
    			return AbstractBoolean.True;
    		if(this.getMinimumValue() >= rhs.getMaximumValue())
    			return AbstractBoolean.False;
    	}
    	
    	return AbstractBoolean.Top;
    }

    /**
     * Returns true if this RIC represents a single number.
     */
    public boolean isSingleNumber()
    {
        return m_stride == 0 || m_rangeMin == m_rangeMax;
    }
    
    /**
     * Returns an iterator that can be used to traverse the numbers 
     * represented by this RIC.
     */
    public IReducedIntervalCongruence.Iterator iterator() { return new Iterator(); }
    public class Iterator implements IReducedIntervalCongruence.Iterator
    {
        public Iterator() 			{ m_currentIndex = (size() != POS_INFINITY) ? getRangeMin() - 1 : getRangeMax(); }
        public boolean 	hasNext() 	{ return m_currentIndex < getRangeMax(); }
        public long		next() 		{ return m_stride * ++m_currentIndex + m_base; }
        private long m_currentIndex;
    }
    
    /**
     * Merges the values in rhs with the values in this RIC.
     */
    public IReducedIntervalCongruence merge(final IReducedIntervalCongruence rhs)
    {
        IReducedIntervalCongruence result;
        
        if(rhs instanceof ReducedIntervalCongruenceTop)
        	return new ReducedIntervalCongruenceTop();
        
        if(rhs instanceof ReducedIntervalCongruenceBottom)
        	return (IReducedIntervalCongruence)this.clone();

        // in all other cases, we define the merger by simply create a new RIC 
        // that represents a range that includes all values in both RICs.  This is 
        // a rather simplistic approach and can be improved by doing certain check
        // such as when both RICs have the same stride, etc.
        result = new ReducedIntervalCongruence();
        result.set(0, 1, Math.min(getMinimumValue(), rhs.getMinimumValue()), 
        		Math.max(getMaximumValue(), rhs.getMaximumValue()) );
        return result;
    }
    
    /**
     * Multiplies this RIC with another RIC.
     * @return this * rhs
     */
    public Value multiply(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        throw new UndefinedOperationException();
    }
    
    /**
     * Multiplies this set with an RIC.
     * @return this * rhs
     */
    public Value multiply(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        return rhs.multiply(this);
    }

    /**
     * @see IReducedIntervalCongruence#organize()
     */
    public void organize()
    {
    	long oldBase; 
    	long oldRangeMax;
    	long oldRangeMin; 
    	int  oldStride;
    	
    	// Don't make any changes if the rangemin equals negative infinity, because
    	// I don't even know how to compute the values represented by such an RIC.
    	if(m_rangeMin == NEG_INFINITY)
    		return;
    	
      if(m_rangeMax < m_rangeMin)
      {
        long oldMin = m_rangeMin;
        m_rangeMin = m_rangeMax;
        m_rangeMax = oldMin;
	    }
        
      oldBase = m_base;
      oldStride = m_stride;
      oldRangeMin = m_rangeMin;
      oldRangeMax = m_rangeMax;
      
      m_base = oldBase + oldStride * oldRangeMin;
      m_stride = oldStride;
      m_rangeMin = 0;
      m_rangeMax = (m_rangeMax == POS_INFINITY) ? POS_INFINITY : oldRangeMax - oldRangeMin;
      
      if(m_rangeMin == m_rangeMax)
      	m_stride = 1;
    }
    
   
    /**
     * Returns true if this RIC overapproximates rhs.
     */
    public boolean overapproximates(final IReducedIntervalCongruence rhs)
    {
        if(rhs.isDefined())
        {
            if(isSingleNumber() && rhs.isSingleNumber())
                return getMinimumValue() == rhs.getMinimumValue();
            else
	            return (getMinimumValue() <= rhs.getMinimumValue()) &&  
	            	(getMaximumValue() >= rhs.getMaximumValue()) && 
	            	getStride() == 1;
        }
        else
            return false;
    }
    
    /**
     * Assigns this to a constant value.
     */
    public void set(long value)
    {
        m_stride 	= 0;
        m_rangeMin 	= 0;
        m_rangeMax 	= 0;
        m_base 		= value;
    }
    
    /**
     * @see IReducedIntervalCongruence#set(long, int, long, long)t
     */
    public void set(long base, int stride, long intervalmin, long intervalmax)
    {
    	m_base 		= base;
    	m_stride 	= stride;
    	m_rangeMin 	= intervalmin;
    	m_rangeMax 	= intervalmax;
    	organize();
    }
    
    /**
     * Returns the number of elemnts represented by this RIC.
     * If the RIC contains an infinite number of elements, then 
     * ReducedIntervalCongruence.POS_INFINITY is returned.
     */
    public long size()
    {
        if(getRangeMin() == NEG_INFINITY || getRangeMin() == POS_INFINITY ||
                getRangeMax() == NEG_INFINITY || getRangeMax() == POS_INFINITY)
            return POS_INFINITY;
        else
            return getRangeMax() - getRangeMin() + 1;
    }
    
    /**
     * Subtracts an RIC from this RIC.
     * @return this - rhs
     */
    public Value subtract(final IReducedIntervalCongruence rhs)
    {
        IReducedIntervalCongruence result;
        
        if(!rhs.isDefined())
        {
            // if rhs is not defined, then the result should be an undefined Value 
            return new Value();
        }
        
        else if(rhs.isSingleNumber())
        {
            // if rhs represents a single number, then the subtracting requires adjusting
            // the base of this RIC.
            result = (IReducedIntervalCongruence)this.clone();
            result.set(m_base - rhs.getMinimumValue(), result.getStride(), 
            		result.getRangeMin(), result.getRangeMax());       	
            return new Value(result);
        }

        else if(this.isSingleNumber())
        {
            // if this RIC represents a single number, then the subtracting requires adjusting
            // the base of rhs.
            result = (IReducedIntervalCongruence)rhs.clone();
            result.set(this.getMinimumValue() - rhs.getMaximumValue(), 
            		result.getStride(), result.getRangeMin(), result.getRangeMax());
            return new Value(result);
        }
        
        else if(m_stride == rhs.getStride())
        {
            // a[b,c]+d - a[b',c']+d'
            // = a[b-b', c-c'] + d - d'
            result = new ReducedIntervalCongruence();
            result.set(m_base - rhs.getBase(), m_stride, 
            		m_rangeMin - rhs.getRangeMin(), m_rangeMax - rhs.getRangeMax() );
            return new Value(result);            
        }
        
        else
        {
            // in all other cases, we create an RIC that is actually a range
            // starting from the lowest possible number in the two RICs to the
            // highest possible number in the two RICs.
	        result = new ReducedIntervalCongruence();
	        result.set(0, 1, getMinimumValue() - rhs.getMinimumValue(), 
	        		getMaximumValue() - rhs.getMaximumValue());
	        return new Value(result);
        }
    }
    
    /**
     * Subtracts a StackLocationSet from this RIC.
     * @return this - rhs
     */
    public Value subtract(final IStackLocationSet rhs) throws UndefinedOperationException
    {
    	if(rhs.isDefined())
    		throw new UndefinedOperationException();
    	return new Value();
    }
    
    /**
     * Returns a string representation of this RIC.  
     */
    public String toString()
    {
        if(this.isSingleNumber())
            return Long.toHexString(this.getMinimumValue());
        else
	        return Long.toHexString(getStride()) + "[" + 
	        		(getRangeMin() == NEG_INFINITY ? "-INF" : Long.toHexString(getRangeMin())) + "," +
	        		(getRangeMax() == POS_INFINITY ? "INF" :  Long.toHexString(getRangeMax())) + "]+" +
	        		Long.toHexString(getBase());
    }
    
    /**
     * Widens this RIC. 
     * @return the widened RIC.
     */
    public IReducedIntervalCongruence widen(final IReducedIntervalCongruence rhs)
    {
        IReducedIntervalCongruence result;
        
        if(rhs.isDefined())
        {
            result = (IReducedIntervalCongruence)this.clone();
            
            // If the two numbers don't have the same stride, then set the
            // stride of the resulting RIC to 1.  This will ensure that the
            // resulting RIC overapproximates rhs.
            if(rhs.getStride() != getStride() && rhs.getMinimumValue() != getMinimumValue())
            {
                int newStride = 1;
                long newRangeMin = Math.min(rhs.getMinimumValue(), getMinimumValue());
                long newRangeMax = Math.max(rhs.getMaximumValue(), getMaximumValue());
                int newBase = 0;
                result.set(newBase, newStride, newRangeMin, newRangeMax);
            }
            
	        if(rhs.getMaximumValue() > getMaximumValue())
	        {
	        	result.set(0, 1, result.getMinimumValue(), POS_INFINITY);
	        }
        
	        if(rhs.getMinimumValue() < getMinimumValue())
	        {
	        	result.set( 0, 1, NEG_INFINITY, result.getMaximumValue());
	        }
        }
        else
            //result = (ReducedIntervalCongruence)this.clone();
            result = new ReducedIntervalCongruenceTop();
        
        return result;
    }

    /**
     * Returns the xor of this and rhs.
     */
    public Value xor(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        Value result;
        
    	long allZeroes = 0;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allZeroes)
    	{
    		return new Value(this);
    	}
    	
    	if(rhs.equals(this))
    	{
    		return new Value(allZeroes);
    	}
    	
        // the only case that we handle is when rhs is defined and both RICs contain
        // just a single number.   If either RIC contains multiple numbers, then we 
        // throw an exception signifying we don't know how to handle it.
        if(rhs.isDefined())
        {
            result = new Value();
            if(isSingleNumber() && rhs.isSingleNumber())
                result.set(getMinimumValue() ^ rhs.getMinimumValue());
            else
                throw new UndefinedOperationException();
        }
        else
            result = new Value();
        
        return result;
    }

    /**
     * Returns the xor of this and rhs.
     */
    public Value xor(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        // if rhs is defined, then we don't know how to handle that.  
        if(rhs.isDefined())
            throw new UndefinedOperationException();
        
        return new Value();
    }   
    /**
     * Returns the or of this and rhs.
     */
    public Value or(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        Value result;

        if(rhs.equals(this))
    	{
    		return new Value(this);
    	}
        
        long allZeroes = 0;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allZeroes)
    	{
    		return new Value(this);
    	}
    	
    	long allOnes = 4294967295L;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allOnes)
    	{
    		Value value = new Value();
    		value.setRIC(rhs);
    		return value;
    	}
        
        // the only case that we handle is when rhs is defined and both RICs contain
        // just a single number.   If either RIC contains multiple numbers, then we 
        // throw an exception signifying we don't know how to handle it.
        
        if(rhs.isDefined())
        {
            result = new Value();
            if(isSingleNumber() && rhs.isSingleNumber())
            {
                result.set(getMinimumValue() | rhs.getMinimumValue());
            }
            else
            {
                throw new UndefinedOperationException();
            }
        }
        else
        {
            result = new Value();
        }
        
        return result;
    }

    /**
     * Returns the or of this and rhs.
     */
    public Value or(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        // if rhs is defined, then we don't know how to handle that.  
        if(rhs.isDefined())
            throw new UndefinedOperationException();
        
        return new Value();
    }  
    
    /**
     * Returns the and of this and rhs.
     */
    public Value and(final IReducedIntervalCongruence rhs) throws UndefinedOperationException
    {
        Value result;

        if(rhs.equals(this))
    	{
    		return new Value(this);
    	}
        
    	long allOnes = 4294967295L;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allOnes)
    	{
    		return new Value(this);
    	}
        
    	long allZeroes = 0;
    	if(rhs.isSingleNumber() && rhs.getMinimumValue() == allZeroes)
    	{
    		Value value = new Value();
    		value.setRIC(rhs);
    		return value;
    	}
        
        // the only case that we handle is when rhs is defined and both RICs contain
        // just a single number.   If either RIC contains multiple numbers, then we 
        // throw an exception signifying we don't know how to handle it.
        
        if(rhs.isDefined())
        {
            result = new Value();
            if(isSingleNumber() && rhs.isSingleNumber())
            {
                result.set(getMinimumValue() & rhs.getMinimumValue());
            }
            else
            {
                throw new UndefinedOperationException();
            }
        }
        else
        {
            result = new Value();
        }
        
        return result;
    }

    /**
     * Returns the and of this and rhs.
     */
    public Value and(final IStackLocationSet rhs) throws UndefinedOperationException
    {
        // if rhs is defined, then we don't know how to handle that.  
        if(rhs.isDefined())
            throw new UndefinedOperationException();
        
        return new Value();
    }
}
package vsaAsgDomain;

import java.util.HashMap;

/**
 * Maintains a table consisting of mappings from MemoryArea to Value.
 * It is different from the MemoryTable in that the MemoryTable provides
 * an efficient means for storage, but may be somewhat slow during
 * value retrieval.  This ValueLookupTable is intended to be used
 * as a cache for quick retrieval and supplements the MemoryTable.
 */
public class ValueLookupTable 
{
	private HashMap<MemoryArea, Value> mappings = new HashMap<MemoryArea, Value>();
	
	/**
	 * Removes all mappings in this table.
	 *
	 */
	public void clear()
	{
		mappings.clear();
	}
	
	/**
	 * Retrieves the value stored at area.  Returns null
	 * if the area does not exist.
	 */
	public Value get(MemoryArea area)
	{
		return (Value)mappings.get(area);
	}
	
	/**
	 * Modifies the mapping so that is is set to "area-->value."  
	 * If a mapping from area does not exist, it is created.
	 */
	public void set(MemoryArea area, Value value)
	{
		mappings.put(area, value);
	}
}

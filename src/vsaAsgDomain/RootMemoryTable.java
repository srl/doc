package vsaAsgDomain;

import java.util.HashMap;
import java.util.Map;

public class RootMemoryTable implements IMemoryTable
{
    /**
     * @see IMemoryTable#addValue(Value)
     */
    public MemoryArea addValue(Value value)
    {
    	throw new UnsupportedOperationException();
    }
    
    /**
     * @see IMemoryTable#clear()
     */
    public void clear()
    {
    }
    
    /**
     * Returns a clone of this MemoryTable.
     */
    public Object clone()
    {
    	return this;
    }
    
    /**
     * @see IMemoryTable#getFullMap()
     * Since this class isn't intended to hold any actual data, an empty
     * map is always returned.
     */
    public Map<MemoryArea, Value> getFullMap()
    {
    	return new HashMap<MemoryArea, Value>();
    }
    
    /**
     * @see IMemoryTable#getValue(MemoryArea)
     * If the MemoryArea is not in the table, then
     * an undefined value is returned.
     */
    public Value getValue(MemoryArea source)
    {
    	Value result = new Value();
    	result.setToBottom();
    	return result;
    }
    
    /**
     * @see IMemoryTable#iterator()
     */
    public IMemoryTable.Iterator iterator()
    {
    	return new MemoryTableIterator();
    }
    
    /**
     * An iterator that can be used to travers this MemoryTable. 
     * Since the RootMemoryTable class is not intended to hold
     * any actual data, the hasNext() function will always
     * return false.
     */
    public class MemoryTableIterator implements Iterator
    {
    	public boolean hasNext()
    	{
    		return false;
    	}
    	
    	public MemoryAreaValuePair next()
    	{
    		throw new UnsupportedOperationException();
    	}
    }
    
    /**
     * @see IMemoryTable#union(IMemoryTable)
     */
    public IMemoryTable union(IMemoryTable rhs)
    {
    	return rhs;
    }
    
    /**
     * @see IMemoryTable#overapproximates(IMemoryTable)
     */
    public boolean overapproximates(final IMemoryTable rhs)
    {
    	return rhs.size() == 0;
    }
    
    /**
     * @see IMemoryTable#setValue(MemoryArea, Value)
     */
    public void setValue(MemoryArea dest, Value value)
    {
    	throw new UnsupportedOperationException();
    }

    /**
     * @see IMemoryTable#size()
     */
    public int size()
    {
    	return 0;
    }

    /**
     * Returns a string representation of this memory table.
     */
    public String toString()
    {
    	return "";
    }

    /**
     * @see IMemoryTable#widen(IMemoryTable)
     */
    public IMemoryTable widen(final IMemoryTable rhs)
    {
    	return this;
    }
}

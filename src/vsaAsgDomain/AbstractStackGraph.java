/*
 * Created on Mar 11, 2005
 */
package vsaAsgDomain;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import x86InstructionSet.Instruction;



/**
 * Maintains a graph represention of the stack at various times during execution.
 * Each node stores an over-approximation of the true run-time value at the 
 * location on the stack.  Methods are provided for adding new nodes to the graph
 * and traversing the graph, however no nodes can be removed.  
 * <p>
 * A special node represents the bottom of the stack.  The bottom of the stack 
 * does not hold any value and is the root of the entire graph--that is, all
 * nodes are successors of the stack bottom.
 */
public class AbstractStackGraph implements Cloneable
{
	private StackLocation		m_bottom;			// the bottom of the stack.
	private int							numEdges = 0;	// number of edges in the graph.
	private int							numNodes = 0;	// number of nodes in the graph.
																				// numEdges and numNodes are used by the
																				// overapproximates function.

    /**
     * Table of instruction mapped to the abstract stack graph nodes they created.
     */
    private static NodeCreatorTable nodeCreatorTable = new NodeCreatorTable(); 

    /**
     * List containing references to all graph nodes.
     */
    private static List<StackLocation> nodes = new LinkedList<StackLocation>(); 
    
    /**
     * Creates a new AbstractStackGraph beloging to the specified state.
     */
    public AbstractStackGraph()
    {
      nodeCreatorTable = new NodeCreatorTable();
      m_bottom = new StackLocation();
      nodes.clear();
      nodes.add(m_bottom);
      numNodes++;
    }
    
    /**
     * Creates an edge from source node to target node. (For instance,
     * source node is pushed on top of target node).
     */
    public void addEdge(StackLocation source, StackLocation target)
    {
    	if(!source.getSuccessors().has(target) || !target.getPredecessors().has(source))
    	{
    		// if the edge does not already exist, then add it.
            source.addSuccessor(target);
            target.addPredecessor(source);
            numEdges ++;
    	}
    }
    
    /**
     * Adds a new node to this graph.
     * @param successor reference to the node that should precede the new node.  
     * @param newNode the node to be added.
     * @param creator instruction that created the new node. 
     */
    public void addNode(StackLocation successor, 	
            			StackLocation newNode,
            			Instruction creator)
    {
        successor.addPredecessor(newNode);
        newNode.addSuccessor(successor);
        
        numNodes++;
        numEdges++;
        
        nodes.add(newNode);
        nodeCreatorTable.add(creator, new NodeChain(newNode, newNode));
    }
    
    /**
     * Adds count number of nodes, beginning from node start.  Each node is 
     * tagged with the address of the instruction which caused the new nodes 
     * to be created. Each node added will be left unitialized.
     * 
     * @param start		Node from which new nodes should succeed.
     * @param count		Number of nodes to be added. must be >= 0
     * @param creator	instruction that created the new nodes.
     * @return	The last node created.
     */
    public StackLocation addNodes(StackLocation start, 
            					  long count, 
            					  Instruction creator)
    {
      StackLocation currentNode;	// the node current being examined.
      StackLocation nextNode;			// the new node being added (the next to be examined).
      NodeChain			new_nodes = new NodeChain();	// holds the chain of nodes created.
        
      if(count < 0)
        throw new IllegalArgumentException();

    	new_nodes = nodeCreatorTable.get(creator);
    	if(new_nodes != null)
    	{
    		if(start != new_nodes.getTop())
    			throw new IllegalArgumentException();
    	}
    	
    	if(new_nodes == null)
    	{
    		new_nodes = new NodeChain();
        nodeCreatorTable.add(creator, new_nodes);
    	}
    
    /* ASSUMPTION: All StackLocation created by an instruction 
     * are created in one go, and are stacked on top of each other.
     * The 'index' of the node gives the order.
     * The 'start' node, the first one (with index 0), is created
     * before this method is called.
    */
      currentNode = start;
      for(int i = 0; i < count; i++)
      {
    	// create a new stack location
        nextNode = new StackLocation();
        nextNode.setCreator(creator);

        // put it in the chain of nodes created by this
        // 'creator'.
        // index is i+1, since 'start' has the index 0
        new_nodes.add(nextNode);
        nextNode.setNodeIndex(i + 1);
        if(new_nodes.size() == 1)  // is first node
          new_nodes.setBottom(nextNode);
        if(i == count-1)    // is last node
          new_nodes.setTop(nextNode);
        
        
        // put the node and edge in ASG
        // create an ASG edge from new (next) node to prev (current) node
        numNodes++;       
        nodes.add(nextNode);
        addEdge(nextNode, currentNode);

        currentNode = nextNode;
      }
        
      return currentNode;
    }
    
    /**
     * Returns a clone of this AbstractStackGraph.  Note that the clone
     * stores references to the nodes in this graph. In other words, a
     * shallow copy of the nodes is performed, not a deep copy.  
     * This should not be a problem given the current implementation.
     */
    public Object clone()
    {
        try
        {
            AbstractStackGraph clone = (AbstractStackGraph)super.clone();
            clone.m_bottom = m_bottom;	// Note: the clone stores the exact same node references!
            clone.numEdges = numEdges;
            clone.numNodes = numNodes;
            return clone;
        }
        catch(CloneNotSupportedException e)
        {
            throw new Error("This should never happen");
        }
    }
    
    /**
     * Returns an array contains the addresses of each node in this graph.
     */
    public Value[] getAllAddresses()
    {
  		StackLocationSet 	address;
    	int 							i;
    	Iterator 					iter;
  		StackLocation 		node;
    	Value 						result[] = new Value[nodes.size()];

    	i = 0;
    	iter = nodes.iterator();
    	while(iter.hasNext())
    	{
    		node 			= (StackLocation)iter.next();
    		address 	= new StackLocationSet();
    		address.addToSet(node);
    		result[i] = new Value( address );
    		i++;
    	}
    	
    	return result;
    }
    
    /**
     * Returns a reference to the bottom of the stack.    
     */
    public StackLocation getBottom()
    {
        return m_bottom;
    }
  
    /** 
     * Returns the chain of nodes created by the input instruction.
     * Return nulli if no nodes have been created by the input instruction.
     */
    public NodeChain getNodesCreatedBy(final Instruction instruction)
    {
        return nodeCreatorTable.get(instruction);
    }
    
  /**
   * Returns the height of a node in the abstract stack graph, relative
   * to bottom.  Height of bottom is zero.
   */
  public int getNodeHeight(StackLocation node)
  {
  	class WorklistElement
  	{
  		int 							height;
  		StackLocation 		node;
  		StackLocationSet 	visited;
  		
  		public WorklistElement(int height, StackLocation node, StackLocationSet visited)
  		{
  			this.height 	= height;
  			this.node 		= node;
  			this.visited 	= visited;
  		}
  	}
  	
		WorklistElement            element;						// an element pulled from the worklist.
		IStackLocationSet.Iterator iter;
		StackLocation 	           next;								// the next stack-location to be added to worklist
  	Queue<WorklistElement>     worklist = 
      new LinkedList<WorklistElement>(); // collection of worklist elements.	
  	
  	worklist.add(new WorklistElement(0, node, new StackLocationSet()));
  	while(!worklist.isEmpty())
  	{
  		element = (WorklistElement)worklist.remove();
  		
  		// if this element is bottom, then we know the height of the node.
  		if(element.node == getBottom())
  			return element.height; 

  		// mark this node as visited to avoid loops.
			element.visited.addToSet(element.node);
  		
  		// add each successor node to the worklist.  
  		iter = element.node.getSuccessors().iterator();
  		while(iter.hasNext())
  		{
  			next = iter.next();
  			if(!element.visited.has(next))
  			{
  				worklist.add( new WorklistElement(element.height + 1, next, 
  						(StackLocationSet)element.visited.clone()) );
  			}
  		}
  	}
  	
  	throw new Error("This should never happen");
  }
    
    /**
     * Returns a set of all nodes that lie on any path from start to end by
     * travelling down the stack (that is, by only travelling to successors,
     * not predecessors). 
     */
    public IStackLocationSet getNodesOnPathTo(StackLocation start, StackLocation end)
    {
    	IStackLocationSet.Iterator iter;
  		StackLocation              node;
    	IStackLocationSet          result = new StackLocationSet();
  		StackLocation              successor;
    	List<StackLocation>        visited = new LinkedList<StackLocation>();
    	Queue<StackLocation>       worklist = new LinkedList<StackLocation>();

    	worklist.add(start);
    	while(!worklist.isEmpty())
    	{
    		node = (StackLocation)worklist.remove();
    		visited.add(node);
    		if(pathExists(node, end))
    		{
    			result.addToSet(node);

    			if(node != end)
  	    	{
  		    	iter = node.getSuccessors().iterator();
  		    	while(iter.hasNext())
  		    	{
  		    		successor = iter.next();
  	    			if(!visited.contains(successor))
  	    				worklist.add(successor);
  		    	}
  	    	}
    		}
    	}
    	
			return result;
    }
    
    /**
     * Returns a set of all nodes that lie on any path from start to end by
     * travelling down the stack (that is, by only travelling to successors,
     * not predecessors), excluding the nodes that may be above the start node.  
     * This function differs from getNodesOnPathTo, getNodesOnPathTo includes
     * nodes that are above the start node (there may be a back-edge that points
     * to that node, for instance). 
     */
    public IStackLocationSet getNodesOnPathToB(StackLocation start, StackLocation end)
    {
      class Element
      {
        public LinkedList<StackLocation> path;
        public LinkedList<StackLocation> visited;
        public Element(LinkedList<StackLocation> path, LinkedList<StackLocation> visited)
        {
          this.path = path;
          this.visited = visited;
        }
      }

      IStackLocationSet.Iterator  iter;
  		StackLocation               node;
  		IStackLocationSet           result = new StackLocationSet();
  		StackLocation               successor;
  		LinkedList<StackLocation>   visited = new LinkedList<StackLocation> ();
    	Queue<Element> 							worklist = new LinkedList<Element>();
      LinkedList<StackLocation>   path = new LinkedList<StackLocation>();

    	
    	path.add(start);
    	visited.add(start);
    	worklist.add( new Element(path, visited) );
    	while(!worklist.isEmpty())
    	{
    		Element element = worklist.remove();
    		path = element.path;
    		visited = element.visited;
    		node = (StackLocation)path.get(path.size()-1);
    		
    		visited.add(node);
    		
    		if(node == end)
    		{
    			java.util.Iterator iter2 = path.iterator();
    			while(iter2.hasNext())
    				result.addToSet((StackLocation)iter2.next());
    		}
    		else
	    	{
  				iter = node.getSuccessors().iterator();
		    	while(iter.hasNext())
		    	{
		    		successor = iter.next();
		    		if(!visited.contains(successor))
		    		{
	    				LinkedList<StackLocation> nextPath = (LinkedList<StackLocation>)path.clone();
		    			nextPath.addLast(successor);	    				
		    			worklist.add( new Element(nextPath, (LinkedList<StackLocation>)visited.clone()) );
		    		}
		    	}
	    	}
    	}
    	
			return result;
    }

    /**
     * Returns true if there exists a path from start to bottom that includes node. 
     */
    public boolean isOnPathToBottom(StackLocation node, StackLocation start)
    {
    	IStackLocationSet path = getNodesOnPathTo(start, getBottom());
    	return path.has(node);
    }
    
    /**
     * Merges this graph with rhs.
     */
    public void merge(AbstractStackGraph rhs)
    {
        this.numEdges = Math.max(this.numEdges, rhs.numEdges);
        this.numNodes = Math.max(this.numNodes, rhs.numNodes);
    }
    
    /**
     * Returns true if this graph overapproximates rhs.  An abstract stack graph 
     * over-approximates another graph if this graph contains at least as many nodes
     * as the other graph and this graph contains at least as many edges as the
     * other graph.
     */
    public boolean overapproximates(final AbstractStackGraph rhs)
    {
      return numEdges >= rhs.numEdges && numNodes >= rhs.numNodes;
    }

    /**
     * Returns true if there exists a path from start to end.
     */
    public boolean pathExists(StackLocation start, StackLocation end)
    {
  		StackLocation 							currentNode;
  		IStackLocationSet.Iterator 	iter;
			StackLocation 							successor;
    	IStackLocationSet 					visited;
    	Queue<StackLocation> 				worklist;

    	worklist 	= new LinkedList<StackLocation>();
    	visited 	= new StackLocationSet();
    	
    	worklist.add(start);
    	while(!worklist.isEmpty())
    	{
    		currentNode = (StackLocation)worklist.remove();
    		if(currentNode == end)
    			return true;
    		visited.addToSet(currentNode);
    		iter = currentNode.getSuccessors().iterator();
    		while(iter.hasNext())
    		{
    			successor = iter.next();
    			if(!visited.has(successor))
    				worklist.add(successor);
    		}
    	}
    	
    	return false;
    }
        
    /**
     * string representation
     */
    public String toString()
    {
        return "numNodes = " + numNodes + ", numEdges = " + numEdges;
    }
    
    /**
     * Widens this abstract stack graph.
     */
    public void widen(final AbstractStackGraph rhs)
    {
        // Widening the abstract stack graph is easy, because each graph
        // consists of references to the same nodes (i.e. a shallow
        // copy is performed when cloned.  Thus, the only thing we need to
        // update is the number of edges/nodes and the nodeCreatorTable.
        this.numEdges = Math.max(this.numEdges, rhs.numEdges);
        this.numNodes = Math.max(this.numNodes, rhs.numNodes);
    }
}

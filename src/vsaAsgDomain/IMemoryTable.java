package vsaAsgDomain;

import java.util.Map;

/**
 * Defines an interface for an implementation of
 * a memory table.  A memory table is some mapping
 * of MemoryAreas to Values.  
 */
public interface IMemoryTable extends Cloneable
{
	/**
     * Adds a new value to the memory table.
     */
    public MemoryArea addValue(Value value);
    
    /**
     * Erases all MemoryAreas from this table.
     * TODO: this method is only used in VsaAsgState.init().  VsaAsgState.init()
     * is probably not needed and neither is this function.
     */
    public void clear();
        
    /**
     * Returns a copy of this.
     */
    public Object clone();
    
    /**
     * Returns a map of MemoryArea-Value pairs. The pairs are obtained from
     * this MemoryTable, plus this MemoryTable's parent, plus this MemoryTable's
     * parent's parent, and so on.  Duplicates are removed--only the most 
     * current value for a particular MemoryArea is kept.
     * NOTE: This function should only be called by derivations of this class.
     * This function is not intended to be called by functions outside of this
     * class.  It is made public only because interfaces in Java cannot have
     * private functions.
     */
    public Map<MemoryArea, Value> getFullMap();
    
    /**
     * Retrieves the value for a specific MemoryArea.  
		 * Returns a reference to the value.
     * If the MemoryArea is not in the table, then
     * an undefined value is returned.
     */
    public Value getValue(MemoryArea source);
    
    /**
     * Returns an iterator that can be used to traverse the MemoryAreas in 
     * this MemoryTable.
     */
    public Iterator iterator();
    
    /**
     * Used for traversing a Memorytable.
     */
    public interface Iterator
    {
    	public boolean hasNext();
    	public MemoryAreaValuePair next();
    }
    
    /**
     * Used by IMemoryTableIterator.  Maintains a pairing of a MemoryArea and the
     * Value stored at that area.
     */
    public class MemoryAreaValuePair
    {
    	private MemoryArea 	memoryarea = null;
    	private Value 		value = null;
    	
    	/**
    	 * Creates this.
    	 */
    	public MemoryAreaValuePair(MemoryArea memoryarea, Value value)
    	{
    		this.memoryarea = memoryarea;
    		this.value 		= value;
    	}
    	
    	/**
    	 * Returns the MemoryArea stored in this pair.
    	 */
    	public MemoryArea getMemoryArea()	
    	{ 
    		return memoryarea; 
    	}
    	
    	/**
    	 * Returns the value portion of this pair.
    	 */
    	public Value getValue()
    	{
    		return value;
    	}
    }
    
    /**
     * Returns true if each value in this memory table
     * overapproximates each value in rhs
     */
    public boolean overapproximates(final IMemoryTable rhs);
    
    /**
     * Sets the value for a specific MemoryArea.e
     */
    public void setValue(MemoryArea dest, Value value);

    /**
     * Returns the number of memory areas in this table.
     */
    public int size();

    /**
     * Returns a string representation of this memory table.
     */
    public String toString();

    /**
     * Returns the union of two MemoryTables.  Each matching 
     * value from each input tables will be merged. 
     */
    public IMemoryTable union(IMemoryTable rhs);
    
    /**
     * Widens this state.  Returns result.
     */
    public IMemoryTable widen(final IMemoryTable rhs);
}

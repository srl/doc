/*
 * UndefinedOperationException.java
 * Created on Mar 21, 2005
 */
package vsaAsgDomain;

/**
 * Signifies event where an arithmetic operation cannot be
 * completed.  For example, subtracting two StackLocations 
 * may throw this execption, since we do not know how to 
 * subtract StackLocations. 
 */
public class UndefinedOperationException extends Exception
{
    public UndefinedOperationException() {}
    
    public UndefinedOperationException(String msg) { super(msg); }
}

/*
 * StackGraphLabelProvider.java
 * Created on Apr 11, 2005
 */
package vsaAsgDomain;

import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.swt.graphics.Image;

import domain.Register;

/**
 * LabelProvider for the StackView.
 */
public class StackGraphLabelProvider implements ui.directedGraph.IDirectedGraphLabelProvider
{

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ILabelProvider#getImage(java.lang.Object)
     */
    public Image getImage(Object element)
    {
        return null;
    }
    
    /**
     * Returns the title of element.
     */
    public String getTitle(Object element)
    {
    	StackLocation node = ((StackGraphContentElement)element).node;
    	return node.getDisplayName();
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.ILabelProvider#getText(java.lang.Object)
     */
    public String getText(Object elementIn)
    {
        StackGraphContentElement element;

        if(elementIn instanceof StackGraphContentElement)
        {
            element = (StackGraphContentElement)elementIn;
            return ((VsaAsgState)element.state).getMemoryTable().getValue(
                    element.node.getMemoryArea()).toString();
        }

        return null;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#addListener(org.eclipse.jface.viewers.ILabelProviderListener)
     */
    public void addListener(ILabelProviderListener listener)
    {

    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#dispose()
     */
    public void dispose()
    {
    }

    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#isLabelProperty(java.lang.Object, java.lang.String)
     */
    public boolean isLabelProperty(Object element, String property)
    {
        return false;
    }

    /**
     * @see ui.directedGraph.IDirectedGraphLabelProvider#isSelected(Object, ISelection)
     */
    public boolean isSelected(Object element, ISelection selection)
    {
    	domain.Value 			esp;
    	StackLocation 		inputElement;
    	IStackLocationSet stackTops;
    	
    	inputElement 	= ((StackGraphContentElement)element).node;
    	esp 					= ((StackGraphContentElement)element).state.getRegisterValue(Register.ESP);
    	stackTops 		= ((Value)esp).getStackLocations();
    	return stackTops.has(inputElement);
    }
    
    /* (non-Javadoc)
     * @see org.eclipse.jface.viewers.IBaseLabelProvider#removeListener(org.eclipse.jface.viewers.ILabelProviderListener)
     */
    public void removeListener(ILabelProviderListener listener)
    {
    }

}

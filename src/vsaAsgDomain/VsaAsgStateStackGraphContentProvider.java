package vsaAsgDomain;

import x86InstructionSet.Instruction;

/**
 * Provides stack contents from the Begin state of an instruction. 
 */
public class VsaAsgStateStackGraphContentProvider extends
    StackGraphContentProvider
{
  /* (non-Javadoc)
   * @see vsaAsgDomain.StackGraphContentProvider#getState(x86InstructionSet.Instruction)
   */
  @Override
  protected domain.State getState(Object obj)
  {
	if (obj instanceof VsaAsgState) 
		return (VsaAsgState) obj;
	if (obj instanceof Instruction) {
	Instruction inst = (Instruction) obj;
		return inst.getBeginState();
	}
	return null;
    // return (VsaAsgState) obj;
  }
}


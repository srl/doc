/*
 * ReducedIntervalCongruence.java
 * Created on Mar 11, 2005
 */
package vsaAsgDomain;

import abstractBoolean.AbstractBoolean;

/**
 * A Reduced Interval Congruence (RIC) is a hybrid domain that merges the 
 * notion of interval with that of congruence. Since an interval captures 
 * the notion of upper and lower bound [4] and a congruence captures the 
 * notion of stride information, one can use RIC�s to combine the best of 
 * both worlds. An RIC is a formal, well defined, and well structured way 
 * of representing a finite set of integers that are equally apart.
 * <p>
 * For example, say we need to over-approximate the set of integers {3,5,9}. 
 * An interval over-approximation of this set would be [3,9] which contains 
 * the integers 3, 4, 5, 6, 7, 8, and 9; a congruence representation would 
 * note that 3, 5, and 9 are odd numbers and over-approximate {3,5,9} with 
 * the set of all odd numbers 1,3,5,7,�. Both of these approximations are 
 * probably much too conservative to achieve a tight approximation of such 
 * a small set. The set of odd numbers is infinite and the interval [3,9] 
 * does not capture the stride information and hence loses some precision.
 * <p>
 * In the above example, the RIC 2[1,4] +1, which represents the set of 
 * integer values {3, 5, 7, 9} clearly is a tighter over-approximation of 
 * our set.
 * <p>
 * In the context of VSA-ASG, an RIC may contain some value or may be
 * undefined.  The classes DefinedReducedIntervalCongruence and
 * UndefinedReducedIntervalCongruence represent these two possibilities. 
 * Given a ReducedIntervalCongruence, the user can determine if it is
 * defined by calling isDefined(), but in most cases, this isn't necessary,
 * since both the defined and undefined RIC classes support many of the
 * same functions.  
 * 
 * @author Michael
 */
public interface IReducedIntervalCongruence extends Cloneable
{
    public final static int NEG_INFINITY = Integer.MIN_VALUE;
    public final static int POS_INFINITY = Integer.MAX_VALUE;
    
    /**
     * Adds this RIC with another RIC.
     * @return this + rhs
     */
    public Value add(final IReducedIntervalCongruence rhs);
    
    /**
     * Adds this set with a StackLocationSet.
     * @return this + rhs
     */
    public Value add(final IStackLocationSet rhs) throws UndefinedOperationException;
    
    /**
     * Returns a clone of this RIC.
     */
    public Object clone();

    /**
     * Divides this RIC by an RIC.
     * @return this / rhs
     * @throws UndefinedOperationException
     */
    public Value divide(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
    /**
     * Divide this set by a stack location set.
     * @return this / rhs
     * @throws UndefinedOperationException
     */
    public Value divide(final IStackLocationSet rhs) throws UndefinedOperationException;

    /**
     * Returns true if this RIC equals rhs. This function uses a different definition of equality
     * than the function isEqualTo.  isEqualTo is intended to be used during the abstract
     * interpretation, whereas equals is used to determine if two RICs hold the same data.  
     * The function equals would state that the RICs 1[1,2]+0 and 1[1,2]+0 are equal, but
     * isEqualTo would return Top, indicating that the two might be equal. 
     */
    public boolean equals(final IReducedIntervalCongruence rhs);
    
    /** 
     * Returns stride value of this RIC.
     */
    public int getStride();
    
    /**
     * Returns minimun range value of this RIC.
     */
    public long getRangeMin();
    
    /**
     * Return maximum range value of this RIC.
     */
    public long getRangeMax();
    
    /** 
     * Returns base value of this RIC.
     */
    public long getBase();
    
    /**
     * Returns the maximum value that this RIC represents.
     */
    public long getMaximumValue();

    /**
     * Returns the minimum value that this RIC represents.
     */
    public long getMinimumValue();
    
    /**
     * Determines if this RIC is defined or undefined.  Used in cases where
     * the caller is about to perform some action that is only valid on
     * a defined (or undefined) RIC.
     * @return true if this RIC is defined.
     */
    public boolean isDefined();
    
    /**
     * Returns abstractBoolean.True if this RIC is guaranteed to be equal
     * to rhs, abstractBoolean.False if this RIC is guaranteed not to be
     * equal to rhs, and abstractBoolean.Top if it is possible for this
     * RIC to be equal to rhs.
     */
    public AbstractBoolean isEqualTo(IReducedIntervalCongruence rhs);
    
    /**
     * Returns abstractBoolean.True if this RIC is guaranteed to be greater
     * than rhs, abstractBoolean.False if this RIC is guaranteed to not be
     * greater than rhs, and abstractBoolean.Top otherwise.
     */
    public AbstractBoolean isGreaterThan(final IReducedIntervalCongruence rhs);

    /**
     * Returns true if this > rhs.  An RIC A is considered greater than
     * a StackLocationSet B if there is at least one member in A that is
     * greater than at least one member in B. 
     */
    public boolean isGreaterThan(final IStackLocationSet rhs);

    /**
     * Returns abstractBoolean.Top if this RIC is guaranteed to be less than
     * rhs, abstractBoolean.False if this RIC is guaranteed to not be less
     * than rhs, abstractBolean.Top if this RIC can be less than rhs, but is not
     * guaranteed to be.
     */
    public AbstractBoolean isLessThan(final IReducedIntervalCongruence rhs);

    /**
     * Returns true if this > rhs.  An RIC A is considered less than
     * a StackLocationSet B if there is at least one member in A that is
     * less than at least one member in B. 
     */
    public boolean isLessThan(final IStackLocationSet rhs);
    
    /**
     * Returns true if this RIC represents only one number.
     */
    public boolean isSingleNumber();

    /**
     * Returns an iterator that can be used to traverse all number represented 
     * by this RIC. 
     */
    public Iterator iterator();
    public interface Iterator
    {
        public boolean 	hasNext();
        public long	 	next();
    }
    
    /**
     * Merges the values in rhs with the values in this RIC.
     * @return this union rhs (result may over-approximate true value)
     */
    public IReducedIntervalCongruence merge(final IReducedIntervalCongruence rhs);
    
    /**
     * Multiplies this RIC with another RIC.
     * @return this * rhs
     * @throws UndefinedOperationException
     */
    public Value multiply(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
    /**
     * Multiplies this set with an RIC.
     * @return this * rhs
     * @throws UndefinedOperationException
     */
    public Value multiply(final IStackLocationSet rhs) throws UndefinedOperationException;

    /**
     * Formats the RIC so that the base will always be nonzero when possible.
     *   Example: 0 + 2[3,5] would become 6 + 2[0,2].
     * Also ensures that rangeMin &lt; rangeMax.
     */
    public void organize();
    
    /**
     * Returns true if this RIC overapproximates rhs.
     */
    public boolean overapproximates(final IReducedIntervalCongruence rhs);
    
    /**
     * Assigns this RIC to a constant value.
     */
    public void set(long value);

    /**
     * Assigns base, stride, and interval range of RIC
     */
    public void set(long base, int stride, long intervalmin, long intervalmax);

    /**
     * Returns the number of elemnts represented by this RIC.
     */
    public long size();

    /**
     * Subtracts an RIC from this RIC.
     * @return this - rhs
     */
    public Value subtract(final IReducedIntervalCongruence rhs);
    
    /**
     * Subtracts a stack location set from this RIC.
     * @return this - rhs
     * @throws UndefinedOperationException
     */
    public Value subtract(final IStackLocationSet rhs) throws UndefinedOperationException;
    
    /**
     * Returns a string representation of this RIC. 
     */
    public String toString();
    
    /**
     * Widens this RIC.
     */
    public IReducedIntervalCongruence widen(final IReducedIntervalCongruence rhs);

    /**
     * Returns the xor of this and rhs.
     * @return this XOR rhs
     */
    public Value xor(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;

    /**
     * Returns the xor of this and rhs.
     * @return this XOR rhs
     */
    public Value xor(final IStackLocationSet rhs) throws UndefinedOperationException;

    /**
     * Returns the or of this and rhs.
     * @return this OR rhs
     */
    public Value or(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;

    /**
     * Returns the or of this and rhs.
     * @return this OR rhs
     */
    public Value or(final IStackLocationSet rhs) throws UndefinedOperationException;

    /**
     * Returns the AND of this and rhs.
     * @return this AND rhs
     */
    public Value and(final IStackLocationSet rhs) throws UndefinedOperationException;

    /**
     * Returns the and of this and rhs.
     * @return this AND rhs
     */
    public Value and(final IReducedIntervalCongruence rhs) throws UndefinedOperationException;
    
}

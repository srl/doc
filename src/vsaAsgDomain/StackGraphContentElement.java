/*
 * StackGraphContentElement.java
 * Created on Apr 14, 2005
 */
package vsaAsgDomain;

import domain.State;

/**
 * Used to dislay the stack graph on the DirectedGraphViewer.  Each
 * element holds a StackLocation and a State.  The 
 * StackGraphContentProvider and StackGraphLabelProvider use this
 * class to communicate with each other and with the DirectedGraphViewer.
 */
public class StackGraphContentElement
{
    public StackLocation node;
    public State state;
    
    public StackGraphContentElement(State state, StackLocation node)
    {
        this.state = state;
        this.node = node;
    }
}

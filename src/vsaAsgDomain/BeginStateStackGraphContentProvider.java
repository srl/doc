package vsaAsgDomain;

import x86InstructionSet.Instruction;

/**
 * Provides stack contents from the Begin state of an instruction. 
 */
public class BeginStateStackGraphContentProvider extends
    StackGraphContentProvider
{
  /* (non-Javadoc)
   * @see vsaAsgDomain.StackGraphContentProvider#getState(x86InstructionSet.Instruction)
   */
  @Override
  protected domain.State getState(Object obj)
  {
	Instruction instruction = (Instruction) obj; 
    return instruction.getBeginState();
  }
}

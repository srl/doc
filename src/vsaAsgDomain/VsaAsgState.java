/*
 * VsaAsgState.java
 * Created on Mar 11, 2005
 */
package vsaAsgDomain;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.Instruction;
import logger.Log;
import domain.BitCount;
import domain.MemoryValue;
import domain.AbstractState;
import domain.Register;
import domain.State;

/**
 * Maintains the state of registers and memory.  The VsaAsgState is capable of managing
 * register values and values placed on the stack, however arbitrary memory locations,
 * such as the heap, are not stored.  
 */
public class VsaAsgState extends AbstractState 
{

	/**
     * The stack graph.    
     */
    private AbstractStackGraph		abstractStackGraph;
    
    /**
     * Stores the Values for each piece of memory.  This includes
     * the Stack and Registers.
     */
    private IMemoryTable memory = new MemoryTable();
    
    /**
     * Represents the set of registers.
     */
    private static MemoryArea	registers[];
    
    /**
     * Creates a new State.  You must call init before using this state
     * for interpretation.
     */
    public VsaAsgState()
    {
    }
    
    /**
     * Returns a copy of this State.
     */
    public Object clone()
    {
        try
        {
            VsaAsgState clone 			= (VsaAsgState)super.clone();
            clone.memory				= (IMemoryTable)memory.clone();
            clone.abstractStackGraph	= (AbstractStackGraph)abstractStackGraph.clone();
            return clone;
        }
        catch(Exception e)
        {
        	throw new Error("This exception should never occur.");
        }
    }
    
    /**
     * Make it 'forward' compatible with its subclasses
     * that extend it to include contexts.
     */
    public Map getMapping()
	{
		Map<String, VsaAsgState> map = new HashMap<String, VsaAsgState>();
		map.put("VSAASG", this);
		return map;
	}
    /**
     * Make it 'forward' compatible with its subclasses
     * that extend it to include contexts
     */
   
    public String getContextString() {
		return "VSAASG";
	}
    /**
     * Make it 'forward' compatible with its subclasses
     * that extend it to include contexts
     */
    
	public VsaAsgState getCurrentState() {
		return this;
	}
	
	  /**
     * Make it 'forward' compatible with its subclasses
     * that extend it to include contexts
     */
	public VsaAsgState get(String context){
		if (context == "VSAASG") return this;
		return null;
	}
    
    /**
     * Returns a new, unitialized Value designed to work with this State.
     */
    public domain.Value createNewValue() 
    {
        return new Value();
    }
    
    /**
     * Returns a new, initialized value designed to work with this State.
     */
    public domain.Value createNewValue(long v)
    {
    	Value result = new Value();
    	result.set(v);
    	return result;
    }
    
    /**
     * Returns a reference to the AbstractStackGraph of this state.
     */
    public AbstractStackGraph getAbstractStackGraph()
    {
        return abstractStackGraph;
    }
    
    /**
     * Returns a reference to the internal memory table.
     */
    public IMemoryTable getMemoryTable()
    {
        return memory;
    }
    
    /* (non-Javadoc)
     * @see domain.State#getRegisterValue(domain.Register)
     */
    public domain.Value getRegisterValue(Register register)
    {
      // if the user requests a value from a half-register or
      // quarter-register, then return an undefined value, because
      // we don't know how to break a 32-bit value into 16 or 8 bits.
      switch(register)
      {
	  	case AH:
	  	case AL:
	  	case AX:
  	    return new Value();

    	case BH:
    	case BL:
    	case BX:
  	    return new Value();
    	    
    	case CH:
    	case CL:
    	case CX:
  	    return new Value();

    	case DH:
    	case DL:
      case DX:
          return new Value();
    	
      case SP:
          return new Value();

      case BP:
          return new Value();

      case SI:
      		return new Value();
      		
      case DI:
          return new Value();
	    }

      return (Value)memory.getValue(registers[register.ordinal()]).clone();
    }
    
    /**
     * Returns a list of addresses that are currently on the 
     * top of the stack, i.e. the return address.  Does not
     * modify the stack pointer (esp).  This function is
     * similar to called State.load(esp), however this
     * function should try to make a best-effort attempt
     * at reducing the amount of over-approximations (whenever possible), 
     * because over-approximating the return address can reduce the
     * analysis results.  Calling State.load(esp) on the other
     * hand may result in too many over-approximations to be
     * useful.
     */
    public AddressList getReturnAddressAsList()
    {
        AddressList 												result = new AddressList();
      	IReducedIntervalCongruence 					ric;
        IReducedIntervalCongruence.Iterator ricIter;
        IStackLocationSet.Iterator 					stackiter;
        IStackLocationSet 									stackTops;
        
        stackTops = ((Value)getRegisterValue(Register.ESP)).getStackLocations();
        for(stackiter = stackTops.iterator(); stackiter.hasNext(); )
        {
        	ric = getMemoryTable().getValue( stackiter.next().getMemoryArea() ).getRIC();

        	if(ric.size() <= 5)
        	{
            ricIter = ric.iterator();
            while(ricIter.hasNext())
            {
                long returnAddress = ricIter.next();
                if(!result.has(new Address(returnAddress)))
                    result.add( new Address(returnAddress));
            }
        	}
        	else
        	{
            Log.write(Log.WARNING, "One of the stack tops contains more than 5 values. " +
                  " It will not be added to the list of return addresses.");
        	}
        }        
        return result;
    }
    
    /**
     * Accesses the stack locations at the top of the stack.
     * 
     * @return Set of stack locations that are stored in register ESP.
     */
    public IStackLocationSet getStackTop()
    {
    	return ((Value)getRegisterValue(Register.ESP)).getStackLocations();
    }

    /**
     * Returns the instructions that created the values on the top of the stack.
     */
    public Instruction[] getStackTopCreators()
    {
    	IStackLocationSet stackTops = ((Value)getRegisterValue(Register.ESP)).getStackLocations();
    	IStackLocationSet.Iterator iter;
    	Instruction[] topInstructions;
    	int i;
    	int numStackLocations;
    	
    	// count the number of stack-locations that are at the top of the stack.  This
    	// is how many instructions we will return. There is one exception however.  
    	// If the stack-location is the bottom of the stack, then it has no
    	// "creator" and so we do not want to include it.
    	numStackLocations = 0;
    	for( iter = stackTops.iterator(); iter.hasNext(); )
    	{
    	    if(iter.next() != getAbstractStackGraph().getBottom())
    	        numStackLocations++;
    	}
    	
    	topInstructions = new Instruction[numStackLocations];
    	i = 0;
    	for(iter = stackTops.iterator(); iter.hasNext(); )
    	{
    	    StackLocation stackLocation = (StackLocation)iter.next();
    	    if(stackLocation != getAbstractStackGraph().getBottom())
    	        topInstructions[i++] = stackLocation.getCreator();
    	}
    		
    	return topInstructions;
    }
        
    /**
     * Resets any initialization that might be needed.  This should be
     * called prior to perform any interpretation.  If a second file 
     * is interpreted, then this method should be called again. 
     * Any registers/memory initialization takes place here.
     */
    public void init()
    {
        memory.clear();
        
        // initialize the stack-location IDs.
        StackLocation.resetId();
        
        // initialize registers
        registers = new MemoryArea[Register.NUM_REGISTERS];
        for(int i = 0; i < Register.NUM_REGISTERS; i++)
            registers[i] = memory.addValue(new Value());
        
        // create the stack graph.
        abstractStackGraph = new AbstractStackGraph();
        
        // initialize ESP register to the top of the stack.
        setRegisterValue(Register.ESP, new Value(abstractStackGraph.getBottom()));
    }
    
    /**
     * @see AbstractState#iterator()
     */
    public java.util.Iterator iterator()
    {
    	return new StateIterator();
    }
    
    /**
     * @see domain.AbstractState#load(Value, BitCount)
     */
    public domain.Value load(domain.Value sourceIn, BitCount size)
    {
        if( !(sourceIn instanceof Value) )
            throw new IllegalArgumentException();
        
        Value source = (Value)sourceIn;
        
        // if the user wants to read 16 or 8 bits, then just store an undefined
        // value over the whole 32-bit region, because 16/8 bits are not yet handled.
        if(size.equals(BitCount._16Bits) || size.equals(BitCount._8Bits) )
            return new Value(); 
        
        if(source.getRIC().isDefined())
        {
            // if the RIC component is defined, then the user wants to load from
            // memory, but we don't yet support that, so return undefined value.
            return new Value();
        }
        else if(source.getStackLocations().isDefined())
        {
            // load from stack.
            Value result = new Value();
            IStackLocationSet.Iterator iter = source.getStackLocations().iterator();
            while(iter.hasNext())
            {
                Value r = memory.getValue(iter.next().getMemoryArea());
                if(result.isDefined() && r.isDefined())
                {
                    result.merge(r);
                }
                else if(r.isDefined() && !result.isDefined())
                    result = (Value)r.clone();
            }
            return (Value)result.clone();
        }
        else
        {
            // the entire value is undefined, meaning we were unable to 
            // determine where to load from.  Therefore, return an undefined value.
            return new Value();
        }
    }
    
    /**
     * Merges this state with rhs depending of each kind of analysis.
     */
    public void merge(final State rhs)
    {
    		memory = memory.union( ((VsaAsgState)rhs).memory); 
    		abstractStackGraph.merge( ((VsaAsgState)rhs).getAbstractStackGraph() );
    }
        
    /**
     * Returns true if this state is either an over-approximation
     * of rhsIn or is equal to rhsIn.  A state over-approximates another state if
     * each member of this state over-approximates each member in the other state.
     */
    public boolean overapproximates(final State rhsIn)
    {
            VsaAsgState rhs = (VsaAsgState)rhsIn;
            return getMemoryTable().overapproximates(rhs.getMemoryTable()) &&
            	abstractStackGraph.overapproximates(rhs.abstractStackGraph);
    }
    
    /**
     * Removes & returns the value at the top of the stack.
     */
    public domain.Value pop()
    {
        domain.Value esp = getRegisterValue(Register.ESP);
        domain.Value topValue = load(esp, BitCount._32Bits);
        setRegisterValue(Register.ESP, esp.add(createNewValue(4)));
        return (Value)topValue.clone();
    }
    
    /**
     * Pushes a value onto the stack.
     */
    public void push(domain.Value value)
    {
        setRegisterValue( Register.ESP, 
                getRegisterValue(Register.ESP).subtract(createNewValue(4)));
        store( getRegisterValue(Register.ESP), value);
    }
    
    /**
     * Assigns a value to a register.
     * @param register The register being written to.  Must be >= REGISTER_MIN and
     * 			<= REGISTER_MAX.  See detectObfuscation.State for possible values.
     * @param newValIn The value to be written.
     */
    public void setRegisterValue(Register register, final domain.Value newValIn)
    {
        Value newVal = (Value)newValIn;
        
        // Writing to one of the half-registers or quarter-registers is not yet
        // handled, so in these cases, just store an undefined value.
        switch(register)
        {
        	case AH:
        	case AL:
        	case AX:
        	    newVal = new Value();
        	    register = Register.EAX;
        	    break;

        	case BH:
        	case BL:
        	case BX:
        	    newVal = new Value();
        	    register = Register.EBX;
        	    break;

        	case CH:
        	case CL:
        	case CX:
        	    newVal = new Value();
        	    register = Register.ECX;
        	    break;

        	case DH:
        	case DL:
          case DX:
        	    newVal = new Value();
        	    register = Register.EDX;
        	    break;
        	
          case SP:
        	    newVal = new Value();
        	    register = Register.ESP;
        	    break;

          case BP:
        	    newVal = new Value();
        	    register = Register.EBP;
        	    break;

          case DI:
        	    newVal = new Value();
        	    register = Register.EDI;
        	    break;
        }

        
        // make sure ESP always points to a stack-location and not an RIC.  If
        // it ever points to an RIC or ever doesn't point to a stack-location, then
        // reset it to point to the stack bottom.
        if(register == Register.ESP &&
                (newVal.getRIC().isDefined() || !newVal.getStackLocations().isDefined()))
        {
            StackLocationSet stackBottom = new StackLocationSet();
            stackBottom.addToSet(abstractStackGraph.getBottom());
            newVal.set(stackBottom);
        }
        
        memory.setValue(registers[register.ordinal()], (Value)newVal.clone());
    }
    
    /**
     * Stores a specified value at the specified destination address.
     */
    public void store(domain.Value destIn, domain.Value valueIn)
    {
        if( !(destIn instanceof Value))
            throw new IllegalArgumentException();
        
        Value dest = (Value)destIn;
        Value value = (Value)valueIn;
        
        // if the user wants to store 16 or 8 bits, then just store an undefined
        // value over the whole 32-bit region, because 16/8 bits are not yet handled.
        if(value.getBitSize().equals(BitCount._16Bits) || value.getBitSize().equals(BitCount._8Bits) )
            value = new Value(); 

        
        if(dest.getRIC().isDefined())
        {
            // uses wants to write to an arbitrary memory location.  We dont yet
            // support this, so do nothing.
        }
        else if(dest.getStackLocations().isDefined())
        {
            // user wants to write to a stack-location(s). we can do this.
            IStackLocationSet.Iterator iter = dest.getStackLocations().iterator();
            while(iter.hasNext())
                memory.setValue(iter.next().getMemoryArea(), value);
        }
        else
        {
            // we were unable to determine where the user wants to write to, so
            // do nothing.
        }            
    }
    
    /**
     * string representation
     */
    public String toString()
    {
        return getMemoryTable().toString() + "\n" + getAbstractStackGraph().toString();
    }
    
    /**
     * Widens this state.
     */
    public void widen(final State rhsIn)
    {
            VsaAsgState rhs = (VsaAsgState)rhsIn;
            memory = memory.widen(rhs.getMemoryTable());
            getAbstractStackGraph().widen(rhs.getAbstractStackGraph());
    }

    /**
     * @see AbstractState#iterator()
     */
    private class StateIterator implements Iterator
    {
    	private Value addresses[] = getAbstractStackGraph().getAllAddresses();
    	private int 	index = 0;
    	
    	public boolean hasNext()
    	{
    		return index < addresses.length;
    	}
    	
    	public Object next()
    	{
    		domain.Value address;
    		domain.Value value;
    		
    		address = addresses[index];
    		value = load(address, BitCount._32Bits);
    		index++;
    		
    		return new MemoryValue(address, value);
    	}
    	
    	public void remove() throws UnsupportedOperationException
    	{
    		throw new UnsupportedOperationException();
    	}
    }
}
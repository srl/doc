/*
 * Value.java
 * Created on Mar 11, 2005
 */
package vsaAsgDomain;

import logger.Log;
import abstractBoolean.AbstractBoolean;
import domain.BitCount;


/**
 * Holds the contents of registers and memory.  Each value consists of a
 * ReducedIntervalCongruence and a set of StackLocations.  Either
 * one may be undefined at any time, or they may both be undefined,
 * meaning that this Value is undefined.  Upon creation, both the RIC
 * and set of stack locations is undefined.
 * 
 * Each Value keeps a reference to the State object that the Value
 * belongs to.  Thus, all constructors have a State parameter.
 * 
 * Functions are provided to perform various arithmetic operations on
 * Values, however these operations only work when given an
 * argument of type vsaAsgDomain.Value, not detectObfuscation.Value (the
 * base class).  An IllegalArgumentException is thrown if the argument
 * is of the wrong type.
 */
public class Value implements domain.Value
{
    private BitCount bitSize = BitCount._32Bits;  // assume 32-bit value.
        
    /**
     * Constructs a new uninitialized Value.
     */
   public Value()
    {
    	setToBottom();
    }
    
    /**
     * Creates a new Value with the RIC component initialized to
     * the ric.
     */
    public Value(IReducedIntervalCongruence ric)
    {
        m_ric 	= ric;
        m_stackLocations = new StackLocationSetTop();
    }
    
    /**
     * Creates a new Value with the StackLocationSet 
     * initialized to the given StackLocation
     */
    public Value(StackLocation stackLocation)
    {
        m_ric 	= new ReducedIntervalCongruenceTop();
        m_stackLocations = new StackLocationSet();
        m_stackLocations.addToSet(stackLocation);
    }

    /**
     * Creates a new Value with the StackLocationSet 
     * initialized to the given StackLocationSet.
     */
    public Value(IStackLocationSet set)
    {
        m_ric 	= new ReducedIntervalCongruenceTop();
        m_stackLocations = set;
    }

    /**
     * Creates a new Value with both the RIC and StackLocationSet
     * components initialized to the given values.
     */
    public Value(IReducedIntervalCongruence ric, 
            	 IStackLocationSet set)
    {
        m_ric 	= ric;
        m_stackLocations = set;
    }
    
    /**
     * Creates a new Value initialized to the constant value specified. 
     */
    public Value(long value)
    {
        m_ric 	= new ReducedIntervalCongruenceTop();
        m_stackLocations = new StackLocationSetTop();
        set(value);
    }
    
    /**
     * Adds this value to another value.
     * @return this + rhs
     */
    public domain.Value add(final domain.Value rhs)
    {
        if( !(rhs instanceof Value) )
            throw new IllegalArgumentException();
        
        Value 	result;
        Value	rhsValue = (Value)rhs;
        
        try
        {
            result = m_ric.add(rhsValue.m_ric);
            
            Value r = m_ric.add(rhsValue.m_stackLocations);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;

            r = m_stackLocations.add(rhsValue.m_ric);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;
            
            r = m_stackLocations.add(rhsValue.m_stackLocations);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;
        }
        catch(UndefinedOperationException e)
        {
            // The addition couldn't be completed.  Return an undefined Value.
            result = new Value();
        }
        return result;
    }
    
    /**
     * Returns a copy of this object.
     */
    public Object clone()
    {
        try
        {
            Value copy;
            copy = (Value)super.clone();
            copy.m_ric = (IReducedIntervalCongruence)m_ric.clone();
            copy.m_stackLocations = (IStackLocationSet)m_stackLocations.clone();
            return copy;
        }
        catch(CloneNotSupportedException e)
        {
            throw new Error("This should never happen.");
        }
    }
    
    /**
     * Divides this by another value..
     * @return this / rhs.
     */
    public domain.Value divide(final domain.Value rhs)
    {
        if( !(rhs instanceof Value) )
            throw new IllegalArgumentException();
                
        Value 	result;
        Value	rhsValue = (Value)rhs;
        
        try
        {
            result = m_ric.divide(rhsValue.m_ric);
	        result.merge( m_ric.divide(rhsValue.m_stackLocations) );
	        result.merge( m_stackLocations.divide(rhsValue.m_ric) );
	        result.merge( m_stackLocations.divide(rhsValue.m_stackLocations) );
        }
        catch(UndefinedOperationException e)
        {
            // The division couldn't be completed.  Return an undefined Value.
            result = new Value();
        }
        return result;
    }
    
    /**
     * Returns true if this equals rhs.  This function is uses a different
     * definition of the word 'equals' than the function isEqualTo does.  isEqualTo
     * is intended to be used during the abstract interpretation process, whereas
     * equals is used to determine if two Values have the same data. isEqualTo would
     * return Top if given the input (T,T); equals would return true given the same
     * input.
     */
    public boolean equals(Object obj)
    {
    	Value rhs = (Value)obj;
    	return getRIC().equals(rhs.getRIC()) && getStackLocations().equals(rhs.getStackLocations());
    }
    
    /**
     * Returns the size of this value in bits.  
     */
    public BitCount getBitSize()
    {
        return bitSize;
    }

    /**
     * Returns a reference to the RIC component of this Value.
     */
    public IReducedIntervalCongruence getRIC()
    {
        return m_ric;
    }

    /**
     * Returns a reference to the StackLocationSet component of this Value.
     */
    public IStackLocationSet getStackLocations()
    {
        return m_stackLocations;
    }

    /**
     * Returns true if this Value is defined.
     */
    public boolean isDefined()
    {
        return getRIC().isDefined() || getStackLocations().isDefined();
    }

    /**
     * Returns this == rhs.
     * @return AbstractBoolean.True if this and rhs both contain only one
     * 				member AND they are the same number.<br>
     *         AbstractBoolean.False if there is no member of this that is
     *         		equal to some member of rhs.<br>
     *         AbstractBoolean.Top   if there is a member of this that is
     *         		equal to some member of rhs AND there is a member of this
     *         		that is not equal to some member of rhs.
     */
    public AbstractBoolean isEqualTo(domain.Value rhsIn)
    {
    	Value rhs = (Value)rhsIn;
    	if(!this.isDefined() || !rhs.isDefined())
    		return AbstractBoolean.Top;
    	
    	if(this.getRIC().isDefined() && rhs.getStackLocations().isDefined())
    		return AbstractBoolean.Top;
    	
    	if(this.getStackLocations().isDefined() && rhs.getRIC().isDefined())
    		return AbstractBoolean.Top;
    	
    	if(!this.getStackLocations().isDefined() && !rhs.getStackLocations().isDefined())
    		return this.getRIC().isEqualTo(rhs.getRIC());
    	
    	if(!this.getRIC().isDefined() && !rhs.getRIC().isDefined())
    		return this.getStackLocations().isEqualTo(rhs.getStackLocations());
    	
    	return AbstractBoolean.Top;
    }
    
    /**
     * Returns this > rhs.
     * @return AbstractBoolean.True if all members of this
     *      		are greater than all members of rhs.<br>
     *         AbstractBoolean.False if there is no member of this
     *         		greater than at least one member of rhs.<br>
     *         AbstractBoolean.Top   if there is at least one member of
     *         		this that is greater than at least one member of rhs AND
     *         		there is at least one member of this that is not greater 
     *         		than at least one member of rhs.
     */
    public abstractBoolean.AbstractBoolean isGreaterThan(final domain.Value rhsIn)
    {
        Value rhs = (Value)rhsIn;
        
        if(!this.isDefined() || !rhs.isDefined())
        	return AbstractBoolean.Top;
        
        if(this.getStackLocations().isDefined() || rhs.getStackLocations().isDefined())
        	return AbstractBoolean.Top;
        
        return this.getRIC().isGreaterThan(rhs.getRIC());
    }
    
    /**
     * Returns this &lt; rhs.  
     * @return abstractBoolean.True if all members of this
     *      		are less than all members of rhs.<br>
     *         abstractBoolean.False if there is no member of this
     *         		less than at least one member of rhs.<br>
     *         abstractBoolean.Top   if there is at least one member of
     *         		this that is less than at least one member of rhs AND
     *         		there is at least one member of this that is not less 
     *         		than at least one member of rhs.
     */
    public abstractBoolean.AbstractBoolean isLessThan(final domain.Value rhsIn)
    {
        Value rhs = (Value)rhsIn;

        // The current implementation does not support comparisons of 
        // stack-locations.  If either Value contains a stack-location, return
        // Top.
        if(this.getStackLocations().isDefined() || rhs.getStackLocations().isDefined())
        	return AbstractBoolean.Top;
        
        // If either Value is undefined, return Top.
        if(!this.isDefined() || !rhs.isDefined())
        	return AbstractBoolean.Top;
        
        return this.getRIC().isLessThan(rhs.getRIC());
    }
    
    /**
     * Returns an iterator that can be used to traverse all numbers represented 
     * by this Value.
     */
    public domain.Value.Iterator iterator()
    {
        return new Iterator();
    }
    
    public class Iterator implements domain.Value.Iterator
    {
        public Iterator()
        { 
            // If the stackLocationSet is defined, then don't iterate
            // anything.  There is no way to convert a stack-location
            // to an integer.
            if(!m_stackLocations.isDefined())
            {
                m_iter = m_ric.iterator();
            }
        }
        
        public boolean 	hasNext()	{ return (m_iter == null) ? false : m_iter.hasNext(); }
        public long 	next()		
        { 
            if (size() > 100)
            {
              Log.write(Log.WARNING, "Someone is iterating a Value containing " + 
                  size() + " elements. This might be time-consuming.");
            }
            return m_iter.next();    
        }
        private IReducedIntervalCongruence.Iterator m_iter = null;
    }
    
    /**
     * Converts this Value to one that has not been assigned.
     */
    /**
     * TODO: This seems to be a misnomer. Undefined should be Bottom.
     * This is assigning Top.
     */
    public void makeUndefined()
    {
        m_ric = new ReducedIntervalCongruenceTop();
        m_stackLocations = new StackLocationSetTop();
    }
    
    /**
     * Merges the values in rhs with the values in this.
     */
    public void merge(final Value rhs)
    {
			m_ric = m_ric.merge(rhs.m_ric); 
      m_stackLocations = m_stackLocations.merge(rhs.m_stackLocations);
    }
    
    /**
     * Multipies this value with rhs.
     * @return this * rhs
     */
    public domain.Value multiply(final domain.Value rhs)
    {
        if( !(rhs instanceof Value) )
            throw new IllegalArgumentException();
        
        Value 	result;
        Value	rhsValue = (Value)rhs;
        
        try
        {
            result = m_ric.multiply(rhsValue.m_ric);
	        result.merge( m_ric.multiply(rhsValue.m_stackLocations) );
	        result.merge( m_stackLocations.multiply(rhsValue.m_ric) );
	        result.merge( m_stackLocations.multiply(rhsValue.m_stackLocations) );
        }
        catch(UndefinedOperationException e)
        {
            // the multiplication failed.  return an undefined Value.  
            result = new Value();
        }
        return result;
    }
    
    /**
     * Returns true if this value overapproximates rhs
     */
    public boolean overapproximates(final Value rhs)
    {
        if(!getRIC().isDefined() && getStackLocations().isDefined() && rhs.getRIC().isDefined())
            return false;
        
        if(!getStackLocations().isDefined() && getRIC().isDefined() && rhs.getStackLocations().isDefined())
            return false;
        
        return getRIC().overapproximates(rhs.getRIC()) &&
        	getStackLocations().overapproximates(rhs.getStackLocations());
    }
    
    /**
     * Assigns this to a constant.
     */
    public void set(long value)
    {
        m_ric = new ReducedIntervalCongruence();
        m_ric.set(value);
        m_stackLocations.clear();
    }

    /**
     * Assigns this Value to another value.
     */
    public void set(domain.Value newVal)
    {
        Value newValue = (Value)newVal;
        m_ric = (IReducedIntervalCongruence)newValue.m_ric.clone();
        m_stackLocations = (IStackLocationSet)newValue.m_stackLocations.clone();
    }
    
    /**
     * Assigns this to a StackLocationSet
     */
    public void set(IStackLocationSet set)
    {
        m_ric = new ReducedIntervalCongruenceTop();
        m_stackLocations = set;
    }
    
    /**
     * Assigns the size of this value in bits.
     */
    public void setBitSize(BitCount size)
    {
        bitSize = size;
    }
    
    /**
     * Assigns the RIC component of this Value.  The
     * StackLocationSet component is left unchanged.
     */
    public void setRIC(IReducedIntervalCongruence ric)
    {
        m_ric = ric;
    }
    
    /**
     * Assigns the StackLocationSet component of this Value.  
     * The RIC component is left unchanged.
     */
    public void setStackLocations(IStackLocationSet set)
    {
        m_stackLocations = set;
    }
    
    /**
     * Sets this to Top, meaning that this Value represents
     * all possible values.
     */
    public void setToTop()
    {
    	makeUndefined();
    }
    
    /**
     * Sets this Value to Bottom, meaning that this Value does
     * not represent any value.
     *
     */
    public void setToBottom()
    {
    	m_ric = new ReducedIntervalCongruenceBottom();
    	m_stackLocations = new StackLocationSetBottom();
    }
    
    /** 
     * Returns the number of elements represented by this Value.
     * If the value contains an infinite number of elements, then
     * 0 is returned.
     */
    public long size()
    {	
        // if this value represents stack-locoations, then return 0.  
        // This is because we can't convert the stack-lcoations into
        // any king of value, thus from the caller's point of view,
        // they don't represent anything.  (Note: we can convert
        // the stack-location's contents into a value, but not the
        // stack-location itself.
        if(m_stackLocations.isDefined())
            return 0;
        else
        {
            long size = m_ric.size();
            if(size == IReducedIntervalCongruence.POS_INFINITY)
                return 0;
            else             
                return m_ric.size();
        }
    }
    
    /** 
     * subtracts rhs from this value
     * @return this - rhs
     */
    public domain.Value subtract(final domain.Value rhs)
    {
        if( !(rhs instanceof Value) )
            throw new IllegalArgumentException();
        
        Value 	result;
        Value	rhsValue = (Value)rhs;
        
        try
        {
            result = m_ric.subtract(rhsValue.m_ric);
            
            Value r = m_ric.subtract(rhsValue.m_stackLocations);
            if(result.isDefined())
            {
                if(r.isDefined())
                    result.merge(r);
            }
            else
                result = r;
            
            r = m_stackLocations.subtract(rhsValue.m_ric);
            if(result.isDefined())
            {
                if(r.isDefined())
                    result.merge(r);
            }
            else
                result = r;
            
            r = m_stackLocations.subtract(rhsValue.m_stackLocations);
            if(result.isDefined())
            {
                if(r.isDefined())
                    result.merge(r);
            }
            else
                result = r;
        }
        catch(UndefinedOperationException e)
        {
            // the subtraction failed.  return an undefined Value.
            result = new Value();
        }
        return result;
    }
    
    /**
     * Returns a string representation of this Value.
     */
    public String toString()
    {
        return "(" + m_ric.toString() + ", " + m_stackLocations.toString() + ")";
    }
    
    /**
     * Widens this value.
     */
    public void widen(final Value rhs)
    {
        m_ric 				= getRIC().widen(rhs.getRIC());
        m_stackLocations 	= getStackLocations().widen(rhs.getStackLocations());            
    }
    
    /**
     * Returns the XOR of this and rhs.
     */
    public domain.Value xor(final domain.Value rhs)
    {
        if( !(rhs instanceof Value) )
            throw new IllegalArgumentException();
        
        Value 	result;
        Value	rhsValue = (Value)rhs;
        
        try
        {
            result = m_ric.xor(rhsValue.m_ric);
            
            Value r = m_ric.xor(rhsValue.m_stackLocations);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;
            
            r = m_stackLocations.xor(rhsValue.m_ric);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;
                
	        r = m_stackLocations.xor(rhsValue.m_stackLocations);
	        if(result.isDefined() && r.isDefined())
	            result.merge(r);
	        else if(r.isDefined() && !result.isDefined())
	            result = r;
        }
        catch(UndefinedOperationException e)
        {
            // the xor failed.  return an undefined Value.
            result = new Value();
        }
        return result;
    }
    
    /**
     * Returns the OR of this and rhs.
     */
    public domain.Value or(final domain.Value rhs)
    {
    	
    	if( !(rhs instanceof Value) )
            throw new IllegalArgumentException();
        
        Value 	result;
        Value	rhsValue = (Value)rhs;
        
        try
        {
            result = m_ric.or(rhsValue.m_ric);
            
            Value r = m_ric.or(rhsValue.m_stackLocations);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;
            
            r = m_stackLocations.or(rhsValue.m_ric);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;
                
	        r = m_stackLocations.or(rhsValue.m_stackLocations);
	        if(result.isDefined() && r.isDefined())
	            result.merge(r);
	        else if(r.isDefined() && !result.isDefined())
	            result = r;
        }
        catch(UndefinedOperationException e)
        {
            // the or failed.  return an undefined Value.
            result = new Value();
        }
        return result;
    }
    
    /**
     * Returns the AND of this and rhs.
     */
    public domain.Value and(final domain.Value rhs)
    {	
    	if( !(rhs instanceof Value) )
            throw new IllegalArgumentException();
        
        Value 	result;
        Value	rhsValue = (Value)rhs;
        
        try
        {
            result = m_ric.and(rhsValue.m_ric);
            
            Value r = m_ric.and(rhsValue.m_stackLocations);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;
            
            r = m_stackLocations.and(rhsValue.m_ric);
            if(result.isDefined() && r.isDefined())
                result.merge(r);
            else if(r.isDefined() && !result.isDefined())
                result = r;
                
	        r = m_stackLocations.and(rhsValue.m_stackLocations);
	        if(result.isDefined() && r.isDefined())
	            result.merge(r);
	        else if(r.isDefined() && !result.isDefined())
	            result = r;
        }
        catch(UndefinedOperationException e)
        {
            // the and failed.  return an undefined Value.
            result = new Value();
        }
        return result;
    }
    
    /**
     * The RIC component of this Value.
     */
    private IReducedIntervalCongruence	m_ric;
    
    /**
     * The StackLocationSet component of this Value
     */
    private IStackLocationSet			m_stackLocations;
}

package controlFlowGraph;

import java.util.ArrayList;
import java.util.Iterator;

import x86InstructionSet.Instruction;

/**
 * A set of instruction that make up a straight-line sequence of code with
 * only one entry point and one exit point.
 */
public class ControlFlowGraphBlock
{
  /** 
   * Instructions placed within this block. Sorted based on conditions specified by Add
   * functions. 
   */
  ArrayList<Instruction> instructions = new ArrayList<Instruction>();
  
  /**
   * Adds an instruction to this block at the specified index. If an instruction is
   * already located at the specified index, it will be moved to index+1, as will each
   * following instruction. If the index is greater than the size of this block, the
   * instruction will be added to the end of this block. 
   * @param index the location where the instruction should be added.  Range[0, inf)
   * @param instruction the instruction to add.
   */
  public void add(int index, Instruction instruction)
  {
    index = min(index, size());
    instructions.add(index, instruction);
  }
  
  /**
   * Adds an instruction to the end of this block.
   * @param instruction the instruction to add.
   */
  public void addToEnd(Instruction instruction)
  {
    add(size(), instruction);
  }
  
  /**
   * Returns the specified instruction, based on index.  
   * @param index zero-based index of the instruction to retrieve. Range: [0,size())
   * @return the specified instruction.
   */
  public Instruction get(int index)
  {
    assert(index >= 0 && index < size());
    return instructions.get(index);
  }
  
  /**
   * returns the first instruction within this block.
   * @return the first instruction.
   */
  public Instruction getFirst()
  {
    return get(0);
  }
  
  /**
   * Returns the last instruction within this block.
   * @return the last instruction.
   */
  public Instruction getLast()
  {
    return get(size()-1);
  }
  
  /**
   * Returns the minimun of two integers.
   * @return the minimum of two integers.
   */
  private int min(int a, int b)
  {
    return (a < b) ? a : b;
  }
  
  /**
   * Returns the number of instructions in this block.
   * @return the size of this block.
   */
  public int size()
  {
    return instructions.size();
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    Iterator<Instruction> iter;
    StringBuffer           result = new StringBuffer();
    
    iter = instructions.iterator();
    result.append(iter.next());
    while(iter.hasNext())
      result.append("\n" + iter.next());
    
    return result.toString();
  }
}

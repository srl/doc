/*
 * ControlFlowGraphNodeSet.java
 * Created on May 5, 2005
 */
package controlFlowGraph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import x86InstructionSet.Instruction;



/**
 * A list of ControlFlowGraphNodes.  Provides methods for
 * adding nodes and searching for/retrieving a specific node.
 */
public class ControlFlowGraphNodeList implements Cloneable
{
    private LinkedList<ControlFlowGraphNode>  nodes = new LinkedList<ControlFlowGraphNode>();
    
    /**
     * Identifies whether this list is currently sorted.
     * To improve performance, the list is only be sorted when absolutely 
     * necessary (such as just before a search is performed).  Thus,
	 * if an operation requires the list to be sorted, remember to call the sort()
	 * function.  This function will only sort the list if it is not already sorted.
	 * Likewise, if you modify the order of the list, call the setNotSorted() method
	 * to indicate that the list is no longer sorted.
     */
	private boolean isSorted = true;
	
    /**
     * Adds a node to this list.  Duplicates are not added.
     */
    public void add(ControlFlowGraphNode node)
    {
        nodes.add(node);
        setNotSorted();
    }
    
    /**
     * Returns a copy of this.
     */
    public Object clone()
    {
    	ControlFlowGraphNodeList clone;
    	
    	try
    	{
    		clone = (ControlFlowGraphNodeList)super.clone();
    		clone.isSorted = isSorted;
    		clone.nodes = (LinkedList<ControlFlowGraphNode>)nodes.clone();
    		return clone;
    	}
    	catch(Exception e)
    	{
    		throw new Error("This should never happen");
    	}
    }
    
    /**
     * @see Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj)
    {
    	ControlFlowGraphNodeList nodes;
    	
    	if(obj instanceof ControlFlowGraphNodeList)
    	{
    		nodes = (ControlFlowGraphNodeList)obj;
    		return this.nodes.equals(nodes.nodes);
    	}
    	return false;
    }
    
    /**
     * Returns the node that references the input instruction.
     * Return null if no such no exists.
     */
    public ControlFlowGraphNode get(Instruction instruction)
    {
    	boolean itemFound;    	
    	int 	index;
    
    	sort();
    	index = Collections.binarySearch(nodes, new ControlFlowGraphNode(instruction));
    	itemFound = (index >= 0);
    	if(itemFound)
    		return (ControlFlowGraphNode)nodes.get(index);
    	else
    		return null;
    }

    /**
     * Returns true if this list contains a reference to node.
     */
    public boolean has(final ControlFlowGraphNode node)
    {
        Iterator iter = iterator();
        while(iter.hasNext())
            if(iter.next() == node)
                return true;
        return false;
    }
    
    /**
     * Returns an iterator that can be used to traverse the
     * nodes in this list.
     */
    public Iterator iterator()	{ return new Iterator(); }
    public class Iterator
    {
        private java.util.Iterator	iterator; 
        public Iterator() 					{ iterator = nodes.iterator(); }
        public boolean hasNext() 			{ return iterator.hasNext(); }
        public ControlFlowGraphNode next() 	{ return (ControlFlowGraphNode)iterator.next(); }
    }

    /**
     * Sets an internal data member to indicate that the ordering of this
     * list has changed and is no longer sorted.
     */
	private void setNotSorted()
	{
		isSorted = false;
	}
    
    /**
     * Returns the number of nodes in this list.
     * @return the number of nodes in this list.
     */
    public int size()
    {
        return nodes.size();
    }

    /**
     * Sorts this list only if the list is not already sorted.
     */
	private void sort()
	{
		if(!isSorted)
		{
			Collections.sort(nodes);
			isSorted = true;
		}
	}
    
	/**
	 * Returns an array representation of this node list.
	 */
	public Object[] toArray()
	{
		Iterator 	iter;
		ArrayList<ControlFlowGraphNode> 	result = new ArrayList<ControlFlowGraphNode>();
		
		iter 	= iterator();
		while(iter.hasNext())
			result.add(iter.next());
		return result.toArray();
	}

	/**
	 * String rperesentation
	 */
	public String toString()
	{
		return nodes.toString();
	}
}

/*
 * ControlFlowGraphNode.java
 * Created on May 5, 2005
 */
package controlFlowGraph;

import x86InstructionSet.Instruction;


/**
 * A node in the control flow graph.  Each node
 * in the graph references one instruction.
 */
public class ControlFlowGraphNode implements Comparable<ControlFlowGraphNode>
{
    Instruction					instruction;	// each node reference exactly one instruction
    ControlFlowGraphNodeList	predecessors	= new ControlFlowGraphNodeList();
    ControlFlowGraphNodeList	successors		= new ControlFlowGraphNodeList();
    
    /**
     * Creates a new node that references the input instruction
     * and has no predecessors or successors.
     */
    public ControlFlowGraphNode(Instruction instruction)
    {
        this.instruction = instruction;
    }
    
    /**
     * Adds a predecessor node (ie, adds an incoming edge)
     */
    public void addPredecessorNode(ControlFlowGraphNode predecessor)
    {
        predecessors.add(predecessor);
    }
  
    /**
     * Adds a successor node (ie adds an outgoing edge)
     */
    public void addSuccessorNode(ControlFlowGraphNode successor)
    {
        successors.add(successor);
    }
    
    /**
     * @see Comparable#compareTo
     */
    public int compareTo(ControlFlowGraphNode obj)
    {
      return instruction.compareTo(obj.instruction);
    }

    /**
     * Returns true if this node equals rhs.  Two nodes
     * are considered equal if they reference the same
     * instruction.
     */
    public boolean equals(Object rhs)
    {
        return instruction.equals( ((ControlFlowGraphNode)rhs).instruction );
    }

    /**
     * Returns the instruction associated with this node.
     */
    public Instruction getInstruction()
    {
    	return instruction;
    }
    
    /**
     * Returns the immediate predecessors of this node.
     */
    public ControlFlowGraphNodeList	getPredecessors()
    {
        return predecessors;
    }
    
    /**
     * Returns the immediate successors of this node.
     */
    public ControlFlowGraphNodeList getSuccessors()
    {
        return successors;
    }

		/**
		 * String representation
		 */
    public String toString()
    {
    	return instruction.getAddress().toString();
    }
}

/*
 * ControlFlowGraph.java
 * Created on May 5, 2005
 */
package controlFlowGraph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import logger.Log;
import x86InstructionSet.Address;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.Instruction;
import x86InstructionSet.JaJnbeInstruction;
import x86InstructionSet.JaeInstruction;
import x86InstructionSet.JbInstruction;
import x86InstructionSet.JbeInstruction;
import x86InstructionSet.JeInstruction;
import x86InstructionSet.JecxzInstruction;
import x86InstructionSet.JgInstruction;
import x86InstructionSet.JgeInstruction;
import x86InstructionSet.JlInstruction;
import x86InstructionSet.JleInstruction;
import x86InstructionSet.JmpInstruction;
import x86InstructionSet.JnbInstruction;
import x86InstructionSet.JneInstruction;
import x86InstructionSet.JnsInstruction;
import x86InstructionSet.JnzInstruction;
import x86InstructionSet.JsInstruction;
import x86InstructionSet.Program;
import x86InstructionSet.RetInstruction;
import x86InstructionSet.UnknownAddress;
import x86Interpreter.InstructionList;

/**
 * Maintains a control-flow graph.
 */
public class ControlFlowGraph implements Cloneable
{
    /**
     * An observer interface for monitoring changes made to a
     * CFG.
     */ 
    public interface Observer
    {
    	/**
    	 * Called when an edge is added.  duplicate is set
    	 * to true if the edge already existed.
    	 * @param activatedBy ControlFlowGraph that was modified.
    	 */
    	public void edgeAdded(ControlFlowGraph activatedBy, 
    			ControlFlowGraphNode source, ControlFlowGraphNode target, boolean duplicate);
    	
    	/**
    	 * Called when a node is added.  duplicate is set to 
    	 * true if the node already existed.
    	 * @param activatedBy ControlFlowGraphthat was modified
    	 * @param node The new node
    	 * @param duplicate True if this node already existed in the graph.
    	 */
    	public void nodeAdded(ControlFlowGraph activatedBy, ControlFlowGraphNode node, boolean duplicate);
    }
    
    private Address                   entryPoint = UnknownAddress.UNKNOWN;
    private ControlFlowGraphNodeList	nodes = new ControlFlowGraphNodeList();
    private LinkedList<Observer>			observers = new LinkedList<Observer>();
    
    /**
     * This field is add only to support rachit's block LVA and will be
     * removed when no longer needed.
     */
    public Program                   program = null;

    /**
     * Adds an edge in the control-flow graph from source to target.
     * Before calling AddEdge, a node in the CFG must be created 
     * for each input instruction by called AddNode.  Duplicate
     * edges are quietly discarded.
     */
    public void addEdge(Instruction source, Instruction target)
    {
      boolean								preexisting;	// true if edge already exists.
      ControlFlowGraphNode 	sourceNode;
      ControlFlowGraphNode 	targetNode;
        
      sourceNode = nodes.get(source);
      if(sourceNode == null)
          throw new Error("Source instruction not found in CFG.");
      
      targetNode = nodes.get(target);
      if(targetNode == null)
          throw new Error("Target instruction not found in CFG.");

      preexisting = hasEdge(source, target);
      if(!preexisting)
      {
        if(sourceNode.getSuccessors().get(target) == null)
          sourceNode.addSuccessorNode(targetNode);
      
	      if(targetNode.getPredecessors().get(source) == null)
	          targetNode.addPredecessorNode(sourceNode);        
      }
      notifyEdgeAdded(sourceNode, targetNode, preexisting);
    }
    
    /**
     * Creates a new node in the control-flow graph.  Once created,
     * the node is not connected to any other node.  To connect
     * the node, call AddEdge.  Duplicate nodes are quietly discarded.
     */
    public void addNode(Instruction instruction)
    {
    	ControlFlowGraphNode 	newNode;			// newly added node.
    	boolean 							preexisting; 	// true if node already exsits.
    	
    	preexisting = hasNode(instruction);
    	if(!preexisting)
    	{
    		newNode = new ControlFlowGraphNode(instruction);
    		nodes.add(newNode); 
    	}
    	else
    		newNode = nodes.get(instruction);
        
    	notifyNodeAdded(newNode, preexisting);
    }
    
    /**
     * Adds an observer for observing changes made to this CFG.
     * Duplicate observers are quietly discarded.
     */
    public void addObserver(Observer observer)
    {
    	if(!observers.contains(observer))
    		observers.add(observer);
    }
    
    /**
     * Returns copy of this.
     */
    public Object clone()
    {
    	ControlFlowGraph clone;
    	try
    	{
    		clone = (ControlFlowGraph)super.clone();
    		clone.nodes = (ControlFlowGraphNodeList)nodes.clone();
    		return clone;
    	}
    	catch(Exception e)
    	{
    		throw new Error("This should never happen.");
    	}
    }
    
    /**
     * @see Object#equals(java.lang.Object)
     */
    public boolean equals(Object obj)
    {
    	ControlFlowGraph cfg;
    	
    	if(obj instanceof ControlFlowGraph)
    	{
    		cfg = (ControlFlowGraph)obj;
    		return this.nodes.equals(cfg.nodes);
    	}
    	return false;
    }
    
    /**
     * Returns all control flow graph blocks that make up this
     * control flow graph.
     * @return this control flow graph's blocks.
     */
    public ControlFlowGraphBlock[] getBlocks()
    {
      ControlFlowGraphBlock             block;
      ArrayList<ControlFlowGraphBlock>  blocks = new ArrayList<ControlFlowGraphBlock>();
      ControlFlowGraphBlock[]           result;
      boolean                           isBlockInList = false;
      
      Iterator<Instruction> iter;
      Instruction instruction;
      
      block = new ControlFlowGraphBlock();
      iter  = program.iterator();
      while(iter.hasNext())
      {
        instruction = iter.next();
        block.addToEnd(instruction);
        if(!isBlockInList)
          blocks.add(block);
        
        if(instruction instanceof CallInstruction ||
            instruction instanceof JaeInstruction ||
            instruction instanceof JaJnbeInstruction ||
            instruction instanceof JbeInstruction ||
            instruction instanceof JbInstruction ||
            instruction instanceof JecxzInstruction ||
            instruction instanceof JeInstruction ||
            instruction instanceof JgInstruction ||
            instruction instanceof JgeInstruction ||
            instruction instanceof JleInstruction ||
            instruction instanceof JlInstruction ||
            instruction instanceof JmpInstruction ||
            instruction instanceof JnbInstruction ||
            instruction instanceof JneInstruction ||
            instruction instanceof JnsInstruction ||
            instruction instanceof JnzInstruction ||
            instruction instanceof JsInstruction ||
            instruction instanceof RetInstruction)
        {
          block = new ControlFlowGraphBlock();
        }
      }
      
      result = new ControlFlowGraphBlock[blocks.size()];
      return blocks.toArray(result);
      
      /*
       * NOTE: The commented code below implements the getBlocks function
       * by looking at the structure of the CFG and creating blocks from that.
       * In my opinion, it is the perferred method of creating the CFG blocks, but
       * for a specific project , someone wanted the CFG blocks to be created by
       * looking at the instruction type, so this code was commented out and the
       * new code was placed above.  Later, I'd like to remove the code above and
       * use the commented code below --mpv.
       */
      /*
      ControlFlowGraphBlock             block;
      ArrayList<ControlFlowGraphBlock>  blocks = new ArrayList<ControlFlowGraphBlock>();
      Instruction                       instruction;
      ControlFlowGraphNodeList.Iterator iter;
      ControlFlowGraphNode              node;
      ControlFlowGraphBlock[]           result;
      List<ControlFlowGraphNode>        visited = new LinkedList<ControlFlowGraphNode>();

      // Loop through all instructions...
      iter = nodes.iterator();
      while(iter.hasNext())
      {
        // create a new block to place instructions.  
        block = new ControlFlowGraphBlock();
        node = iter.next();
        
        if(visited.contains(node))
          continue;
        visited.add(node);
        
        instruction = node.getInstruction();
        block.addToEnd(instruction);
        // Keep placing instructions in this block until an instruction has either no
        // successors or more than one successors.  Once this happens, we create a new
        // block and resume adding instructions to it.
        while(node.getSuccessors().size() == 1)
        {
          node = node.getSuccessors().iterator().next();
          if(visited.contains(node))
            break;
          visited.add(node);
          instruction = node.getInstruction();
          block.addToEnd(instruction);
        }
          
        blocks.add(block);
      }      
      
      result = new ControlFlowGraphBlock[blocks.size()];
      return blocks.toArray(result);
      */
    }
    
    /**
     * Returns the instruction(s) following the input
     * instruction in the control-flow graph.
     */
    public InstructionList getNext(Instruction instruction)
    {
    	ControlFlowGraphNodeList.Iterator iter;
    	ControlFlowGraphNode 							node;
    	InstructionList										result = new InstructionList();
    	ControlFlowGraphNodeList 					successorNodes;
    	
    	
    	node = nodes.get(instruction);
    	if(node != null)
    	{
	    	successorNodes = node.getSuccessors();
	    	iter = successorNodes.iterator();
	    	while(iter.hasNext())
	    	{
	    		node = iter.next();
	    		result.add(node.getInstruction());
	    	}
    	}
    	return result;
    }
    
    /**
     * Returns the CFG node that contains the input instruction.
     */
    public ControlFlowGraphNode getNode(Instruction instruction)
    {
    	return nodes.get(instruction);
    }
    
    /**
     * Returns all nodes in this graph.
     */
    public ControlFlowGraphNodeList getNodes()
    {
    	return nodes;
    }
    
    /**
     * Returns true if an edge exists from source to target.  
     * Returns false if the edge does not exist or if
     * source or target does not exist.
     */
    public InstructionList getPrevious(Instruction instruction)
    {
      ControlFlowGraphNodeList.Iterator iter;
      ControlFlowGraphNode              node;
      InstructionList                  result = new InstructionList();
      ControlFlowGraphNodeList          predecessorNodes;
      
      
      node = nodes.get(instruction);
      if(node != null)
      {
        predecessorNodes = node.getPredecessors();
        iter = predecessorNodes.iterator();
        while(iter.hasNext())
        {
          node = iter.next();
          result.add(node.getInstruction());
        }
      }
      return result;
    }

    /* (non-Javadoc)
     * @see controlFlowGraph.ControlFlowGraph#hasEdge(x86InstructionSet.Instruction, x86InstructionSet.Instruction)
     */
    public boolean hasEdge(Instruction source, Instruction target)
    {
    	ControlFlowGraphNode 			sourceNode;
    	ControlFlowGraphNode			targetNode;
    	
    	sourceNode = nodes.get(source);
    	if(sourceNode.getSuccessors().get(target) == null)
        return false;
    
    	targetNode = nodes.get(target);
      if(targetNode.getPredecessors().get(source) == null)
          return false;
      
      return true;
    }
    
    /**
     * Returns true if a node exists associated with the input
     * instruction
     */
    public boolean hasNode(Instruction instruction)
    {
    	return nodes.get(instruction) != null;
    }
    
    /**
     * Returns true if the input instruction is inside a loop.
     */
    public boolean isInsideLoop(Instruction instruction)
    {
        boolean isLoop = false;
        
        ControlFlowGraphNode thisNode = nodes.get(instruction);
        if(thisNode == null)
        {
            final String msg = "Instruction given to isInsideLoop is " +
            	"not in the CFG";
            Log.write(Log.ERROR, "isInsideLoop() - " + msg);
            throw new Error(msg);
        }
        
        // The algorithm for detecting if this node is in a loop
        // is as follows.  Perform a breadth-first search 
        // starting from this node.  If we eventually reach this
        // node a second time, then assume this node is a loop. 
        // This is not the best algorithm and may even be wrong
        // sometimes, but since our method is not context-
        // sensitive, it should be satisfactory.
        ControlFlowGraphNodeList	visited;	// don't visit nodes twice.
        Queue<ControlFlowGraphNode>  toVisit;	// nodes to be visited. 
        
        visited = new ControlFlowGraphNodeList();
        toVisit = new LinkedList<ControlFlowGraphNode>();
        
        ControlFlowGraphNode currentNode = thisNode;
        
        // add each child to toVisit list.
        ControlFlowGraphNodeList.Iterator iter = currentNode.getSuccessors().iterator();
        while(iter.hasNext())
            toVisit.add(iter.next());
        
        // visit each node in the toVisit list.
        while(!toVisit.isEmpty())
        {
            currentNode = (ControlFlowGraphNode)toVisit.poll();
            if(!visited.has(currentNode))
            {
                if(currentNode == thisNode)
                {
                    isLoop = true;	// we found our node--this node is a loop junction node!
                    toVisit.clear();
                }
                else
                {
                    // this isn't the node we're looking for--keep searching.
                    
                    // add this node to our visited list to avoid visited it again.
                    visited.add(currentNode);
                    
                    // add each child to toVisit list.
                    iter = currentNode.getSuccessors().iterator();
                    while(iter.hasNext())
                        toVisit.add(iter.next());
                }
            }
        }
        return isLoop;        
    }

    /**
     * Returns true if the input instruction is a loop junction 
     * node.  A loop junction node is an instruction with 2
     * or more incoming edges such that one of the incoming edges
     * forms a loop (i.e. this instruction is the beginning of a 
     * loop).
     */
    public boolean isLoopJunctionNode(Instruction instruction)
    {
        ControlFlowGraphNode thisNode = nodes.get(instruction);
        if(thisNode == null)
        {
            final String msg = "Instruction given to isLoopJunctionNode is " +
            	"not in the CFG";
            System.err.println(msg);
            throw new Error(msg);
        }
        
        return (thisNode.getPredecessors().size() > 1 && isInsideLoop(instruction)) ||
        	(thisNode.getInstruction().getAddress().equals(entryPoint) &&
        			thisNode.getPredecessors().size() > 0 && isInsideLoop(instruction));
    }

    /**
     * Returns true if the input instruction is a simple junction
     * node.  A simple junction node is an instruction with 2 or
     * more incoming edges such that the incoming edges represent
     * 2 or more possible execution paths and the instruction is
     * not a loop junction node (i.e. the instruction is at the
     * end of the two paths taken by an if statement, etc.).
     */
    public boolean isSimpleJunctionNode(Instruction instruction)
    {
        ControlFlowGraphNode node = nodes.get(instruction);
        if(node != null)
            return !isLoopJunctionNode(instruction) &&
            	node.getPredecessors().size() > 1;
        else
            throw new Error("Input instruction is not in the CFG.");
        	
    }
    
    /**
     * Notifies observers about an added edge.
     * @see Observer#edgeAdded(ControlFlowGraph, ControlFlowGraphNode, ControlFlowGraphNode, boolean)
     */
    private void notifyEdgeAdded(ControlFlowGraphNode source,
    		ControlFlowGraphNode target, boolean duplicate)
    {
    	Iterator iter = observers.iterator();
    	while(iter.hasNext())
    		((Observer)iter.next()).edgeAdded(this, source, target, duplicate);
    }
    
    /**
     * Notifies observers about an added node.
     * @see Observer#nodeAdded(ControlFlowGraph, ControlFlowGraphNode, boolean)
     */
    private void notifyNodeAdded(ControlFlowGraphNode node, 
    		boolean duplicate) 
    {
    	Iterator iter = observers.iterator();
    	while(iter.hasNext())
    		((Observer)iter.next()).nodeAdded(this, node, duplicate);
    }
    
    /**
     * Removes an observer from this CFG.  The call has no effect
     * if the observer is not registered as an observer of this CFG.
     */
    public void removeObserver(Observer observer)
    {
    	observers.remove(observer);
    }
    
    /**
     * Informs this CFG about the entry point so that
     * correct decisions can be made when determining if
     * a node is a loop header or not, etc. 
     */
    public void setEntryPoint(Address address)
    {
    	entryPoint = address;
    }
    
    /**
     * String representation
     */
    public String toString()
    {
      ControlFlowGraphNodeList.Iterator nodesIter;
      ControlFlowGraphNodeList.Iterator successorIter;
      StringBuffer result = new StringBuffer();
      ControlFlowGraphNode sourceNode;
      
      result.append(nodes.toString());
      
      nodesIter = nodes.iterator();
      while(nodesIter.hasNext())
      {
        sourceNode = nodesIter.next();
        successorIter = sourceNode.getSuccessors().iterator();
        while(successorIter.hasNext())
          result.append("\n" + sourceNode + " --> " + successorIter.next());
      }
      
      return result.toString();
    }
}

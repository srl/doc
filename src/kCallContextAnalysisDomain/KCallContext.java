package kCallContextAnalysisDomain;

/**
 * @author Davidson Boccardo 
 */

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.Map.Entry;

import csAnalysisDomain.csAnalysisDomain;


import obfuscations.CallObfuscation;

import logger.Log;
import vsaAsgDomain.AbstractStackGraph;
import vsaAsgDomain.IMemoryTable;
import vsaAsgDomain.IReducedIntervalCongruence;
import vsaAsgDomain.IStackLocationSet;
import vsaAsgDomain.StackLocation;
import vsaAsgDomain.VsaAsgState;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.Instruction;
import x86InstructionSet.Operand;
import x86InstructionSet.RetInstruction;
import x86Interpreter.Interpreter;
import domain.AbstractState;
import domain.BitCount;
import domain.Register;
import domain.State;
import domain.Value;

/**
 * Maintains the state of registers and memory for each stackString.  The Context 
 * is capable of managing register values and values placed on the stack, however 
 * arbitrary memory locations, such as the heap, are not stored. (future work) 
 */

public class KCallContext extends csAnalysisDomain
{
	/**
	 * k length
	 */
	
	private final int k = 100;
	/**
	 * Creates a context with a initial stack string and an initial
	 * empty state.  The object created by this constructor is not initialized.
	 * Must call init() before using this object.  The reason for this is 
	 * because we must delay initialization until the interpreter has had a 
	 * chance to assign the Interpreter.activeState static variable.
	 */
	public KCallContext()
	{
	}
	
	 /**
     * Returns a copy of this State.
     */

	
	public AddressList getReturnAddressAsList()
    {
		AddressList 								result = new AddressList();
      	IReducedIntervalCongruence 					ric;
        IReducedIntervalCongruence.Iterator 		ricIter;
        IStackLocationSet.Iterator 					stackiter;
        IStackLocationSet 							stackTops;
        
        IStackLocationSet.Iterator 					stackiter2;
        stackTops = ((vsaAsgDomain.Value)getRegisterValue(Register.ESP)).getStackLocations();
        
        if (contextString.equals("T")){
        	
        	for(stackiter = stackTops.iterator(); stackiter.hasNext(); )
            {
            	ric = getMemoryTable().getValue( stackiter.next().getMemoryArea() ).getRIC();

            	if(ric.size() <= 5)
            	{
                ricIter = ric.iterator();
                while(ricIter.hasNext())
                {
                    long returnAddress = ricIter.next();
                    if(!result.has(new Address(returnAddress)))
                        result.add( new Address(returnAddress));
                }
            	}
            	else
            	{
               // Log.write(Log.WARNING, "One of the stack tops contains more than 5 values. " +
                 //     " It will not be added to the list of return addresses.");
            	}
            }        
            return result;
      
        }
        
       	for(stackiter = stackTops.iterator(); stackiter.hasNext(); )
            {
            	        	
            	int trace = contextString.lastIndexOf("-");
            	String cont = contextString.substring(trace+1, contextString.length());
            	
            	int check = Integer.parseInt( cont );
       		 	
       		 	StackLocation temp2 = stackiter.next();
       		 	
       		 	if (temp2.getCreator() == null) continue;
       		 	
       		 	int temp = temp2.getCreator().getIndex()+1;
       		 	
       		 	if( temp == check ) {
       		 	
       		 		ric = getMemoryTable().getValue( temp2.getMemoryArea() ).getRIC();
       		 
       		 		if(ric.size() <= 5)
       		 		{
       		 			ricIter = ric.iterator();
       		 			while(ricIter.hasNext())
       		 			{
       		 				long returnAddress = ricIter.next();
       		 				if(!result.has(new Address(returnAddress)))
       		 					result.add( new Address(returnAddress));
       		 			}
       		 		}
       		 		else
       		 		{
       		 		//	Log.write(Log.WARNING, "One of the stack tops contains more than 5 values. " +
       		 		//	" It will not be added to the list of return addresses.");
       		 		}
       		 	}
       		} 
                            
        return result;
    }
	
	



	/**
     * Removes & returns the value at the top of the stack. and chop the last element
     * of the stack-string
     */
	
	public Value pop() {
		VsaAsgState state;
		Value value;
		int n = 1; // how many strings are chopped from the stack-string
		
		Operand numBytesOperand;

		Value numBytes;

		if ( Interpreter.getActiveInstruction() instanceof RetInstruction )
		{
			numBytesOperand = ((RetInstruction)Interpreter.getActiveInstruction()).getDstOperand();
			
			if (numBytesOperand == null) n = 1; else { //caso se the ret instruction is only 4 bytes
			
				numBytes = (Value) numBytesOperand.interpret(Interpreter.getActiveState());
				IReducedIntervalCongruence ric = ((vsaAsgDomain.Value)numBytes).getRIC();

				if( ric.isDefined())
				{
					assert ric.size() == 1;	
					n = (int) ric.getMinimumValue();
				}

				n = n/4 + 1; // 1 stack-location = 4 bytes
			}

		}
		
		String stackString = getContextString();
		
		if (!(stackString.endsWith("+"))) {
			
			popSuffix(n);
			state = (VsaAsgState) get(stackString);
			value = (Value) state.pop();
			updateAddMapping(getContextString(), state );
			assert map.containsKey( getContextString() );
			return value;
		
		} 	else { //check if POP instructions
			
			state = (VsaAsgState) get(stackString);
			value = (Value) state.pop();
			
			
			
			
	        return value;
			
		}
		
	}
    
	
	
	 /**
     * Pushes a value onto the stack & add the address to the stack-string
     */
	
	public void push(Value value) 
	{
		VsaAsgState state;
		Instruction instruction;
		StackLocation newStackLocation;	// stack location added by the push operation.	
						
		state = (VsaAsgState)get(getContextString()).clone();
		
		state.push(value);
		
		newStackLocation = getStackTop(state);
		
		instruction = newStackLocation.getCreator();
		
		pushSuffix(Integer.toString(instruction.getIndex()+1)); // getIndex() returns the line of stack-location-1
		updateAddMapping(getContextString(), state);
		
	}

	
	
	/**
	 * Retrieves k length
	 */
	
	public int getBound()
	{
		
		return k;
	
	}
	
	/**
     * Updates the stack-string appending some program-location (label) on it. 
     */

	public String pushSuffix(String id)
	{
		//return stackString;
		
		int k = getBound();
		
		if (k == 0) return contextString;
		
		if ( Interpreter.getActiveInstruction() instanceof CallInstruction ){	
		
			char ch;
			int count = 0;
		
			for (int i = 0; i < contextString.length(); i++ ){
				ch = contextString.charAt(i);
				if ( ch == '-' ) count++;
			}
			
			if (count >= k){
			
				contextString = contextString.substring(2, contextString.length());
				do {
					contextString = contextString.substring(1, contextString.length());
				} while ((!(contextString.startsWith("-"))) && (contextString.length() > 0) );
			
				contextString = "T" + contextString + "-" + id;
			}
			
			else 
				contextString += "-" + id; 
		
		
			return contextString;}
		
		return contextString;
				
	}

	/**
     * Updates the stack-string removing n program-locations. This n is determined by
     * the number of bytes. 
     */
	
	public String popSuffix(int bytes){
		
		if ( Interpreter.getActiveInstruction() instanceof RetInstruction ){      
		int lenght = contextString.length(); 
						
		while ((bytes != 0) && (lenght > 1)){
			
			while (contextString != null) {
				
			contextString = contextString.substring(0, lenght-1);
			lenght--;
			if (contextString.endsWith("-")) break;
										
			}
				
			contextString = contextString.substring(0, lenght-1);
			lenght--;
			bytes--;
		}
		
		return contextString;}
		return contextString;
	}
	
	

	public void changeTop() {
	
		IStackLocationSet.Iterator 					stackiter;
	    IStackLocationSet 							stackTops;
        StackLocation                               stackTop = null;
        String 										contTop;
       		
        stackTops = ((vsaAsgDomain.Value)getRegisterValue(Register.ESP)).getStackLocations();
        
        if (!(contextString.equals("T"))){
        
        	int trace = contextString.lastIndexOf("-");

        	contTop = contextString.substring(trace+1, contextString.length());
        
        	stackiter 				= stackTops.iterator();
		
        	while (stackiter.hasNext())
        	{
        		stackTop 				= stackiter.next();
        		if (stackTop.getCreator() == null) continue;
        		int check               = stackTop.getCreator().getIndex()+1;
        		if (check == Integer.parseInt(contTop)) break;
        	}
		
        	Value value = new vsaAsgDomain.Value(stackTop);
        	setRegisterValue(Register.ESP, value) ;
        }
		
	}
	
}

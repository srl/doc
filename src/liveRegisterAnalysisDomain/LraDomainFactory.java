package liveRegisterAnalysisDomain;

import domain.DomainFactory;
import domain.State;
import domain.Value;

/**
 * Factory object reponsible for creating objects associated with the
 * live register analysis domain. 
 */
public class LraDomainFactory extends DomainFactory
{
  /* (non-Javadoc)
   * @see domain.DomainFactory#createState()
   */
  @Override
  public State createState()
  {
    return new LraState();
  }

  /* (non-Javadoc)
   * @see domain.DomainFactory#createValue()
   */
  @Override
  public Value createValue()
  {
    return new DeadValue();
  }

  /* (non-Javadoc)
   * @see domain.DomainFactory#createValue(int)
   */
  @Override
  public Value createValue(int init)
  {
    return new DeadValue();  }

  /* (non-Javadoc)
   * @see domain.DomainFactory#createValue(int, int)
   */
  @Override
  public Value createValue(int start, int end)
  {
    return new DeadValue();
  }
}

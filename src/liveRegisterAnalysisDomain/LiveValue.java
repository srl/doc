package liveRegisterAnalysisDomain;

import abstractBoolean.AbstractBoolean;
import domain.BitCount;
import domain.Value;

/**
 * Can be assigned to a register to mark the register as live. 
 */
public class LiveValue implements Value
{

  /* (non-Javadoc)
   * @see domain.Value#add(domain.Value)
   */
  public Value add(Value rhs)
  {
    return new DeadValue();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#clone()
   */
  public Object clone()
  {
    return new DeadValue();
  }
  
  /* (non-Javadoc)
   * @see domain.Value#divide(domain.Value)
   */
  public Value divide(Value rhs)
  {
    return new DeadValue();
  }

  /* (non-Javadoc)
   * @see domain.Value#getBitSize()
   */
  public BitCount getBitSize()
  {
    return BitCount._32Bits;
  }

  /* (non-Javadoc)
   * @see domain.Value#isEqualTo(domain.Value)
   */
  public AbstractBoolean isEqualTo(Value rhs)
  {
    return AbstractBoolean.Top;
  }

  /* (non-Javadoc)
   * @see domain.Value#isGreaterThan(domain.Value)
   */
  public AbstractBoolean isGreaterThan(Value rhs)
  {
    return AbstractBoolean.Top;
  }

  /* (non-Javadoc)
   * @see domain.Value#isLessThan(domain.Value)
   */
  public AbstractBoolean isLessThan(Value rhs)
  {
    return AbstractBoolean.Top;
  }

  /* (non-Javadoc)
   * @see domain.Value#iterator()
   */
  public Iterator iterator()
  {
    return new Iterator()
    {
      public boolean  hasNext() { return false; }
      public long     next()    { return 0; } 
    };
  }

  /* (non-Javadoc)
   * @see domain.Value#multiply(domain.Value)
   */
  public Value multiply(Value rhs)
  {
    return new DeadValue();
  }

  /* (non-Javadoc)
   * @see domain.Value#set(long)
   */
  public void set(long value)
  {
  }

  /* (non-Javadoc)
   * @see domain.Value#set(domain.Value)
   */
  public void set(Value newVal)
  {
  }

  /* (non-Javadoc)
   * @see domain.Value#setBitSize(domain.BitCount)
   */
  public void setBitSize(BitCount size)
  {
  }

  /* (non-Javadoc)
   * @see domain.Value#size()
   */
  public long size()
  {
    return 0;
  }

  /* (non-Javadoc)
   * @see domain.Value#subtract(domain.Value)
   */
  public Value subtract(Value rhs)
  {
    return new DeadValue();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return "LIVE";
  }
  
  /* (non-Javadoc)
   * @see domain.Value#xor(domain.Value)
   */
  public Value xor(Value rhs)
  {
    return new DeadValue();
  }
  
  /* (non-Javadoc)
   * @see domain.Value#or(domain.Value)
   */
  public Value or(Value rhs)
  {
    return new DeadValue();
  }
  
  /* (non-Javadoc)
   * @see domain.Value#and(domain.Value)
   */
  public Value and(Value rhs)
  {
    return new DeadValue();
  }

}

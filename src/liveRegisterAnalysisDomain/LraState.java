package liveRegisterAnalysisDomain;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import logger.Log;
import x86InstructionSet.AddressList;
import x86InstructionSet.Instruction;
import abstractBoolean.AbstractBoolean;
import domain.BitCount;
import domain.Register;
import domain.State;
import domain.Value;

/**
 * Maintains state information related to live register analysis. 
 */
public class LraState implements State
{
  /** Stores liveness information for each register. */
  private Value registers[] = new Value[Register.NUM_REGISTERS];
  
  private Map<Integer, Value> stack;
  
  
  public void setStackValue(Integer offset, Value value)
  {
    stack.put(offset, value);
  }
  
  public Value getStackValue(Integer offset)
  {
    Value result;
    result = stack.get(offset);
    if(result == null)
      result = new LiveValue();
    return result;
      
  }
                   
  /* (non-Javadoc)
   * @see domain.State#createNewValue()
   */
  public Value createNewValue()
  {
    return new DeadValue();
  }

  /* (non-Javadoc)
   * @see java.lang.Object#clone()
   */
  public Object clone()
  {
    LraState clone = null;
    
    try
    {
      clone = (LraState)super.clone();
      
      clone.stack = new HashMap<Integer, Value>();
      Iterator<Entry<Integer, Value>> iter = stack.entrySet().iterator();
      while(iter.hasNext())
      {
        Entry<Integer, Value> entry = iter.next();
        clone.stack.put( entry.getKey(), entry.getValue() );
      }
      
      // initialize register liveness informaiton.
      clone.registers = new Value[Register.NUM_REGISTERS];
      for(int i = 0; i < Register.NUM_REGISTERS; i++)
        clone.registers[i] = registers[i];
    }
    catch(CloneNotSupportedException e)
    {
      Log.write(Log.ERROR, "CloneNotSupportedException occured in LraState.clone().");
      throw new Error("This should never happen.");
    }
    
    return clone;
  }

  /* (non-Javadoc)
   * @see domain.State#createNewValue(long)
   */
  public Value createNewValue(long v)
  {
    return new DeadValue();
  }

  /* (non-Javadoc)
   * @see domain.State#getFlagValue(int)
   */
  public AbstractBoolean getFlagValue(int flagRegister)
  {
    return AbstractBoolean.Top;
  }

  /* (non-Javadoc)
   * @see domain.State#getFlagValueInteger(int)
   */
  public Value getFlagValueInteger(int flagRegister)
  {
    return new DeadValue();
  }

  /* (non-Javadoc)
   * @see domain.State#getRegisterValue(domain.Register)
   */
  public Value getRegisterValue(Register register)
  {
    return registers[register.ordinal()];
  }

  /* (non-Javadoc)
   * @see domain.State#getReturnAddressAsList()
   */
  public AddressList getReturnAddressAsList()
  {
    return new AddressList();
  }

  /* (non-Javadoc)
   * @see domain.State#getStackTopCreators()
   */
  public Instruction[] getStackTopCreators()
  {
    return new Instruction[0];
  }

  /* (non-Javadoc)
   * @see domain.State#init()
   */
  public void init()
  {
    stack = new HashMap<Integer, Value>();
    for(int i = 0; i < Register.NUM_REGISTERS; i++)
      registers[i] = new LiveValue();
  }
 
  /* (non-Javadoc)
   * @see domain.State#iterator()
   */
  public Iterator iterator()
  {
    return new Iterator()
    {
      public boolean  hasNext() { return false; }
      public Object   next()    { return 0; }
      public void     remove()  { }
    };
  }

  /* (non-Javadoc)
   * @see domain.State#pop()
   */
  public Value pop()
  {
    return new DeadValue();
  }

  /* (non-Javadoc)
   * @see domain.State#push(domain.Value)
   */
  public void push(Value value)
  {
  }

  /* (non-Javadoc)
   * @see domain.State#setRegisterValue(domain.Register, domain.Value)
   */
  public void setRegisterValue(Register register, Value newVal)
  {
    registers[register.ordinal()] = newVal;
  }

  /* (non-Javadoc)
   * @see domain.State#setFlagValue(int, abstractBoolean.AbstractBoolean)
   */
  public void setFlagValue(int flagRegister, AbstractBoolean value)
  {
  }

  /* (non-Javadoc)
   * @see domain.State#load(domain.Value, domain.BitCount)
   */
  public Value load(Value source, BitCount size)
  {
    return new LiveValue();
  }

  /* (non-Javadoc)
   * @see domain.State#merge(domain.State)
   */
  public void merge(State rhs)
  {
  }

  /* (non-Javadoc)
   * @see domain.State#overapproximates(domain.State)
   */
  public boolean overapproximates(State rhs)
  {
    return false;
  }

  /* (non-Javadoc)
   * @see domain.State#store(domain.Value, domain.Value)
   */
  public void store(Value dest, Value value)
  {
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return "{" + registers[Register.EAX.ordinal()] + "," +
      registers[Register.ECX.ordinal()] + "," +
      registers[Register.EDX.ordinal()] + "}";
  }
  
  /* (non-Javadoc)
   * @see domain.State#widen(domain.State)
   */
  public void widen(State rhs)
  {
  }
}

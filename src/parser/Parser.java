package parser;

import java.io.FileNotFoundException;
import ollydbgParser.OllydbgParser;
import x86InstructionSet.Program;

/**
 * Defines methods to be implemented by various parsers.  Provides a method to 
 * retrieve the parser to be used, though later this should be replaced by
 * a preference option. 
 */
public abstract class Parser
{
  /** The one and only parser */
  private static Parser instance = null;
  
  /**
   * Returns an instance of a parser that can be used to parse x86 assembly files.
   */
  public static Parser getInstance()
  {
    if(null == instance)
      instance = new OllydbgParser();
    return instance;
  }

  /**
   * Parses a file and appends the resulting program to initialProgram.
   * @throws FileNotFoundException
   * @throws ParseException
   */
  public abstract Program parse(String filepath, Program initialProgram)
      throws FileNotFoundException, ParseException;

  /**
   * Parses a stream and appends the resulting program to initialProgram
   * @throws ParseException
   */
  public abstract Program parse(java.io.Reader stream, Program initialProgram)
      throws ParseException;

  /**
   * Parses the program specified in filePath.  Returns the resulting program
   */
  public abstract Program parse(String filePath) throws FileNotFoundException,
      ParseException;

  /**
   * Parses the input stream.  Returns the resulting program.
   */
  public abstract Program parse(java.io.Reader stream) throws ParseException;

}
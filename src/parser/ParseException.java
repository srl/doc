package parser;

/**
 * Defines an interface for parse exceptions to be used by any 
 * user-created parsers.
 */
public abstract class ParseException extends Exception
{
  /**
   * creates this. 
   */
  public ParseException()
  {
    super();
  }
  
  /**
   * creates this.
   */
  public ParseException(String message)
  {
    super(message);
  }
  
  /**
   * creates this
   */
  public ParseException(String message, Throwable cause)
  {
    super(message, cause);
  }
  
  /**
   * creates this.
   */
  public ParseException(Throwable cause)
  {
    super(cause);
  }
  
  /* (non-Javadoc)
   * @see java.lang.Throwable#getMessage()
   */
  public String getMessage()
  {
    return super.getMessage();
  } 
}
package defUseAnalysisDomain;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import logger.Log;
import x86InstructionSet.AddressList;
import x86InstructionSet.Instruction;
import domain.AbstractState;
import domain.BitCount;
import domain.Register;
import domain.State;
import domain.Value;

/**
 * Maintains information regarding live variable analysis related to the
 * registers, does not handle any form of memory.  It is expected that
 * live variable analysis will be performed in a backward analysis order.
 */
public class DefUseState extends AbstractState
{
  private Set<Object> defset;
  private Set<Object> useset;
  
  /** Offset of stack pointer */
  private Integer stackOffset;
  
  private boolean writesToMemory;
  
  public boolean writesToMemory()
  {
    return writesToMemory;
  }
  
  /**
   * Creates this.
   */
  public DefUseState()
  {
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#clone()
   */
  public State clone()
  {
    DefUseState clone;
    try
    {
      clone = (DefUseState)super.clone();
      clone.defset = new HashSet<Object>();
      clone.useset = new HashSet<Object>();
      clone.stackOffset = stackOffset;
      clone.writesToMemory = false;
      return clone;
    }
    catch(Exception e)
    {
      Log.write(Log.ERROR, "This should never happen", e);
      throw new Error("This should never happen.", e);
    }    
  }
  
  /* (non-Javadoc)
   * @see domain.State#createNewValue()
   */
  @Override
  public Value createNewValue()
  {
    return new NullValue();
  }

  /* (non-Javadoc)
   * @see domain.State#createNewValue(long)
   */
  @Override
  public Value createNewValue(long v)
  {
    return new NullValue();
  }

  /**
   * Returns the set of registers defined by this instruction.
   * @return def set.
   */
  public Set<Object> getDefRegisterSet() 
  { 
    return defset;
  }
  
  /**
   * Returns the set of registers used by this instruction.
   * @return use set.
   */
  public Set<Object> getUseRegisterSet() 
  { 
    return useset;
  }

  /* (non-Javadoc)
   * @see domain.State#getRegisterValue(domain.Register)
   */
  @Override
  public Value getRegisterValue(Register register)
  {
    useset.add(register);
    return new NullValue();
  }

  /* (non-Javadoc)
   * @see domain.State#getReturnAddressAsList()
   */
  @Override
  public AddressList getReturnAddressAsList()
  {
    return new AddressList();
  }

  /* (non-Javadoc)
   * @see domain.State#getStackTopCreators()
   */
  @Override
  public Instruction[] getStackTopCreators()
  {
    return new Instruction[0];
  }
  
  /* (non-Javadoc)
   * @see domain.State#init()
   */
  @Override
  public void init()
  {
    defset = new HashSet<Object>();
    useset = new HashSet<Object>();
    stackOffset = 0;
    writesToMemory = false;
  }

  /* (non-Javadoc)
   * @see domain.State#iterator()
   */
  @Override
  public Iterator iterator()
  {
    return new Iterator() 
    {
      public boolean  hasNext() { return false; }
      public Value    next()    { return null; }
      public void     remove()  {}
    };
  }

  /* (non-Javadoc)
   * @see domain.State#pop()
   */
  @Override
  public Value pop()
  {
    useset.add(Register.ESP);
    useset.add(stackOffset++);
    defset.add(Register.ESP);
    return new NullValue();
  }

  /* (non-Javadoc)
   * @see domain.State#push(domain.Value)
   */
  @Override
  public void push(Value value)
  {
    useset.add(Register.ESP);
    defset.add(Register.ESP);
    defset.add(--stackOffset);
  }

  /* (non-Javadoc)
   * @see domain.State#setRegisterValue(domain.Register, domain.Value)
   */
  @Override 
  public void setRegisterValue(Register register, Value newVal)
  {
    defset.add(register);
  }

  /* (non-Javadoc)
   * @see domain.State#load(domain.Value, domain.BitCount)
   */
  @Override
  public Value load(Value source, BitCount size)
  {
    return new NullValue();
  }

  /* (non-Javadoc)
   * @see domain.State#merge(domain.State)
   */
  @Override
  public void merge(State rhs)
  {
  }

  /* (non-Javadoc)
   * @see domain.State#overapproximates(domain.State)
   */
  @Override
  public boolean overapproximates(State rhs)
  {
    return false;
  }

  /* (non-Javadoc)
   * @see domain.State#store(domain.Value, domain.Value)
   */
  @Override
  public void store(Value dest, Value value)
  {
    writesToMemory = true;
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return "DEF = {" + getDefRegisterSet() + 
      "}, USE = {" + getUseRegisterSet() + "}";
  }
  
  /* (non-Javadoc)
   * @see domain.State#widen(domain.State)
   */
  @Override
  public void widen(State rhs)
  {
  }
}

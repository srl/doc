package defUseAnalysisDomain;

import domain.DomainFactory;
import domain.State;
import domain.Value;

/**
 * Domain factory for classes related to def-use analysis.
 */
public class DefUseDomainFactory extends DomainFactory
{
  /** the one and only instance */
  private static DomainFactory instance = null;
  
  /* (non-Javadoc)
   * @see domain.DomainFactory#createState()
   */
  @Override
  public State createState()
  {
    return new DefUseState();
  }

  /* (non-Javadoc)
   * @see domain.DomainFactory#createValue()
   */
  @Override
  public Value createValue()
  {
    return new NullValue();
  }

  /* (non-Javadoc)
   * @see domain.DomainFactory#createValue(int)
   */
  @Override
  public Value createValue(int init)
  {
    return new NullValue();
  }

  /* (non-Javadoc)
   * @see domain.DomainFactory#createValue(int, int)
   */
  @Override
  public Value createValue(int start, int end)
  {
    return new NullValue();
  }

  /* (non-Javadoc)
   * @see domain.DomainFactory#getInstance()
   */
  public static DomainFactory getInstance()
  {
    if(null == instance)
      instance = new DefUseDomainFactory();
    return instance;
  }  
}

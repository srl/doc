package x86InstructionSet;


public interface DecInstruction extends Instruction
{
  public abstract Operand getDstOperand();
}
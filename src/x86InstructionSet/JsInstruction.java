package x86InstructionSet;


/**
 * JS Instruction
 * <p>
 * Jump short if sign (SF=0)
 * <p>
 * This instruction checks the state of one or more of the status 
 * flags in the EFLAGS register (CF, OF, PF, SF, and ZF) and, if the 
 * flags are in the specified state (condition), performs a jump to the
 * target instruction specified by the destination operand. A 
 * condition code (cc) is associated with each instruction to indicate 
 * the condition being tested for. If the condition is not satisfied, the 
 * jump is not performed and execution continues with the instruction 
 * following the Jcc instruction.
 */
public interface JsInstruction extends Instruction
{
  /**
   * Returns the target operand of this jump instruction.
   * @return the target operand.
   */
  public abstract Operand getTargetOperand();
}
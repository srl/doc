package x86InstructionSet;


public interface RepzStosInstruction extends Instruction
{
  /**
   * Returns the destination oeprand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();
  
  /**
   * Returns the source operand.  Returns null if
   * the operand doesn't exist.
   * @return the source operand.
   */
  public abstract Operand getSrcOperand();
}

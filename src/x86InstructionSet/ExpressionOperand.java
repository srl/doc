package x86InstructionSet;

public interface ExpressionOperand
{

  /**
   * Interprets this operand.  Interpretation is performed by
   * evaluating the expression.  The result is returned.  
   */
  public abstract domain.Value interpret(domain.State state);

  public abstract String toString();

}
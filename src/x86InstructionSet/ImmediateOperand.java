package x86InstructionSet;

public interface ImmediateOperand
{

  /**
   * Interprets this immediate operand.  Since an immediate operand is
   * a constant, this function simple returns a Value representing that 
   * constant.
   */
  public abstract domain.Value interpret(domain.State state);

  /**
   * Returns a string representation of this.
   */
  public abstract String toString();

}
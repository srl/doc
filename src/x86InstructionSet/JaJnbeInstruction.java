package x86InstructionSet;


public interface JaJnbeInstruction  extends Instruction
{
  /**
   * Returns the target operand for this jump.
   * @return the target operand.
   */
  public abstract Operand getTargetOperand();
}
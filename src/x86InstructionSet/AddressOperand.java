package x86InstructionSet;

import domain.State;
import domain.Value;

public interface AddressOperand
{

  /**
   * Returns the address represented by this <code>Operand</code>.
   * @return the address.
   */
  public abstract Address getAddress();

  public abstract Value interpret(State state);

  public abstract String toString();

}
package x86InstructionSet;


public interface IndirectOperand
{

  /**
   * Returns the operand placed in this indirect operand, without
   * doing any interpretation.  Thus, if this operand is 
   * <code>eax</code>, then <code>eax</code> will be returned, not
   * the value placed in <code>eax</code>. 
   * @return the operand.
   */
  public abstract Operand getOperand();

  /**
   * Returns the address placed in this operand, but doesn't
   * dereference the address.  In other words, if this operand is the target
   * of a MOV instruction, then the value returned is the address to be
   * MOVed to.
   */
  public abstract domain.Value getTarget(domain.State state);

  /**
   * Interprets this indirect operand.  I.e., determines which address this
   * operand refers to and retrieves the value stored at the address
   * (may be from the stack of global memory).  
   */
  public abstract domain.Value interpret(domain.State state);

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public abstract String toString();

}
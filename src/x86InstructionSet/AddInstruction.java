package x86InstructionSet;


public interface AddInstruction extends Instruction
{

  /**
   * Returns the destination operand for this <code>ADD</code> instruction.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /** 
   * Returns the source operand for this <code>ADD</code> instruction.
   * @return the destination operand.
   */
  public abstract Operand getSrcOperand();
}
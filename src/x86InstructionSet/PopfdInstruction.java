package x86InstructionSet;

/**
 * <b>Description<b>
 * Pops a doubleword (POPFD) from the top of the stack (if 
 * the current operand-size attribute is 32) and stores the 
 * value in the EFLAGS register, or pops a word from the top 
 * of the stack (if the operand-size attribute is 16) and 
 * stores it in the lower 16 bits of the EFLAGS register (that is,
 * the FLAGS register). These instructions reverse the 
 * operation of the PUSHF/PUSHFD instructions. 
 * <p>
 * The POPF (pop flags) and POPFD (pop flags double) mnemonics 
 * reference the same opcode. The POPF instruction is intended 
 * for use when the operand-size attribute is 16 and the POPFD
 * instruction for when the operand-size attribute is 32. Some 
 * assemblers may force the operand size to 16 when POPF is 
 * used and to 32 when POPFD is used. Others may treat these 
 * mnemonics as synonyms (POPF/POPFD) and use the current 
 * setting of the operand-size attribute to determine the size 
 * of values to be popped from the stack, regardless of the 
 * mnemonic used. 
 * <p>
 * The effect of the POPF/POPFD instructions on the EFLAGS 
 * register changes slightly, depending on the mode of 
 * operation of the processor. When the processor is operating 
 * in protected mode at privilege level 0 (or in real-address 
 * mode, which is equivalent to privilege level 0), all the 
 * non-reserved flags in the EFLAGS register except the VIP, 
 * VIF, and VM flags can be modified. The VIP and VIF flags 
 * are cleared, and the VM flag is unaffected.
 * <p>
 * When operating in protected mode, with a privilege level 
 * greater than 0, but less than or equal to IOPL, all the 
 * flags can be modified except the IOPL field and the VIP, 
 * VIF, and VM flags. Here, the IOPL flags are unaffected, 
 * the VIP and VIF flags are cleared, and the VM flag is 
 * unaffected. The interrupt flag (IF) is altered only when 
 * executing at a level at least as privileged as the IOPL. 
 * If a POPF/POPFD instruction is executed with insufficient 
 * privilege, an exception does not occur, but the 
 * privileged bits do not change. 
 * <p>
 * When operating in virtual-8086 mode, the I/O privilege 
 * level (IOPL) must be equal to 3 to use POPF/POPFD 
 * instructions and the VM, RF, IOPL, VIP, and VIF flags 
 * are unaffected. If the IOPL is less than 3, the 
 * POPF/POPFD instructions cause a general-protection 
 * exception (#GP).
 * <p>
 * See the section titled "EFLAGS Register" in Chapter 3 of 
 * the IA-32 Intel Architecture Software Developerís Manual, 
 * Volume 1, for information about the EFLAGS registers.
 * <p>
 * <b>Operation<b>
 * <code>
 * IF VM=0 (* Not in Virtual-8086 Mode *)<br>
 * &nbsp; THEN IF CPL=0<br>
 * &nbsp; &nbsp; THEN<br>
 * &nbsp; &nbsp; &nbsp; IF OperandSize = 32;<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; THEN<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; EFLAGS ? Pop();<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; (* All non-reserved flags except VIP, VIF, and VM can be modified; *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; (* VIP and VIF are cleared; VM is unaffected*)<br>
 * &nbsp; &nbsp; &nbsp; ELSE (* OperandSize = 16 *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; EFLAGS[15:0] ? Pop(); (* All non-reserved flags can be modified; *)<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; ELSE (* CPL > 0 *)<br>
 * &nbsp; &nbsp; IF OperandSize = 32;<br>
 * &nbsp; &nbsp; &nbsp; THEN<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; EFLAGS ? Pop()<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; (* All non-reserved bits except IOPL, VIP, and VIF can be modified; *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; (* IOPL is unaffected; VIP and VIF are cleared; VM is unaffected *)<br>
 * &nbsp; &nbsp; &nbsp; ELSE (* OperandSize = 16 *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; EFLAGS[15:0] ? Pop();<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; (* All non-reserved bits except IOPL can be modified *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; (* IOPL is unaffected *)<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; FI;<br>
 * &nbsp; ELSE (* In Virtual-8086 Mode *)<br>
 * &nbsp; &nbsp; IF IOPL=3<br>
 * &nbsp; &nbsp; &nbsp; THEN IF OperandSize=32<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; THEN<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; EFLAGS ? Pop()<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (* All non-reserved bits except VM, RF, IOPL, VIP, and VIF *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (* can be modified; VM, RF, IOPL, VIP, and VIF are unaffected *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; ELSE<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; EFLAGS[15:0] ? Pop()<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (* All non-reserved bits except IOPL can be modified *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; (* IOPL is unaffected *)<br>
 * &nbsp; &nbsp; &nbsp; FI;<br>
 * &nbsp; &nbsp; &nbsp; ELSE (* IOPL < 3 *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; #GP(0); (* trap to virtual-8086 monitor *)<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; FI;<br>
 * FI;<br>
 * </code>
 * 
 * @author mpv7292
 */
public interface PopfdInstruction extends Instruction
{

}

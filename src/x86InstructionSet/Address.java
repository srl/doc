/*
 * Address.java
 * Created on May 24, 2005
 */
package x86InstructionSet;

import java.util.LinkedList;

import ui.outputters.Outputter;


/**
 * Represents an address for an instruction.
 */
public class Address implements Comparable
{
    private long address;
    
    /**
     * Creates an unitialized address.  provide for testing only.
     */
    public Address()
    {
    }
    
    /**
     * Creates a new address object initialized to the input address. 
     * @param address the initial address in hex format.
     */
    public Address(String address)
    {
        this.address = Long.parseLong(address, 16);
    }

    /**
     * Creates a new address initialized to the input paramaeter.
     * @param address
     */
    public Address(long address)
    {
        this.address = address;
    }
    
    /**
     * Returns a clone of this object.
     */
    public Object clone()
    {
        Address clone = new Address();
        clone.address = address;
    	return clone;
    }
    
    
    /**
     * @see Comparable#compareTo
     */
    public int compareTo(Object obj)
    {
		Address rhs = (Address)obj;
		if(address < rhs.address)
			return -1;
		else if (address == rhs.address)
			return 0;
		else
			return 1;
    }
    
    /**
     * Returns true if this address equals rhs.
     */
    public boolean equals(Object rhs)
    {
        return address == ((Address)rhs).address;
    }
    
    /**
     * Converts the input array of longs to an array of Addresses.
     */
    public static Address[] toArray(long[] input)
    {
      	Address[] result = new Address[input.length];
      	for(int i = 0; i < input.length; i++)
      	    result[i] = new Address(input[i]);
      	return result;
    }

    /**
     * Converts the address to a long.
     */
    public long toLong()
    {
        return address;
    }
    
    /**
     * String representation
     */
    public String toString()
    {
      return Outputter.getInstance().toString(this).toUpperCase();
    }
}

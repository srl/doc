package x86InstructionSet;


public interface ShrInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns the source operand. This operand is optional.  If the input file
   * does not specify this operand, then <code>null</code> is returned.
   * @return the source operand. may be <code>null</code>.
   */
  public abstract Operand getSrcOperand();
}
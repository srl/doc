package x86InstructionSet;


/**
 * SBB instruction
 * 
 * <p>
 * Operation:<br>
 * <code>
 * &nbsp; DEST <- DEST - (SRC + CF)
 * </code>
 *   
 * <p>
 * Flags affected:<br>
 *   OF, SF, ZF, AF, PF, CF
 */
public interface SbbInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns the source operand.
   * @return the source operand.
   */
  public abstract Operand getSrcOperand();
}
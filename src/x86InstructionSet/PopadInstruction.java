package x86InstructionSet;

/**
 * <b>Description</b>
 * Pops doublewords (POPAD) from the stack into the general-purpose 
 * registers.  The registers are loaded in the following order: 
 * EDI, ESI, EBP, EBX, EDX, ECX, and EAX. (This instruction 
 * reverses the operation of the PUSHAD instruction.)  The value 
 * on the stack for the ESP register is ignored. Instead, the 
 * ESP register is incremented after each register is loaded.
 * <p>
 * The POPAD (pop all double) mnemonic reference the same opcode. The
 * POPA instruction is intended for use when the operand-size 
 * attribute is 16 and the POPAD instruction for when the 
 * operand-size attribute is 32. Some assemblers may force 
 * the operand size to 16 when POPA is used and to 32 when POPAD 
 * is used (using the operand-size override prefix [66H] if necessary). 
 * Others may treat these mnemonics as synonyms (POPA/POPAD) and use 
 * the current setting of the operand-size attribute to determine 
 * the size of values to be popped from the stack, regardless of 
 * the mnemonic used. (The D flag in the current code segment�s 
 * segment descriptor determines the operand-size attribute.)
 * 
 * <b>Operation</b>
 * <code>
 * IF OperandSize = 32 (* instruction = POPAD *)<br>
 * THEN<br>
 * &bnsp; EDI <-- Pop();<br>
 * &bnsp; ESI <-- Pop();<br>
 * &bnsp; EBP <-- Pop();<br>
 * &bnsp; increment ESP by 4 (* skip next 4 bytes of stack *)<br>
 * &bnsp; EBX <-- Pop();<br>
 * &bnsp; EDX <-- Pop();<br>
 * &bnsp; ECX <-- Pop();<br>
 * &bnsp; EAX <-- Pop();<br>
 * </code>
 * @author mpv
 */
public interface PopadInstruction extends Instruction
{
}

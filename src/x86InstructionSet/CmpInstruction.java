package x86InstructionSet;


public interface CmpInstruction extends Instruction
{
  /**
   * Returns destination operand.
   * @return destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns source operand.
   * @return source operand.
   */
  public abstract Operand getSrcOperand();
}
package x86InstructionSet;


/**
 * Copies the value from the I/O port specified with the second 
 * operand (source operand) to the destination operand (first operand). 
 * The source operand can be a byte-immediate or the DX register; 
 * the destination operand can be register AL, AX, or EAX, depending 
 * on the size of the port being accessed (8, 16, or 32 bits, respectively). 
 * Using the DX register as a source operand allows I/O port addresses 
 * from 0 to 65,535 to be accessed; using a byte immediate allows I/O
 * port addresses 0 to 255 to be accessed.
 * <p>
 * When accessing an 8-bit I/O port, the opcode determines the port 
 * size; when accessing a 16- and 32-bit I/O port, the operand-size 
 * attribute determines the port size. 
 * <p>
 * At the machine code level, I/O instructions are shorter when 
 * accessing 8-bit I/O ports. Here, the upper eight bits of the 
 * port address will be 0.
 * <p>
 * This instruction is only useful for accessing I/O ports located 
 * in the processorís I/O address space. See Chapter 13, Input/Output, 
 * in the IA-32 Intel Architecture Software Developerís Manual, 
 * Volume 1, for more information on accessing I/O ports in the I/O 
 * address space.
 */
public interface InInstruction extends Instruction
{
  /**
   * Returns the destination operand of this instruction.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns the source operand of this instruction.
   * @return the source operand.
   */
  public abstract Operand getSrcOperand();
}
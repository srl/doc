package x86InstructionSet;

import domain.Register;
import domain.Value;

/**
 *
 */
public interface SegmentRegisterOperand
{
  /**
   * @return the register represented by this object.
   */
  public abstract Register getRegister();

  /**
   * @param state
   * @return the <code>Value</code> of this register operand after interpreting.  
   *    That is, the <code>value</code> placed in this register. 
   */
  public abstract Value interpret(domain.State state);

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public abstract String toString();

}
package x86InstructionSet;


public interface OrInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getSrcOperand();
}
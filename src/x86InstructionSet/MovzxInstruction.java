package x86InstructionSet;


public interface MovzxInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Return the source operand.
   * @return the source operand.
   */
  public abstract Operand getSrcOperand();
}
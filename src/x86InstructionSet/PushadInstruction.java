package x86InstructionSet;

/**
 * <b>Description</b>
 * Pushes the contents of the general-purpose registers onto the stack. 
 * The registers are stored on the stack in the following order: 
 * <code>EAX</code>, <code>ECX</code>, <code>EDX</code>, <code>EBX</code>, 
 * <code>ESP</code> (original value), <code>EBP</code>, <code>ESI</code>, 
 * and <code>EDI</code>. 
 * This instruction performs the reverse operation of the 
 * <code>POPAD</code> instruction. The value pushed for the 
 * <code>ESP</code> or <code>SP</code> register is its value
 * before prior to pushing the first register (see the "Operation" 
 * section below).
 * <p>
 * The <code>PUSHA</code> (push all) and <code>PUSHAD</code> 
 * (push all double) mnemonics reference the same opcode. 
 * The PUSHA instruction is intended for use when the 
 * operand-size attribute is 16 and the 
 * <code>PUSHAD</code> instruction for when the operand-size attribute 
 * is 32. Some assemblers may force the operand size to 16 when 
 * <code>PUSHA</code> is used and to 32 when <code>PUSHAD</code> 
 * is used. Others may treat these mnemonics as synonyms 
 * (<code>PUSHA</code>/<code>PUSHAD</code>) and use the current 
 * setting of the operandsize attribute to determine the size of 
 * values to be pushed from the stack, regardless of the 
 * mnemonic used.
 * <p>
 * In the real-address mode, if the <code>ESP</code> or 
 * <code>SP</code> register is 1, 3, or 5 when the 
 * <code>PUSHA</code>/<code>PUSHAD</code> instruction is executed, 
 * the processor shuts down due to a lack of stack space. No 
 * exception is generated to indicate this condition.
 * <p>
 * <b>Operation</b>
 * <code>
 * Temp <-- (ESP);
 * Push(EAX);
 * Push(ECX);
 * Push(EDX);
 * Push(EBX);
 * Push(Temp);
 * Push(EBP);
 * Push(ESI);
 * Push(EDI);
 * </code>
 * <p>
 * @author mpv
 */
public interface PushadInstruction extends Instruction
{
}

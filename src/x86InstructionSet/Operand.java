/*
 * Operand.java
 * Created on Apr 9, 2005
 */
package x86InstructionSet;

/**
 * Interface for nodes in the syntax tree that are intended
 * to hold Operands.
 */
public interface Operand
{
    /**
     * Returns the value this operand stores..
     */
    public domain.Value interpret(domain.State state);
}

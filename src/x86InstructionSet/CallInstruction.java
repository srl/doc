package x86InstructionSet;


public interface CallInstruction extends Instruction
{
  /**
   * Returns the target operand of this call instruction. 
   * @return the target of this call.
   */
  public abstract Operand getTargetOperand();
}
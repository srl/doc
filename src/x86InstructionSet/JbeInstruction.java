package x86InstructionSet;


public interface JbeInstruction extends Instruction
{
  /**
   * Returns the target operand of this jump.
   * @return the target operand.
   */
  public abstract Operand getTargetOperand();
}
/*
 * AddressList.java
 * Created on May 24, 2005
 */
package x86InstructionSet;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Maintains a list of addresses.
 */
public class AddressList implements Iterable<Address>
{
    LinkedList<Address>	addresses = new LinkedList<Address>();
    
    /**
     * Creates an empty AddressList
     */
    public AddressList()
    {
    }
    
    /**
     * Creates an AddressList containing one address.
     */
    public AddressList(Address address)
    {
        add(address);
    }
    
    /**
     * Adds an address to this list.
     */
    public void add(Address address)
    {
        addresses.add(address);
    }
    
    /**
     * Adds all items from <code>list</code> to this.
     * @param list an <code>AddressList</code> to add items from.
     */
    public void addAll(AddressList list)
    {
      addresses.addAll(list.addresses);
    }
    
    /**
     * Removes all items from this list. 
     */
    public void clear()
    {
      addresses.clear();
    }
    
    /**
     * Returns a clone of this object.
     */
    public Object clone()
    {
        AddressList clone = new AddressList();
        clone.addresses = (LinkedList<Address>)addresses.clone();
        return clone;
    }
    
    /**
     * Returns true if this list contains an address
     * equivalent to the input address.
     */
    public boolean has(Address address)
    {
        Iterator iter = iterator();
        while(iter.hasNext())
        {
            if(iter.next().equals(address))
                return true;
        }
        return false;
    }
    
    /**
     * Returns an iterator to iterate this list
     * of addresses.
     */
    public Iterator<Address> iterator()
    {
        return addresses.iterator();
    }
    
    /**
     * Returns the number of addresses in this list.
     */
    public int size()
    {
        return addresses.size();
    }

    /**
     * string representation
     */
    public String toString()
    {
        return addresses.toString();
    }
}

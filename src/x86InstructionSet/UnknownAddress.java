/*
 * UnknownAddress.java
 * Created on May 24, 2005
 */
package x86InstructionSet;

import ui.outputters.Outputter;

/**
 * Represents an address that cannot be determined.
 */
public class UnknownAddress extends Address
{
    /** 
     * a static UnknownAddress object supplied to facilitate
     * comparisions to the UnknownAddress type.
     */
    
    public static final UnknownAddress UNKNOWN = new UnknownAddress();
    
    /**
     * Creates a new unknown address.
     */
    public UnknownAddress()
    {
        super(-1);
    }
    
    public int compareTo(Object obj)
    {
      return 0;
    }
    
    /**
     * Returns true if rhs is equal to this.  
     */
    public boolean equals(Object rhs)
    {
        return (rhs instanceof UnknownAddress);
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString()
    {
      return Outputter.getInstance().toString(this);
    }
}

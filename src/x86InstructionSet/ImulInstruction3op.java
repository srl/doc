package x86InstructionSet;


/**
 * This instruction performs a signed multiplication of two operands.
 * <p> 
 * This instruction has three forms, depending on the number of operands.<br>
 * <ul>
 * <li>One-operand form. This form is identical to that used by the 
 *        MUL instruction. Here, the source operand (in a 
 *        general-purpose register or memory location) is multiplied 
 *        by the value in the AL, AX, or EAX register (depending on the 
 *        operand size) and the product is stored in the AX, DX:AX, 
 *        or EDX:EAX registers, respectively.
 * <li>Two-operand form. With this form the destination operand (the first 
 *        operand) is multiplied by the source operand (second 
 *        operand). The destination operand is a generalpurpose
 *        register and the source operand is an immediate value, 
 *        a general-purpose register, or a memory location. The 
 *        product is then stored in the destination operand location.
 * <li>Three-operand form. This form requires a destination operand (the first 
 *        operand) and two source operands (the second and the 
 *        third operands). Here, the first source operand (which
 *        can be a general-purpose register or a memory location) is 
 *        multiplied by the second source operand (an immediate value). 
 *        The product is then stored in the destination operand (a 
 *        general-purpose register).
 * </ul>
 * <P>
 * When an immediate value is used as an operand, it is sign-extended to 
 *  the length of the destination operand format.
 * <p>
 * The CF and OF flags are set when significant bits are carried into the 
 * upper half of the result.  The CF and OF flags are cleared when the result 
 * fits exactly in the lower half of the result.  The three forms of the IMUL 
 * instruction are similar in that the length of the product is calculated
 * to twice the length of the operands. With the one-operand form, the 
 * product is stored exactly in the destination. With the two- and three- 
 * operand forms, however, result is truncated to the length of the destination 
 * before it is stored in the destination register. Because of this truncation,
 * the CF or OF flag should be tested to ensure that no significant bits are lost.
 * The two- and three-operand forms may also be used with unsigned operands 
 * because the lower half of the product is the same regardless if the operands 
 * are signed or unsigned. The CF and OF flags, however, cannot be used to 
 * determine if the upper half of the result is non-zero.
 * <p>
 * Operation<br>
 * <code>
 * IF (NumberOfOperands = 1)<br>
 * THEN <br>
 * &nbsp; IF (OperandSize = 8)<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; AX <-- AL * SRC (* signed multiplication *)<br>
 * &nbsp; &nbsp; IF ((AH = 00H) OR (AH = FFH))<br>
 * &nbsp; &nbsp; THEN CF = 0; OF = 0;<br>
 * &nbsp; &nbsp; ELSE CF = 1; OF = 1;<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; ELSE IF OperandSize = 16<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; DX:AX <-- AX * SRC (* signed multiplication *)<br>
 * &nbsp; &nbsp; IF ((DX = 0000H) OR (DX = FFFFH))<br>
 * &nbsp; &nbsp; THEN CF = 0; OF = 0;<br>
 * &nbsp; &nbsp; ELSE CF = 1; OF = 1;<br>
 * &nbsp; FI;<br>
 * &nbsp; ELSE (* OperandSize = 32 *)<br>
 * &nbsp; &nbsp; EDX:EAX < EAX * SRC (* signed multiplication *) <br>
 * &nbsp; &nbsp; IF ((EDX = 00000000H) OR (EDX = FFFFFFFFH))<br>
 * &nbsp; &nbsp; THEN CF = 0; OF = 0;<br>
 * &nbsp; &nbsp; ELSE CF = 1; OF = 1;<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; FI;<br>
 * ELSE IF (NumberOfOperands = 2)<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; temp <-- DEST * SRC (* signed multiplication; temp is double DEST size*)<br>
 * &nbsp; &nbsp; DEST <-- DEST * SRC (* signed multiplication *)<br>
 * &nbsp; &nbsp; IF temp != DEST<br>
 * &nbsp; &nbsp; THEN CF = 1; OF = 1;<br>
 * &nbsp; &nbsp; ELSE CF = 0; OF = 0;<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; ELSE (* NumberOfOperands = 3 *)<br>
 * &nbsp; &nbsp; DEST <-- SRC1 * SRC2 (* signed multiplication *)<br>
 * &nbsp; &nbsp; temp <-- SRC1 * SRC2 (* signed multiplication; temp is double SRC1 size *)<br>
 * &nbsp; &nbsp; IF temp != DEST<br>
 * &nbsp; &nbsp; THEN CF = 1; OF = 1;<br>
 * &nbsp; &nbsp; ELSE CF = 0; OF = 0;<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; FI;<br>
 * FI;<br>
 * </code>
 */
public interface ImulInstruction3op extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns the first of the two source operands.
   * @return the first source operand.
   */
  public abstract Operand getSrc1Operand();

  /**
   * Returns the second of the two source operands.
   * @return the second source operand.
   */
  public abstract Operand getSrc2Operand();
}
package x86InstructionSet;


/**
 * Adds the destination operand (first operand), the source 
 * operand (second operand), and the carry (CF) flag and 
 * stores the result in the destination operand. The destination 
 * operand can be a register or a memory location; the source 
 * operand can be an immediate, a register, or a memory location. 
 * (However, two memory operands cannot be used in one instruction.) 
 * The state of the CF flag represents a carry from a previous 
 * addition. When an immediate value is used as an operand, it 
 * is sign-extended to the length of the destination operand format.
 * <p>
 * The ADC instruction does not distinguish between signed or unsigned 
 * operands. Instead, the processor evaluates the result for both 
 * data types and sets the OF and CF flags to indicate a carry 
 * in the signed or unsigned result, respectively. The SF flag 
 * indicates the sign of the signed result. 
 * <p>
 * The ADC instruction is usually executed as part of a multibyte 
 * or multiword addition in which an ADD instruction is followed by an 
 * ADC instruction.
 * <p>
 * This instruction can be used with a LOCK prefix to allow the 
 * instruction to be executed atomically.
 */
public interface AdcInstruction extends Instruction
{  
  /**
   * Returns the destination operand for this instruction.
   * @return the destination operand.
   */
  public Operand getDstOperand();

  /** 
   * Returns the source operand for this instruction.
   * @return the destination operand.
   */
  public Operand getSrcOperand();
}
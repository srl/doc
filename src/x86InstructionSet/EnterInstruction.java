package x86InstructionSet;


/**
 * ENTER instruction.
 * <p>
 * <b>Description</b><br>
 * Creates a stack frame for a procedure. The first operand (size operand) 
 * specifies the size of the stack frame (that is, the number of bytes of 
 * dynamic storage allocated on the stack for the procedure).  The second 
 * operand (nesting level operand) gives the lexical nesting level (0 to 31) 
 * of the procedure. The nesting level determines the number of stack frame 
 * pointers that are copied into the �display area� of the new stack frame 
 * from the preceding frame. Both of these operands are immediate values.
 * <p>
 * The stack-size attribute determines whether the BP (16 bits) or EBP 
 * (32 bits) register specifies the current frame pointer and whether SP 
 * (16 bits) or ESP (32 bits) specifies the stack pointer. 
 * <p>
 * The ENTER and companion LEAVE instructions are provided to support block 
 * structured languages. The ENTER instruction (when used) is typically the 
 * first instruction in a procedure and is used to set up a new stack frame 
 * for a procedure. The LEAVE instruction is then used at the end of the 
 * procedure (just before the RET instruction) to release the stack frame.
 * <p> 
 * If the nesting level is 0, the processor pushes the frame pointer from 
 * the EBP register onto the stack, copies the current stack pointer from 
 * the ESP register into the EBP register, and loads the ESP register with 
 * the current stack-pointer value minus the value in the size operand. 
 * For nesting levels of 1 or greater, the processor pushes additional 
 * frame pointers on the stack before adjusting the stack pointer. These 
 * additional frame pointers provide the called procedure with access points 
 * to other nested frames on the stack. See �Procedure Calls for Block-Structured 
 * Languages� in Chapter 6 of the IA-32 Intel Architecture Software 
 * Developer�s Manual, Volume 1, for more information about the actions of 
 * the ENTER instruction.
 * <p>
 * <b>Operation</b>
 * <code>
 * NestingLevel ? NestingLevel MOD 32<br>
 * IF StackSize = 32<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; Push(EBP) ;<br>
 * &nbsp; &nbsp; FrameTemp ? ESP;<br>
 * &nbsp; ELSE (* StackSize = 16*)<br>
 * &nbsp; &nbsp; Push(BP);<br>
 * &nbsp; &nbsp; FrameTemp ? SP;<br>
 * FI;<br>
 * IF NestingLevel = 0<br>
 * &nbsp; THEN GOTO CONTINUE;<br>
 * FI;<br>
 * IF (NestingLevel > 0)<br>
 * FOR i ? 1 TO (NestingLevel ? 1)<br>
 * &nbsp; DO<br>
 * &nbsp; &nbsp; IF OperandSize = 32<br>
 * &nbsp; &nbsp; &nbsp; THEN<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; IF StackSize = 32<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; EBP ? EBP ? 4;<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Push([EBP]); (* doubleword push *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; ELSE (* StackSize = 16*)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; BP ? BP ? 4;<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Push([BP]); (* doubleword push *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; FI;<br>
 * &nbsp; &nbsp; &nbsp; ELSE (* OperandSize = 16 *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; IF StackSize = 32<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; THEN<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; EBP ? EBP ? 2;<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Push([EBP]); (* word push *)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ELSE (* StackSize = 16*)<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; BP ? BP ? 2;<br>
 * &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Push([BP]); (* word push *)<br>
 * &nbsp; &nbsp; &nbsp; &nsbsp; FI;<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; OD;<br>
 * &nbsp; IF OperandSize = 32<br>
 * &nbsp; &nbsp; THEN<br>
 * &nbsp; &nbsp; &nbsp; Push(FrameTemp); (* doubleword push *)<br>
 * &nbsp; &nbsp; ELSE (* OperandSize = 16 *)<br>
 * &nbsp; &nbsp; &nbsp; Push(FrameTemp); (* word push *)<br>
 * &nbsp; FI;<br>
 * &nbsp; GOTO CONTINUE;<br>
 * FI;<br>
 * CONTINUE:<br>
 * IF StackSize = 32<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; EBP ? FrameTemp<br>
 * &nbsp; &nbsp; ESP ? EBP ? Size;<br>
 * &nbsp; ELSE (* StackSize = 16*)<br>
 * &nbsp; &nbsp; BP ? FrameTemp<br>
 * &nbsp; &nbsp; SP ? BP ? Size;<br>
 * FI;<br>
 * END;<br>
 * </code>
 * <p>
 * <b>Flags Affected</b><br>
 * None.
 */
public interface EnterInstruction extends Instruction
{

  /**
   * Returns the nesting level operand (the second operand).
   * @return the nesting level operand.
   */
  public abstract Operand getNestingLevelOperand();

  /**
   * Returns the size operand (the first operand).
   * @return the size operand.
   */
  public abstract Operand getSizeOperand();
}
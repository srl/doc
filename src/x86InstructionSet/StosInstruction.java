package x86InstructionSet;


public interface StosInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();
}
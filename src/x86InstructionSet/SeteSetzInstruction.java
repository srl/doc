package x86InstructionSet;


/**
 * Sets the byte in the operand to 1 if the Zero Flag is set, otherwise sets the operand to 0.
 * As of now, it just sets the operand to UNDEFINED.
 */
public interface SeteSetzInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();
}
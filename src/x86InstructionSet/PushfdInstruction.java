package x86InstructionSet;


/**
 * PUSHFD instruction<br>
 * Pushes EFLAGS
 * <p>
 * PUSHF/PUSHFD<br>
 * Description<br>
 * These instructions decrement the stack pointer by four (if the current 
 * operand-size attribute is 32) and pushes the entire contents of the 
 * EFLAGS register onto the stack, or decrements the stack pointer by two 
 * (if the operand-size attribute is 16) and pushes the lower 16 bits of the
 * EFLAGS register (that is, the FLAGS register) onto the stack. (These 
 * instructions reverse the operation of the POPF/POPFD instructions.) 
 * When copying the entire EFLAGS register to the stack, the VM and 
 * RF flags (bits 16 and 17) are not copied; instead, the values 
 * for these flags are cleared in the EFLAGS image stored on the 
 * stack. Refer to Section 3.6.3. in Chapter 3, Basic Execution Environment 
 * of the Intel Architecture Software Developerís Manual, Volume 1, for 
 * information about the EFLAGS registers. 
 * <p>
 * The PUSHF (push flags) and PUSHFD (push flags double) mnemonics reference 
 * the same opcode. The PUSHF instruction is intended for use when the 
 * operand-size attribute is 16 and the PUSHFD instruction for when the 
 * operand-size attribute is 32. Some assemblers may force the operand size to 
 * 16 when PUSHF is used and to 32 when PUSHFD is used. Others may treat these
 * mnemonics as synonyms (PUSHF/PUSHFD) and use the current setting of the 
 * operand-size attribute to determine the size of values to be pushed from
 * the stack, regardless of the mnemonic used.
 * <p>
 * When in virtual-8086 mode and the I/O privilege level (IOPL) is less 
 * than 3, the PUSHF/PUSHFD instruction causes a general protection 
 * exception (#GP). In the real-address mode, if the ESP or SP register 
 * is 1, 3, or 5 when the PUSHA/PUSHAD instruction is executed, the 
 * processor shuts down due to a lack of stack space. No exception is 
 * generated to indicate this condition.
 */
public interface PushfdInstruction extends Instruction
{
}
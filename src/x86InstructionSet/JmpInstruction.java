package x86InstructionSet;


public interface JmpInstruction extends Instruction
{
  /**
   * Returns the target operand of this jump instruction.
   * @return the target operand.
   */
  public abstract Operand getTargetOperand();
}
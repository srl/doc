package x86InstructionSet;


/**
 * MOVSX instruction.
 * <p>
 * This instruction copies the contents of the source operand (register or memory location) 
 * to the destination operand (register) and sign extends the value to 16 or 32 bits. 
 * For more information, refer to Section 6-5, Sign Extension in Chapter 6, 
 * Instruction Set Summary of the Intel Architecture Software Developerís Manual, Volume 1. 
 * The size of the converted value depends on the operand-size attribute. 
 * <p>
 * Operation<br> 
 * <code>
 * DEST <-- SignExtend(SRC);
 * </code>
 * 
 * <p>
 * Instruction Description<br>
 * MOVSX r16,r/m8 &nbsp; Move byte to word with sign-extension<br>
 * MOVSX r32,r/m8 &nbsp; Move byte to doubleword, sign-extension<br>
 * MOVSX r32,r/m16 &nbsp; Move word to doubleword, sign-extension
 * 
 * <p>
 * Flags Affected<br>
 * None.
 * 
 * <p>
 * Protected Mode Exceptions<br>
 * #GP(0) &nbsp; If a memory operand effective address is outside the CS, DS, ES, FS, or
 *      GS segment limit.<br>
 *      &nbsp; &nbsp; If the DS, ES, FS, or GS register contains a null segment selector.<br>
 * #SS(0) &nbsp; If a memory operand effective address is outside the SS segment limit.<br>
 * #PF(fault-code) &nbsp; If a page fault occurs.<br>
 * #AC(0) &nbsp; If alignment checking is enabled and an unaligned memory reference is
 *      made while the current privilege level is 3.
 * 
 * <p>
 * Real-Address Mode Exceptions<br>
 * #GP &nbsp; If a memory operand effective address is outside the CS, DS, ES, FS, or
 *      GS segment limit.<br>
 * #SS &nbsp; If a memory operand effective address is outside the SS segment limit.
 * 
 * <p>
 * Virtual-8086 Mode Exceptions<br>
 * #GP(0) &nbps; If a memory operand effective address is outside the CS, DS, ES, FS, or
 * GS segment limit.<br>
 * #SS(0) If a memory operand effective address is outside the SS segment limit.<br>
 * #PF(fault-code) If a page fault occurs.<br>
 */
public interface MovsxInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns the source operand.
   * @return the source operand.
   */
  public abstract Operand getSrcOperand();
}
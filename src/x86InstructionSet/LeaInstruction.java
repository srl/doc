package x86InstructionSet;


/**
 * LEA Instruction
 * <p>
 * <b>Description</b><br>
 * Computes the effective address of the second operand (the source 
 * operand) and stores it in the first operand (destination operand). 
 * The source operand is a memory address (offset part) specified with 
 * one of the processors addressing modes; the destination operand is 
 * a general-purpose register. The address-size and operand-size 
 * attributes affect the action performed by this instruction, as 
 * shown in the following table. The operand-size attribute of the 
 * instruction is determined by the chosen register; the address-size 
 * attribute is determined by the attribute of the code segment.
 */
public interface LeaInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns the source operand.
   * @return the source operand.
   */
  public abstract Operand getSrcOperand();
}
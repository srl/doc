package x86InstructionSet;

public interface ExternalFunctionOperand
{

  /**
   * Returns the value this operand stores.
   */
  public abstract domain.Value interpret(domain.State state);

  /**
   * Returns the name of this external function in the format
   * DLLNAME.FUNCTIONNAME (ex. KERNEL32.ExitProcess)
   */
  public abstract String getName();

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public abstract String toString();

}
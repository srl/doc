package x86InstructionSet;


public interface IdivInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();
}
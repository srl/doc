package x86InstructionSet;


public interface DivInstruction extends Instruction
{
  /** 
   * Returns the destination operand--the only operand given to a
   * DIV instruction.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();
}
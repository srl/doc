package x86InstructionSet;


/**
 * Stops instruction execution and places the processor 
 * in a HALT state. An enabled interrupt (including NMI 
 * and SMI), a debug exception, the BINIT# signal, the 
 * INIT# signal, or the RESET# signal will resume execution. 
 * If an interrupt (including NMI) is used to resume execution
 * after a HLT instruction, the saved instruction pointer 
 * (CS:EIP) points to the instruction following the HLT 
 * instruction.
 * <p>
 * When a HLT instruction is executed on an IA-32 processor 
 * supporting Hyper-Threading Technology, only the logical 
 * processor that executes the instruction is halted. The 
 * other logical processors in the physical processor remain 
 * active, unless they are each individually halted by 
 * executing a HLT instruction. 
 * <p>
 * The HLT instruction is a privileged instruction. When 
 * the processor is running in protected or virtual-8086 mode, 
 * the privilege level of a program or procedure must be 0 
 * to execute the HLT instruction.
 */
public interface HltInstruction extends Instruction
{
}
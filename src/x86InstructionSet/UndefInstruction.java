package x86InstructionSet;


/**
 * The Undef instruction is not a real x86 instruction.  Instead, it 
 * is intended to be used by the stub dll functions as a means to 
 * mark some register or memory location as undefined.  For example,
 * if a function returns a value in register eax, but you have no 
 * way of determining what that value may be, then you can use
 * undef to set register eax to an undefined value.
 */
public interface UndefInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();
}
package x86InstructionSet;


public interface RetInstruction extends Instruction
{
  /** 
   * Returns the destination operand.  For the return instruction,
   * the destination operand tells how many additional bytes to pop off
   * the stack.  The return value may be NULL, indicating that no
   * additional bytes should be removed from the stack.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();
}
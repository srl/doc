package x86InstructionSet;


/**
 * <b>Description<b><br>
 * Causes the processor�s LOCK# signal to be asserted during 
 * execution of the accompanying instruction (turns the 
 * instruction into an atomic instruction). In a 
 * multiprocessor environment, the LOCK# signal insures that 
 * the processor has exclusive use of any shared memory while 
 * the signal is asserted.
 * <p>
 * Note that in later IA-32 processors (including the Pentium 4, 
 * Intel Xeon, and P6 family processors), locking may occur without 
 * the LOCK# signal being asserted. See IA-32 Architecture 
 * Compatibility below.
 * <p>
 * The LOCK prefix can be prepended only to the following 
 * instructions and only to those forms of the instructions where 
 * the destination operand is a memory operand: ADD, ADC, AND,
 * BTC, BTR, BTS, CMPXCHG, CMPXCH8B, DEC, INC, NEG, NOT, OR, SBB, 
 * SUB, XOR, XADD, and XCHG. If the LOCK prefix is used with one 
 * of these instructions and the source operand is a memory operand, 
 * an undefined opcode exception (#UD) may be generated. An
 * undefined opcode exception will also be generated if the LOCK 
 * prefix is used with any instruction not in the above list. 
 * The XCHG instruction always asserts the LOCK# signal regardless of
 * the presence or absence of the LOCK prefix. 
 * <p>
 * The LOCK prefix is typically used with the BTS instruction to 
 * perform a read-modify-write operation on a memory location in 
 * shared memory environment.
 * <p>
 * The integrity of the LOCK prefix is not affected by the alignment 
 * of the memory field. Memory locking is observed for arbitrarily 
 * misaligned fields. 
 * <p>
 * <b>IA-32 Architecture Compatibility<b><br>
 * Beginning with the P6 family processors, when the LOCK prefix is 
 * prefixed to an instruction and the memory area being accessed is 
 * cached internally in the processor, the LOCK# signal is
 * generally not asserted. Instead, only the processor�s cache is locked. 
 * Here, the processor�s cache coherency mechanism insures that the 
 * operation is carried out atomically with regards to memory. See 
 * �Effects of a Locked Operation on Internal Processor Caches� in 
 * Chapter 7 of IA-32 Intel Architecture Software Developer�s Manual, 
 * Volume 3, the for more information on locking of caches.
 */
public interface LockInstruction extends Instruction
{
  /**
   * Returns the instruction nested within this <code>LOCK</code> 
   * instruction.  The <code>LOCK</code> instruction contains a
   * nested instruction to be executed atomically 
   * (see description for <code>LOCK</code> instruction).  
   * This function returns the nested instruction.  
   * @return the instruction nested within this instruction.
   */
  public abstract Instruction getChildInstruction();
}
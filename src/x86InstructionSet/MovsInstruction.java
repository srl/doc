package x86InstructionSet;


/**
 * MOVS Instruction
 * <p>
 * MOVS/MOVSB/MOVSW/MOVSD�Move Data from String to String
 * <p>
 * Description<br>
 * These instructions move the byte, word, or doubleword specified with the 
 * second operand (source operand) to the location specified with the first 
 * operand (destination operand). Both the source and destination operands 
 * are located in memory. The address of the source operand is read from the 
 * DS:ESI or the DS:SI registers (depending on the address-size attribute of the 
 * instruction, 32 or 16, respectively). The address of the destination operand is 
 * read from the ES:EDI or the ES:DI registers (again depending on the address-size 
 * attribute of the instruction). The DS segment may be overridden with a segment 
 * override prefix, but the ES segment cannot be overridden.
 * <p>
 * At the assembly-code level, two forms of this instruction are allowed: the 
 * �explicit-operands� form and the �no-operands� form. The explicit-operands 
 * form (specified with the MOVS mnemonic) allows the source and destination 
 * operands to be specified explicitly. Here, the source and destination operands 
 * should be symbols that indicate the size and location of the source value and 
 * the destination, respectively. This explicit-operands form is provided to allow 
 * documentation; however, note that the documentation provided by this form can be 
 * misleading.  That is, the source and destination operand symbols must specify the 
 * correct type (size) of the operands (bytes, words, or doublewords), but they 
 * do not have to specify the correct location. The locations of the source and 
 * destination operands are always specified by the DS:(E)SI and ES:(E)DI registers, 
 * which must be loaded correctly before the move string instruction is executed.
 * <p>
 * The no-operands form provides �short forms� of the byte, word, and doubleword 
 * versions of the MOVS instructions. Here also DS:(E)SI and ES:(E)DI are assumed 
 * to be the source and destination operands, respectively. The size of the source 
 * and destination operands is selected with the mnemonic: MOVSB (byte move), MOVSW 
 * (word move), or MOVSD (doubleword move). After the move operation, the (E)SI and 
 * (E)DI registers are incremented or decremented automatically according to the 
 * setting of the DF flag in the EFLAGS register. (If the DF flag is 0, the (E)SI 
 * and (E)DI register are incremented; if the DF flag is 1, the (E)SI and (E)DI 
 * registers are decremented.) The registers are incremented or decremented by 
 * one for byte operations, by two for word operations, or by four for 
 * doubleword operations.
 * <p>
 * The MOVS, MOVSB, MOVSW, and MOVSD instructions can be preceded by the REP 
 * prefix (refer to �REP/REPE/REPZ/REPNE /REPNZ�Repeat String Operation Prefix� 
 * in this chapter) for block moves of ECX bytes, words, or doublewords.
 * <p>
 * Operation<br>
 * <code>
 * DEST <-- SRC;<br>
 * IF (byte move)<br>
 * THEN IF DF = 0<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; (E)SI <-- (E)SI + 1;<br>
 * &nbsp; &nbsp; (E)DI <-- (E)DI + 1;<br>
 * &nbsp; ELSE<br>
 * &nbsp; &nbsp; (E)SI <-- (E)SI � 1;<br>
 * &nbsp; &nbsp; (E)DI <-- (E)DI � 1;<br>
 * &nbsp; FI;<br>
 * ELSE IF (word move)<br>
 * &nbsp; THEN IF DF = 0<br>
 * &nbsp; &nbsp; (E)SI <-- (E)SI + 2;<br>
 * &nbsp; &nbsp; (E)DI <-- (E)DI + 2;<br>
 * &nbsp; ELSE<br>
 * &nbsp; &nbsp; (E)SI <-- (E)SI � 2;<br>
 * &nbsp; &nbsp; (E)DI <-- (E)DI � 2;<br>
 * &nbsp; FI;<br>
 * ELSE (* doubleword move*)<br>
 * &nbsp; THEN IF DF = 0<br>
 * &nbsp; &nbsp; (E)SI <-- (E)SI + 4;<br>
 * &nbsp; &nbsp; (E)DI <-- (E)DI + 4;<br>
 * &nbsp; ELSE<br>
 * &nbsp; &nbsp; (E)SI <-- (E)SI � 4;<br>
 * &nbsp; &nbsp; (E)DI <-- (E)DI � 4;<br>
 * &nbsp; FI;<br>
 * FI;<br>
 * </code>
 * <p>
 * Flags Affected<br>
 * None.
 */
public interface MovsInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns the source operand.
   * @return the source operand.
   */
  public abstract Operand getSrcOperand();
}
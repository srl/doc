package x86InstructionSet;

import instructionInterpreters.InstructionInterpreter;

import java.util.Map;

import domain.State;

/**
 * Specifies a general interface that all instructions should follow.  
 */
public interface Instruction extends Comparable<Instruction>
{
  /**
   * @see Object#equals(java.lang.Object)
   */
  public abstract boolean equals(Object obj);
  
  /**
   * Returns the address of this instruction.
   */
  public abstract Address getAddress();
  
  /**
   * Returns the state produced before interpreting this instruction.
   */
  public abstract State getBeginState();
  
  /**
   * Returns the state produced after interpreting this isntruction.
   */
  public abstract State getEndState();

  /**
   * TODO: Remove?
   */
  public abstract int getIndex();
  
  /**
   * Returns the address of the next executed instruction without performing
   * any interpretation.  This is intended to help the client create a 
   * rough CFG before doing complete interpretation.  Since this
   * function performs no interpretation, it may not always return the 
   * correct result.  For instance, it cannot correctly calculate the
   * target of a RETN instruction and will therefore return an
   * empty AddressList.
   * This implementation provides a default implementation that returns
   * the next address in the program sequence.
   */
  public abstract AddressList getNextQuickly(Address nextAddress); 
  
  /**
   * Returns the state produced by the previous analysis (if there was a previous
   * analysis) before interpreting this instruction.  If there was no previous
   * analysis or if this instruction was not interpreted during the previous
   * analysis, then <code>null</code> is returned. 
   * @return state obtained from previous analysis.
   */
  public abstract State getPrevAnalysisBeginState();
  
  /**
   * Returns the state produced by the previous analysis (if there was a previous
   * analysis) after interpreting this instruction. If there was no previous
   * analysis or if this instruction was not interpreted during the previous 
   * analysis, then <code>null</code> is returned.
   * @return state obtained from previous analysis.
   */
  public abstract State getPrevAnalysisEndState();
  
  /**
   * Interprets this instruction using the given 
   * <code>InstructionInterpreter</code> and returns an array 
   * containing the addresses of the instruction(s) that should be 
   * interpreted next.  
   * 
   * @return  The next instructions to be interpretted along with a state to
   *          be used with each instruction.
   */
  public abstract Map<Address, State> interpret(InstructionInterpreter interpreter,
      State state, Address nextAddress);

  /**
   * Assigns the state obtained before interpreting this instruction.
   */
  public abstract void setBeginState(State state);

  /**
   * Assigns the state obtained after interpreting this instruction.
   */
  public void setEndState(State state);
  
  /**
   * Assigns the state obtained from the previous analysis before interpreting
   * this instruction.
   * @param state the state obtained from the previous analysis.
   */
  public void setPrevAnalysisBeginState(State state);
  
  /**
   * Assigns the state obtained from the previous analysis after interpreting
   * this instruction.
   * @param state the state obtained from the previous analysis.
   */
  public void setPrevAnalysisEndState(State state);

  /**
   * Specifies the program that contains this instruction. 
   */
  public void setProgram(Program program);

  /**
   * String representation
   */
  public abstract String toString();


}

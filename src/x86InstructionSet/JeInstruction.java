package x86InstructionSet;


public interface JeInstruction extends Instruction
{

  /**
   * Returns the target operand of this instruction.
   * @return the target operand.
   */
  public abstract Operand getTargetOperand();
}
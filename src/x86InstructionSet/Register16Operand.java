package x86InstructionSet;

import domain.Register;
import domain.Value;

public interface Register16Operand extends RegisterOperand
{
  /**
   * Returns an integer representing the register that this node holds.
   * See detectObfuscation.State for the possible register values.
   */
  public abstract Register getRegister();

  /**
   * Interprets this register operand.  I.e. returns the value that this
   * register holds.
   */
  public abstract Value interpret(domain.State state);

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public abstract String toString();

}
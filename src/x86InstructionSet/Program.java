/*
 * Created on Mar 7, 2004
 */

package x86InstructionSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import obfuscations.ObfuscationList;
import x86Interpreter.InstructionList;
import controlFlowGraph.ControlFlowGraph;

/**
 * Maintains a set of instructions along with other information relevent
 * to a program.  Each instruction has a state attached to it that is
 * populated by the Interpreter.  Instructions can be added to the program
 * in any order, however upon retrieval, the instructions will be sorted
 * based on address.  It is therefore important that no two instructions
 * share the same address. 
 */
public class Program 
{
	private ControlFlowGraph	controlFlowGraph	= new ControlFlowGraph();
	private Address				entryPoint 			= UnknownAddress.UNKNOWN;
	private InstructionList		instructions		= new InstructionList();
	private ObfuscationList		obfuscations 		= new ObfuscationList();
	private ArrayList<Address>  breakPoints			= new ArrayList();
	
  /**
   * Creates this.
   */
  public Program()
  {
    controlFlowGraph.program = this;
  }
  
	/**
	 * Adds an instruction to this program.
	 * No two instructions should share the same address.  Results are undefined
	 * if two instructions share the same address.
	 */
	public void addInstruction(Instruction instruction) 
	{
		instruction.setProgram(this);
		instructions.add(instruction);
	}
	
	/**
	 * Retrieve the control flow graph for this program.
	 * The CFG is initialy empty.  The client (or interpreter)
	 * is expected to populate it.
	 */
	public ControlFlowGraph getControlFlowGraph()
	{
		return controlFlowGraph;
	}
	
	/**
	 * Returns the address of the entry point insruction.
	 */
	public Address getEntryPoint()
	{
	    return entryPoint;
	}
	
	/**
	 * Returns the index of the instruction with the input address.
	 * Returns -1 if there is no instruction with the input address. 
	 */
	public int getInstructionIndex(Address address)
	{
		return instructions.getIndex(address);
	}
	
	/**
	 * Returns the specified instruciton.  
   * 
   * @param index specifies the index of the instruction to receive.
	 */
	public Instruction getInstruction(int index) {
		return instructions.get(index);
	}

	/**
	 * Returns the instruction with the input address.  Returns null if no
	 * instruction with the address is found.
	 */
	public Instruction getInstruction(Address address)
	{
		return instructions.get(address);
	}
	
	/**
	 * Returns the instruction directly following the input instruciton.
	 * Returns null if there is no next instruction.
	 */
	public Instruction getNextInstruction(Instruction instruction)
	{
	    return getNextInstruction(instruction.getAddress());
	}
	
	/**
	 * Returns the instruction directly following the instruction with the
	 * input address..
	 * Returns null if there is no next instruction.
	 */
	public Instruction getNextInstruction(Address address)
	{
		return instructions.getNext(address);
	}
	
	/**
	 * Returns the known set of obfuscations that are contained in this program.
	 */
	public ObfuscationList getObfuscations()
	{
	    return obfuscations;
	}
	
	/**
	 * Returns an iterator that can be used to traverse the set of instructions
	 * that make up this program.
	 */
	public Iterator<Instruction> iterator() 
	{ 
		return instructions.iterator(); 
	}

	/**
	 * Sets the entry point to this program.
	 */
	public void setEntryPoint(Address address)
	{
	    entryPoint = address;
	}

	/**
	 * Creates a new, empty CFG for this program.
	 */
	public void setNewControlFlowGraph()
	{
		controlFlowGraph = new ControlFlowGraph();
		controlFlowGraph.program = this;
	}
	
	/**
	 * Returns the size of the program.  Size is equal to the number of
	 * instructions in the program.
	 */
	public int size() 
	{
		return instructions.size();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
	    StringBuffer result = new StringBuffer();
	    Iterator<Instruction> iter;
	    Instruction  instruction;
	    
	    iter = iterator();
	    if(iter.hasNext())
	    {
	      instruction = iter.next();
	      result.append(instruction);
		    while(iter.hasNext())
		    {
		      instruction = iter.next();
		      result.append("\r\n" + instruction);
		    }
	    }
	    return result.toString();	        
	}
	
	/**
	 * Set a new break point
	 */
	public void setBreakPoint(Address address) 
	{
		// TODO Confirm that this sort works
		breakPoints.add(address);
		Collections.sort(breakPoints);
	}
	
	/**
	 * Get a break point
	 */
	public ArrayList<Address> getBreakPoints()
	{
	    return breakPoints;
	}

	public void removeBreakPoint(Address address) {
		breakPoints.remove(address);
	}
}

package x86InstructionSet;


/**
 * SAL/SAR/SHL/SHR--Shift
 * <p>
 * These instructions shift the bits in the first operand (destination operand) 
 * to the left or right by the number of bits specified in the second operand 
 * (count operand). Bits shifted beyond the destination operand boundary are 
 * first shifted into the CF flag, then discarded. At the end of the shift operation, 
 * the CF flag contains the last bit shifted out of the destination operand.  
 * <p>
 * The destination operand can be a register or a memory location. The count 
 * operand can be an immediate value or register CL. The count is masked to 
 * five bits, which limits the count range to 0 to 31. A special opcode 
 * encoding is provided for a count of 1.  
 * <p>
 * The shift arithmetic left (SAL) and shift logical left (SHL)instructions 
 * perform the same operation; they shift the bits in the destination
 * operand to the left (toward more significant bit locations).  For each shift count, 
 * the most significant bit of the destination operand is shifted into the 
 * CF flag, and the least significant bit is cleared. Refer to Figure 6-6 in 
 * Chapter 6, Instruction Set Summary of the Intel Architecture Software 
 * Developer�s Manual, Volume 1.
 * <p>
 * The shift arithmetic right (SAR) and shift logical right (SHR) instructions 
 * shift the bits of the destination operand to the right (toward less significant 
 * bit locations). For each shift count, the least significant bit of the 
 * destination operand is shifted into the CF flag, and the most significant 
 * bit is either set or cleared depending on the instruction type. The SHR 
 * instruction clears the most significant bit. For more information, 
 * refer to Figure 6-7 in Chapter 6, Instruction Set Summary of the Intel 
 * Architecture Software Developer�s Manual, Volume 1.  The SAR instruction 
 * sets or clears the most significant bit to correspond 
 * to the sign (most significant bit) of the original value in the destination 
 * operand. In effect, the SAR instruction fills the empty bit position�s shifted
 * value with the sign of the unshifted value. For more information, refer to 
 * Figure 6-8 in Chapter 6, Instruction Set Summary of the Intel Architecture 
 * Software Developer�s Manual, Volume 1.
 * <p>
 * The SAR and SHR instructions can be used to perform signed or 
 * unsigned division, respectively, of the destination operand by powers 
 * of 2. For example, using the SAR instruction to shift a signed integer one bit 
 * to the right divides the value by 2.  
 * <p>
 * Using the SAR instruction to perform a division operation does not 
 * produce the same result as the IDIV instruction. The quotient from the 
 * IDIV instruction is rounded toward zero, whereas the �quotient� of the SAR 
 * instruction is rounded toward negative infinity. This difference is
 * apparent only for negative numbers. For example, when the IDIV instruction 
 * is used to divide -9 by 4, the result is -2 with a remainder of -1. 
 * If the SAR instruction is used to shift -9 right by two bits, the result 
 * is -3 and the �remainder� is +3; however, the SAR instruction stores only the
 * most significant bit of the remainder (in the CF flag).
 * <p>
 * The OF flag is affected only on 1-bit shifts. For left shifts, the OF flag 
 * is cleared to 0 if the mostsignificant bit of the result is the same as 
 * the CF flag (that is, the top two bits of the original operand were the same); 
 * otherwise, it is set to 1. For the SAR instruction, the OF flag is cleared 
 * for all 1-bit shifts. For the SHR instruction, the OF flag is set to the 
 * most-significant bit of the original operand.
 * 
 * <p>
 * Operation<br>
 * <code>
 * tempCOUNT <-- (COUNT AND 1FH);<br>
 * tempDEST <-- DEST;<br>
 * WHILE (tempCOUNT != 0)<br>
 * DO<br>
 * &nbsp; IF instruction is SAL or SHL<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; CF <-- MSB(DEST);<br>
 * &nbsp; ELSE (* instruction is SAR or SHR *)<br>
 * &nbsp; &nbsp; CF <-- LSB(DEST);<br>
 * &nbsp; FI;<br>
 * &nbsp; IF instruction is SAL or SHL<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; DEST <-- DEST * 2;<br>
 * &nbsp; ELSE<br>
 * &nbsp; &nbsp; IF instruction is SAR<br>
 * &nbsp; &nbsp; THEN<br>
 * &nbsp; &nbsp; &nbsp; DEST <-- DEST / 2 (*Signed divide, rounding toward negative infinity*);<br>
 * &nbsp; &nbsp; ELSE (* instruction is SHR *)<br>
 * &nbsp; &nbsp; &nbsp; DEST <-- DEST / 2 ; (* Unsigned divide *);<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; FI;<br>
 * &nbsp; tempCOUNT <--tempCOUNT � 1;<br>
 * OD;<br>
 * (* Determine overflow for the various instructions *)<br>
 * IF COUNT = 1<br>
 * THEN<br>
 * &nbsp; IF instruction is SAL or SHL<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; OF <-- MSB(DEST) XOR CF;<br>
 * &nbsp; ELSE<br>
 * &nbsp; &nbsp; IF instruction is SAR<br>
 * &nbsp; &nbsp; THEN<br>
 * &nbsp; &nbsp; &nbsp; OF <-- 0;<br>
 * &nbsp; &nbsp; ELSE (* instruction is SHR *)<br>
 * &nbsp; &nbsp; &nbsp; OF <-- MSB(tempDEST);<br>
 * &nbsp; &nbsp; FI;<br>
 * &nbsp; FI;<br>
 * ELSE <br>
 * &nbsp; IF COUNT = 0<br>
 * &nbsp; THEN<br>
 * &nbsp; &nbsp; All flags remain unchanged;<br>
 * &nbsp; ELSE (* COUNT neither 1 or 0 *)<br>
 * &nbsp; &nbsp; OF <-- undefined;<br>
 * &nbsp; FI;<br>
 * FI;<br>
 * </code>
 * 
 * <p>
 * Flags Affected<br>
 * The CF flag contains the value of the last bit shifted out of the 
 * destination operand; it is undefined for SHL and SHR instructions 
 * where the count is greater than or equal to the size (in bits)
 * of the destination operand. The OF flag is affected only for 1-bit 
 * shifts (refer to �Description� above); otherwise, it is undefined. 
 * The SF, ZF, and PF flags are set according to the result. If the 
 * count is 0, the flags are not affected. For a non-zero count, 
 * the AF flag is undefined.
 */
public interface SarInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();

  /**
   * Returns the source operand.
   * @return the source operand.
   */
  public abstract Operand getSrcOperand();
}
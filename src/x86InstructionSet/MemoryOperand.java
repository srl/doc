package x86InstructionSet;

import domain.BitCount;
import domain.Register;
import domain.State;
import domain.Value;

public interface MemoryOperand extends Operand
{
  public abstract Value interpret(State state);

  /**
   * Returns the size in bits of this register operand.
   * @return the size in bits.
   */
  public BitCount getBitSize();

  /**
   * Returns the operand that holds the memory address, 
   * excluding the segment register
   * @return the operand.
   * @see MemoryOperand#getSegmentRegister()
   */
  public abstract Operand getOperand();

  /**
   * Returns the segment register for this memory operand. 
   * The segement register is an integer and corresponds to
   * the register constants located in the <code>State</code>
   * class.
   * @return the segment regsiter.
   */
  public abstract Register getSegmentRegister();

  /**
   * Returns the address placed in this operand, but doesn't
   * dereference the address.  In other words, if this operand is the target
   * of a MOV instruction, then the value returned is the address to be
   * MOVed to.
   */
  public abstract domain.Value getTarget(domain.State state);

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public abstract String toString();
}

package x86InstructionSet;


public interface JgeInstruction extends Instruction
{
  /**
   * Returns the target operand of this instruction.
   * @return the target operand.
   */
  public abstract Operand getTargetOperand();
}
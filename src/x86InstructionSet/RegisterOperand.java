/*
 * RegisterOperand.java
 * Created on May 24, 2005
 */
package x86InstructionSet;

import domain.BitCount;
import domain.Register;


/**
 * Interface for nodes in the syntax tree that are intended
 * to hold register operands.
 */
public interface RegisterOperand extends Operand
{
  /**
   * Returns the size in bits of this register operand.
   * @return the size in bits.
   */
  public BitCount getBitSize();

  /**
   * Returns an integer representing the register that this node holds.
   * See detectObfuscation.State for the possible register values.
   */
  public Register getRegister();
}

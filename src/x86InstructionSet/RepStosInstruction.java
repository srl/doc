package x86InstructionSet;


public interface RepStosInstruction extends Instruction
{
  /**
   * Returns the destination oeprand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();
}
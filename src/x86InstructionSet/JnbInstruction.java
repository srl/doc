package x86InstructionSet;


public interface JnbInstruction extends Instruction
{
  /**
   * Returns the target operand of this jump instruction.
   * @return the target operand.
   */
  public abstract Operand getTargetOperand();
}
package x86InstructionSet;


/**
 * Set if Less / Set if Not Greater or Equal (386+)
 * As of now, it just sets the operand to UNDEFINED.
 */
public interface SetlSetngeInstruction extends Instruction
{
  /**
   * Returns the destination operand.
   * @return the destination operand.
   */
  public abstract Operand getDstOperand();
}
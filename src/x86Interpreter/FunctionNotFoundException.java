/*
 * FunctionNotFoundException.java
 * Created on May 21, 2005
 */
package x86Interpreter;

/**
 * A specified function could not be found.  Intended for
 * use by ExportTable and ExportedFunctionDescription
 */
public class FunctionNotFoundException extends Exception
{
    public FunctionNotFoundException()
    {
        super();
    }

    public FunctionNotFoundException(String message)
    {
        super(message);
    }
}

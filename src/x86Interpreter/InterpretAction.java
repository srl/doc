package x86Interpreter;

/**
 * Defines the possible actions an <code>Interpreter</code> can
 * take when an instruction is encountered.
 */
public enum InterpretAction
{
  DO_NOT_INTERPRET,
  INTERPRET,
  MERGE_AND_INTERPRET,
  WIDEN_AND_INTERPRET
}

package x86Interpreter;

import java.util.Map;

import x86InstructionSet.Address;
import x86InstructionSet.Instruction;
import x86InstructionSet.Program;
import controlFlowGraph.ControlFlowGraph;
import domain.State;

/**
 * Defines the interfaces for a worklist.  The job of the
 * worklist is to decide which instructions should be
 * interpreted and in which order. Different worklists
 * can be created to handle different analysis 
 * situations, such as backward or forward analysis.
 */
public interface Worklist
{
  /**
   * Adds an element to this worklist.
   */
  public void add(WorkListElement element);

  /**
   * Searches this worklist for an element with the input address.  If found,
   * the element is returned.  Otherwise, null is returned.
   */
  public WorkListElement find(Address address, State state);

  /**
   * Returns an element from this list.  The element is
   * also removed from this list.
   */
  public WorkListElement getNext(NodeDependencyTable dependencyTable,
      ControlFlowGraph cfg);

  /**
   * Returns true if this worklist has an element with the input address.
   */
  public boolean has(Address address, State state);

  /**
   * Returns true if this worklist contains no elements.
   */
  public boolean isEmpty();

  /**
   * Initializes this worklist, using data obtained from the input program.
   *  
   * @param program             the program used to initialize the worklist.
   * @param initialState        the initial state used when interpreting 
   *                            the first instruction.
   * @param initialInstruction  address of the first instruction that 
   *                            should be interpreted.  
   */
  public void init(Program program, State initialState, Address initialInstruction);
  
  /**
   * Removes the worklist element with the input address.  If no such
   * element exists, returns false.  Otherwise, returns true.
   */
  public boolean remove(Address address, State state);

  /**
   * Number of items in this worklist. 
   * 
   * @return size of worklist.
   */
  public int size();
  
  /**
   * Updates this worklist by adding new instructions. 
   * 
   * @param program           the program being interpretted.
   * @param lastInstruction   the last instruction to be interpretted. 
   *                          In other words, this instruction has just 
   *                          been interpreted.  This function should 
   *                          add the next instruction to be 
   *                          interpreted to the worklist.
   * @param next              Map of instructions to be interpreted next along
   *                          with the incoming state to be used with each instruction.
   */
  public void update(Program program, Instruction lastInstruction, Map<Address, State> next); 
}
/*
 * Interpreter.java
 * Created on May 21, 2005
 */
package x86Interpreter;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Map.Entry;

import kCallContextAnalysisDomain.KCallContext;

import lCallContextAnalysisDomain.LCallContext;
import lStackContextAnalysisDomain.LStackContext;
import logger.Log;
import logger.Trace;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;


import ui.actions.RunAnalysisAction;
import ui.controlFlow.ControlFlowGraphView;
import ui.programViewer.AsmFileDocumentProvider;
import ui.programViewer.InstructionSelection;
import ui.programViewer.ProgramViewer;
import vsaAsgDomain.VsaAsgState;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.Instruction;
import x86InstructionSet.Program;
import x86InstructionSet.UnknownAddress;
import analyses.Analysis;
import controlFlowGraph.ControlFlowGraph;
import controlFlowGraph.ControlFlowGraphNode;
import csAnalysisDomain.csAnalysisDomain;
import dependencyTable.DependencyTable;
import dependencyTable.Item;
import detectObfuscation.Strings;
import domain.State;

/**
 * Interprets an x86 program.
 */
public class Interpreter implements IRunnableWithProgress 
{
	private static Analysis         	analysis = null;
	private static ControlFlowGraph		activeControlFlowGraph;
	private static Instruction 			activeInstruction;
	private static State				activeState;
	private static ExportTableSet		imports = new ExportTableSet();
	
	
  /** Tracks instructions that have been interpreted. */
	private InstructionHistoryList history;
  
    private int numberInstruction = 0;
	private State 						initialState; 
	private Program 					program; 
	private Instruction 				start; 
	private Instruction     			stop;
	
	
	 /**
     * The mapping for Addresses to stack-strings for the Context-Sensitive analysis.    
     */
	 static  Map<String, ArrayList> mapping = new HashMap<String, ArrayList>();



  /**
   * Creates a new interpreter.
   * @param program the program to be interpreted.
   * @param analysis the analysis to use during the interpretation. The analysis
   *          specifies all the interpretation properties.
   * @param start the first instruction to be interpreted.  If <code>null</code>, 
   *          the entry point is used.  The <code>Worklist</code> specified by
   *          the worklist may override this value.
   * @param stop the last instruction that should be interpreted.  When the interpreter
   *          encounters this instruction, the instruction will be interpreted, but
   *          the following instruction(s) will not be added to the worklist.
   *          If <code>null</code>, then the Interpreter will keep interpreting until
   *          nothing more needs to be interpreted.
   * @param initialState the state used for the initial begin state of the first 
   *          interpreted instruction.
   * @param imports a set of export tables that define the addresses of functions
   *          exported by other files, such as DLLs.  It is expected that, among
   *          other uses, this will be used to specify the addresses of system
   *          functions.
   */
	public Interpreter(
	    Program                program,
	    Analysis               analysis,
	    Instruction            start,
	    Instruction            stop,
	    State                  initialState,
	    ExportTableSet         imports)
	{
		this.program 			= program;
		Interpreter.analysis    = analysis;
		this.start   			= start;
		this.stop               = stop;
		this.initialState 		= initialState;
		this.history            = analysis.getInstructionHistoryList();
		Interpreter.imports		= imports;
		mapping.clear();
    
    if(this.start == null)
      this.start = program.getInstruction(program.getEntryPoint());
		
    if(start == UnknownAddress.UNKNOWN)
      start = program.getInstruction(0);
    
		setActiveState(initialState);
	}
	
  /**
   * Builds a preliminary control-flow graph.  The flow graph created is
   * incomplete, because indirect transfers are not taken into account.
   * This function is intended to provide an initial CFG that can be used
   * for further analysis.  The caller can populate the input cfg after
   * calling this function to fill in gaps that were missed by this function.
   * The caller can call this function after finding an indirect transfer, and
   * this function will build a preliminary CFG starting from the indirect
   * transfer.  Using this approach, this function and the caller can work
   * in tandem to build a complete, full control-flow graph.  
   * 
   * @param start Node to build CFG from.  This node should already exist in the CFG.
   */
  private void buildFullCfg(Program program, ControlFlowGraph cfg, DependencyTable dependencies)
  {
    Instruction currentInstruction; // current instruction from worklist.
    Iterator<Address> iter;
    Instruction nextInSequence;     // next instruction in actual program.
    AddressList nextInstructions;   // next instructions in the CFG
    Queue<Instruction>  worklist = new LinkedList<Instruction>();
    List<Instruction>   visited = new LinkedList<Instruction>();
    
    Iterator instructionIter = program.iterator();
    while(instructionIter.hasNext())
      worklist.add((Instruction)instructionIter.next());
    
    while(!worklist.isEmpty())
    {
      currentInstruction = (Instruction)worklist.remove();
      
      if(visited.contains(currentInstruction))
        continue;
      visited.add(currentInstruction);
      
      if(!cfg.hasNode(currentInstruction))
        cfg.addNode(currentInstruction);
      
      // get the next instructions in cfg from the current instruction.
      nextInSequence = program.getNextInstruction(currentInstruction);
      nextInstructions = currentInstruction.getNextQuickly(
          nextInSequence != null ? nextInSequence.getAddress() : UnknownAddress.UNKNOWN );
      
      // TODO: Temporary code to test feasibility of priorities in dependency table
      // each successor instruction of a CALL is set to a lower priority.
      
      if(currentInstruction instanceof CallInstruction)
      {
        if(nextInSequence != null)
        {
          if(!cfg.hasNode(nextInSequence))
          {
            cfg.addNode(nextInSequence);
            worklist.add(nextInSequence);
          }
          ControlFlowGraphNode node = program.getControlFlowGraph().getNode(nextInSequence);
  
          Item item = dependencies.find(node);
          if(item!=null)
            item.decreasePriority();
        }
      }

      // add edges from current instruction to the next instructions in the cfg.
      // also add next instructions in cfg to worklist.
      iter = nextInstructions.iterator();
      while(iter.hasNext())
      {
        Instruction next = program.getInstruction(iter.next());
        if(next != null)
        {
          if(!cfg.hasNode(next))
            cfg.addNode(next);
          if(!cfg.hasEdge(currentInstruction, next))
          {
            cfg.addEdge(currentInstruction, next);
//          worklist.add(next);
          }
        }
      }
    }
  }
  
	/**
	 * Builds a preliminary control-flow graph.  The flow graph created is
	 * incomplete, because indirect transfers are not taken into account.
	 * This function is intended to provide an initial CFG that can be used
	 * for further analysis.  The caller can populate the input cfg after
	 * calling this function to fill in gaps that were missed by this function.
	 * The caller can call this function after finding an indirect transfer, and
	 * this function will build a preliminary CFG starting from the indirect
	 * transfer.  Using this approach, this function and the caller can work
	 * in tandem to build a complete, full control-flow graph.  
	 * 
	 * @param start Node to build CFG from.  This node should already exist in the CGF.
	 */
	private void buildPartialCfg(Program program, Instruction start, ControlFlowGraph cfg, 
      DependencyTable dependencies)
	{
		Instruction currentInstruction;	// current instruction from worklist.
		Iterator<Address> iter;
		Instruction nextInSequence;			// next instruction in actual program.
		AddressList nextInstructions; 	// next instructions in the CFG
		Queue<Instruction>	worklist = new LinkedList<Instruction>();

    worklist.add(start);
    
		while(!worklist.isEmpty())
		{
			currentInstruction = (Instruction)worklist.remove();
      
			// get the next instructions in cfg from the current instruction.
			nextInSequence = program.getNextInstruction(currentInstruction);
			nextInstructions = currentInstruction.getNextQuickly(
					nextInSequence != null ? nextInSequence.getAddress() : UnknownAddress.UNKNOWN );
			
			// TODO: Temporary code to test feasibility of priorities in dependency table
			// each successor instruction of a CALL is set to a lower priority.
			
			if(currentInstruction instanceof CallInstruction)
			{
				if(nextInSequence != null)
				{
					if(!cfg.hasNode(nextInSequence))
					{
						cfg.addNode(nextInSequence);
						worklist.add(nextInSequence);
					}
					ControlFlowGraphNode node = program.getControlFlowGraph().getNode(nextInSequence);
	
					Item item = dependencies.find(node);
					if(item!=null)
						item.decreasePriority();
				}
			}

			// add edges from current instruction to the next instructions in the cfg.
			// also add next instructions in cfg to worklist.
			iter = nextInstructions.iterator();
			while(iter.hasNext())
			{
				Instruction next = program.getInstruction(iter.next());
				if(next != null)
				{
					if(!cfg.hasNode(next))
						cfg.addNode(next);
					if(!cfg.hasEdge(currentInstruction, next))
					{
						cfg.addEdge(currentInstruction, next);
		//				worklist.add(next);
					}
				}
			}
		}
	}

  /**
   * Updates the begin <code>State</code> of the given 
   * <code>Instruction</code>, interprets the <code>Instruction</code>,
   * then updates the end <code>State</code> with the 
   * resulting <code>State</code> after performing the interpretation.
   * Returns the resulting <code>State</code>. Also updates the parameter
   * <code>nextInstructions</code> so that after execution of this function,
   * it will contain the <code>Address</code>es of the 
   * <code>Instruction</code>s that should be interpreted next.
   *  
   * @param instruction       the <code>Instruction</code> to be interpreted.
   * @param beginState        the begin <code>State</code>. 
   * @param nextAddress       the <code>Address</code> of the 
   *                          <code>Instruction</code> directly following this 
   *                          one in the program layout (not necessary in the 
   *                          program flow graph).
   * @param nextInstructions  upon exit of this method, will contain
   *                          a list of <code>Address</code>es of the 
   *                          <code>Instruction</code>s that should be 
   *                          interpreted after this <code>Instruction</code>.
   * @return  the address of instructions to be interpreted next, along with the 
   *          state to be used for each instruction.
   */
  public Map<Address, State> interpret(
      Instruction instruction,
      State beginState, 
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
        
    instruction.setBeginState(beginState);
    State beginStateClone = (State)beginState.clone();
    next = instruction.interpret(analysis.getInstructionInterpreter(), beginStateClone, nextAddress);
    instruction.setEndState(beginStateClone);
    
    if (beginState instanceof csAnalysisDomain)
    	updateMapping(instruction, beginState);

    return next;
  }
  
	/**
	 * Interprets the input program.  Returns the same program with state
	 * information attached to each instruction.
	 * @throws NoEntryPointException
	 * @throws IOException 
	 */
	public Program interpret(IProgressMonitor monitor) throws NoEntryPointException, InterruptedException, IOException
	{
		final String TASK_TITLE = "Performing " + analysis.getTitle();
		final int TASK_SIZE = IProgressMonitor.UNKNOWN;
    
		Analysis analysis = getActiveAnalysis();
		
		WorkListElement		currentElement;
		NodeDependencyTable dependencies 		=	new NodeDependencyTable();
		AddressList			nextInstructions 	=	new AddressList();
		State             	state     			=	initialState;
		Worklist 			worklist			=	analysis.createWorklist();
		
		monitor.beginTask(TASK_TITLE, TASK_SIZE);
		
		program.getControlFlowGraph().addObserver(dependencies);
		
		// prepare interpreter
		Interpreter.setImports(imports);
		
		createInitialCfg(dependencies);
		
		long startTime = System.currentTimeMillis();
		Log.write(Log.INFO, TASK_TITLE);
		Log.write(Log.INFO, "Interpretation begins");
		
		int countSteps = 0;
		
    worklist.init(program, state, start.getAddress());
		while(!worklist.isEmpty())
		{
			if(monitor.isCanceled())
				throw new InterruptedException();
			
			currentElement = worklist.getNext(dependencies, program.getControlFlowGraph());
			processWorklistElement(currentElement, worklist, dependencies, nextInstructions);
			Address currentAddress = currentElement.getInstruction().getAddress();
			countSteps ++;
			
			//This is how stepping stops interpretation.  It is normal
			//for this InterruptedException to be thrown if using stepping.
			if(countSteps == AsmFileDocumentProvider.stepNumber)
			{
				AsmFileDocumentProvider.stepNumber = countSteps;
				AsmFileDocumentProvider.currentInstruction = currentAddress;
				Instruction currInstruction = currentElement.getInstruction();
				InstructionSelection iSelect = new InstructionSelection(currInstruction);
				AsmFileDocumentProvider.currentSelection = iSelect;
				//throw new InterruptedException();
				break;
			}
			
			//This is how break points stop interpretation.  It is normal
			//for this InterruptedException to be thrown when using break points.
			Boolean isBreak = false;
			for(int i=0;i<program.getBreakPoints().size();i++)
			{
				if(currentAddress==program.getBreakPoints().get(i))
				{
					AsmFileDocumentProvider.stepNumber = countSteps;
					program.getBreakPoints().remove(i);
					AsmFileDocumentProvider.currentInstruction = currentAddress;
					Instruction currInstruction = currentElement.getInstruction();
					InstructionSelection iSelect = new InstructionSelection(currInstruction);
					AsmFileDocumentProvider.currentSelection = iSelect;
					//throw new InterruptedException();
					isBreak = true;
				}
			}
			if(isBreak)
				break;
		}
	
		
    Log.write(Log.INFO, "Interpretation ends");
    long endTime = System.currentTimeMillis();
    Log.write(Log.INFO, "Time(ms): "+ Long.toString(endTime-startTime));
	Log.write(Log.INFO, "# Interpreted instructions: " + Integer.toString(numberInstruction));
	
	/**
	 * Collecting data #CS = number of different call-strings, #CSPN = maximum number
	 *  of call strings for any node and MaxLength
	 */
	
	  Entry<String, ArrayList> entryContext;
	  Iterator<Entry<String, ArrayList>> iterContext;
	  ArrayList<String> list;
	  String string;
	  
	  ArrayList<String> listCS = new ArrayList<String>();
	  
	  int numberCallStrings = 0; //#CS
	  int maximum = 0;           //#CSPN
	  int maxLength = 0;		 //MaxLength
	 
	  
	  iterContext = getMappingAddress().entrySet().iterator();
	  
	  while(iterContext.hasNext()){
		  
		  entryContext = iterContext.next();
		  list = (ArrayList<String>) entryContext.getValue();
		  
		  if (listCS.size() == 0)
			  listCS.addAll(list);
		  else {
			  Iterator<String> iterList = list.iterator();
			  while (iterList.hasNext()){
				  String contextString = iterList.next();
				  if (listCS.contains(contextString)) continue;
				  else listCS.add(contextString);
				 				  
			  }
			  
		  }
			  
		  numberCallStrings = listCS.size();
		
		  if (list.size() > maximum)
			  maximum = list.size();
		  
		  for (int k = 0; k < list.size(); k++){
			 string = list.get(k).toString();
			 int count = 0;
			 
			 for (int i = 0; i < string.length(); i++ ){
				    
					char ch = string.charAt(i);
					if ( ch == '-' ) count++;
					
			 }
			 
			 if (count > maxLength)
				 maxLength = count;
		  }
		  
	  }
	
	  Log.write(Log.INFO, "#CS: " + numberCallStrings);
	  Log.write(Log.INFO, "#CSPN: " + maximum);
	  Log.write(Log.INFO, "MaxLength: " + maxLength);
	  
	  //I need to output #CS:, #CSPN, MaxLength, interpreted instructions, and time
	  BufferedWriter out = new BufferedWriter(new FileWriter("results.txt", true));
	  out.write(analysis.getTitle()+",");
	  out.write(numberCallStrings+",");
	  out.write(maximum+",");
	  out.write(maxLength+",");
	  out.write(Integer.toString(numberInstruction)+",");
	  out.write(Long.toString(endTime-startTime)+"\r\n");
	  out.close();
	    
	  monitor.done();
		
	  return program;
	}

  /**
   * Creates an initial CFG.  Call be begining interpretation.
   * @param dependencies Dependency table associated with the cfg.
   */
  private void createInitialCfg(NodeDependencyTable dependencies)
  {
	  program.getControlFlowGraph().addNode(program.getInstruction(start.getAddress()));
	  program.getControlFlowGraph().setEntryPoint(start.getAddress());
	  buildFullCfg(program, program.getControlFlowGraph(), dependencies);
  }

  /**
   * Extracts an <code>Instruction</code> from the given 
   * <code>WorkListElement</code> and interprets the <code>Instruction</code>
   * 
   * @param element           element to process
   * @param worklist          the current worklist.
   * @param dependencies      the <code>NodeDependencyTable</code>
   *                          associated with the worklist.
   * @param nextInstructions  upon exit, will contain <code>Address</code>es
   *                          of <code>Instruction</code>s to interpret next.
   */
  private void processWorklistElement(
      WorkListElement element, 
      Worklist worklist, 
      NodeDependencyTable dependencies, 
      AddressList nextInstructions) throws Error
  {
    Instruction         currentInstruction;
    Map<Address, State> next;
    Address             nextAddress;
    Instruction         nextInstruction;
    State               state;
    
    currentInstruction 	= element.getInstruction();
    
    // make sure we found the instruction with that address.
    if(currentInstruction != null)	
    {
    	nextInstruction	= program.getNextInstruction(currentInstruction);
    	nextAddress 	= (nextInstruction == null) ? UnknownAddress.UNKNOWN : nextInstruction.getAddress();
    	state 			= element.getState();
    									
    	setActiveControlFlowGraph(program.getControlFlowGraph());
    	setActiveInstruction(currentInstruction);
    	setActiveState(state);

      next = interpretInstruction(currentInstruction, state, nextAddress);

      updateCfgWithNewInfo(currentInstruction, next, dependencies);

      // HACK!: Only update the worklist if the current instruction
      // is not the last instruction in the valid instructions list.
      // make sure current instruction is not the last instruction to interpret.
      if(stop == null || stop != currentInstruction) 
    	  updateWorklist(worklist, currentInstruction, next);
           
      }
      else
      {
      Log.write(Log.INFO, "interpret() - instruction " + 
          element.getInstruction().getAddress() + " does not exist.");
    }
  }

  /**
   * Interprets the given <code>Instruction</code> using the given
   * <code>State</code>.
   * 
   * @param instruction   Instruction to interpret.
   * @param state         Incoming state.
   * @param nextAddress   The address of the instruction immediately following
   *                      this instruction.
   * @return              Map containing next instructions to interpret along with
   *                      incoming state.
   */
  private Map<Address, State> interpretInstruction(
      Instruction instruction, 
      State state, 
      Address nextAddress)
  {
	  // *** trace **
	Trace.marker(instruction);
	
    Map<Address, State> next;
    InterpretAction action = whatToDo(instruction, state);
    String action_type = "Undefined";
    switch(action)
    {
    case DO_NOT_INTERPRET:
    	 
      next = new HashMap<Address, State>();
      break;
      
    case INTERPRET:
    	action_type = "INTERPRET";
        //Log.write(Log.INFO, "Interpreting " + instruction + " in " + ((Context) state).getStackString());
    	numberInstruction++; 
    	//Log.write(Log.INFO, Integer.toString(numberInstruction) + "  Interpreting " + instruction);
        next = interpret(instruction, state, nextAddress);
       	break;
      
    case MERGE_AND_INTERPRET:
    	action_type = "MERGE_AND_INTERPRET";
        //Log.write(Log.INFO, "Performing a merge and interpreting " + instruction + " in " + ((Context) state).getStackString());
    	numberInstruction++;   
    	//Log.write(Log.INFO, Integer.toString(numberInstruction) + "  Performing a merge and interpreting " + instruction);
      	next = mergeAndInterpret(instruction, state, nextAddress);
       	break;
      
    case WIDEN_AND_INTERPRET:
    	action_type = "WIDEN_AND_INTERPRET";
        //Log.write(Log.INFO, "Performing a widen and Interpreting " + instruction + " in " + ((Context) state).getStackString());
      	numberInstruction++; 
        //Log.write(Log.INFO, Integer.toString(numberInstruction) + "  Performing a widen and Interpreting " + instruction);
       	next = widenAndInterpret(instruction, state, nextAddress);
       	break;
      
    default:
      Log.write(Log.ERROR, "Unhandled Interpret Action.");
      throw new Error("Unhandled Interpret Action.");
    }
    
    Trace.analysis(action_type, instruction, next);
    
    history.interpreted(instruction);
    return next;
  }

  /**
   * Updates the worklist with the instructions to be interpreted next.
   * 
   * @param worklist            The worklist to be updated.
   * @param currentInstruction  The instruction that has just been interpreted.
   * @param next                Mapping of next instructions to interpret along
   *                            with the incoming state to be used when the instruction
   *                            is interpreted.  A list of the next instructions 
   *                            to be interprted, as provided by the currentInstruction. 
   */
  private void updateWorklist(Worklist worklist, Instruction currentInstruction, 
      Map<Address, State> next)
  {
    // XXX: This hack is placed here because the
    // forward analysis update function must only
    // be called when the interpretted instruction
    // says we should continue (nextInstructions.size() > 0)
    // The backward analysis on the other hand must
    // always be called.  A uniform method is needed
    // to handle this situation.
//	  Map<Address, State> nextInSequenceMap = new HashMap<Address, State>();
	  
    if(worklist instanceof ForwardAnalysisWorklist)
    {
      if(next.size() > 0){
        worklist.update(program, currentInstruction, next);
     /*   if (currentInstruction instanceof CallInstruction) {
        	Instruction nextInSequence = program.getNextInstruction(currentInstruction);
        	nextInSequenceMap.put(nextInSequence.getAddress(), currentInstruction.getEndState());
        	worklist.update(program, currentInstruction, nextInSequenceMap);
        }*/
      }
    }
    else
      worklist.update(program, currentInstruction, next);
  }

  /**
   * Returns the analysis currently being used.
   */
  public static Analysis getActiveAnalysis()
  {
    return analysis;
  }
  
	/**
	 * Returns the control flow graph that can be used to assist
	 * in interpreting the current instruction.  
	 */
	public static ControlFlowGraph getActiveControlFlowGraph()
	{
		return activeControlFlowGraph;
	}
	
	/**
	 * Returns the instruction currently being interpreted.  The interpreter
	 * is expected to call setActiveInsutrction before interpreting the
	 * instruction.  
	 */
	public static Instruction getActiveInstruction()
	{
		return activeInstruction;
	}
	
	/**
	 * Returns the currently active state.  See setActiveState for more info.
	 */
	public static State getActiveState()
	{
		return activeState;
	}
	
	/**
	 * Returns the imported functions for use with the program under
	 * interpretation.
	 */
	public static ExportTableSet getImports()
	{
		return imports;
	}

  /**
   * Returns a variable that defines how an <code>Instruction</code>
   * should be interpreted.  The decision is made based on the input
   * parameters that define various properties of the 
   * <code>Instruction</code>.
   * @param isLoopJunctionNode true if the <code>Instruction</code> 
   *      is a loop junction node.
   * @param isSimpleJunctionNode true if the <code>Instruction</code> 
   *      is a simple junction node.
   * @param isVisited true if this <code>Instruction</code> 
   *      has been interpreted before.
   * @param oldStateOverapproximatesNewState true if the original 
   *      <code>State</code> given to the <code>Instruction</code> 
   *      overapproximates the new incoming <code>State</code>.
   * @return an <code>InterpretationAction</code> object that defines
   *      how an <code>Instruction</code> with the given properties
   *      should be interpreted.
   */
  private InterpretAction howToInterpretInstruction(
      boolean isLoopJunctionNode, 
      boolean isSimpleJunctionNode, 
      boolean isVisited, 
      boolean oldStateOverapproximatesNewState)
  {
    InterpretAction result;
    
    if (!isVisited)  {
    	// first visit, always interpret
    	 result = InterpretAction.INTERPRET;
    } else if (oldStateOverapproximatesNewState) {
    	// subsequent visit, old state is a bigger approximation
    	// do not interpret again
    	 result = InterpretAction.DO_NOT_INTERPRET;
    } else if (isSimpleJunctionNode) {
    	// new state is a bigger approximation, and the node
    	// is a simple junction node, join the two states
    	 result = InterpretAction.MERGE_AND_INTERPRET;
    } else if (isLoopJunctionNode) {
    	// the node is a loop junction node
    	// widen the old state, before interpreting
   	     result = InterpretAction.WIDEN_AND_INTERPRET;
   	} else {
   		// it's possible something is neither SimpleJunctionNode nor LoopJunctionNode
   		// if its visited again, we just interpret it.
   		result = InterpretAction.INTERPRET;
   	}
    return result;
    /*  DELETE THIS PART ON FUTURE VISIT
    if(isVisited && isSimpleJunctionNode && !oldStateOverapproximatesNewState)
      result = InterpretAction.MERGE_AND_INTERPRET;

    else if(isVisited && isSimpleJunctionNode && oldStateOverapproximatesNewState)
      result = InterpretAction.DO_NOT_INTERPRET;

    else if(isVisited && isLoopJunctionNode && !oldStateOverapproximatesNewState)
      result = InterpretAction.WIDEN_AND_INTERPRET;

    else if(isVisited && isLoopJunctionNode && oldStateOverapproximatesNewState)
      result = InterpretAction.DO_NOT_INTERPRET;
    
    else if (this.initialState instanceof KCallContext && isVisited && oldStateOverapproximatesNewState) 
      result = InterpretAction.DO_NOT_INTERPRET;
    
    else if (this.initialState instanceof Context && isVisited && oldStateOverapproximatesNewState) 
        result = InterpretAction.DO_NOT_INTERPRET;
   
    else if (this.initialState instanceof ContextCall && isVisited && oldStateOverapproximatesNewState) 
        result = InterpretAction.DO_NOT_INTERPRET;
    
    else
      result = InterpretAction.INTERPRET;
    
    return result;
    /**/
  }
  
	/**
	 * Returns true if this instruction has been previously visited.
   * The InstructionHistoryList determines if an instruction has
   * been visited.  Thus, each different Analysis can have its own
   * method of determining if an instruction has been visited.
	 */
	public boolean isVisited(Instruction instruction, State state)
	{
    return history.isInterpreted(instruction, state);
	}

  /**
   * Updates the begin <code>State</code> of the given 
   * <code>Instruction</code> by merging it with the new begin state
   * specified by the parameter <code>beginState</code>; 
   * interprets the <code>Instruction</code>;
   * then updates the end <code>State</code> with the 
   * resulting <code>State</code> after performing the interpretation.
   * Returns the resulting <code>State</code>. Also updates the parameter
   * <code>nextInstructions</code> so that after execution of this function,
   * it will contain the <code>Address</code>es of the 
   * <code>Instruction</code>s that should be interpreted next.
   *  
   * @param instruction the <code>Instruction</code> to be interpreted.
   * @param beginState  the begin <code>State</code>. 
   * @param nextAddress the <code>Address</code> of the 
   *                    <code>Instruction</code> directly following this 
   *                    one in the program layout (not necessary in the 
   *                    program flow graph).
   * @return            Map containing instructions to interpret next along
   *                    with incoming state.
   */
  public Map<Address, State> mergeAndInterpret(
      Instruction instruction,
      State beginState, 
      Address nextAddress)
  {
	  
	  if (beginState instanceof csAnalysisDomain){

	    	String stackString;
	    	stackString = ((csAnalysisDomain)beginState).getContextString();
	    	       	
	    	VsaAsgState vsaState = (VsaAsgState)  ((csAnalysisDomain)instruction.getBeginState()).get(stackString);
	    	
	    	VsaAsgState vsaState2 = (VsaAsgState) ((csAnalysisDomain) beginState).get(stackString);
	    	if ( vsaState == null ) {
	    		return interpret(instruction, beginState, 
	    	            nextAddress);
	    	}
	    	
	    	vsaState.merge(vsaState2);
	    	
	    	((csAnalysisDomain)instruction.getBeginState()).updateAddMapping(stackString,  vsaState);
	    	((csAnalysisDomain)instruction.getBeginState()).changeContextString(stackString);
	    	
	    	return interpret(instruction, instruction.getBeginState(), 
	            nextAddress);
	    	
	    } else {
  				beginState.merge(instruction.getBeginState());
  			    return interpret(instruction, beginState, nextAddress);
  		}

    
  }
  
	/**
   * Performs the interpretation using the options specified in the constructor.
   */
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException 
	{
		try
		{
      interpret(monitor);
		}
		catch(NoEntryPointException e)
		{
      Log.write(Log.WARNING, Strings.NO_ENTRY_POINT);
		}
    catch (Exception e) 
    {
      Log.write(Log.ERROR, Strings.getShortUnexpectedInterpretationErrorMsg(e.getMessage()), e);
      throw new InvocationTargetException(e);
    }
	}
	
	/**
	 * Informs the state about the CFG that should be used to interpret
	 * the next instruction, if any CFG is needed at all.  Some instruction
	 * may require knowledge of the CFG.  It is made accessible through this
	 * function.
	 */
	public static void setActiveControlFlowGraph(final ControlFlowGraph graph)
	{
		activeControlFlowGraph = graph;
	}
	
	/**
	 * Informs the state about which instruction is currently being
	 * interpreted.  The interpreted is expected to call this function
	 * before interpreting each instruction.  This is needed, because in
	 * some cases, the state needs to know which instruction is being
	 * interpreted in order to carry out the interpretation correctly.
	 */
	public static void setActiveInstruction(final Instruction instruction)
	{
		activeInstruction = instruction;
	}
	
	/**
	 * Saves a reference to the state that should be used when interpreting the next
	 * instruction.  This function should be called before interpreting each
	 * instruction.  This is needed because some components, such as the Value
	 * class, need to know which state is currently active, but have no way
	 * to determine that by themselves.
	 */
	public static void setActiveState(State state)
	{
		activeState = state;
	}
	
	/**
	 * Saves a reference to the imported functions to be used when interpreting.
	 * If this function is not called, then the interpretation will
	 * assume there are no imported functions.
	 */
	public static void setImports(ExportTableSet imports)
	{
		Interpreter.imports = imports;
	}

  /**
   * For each <code>Instruction</code> in <code>nextInstructions</code>,
   * if the <code>Instruction</code> is not in <code>cfg</code>, 
   * then it is added and <code>cfg</code> is updated to include any 
   * <code>Instruction</code>s following <code>next</code>, at least as
   * much as possible. 
   * 
   * @param currentInstruction  the Instruction just before
   *                            next. Must already be in cfg.
   * @param next                Mapping of the next instructions to be interpreted
   *                            along with the incoming state for each instruction. 
   *                            The instructions in this map may or may not
   *                            already be in cfg.  These Address   
   *                            represents Instructions that occur directly 
   *                            after currentInstruction.
   * @param dependencies        the NodeDependencyTable in use with the cfgs
   */
  private void updateCfgWithNewInfo(
      Instruction currentInstruction, 
      Map<Address, State> next,
      NodeDependencyTable dependencies)
  {
    Address address;
    AddressList addressList = new AddressList();
    Instruction instruction;
    Iterator<Address> iter;
    
    iter = next.keySet().iterator();
    while(iter.hasNext())
    {
      address = iter.next();
      instruction = program.getInstruction(address);
      if(next != null && instruction != null)
      {
        if(!program.getControlFlowGraph().hasEdge(currentInstruction, instruction))
        {
          if(!program.getControlFlowGraph().hasNode(instruction))
            program.getControlFlowGraph().addNode(instruction);
          program.getControlFlowGraph().addEdge(currentInstruction, instruction);
          dependencies.decrement( program.getControlFlowGraph().getNode(instruction) ); 
          buildPartialCfg(program, instruction, program.getControlFlowGraph(), 
              dependencies);
        }
      } else {
    	  addressList.add(address);
      }
    }
    
    iter = addressList.iterator();
    while (iter.hasNext())
    	next.remove(iter.next());
    
  }
  
  /**
   * Returns the appropriate action that should be taken when
   * this instruction is encountered by the <code>Interpreter</code>.
   * @param instruction the <code>Instruction</code> encountered
   * @param state the new incoming <code>State</code> at the 
   *      <code>Instruction</code>.
   * @return the action that should be taken on this 
   *      <code>Instruction</code>.
   */
  public InterpretAction whatToDo(Instruction instruction, State state)
  {
    boolean isLoopJunctionNode;
    boolean isSimpleJunctionNode;
    boolean isVisited;
    boolean oldStateOverapproximatesNewState;
    
    ControlFlowGraph cfg = program.getControlFlowGraph();
    isLoopJunctionNode = cfg.isLoopJunctionNode(instruction);    
    isSimpleJunctionNode = cfg.isSimpleJunctionNode(instruction);

    isVisited = isVisited(instruction, state);
       
    if(instruction.getBeginState() == null)
      oldStateOverapproximatesNewState = false;
    else
    {
    	if (state instanceof csAnalysisDomain){

        	String stackString;
        	stackString = ((csAnalysisDomain)state).getContextString();
        	       	
        	VsaAsgState vsaState = (VsaAsgState)  ((csAnalysisDomain)instruction.getBeginState()).get(stackString);
        	
        	VsaAsgState vsaState2 = (VsaAsgState) ((csAnalysisDomain) state).get(stackString);
        	        	
        	if (vsaState == null)
        		oldStateOverapproximatesNewState = false;
        	else
        	oldStateOverapproximatesNewState = 
                vsaState.overapproximates(vsaState2);
        	
        }   else {
    	  
        	oldStateOverapproximatesNewState = 
                instruction.getBeginState().overapproximates(state);}
    	

    }
    
    return howToInterpretInstruction(isLoopJunctionNode, 
        isSimpleJunctionNode, isVisited, oldStateOverapproximatesNewState);
  }

  /**
   * Updates the begin <code>State</code> of the given 
   * <code>Instruction</code> by widening it relative to the 
   * new begin state specified by the parameter 
   * <code>beginState</code>; interprets the <code>Instruction</code>;
   * then updates the end <code>State</code> with the 
   * resulting <code>State</code> after performing the interpretation.
   * Returns the resulting <code>State</code>. Also updates the parameter
   * <code>nextInstructions</code> so that after execution of this function,
   * it will contain the <code>Address</code>es of the 
   * <code>Instruction</code>s that should be interpreted next.
   *  
   * @param instruction   the <code>Instruction</code> to be interpreted.
   * @param beginState    the begin <code>State</code>. 
   * @param nextAddress   the <code>Address</code> of the 
   *                      <code>Instruction</code> directly following this 
   *                      one in the program layout (not necessary in the 
   *                      program flow graph).
   * @return              Mapping of instructions to interpret next along
   *                      with incoming state.
   */
  public Map<Address, State> widenAndInterpret(
      Instruction instruction,
      State beginState, 
      Address nextAddress)
  {
    if (beginState instanceof csAnalysisDomain){

    	String stackString;
    	stackString = ((csAnalysisDomain)beginState).getContextString();
    	       	
    	VsaAsgState vsaState = (VsaAsgState)  ((csAnalysisDomain)instruction.getBeginState()).get(stackString);
    	
    	VsaAsgState vsaState2 = (VsaAsgState) ((csAnalysisDomain) beginState).get(stackString);
    	if ( vsaState == null ) {
    		return interpret(instruction, beginState, 
    	            nextAddress);			
    		//return new HashMap<Address, State>();
    	}
    	
    	vsaState.widen(vsaState2);
    	Trace.widen("Context", vsaState, vsaState2);
    	
    	((csAnalysisDomain)instruction.getBeginState()).updateAddMapping(stackString,  vsaState);
    	((csAnalysisDomain)instruction.getBeginState()).changeContextString(stackString);
    	
    	return interpret(instruction, instruction.getBeginState(), 
            nextAddress);
    	
    }  else {
	  // Context Insensitive analysis
	  instruction.getBeginState().widen(beginState);
	  Trace.widen("Insensitive", instruction.getBeginState(), beginState);
	    
	  return interpret(instruction, instruction.getBeginState(), 
        nextAddress);}
  }
  
  public void updateMapping(Instruction instruction, State state){
		
       //*** GET THE CONTEXT STRING TO KEEP WITH THE INSTRUCTION **
		String contextString;		
		if (state instanceof csAnalysisDomain){
			csAnalysisDomain context = (csAnalysisDomain) state.clone();
			contextString = context.getContextString();
		}  else return;

        // *** GET INSTRUCTION ADDRESS USED IN THE MAP ***
		String address;
		address = instruction.getAddress().toString();

		// GET THE VALUE FOR THE ADDESS 
		ArrayList list = mapping.get(address);
		// If no previous list, make one
		if (list == null) list = new ArrayList(); 
		// add context string if it is not in it.
		if (!(list.contains(contextString))) {
			list.add(contextString);
			mapping.put( address, list );
		}
	}
  
  
  
	public static Map getMappingAddress(){
		return mapping;
	}
	
	
}


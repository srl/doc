/*
 * WorkListElement.java
 * Created on Apr 13, 2005
 */
package x86Interpreter;

import x86InstructionSet.Address;
import x86InstructionSet.Instruction;
import domain.State;

/**
 * Used by the Program interpreter to track which instructions need to
 * be interpreted.
 */
public class WorkListElement
{
    private Instruction	instruction;
    private State				state;
        
    /**
     * Creates this, unitialized.  Provided only for testing.
     */
    public WorkListElement()
    { 
    }
    
    /**
     * Returns a clone of this object.
     */
    public Object clone()
    {
    	WorkListElement clone = new WorkListElement();
        clone.instruction = instruction;
        clone.state = state;
    	return clone;
    }
    
    /**
     * Constructs a new element consisting of the input instruction & state.
     */
    public WorkListElement(Instruction instruction, State state)
    {
        this.instruction = instruction;
        this.state			= state;
    }
    
    /**
     * Returns true if this equals obj.  Two WorkListElements are
     * considered equal if they share the same address, regardless
     * of state contents.
     */
    public boolean equals(Object obj)
    {
    	return instruction.equals( ((WorkListElement)obj).getInstruction() );
    }
    
    /* (non-Javadoc)
     * @see x86Interpreter.IWorkListElement#getInstruction()
     */
    public Instruction getInstruction()
    {
        return instruction;
    }
    
    /**
     * Returns the state associated with this element.
     */
    public State getState()
    {
        return state;
    }

    /**
     * string representation
     */
    public String toString()
    {
        return instruction.getAddress().toString();
    }
}

package x86Interpreter;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import x86InstructionSet.Address;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.Instruction;
import x86InstructionSet.JaJnbeInstruction;
import x86InstructionSet.JaeInstruction;
import x86InstructionSet.JbInstruction;
import x86InstructionSet.JbeInstruction;
import x86InstructionSet.JeInstruction;
import x86InstructionSet.JecxzInstruction;
import x86InstructionSet.JgInstruction;
import x86InstructionSet.JgeInstruction;
import x86InstructionSet.JlInstruction;
import x86InstructionSet.JleInstruction;
import x86InstructionSet.JmpInstruction;
import x86InstructionSet.JnbInstruction;
import x86InstructionSet.JneInstruction;
import x86InstructionSet.JnsInstruction;
import x86InstructionSet.JnzInstruction;
import x86InstructionSet.JsInstruction;
import x86InstructionSet.Program;
import x86InstructionSet.RetInstruction;
import controlFlowGraph.ControlFlowGraph;
import controlFlowGraph.ControlFlowGraphBlock;
import domain.State;

/**
 * Directs the analysis process by processing each instruction before
 * processing the proceeding instruction (your standard backward
 * analysis).  There is one notable difference: the analysis only
 * processing whole blocks at a time (that is, the analysis results
 * do not flow from one block to the next).
 */
public class BackwardBlockAnalysisWorklist implements Worklist
{
  Queue<WorkListElement> worklist = new LinkedList<WorkListElement>();
  
  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#add(x86Interpreter.WorkListElement)
   */
  public void add(WorkListElement element)
  {
    WorkListElement item;
    
    item = find(element.getInstruction().getAddress(), element.getState());
    if(item != null)
      item.getState().merge(element.getState());
    else
      worklist.add(element);
  }

  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#getNext(x86Interpreter.NodeDependencyTable, controlFlowGraph.ControlFlowGraph)
   */
  public WorkListElement getNext(NodeDependencyTable dependencyTable,
      ControlFlowGraph cfg)
  {
    return worklist.poll();
  }

  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#init(x86InstructionSet.Program, domain.State)
   */
  public void init(Program program, State initialState, Address initialInstruction)
  {
    ControlFlowGraphBlock[] blocks;
    
    blocks = program.getControlFlowGraph().getBlocks();
    for(int i = 0; i < blocks.length; i++)
      add( new WorkListElement(blocks[i].getLast(), (State)initialState.clone()) );
  }

  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#find(x86InstructionSet.InstructionAddress)
   */
  public WorkListElement find(Address address, State state)
  {
    Iterator          iter;
    WorkListElement  next;
    
    iter = worklist.iterator();
    while(iter.hasNext())
    {
      next = (WorkListElement)iter.next();
      if(next.getInstruction().getAddress().equals(address))
        return next;
    }
    return null;
  }

  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#has(x86InstructionSet.InstructionAddress)
   */
  public boolean has(Address address, State state)
  {
    return find(address, state) != null;
  }

  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#isEmpty()
   */
  public boolean isEmpty()
  {
    return size() == 0;
  }

  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#remove(x86InstructionSet.InstructionAddress)
   */
  public boolean remove(Address address, State state)
  {
    Iterator<WorkListElement> iter = worklist.iterator();
    while(iter.hasNext())
    {
      if( iter.next().getInstruction().getAddress().equals(address) )
      {
        iter.remove();
        return true;
      }
    }
    return false;
  }

  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#size()
   */
  public int size()
  {
    return worklist.size();
  }

  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#update(x86InstructionSet.Program, x86InstructionSet.Instruction)
   */
  public void update(Program program, Instruction lastInstruction, Map<Address, State> next)
  {
    Iterator<Instruction> iter;
    InstructionList previousInstructions;
    State state;
    
    // Because this worklist does not use different states for each instruction,
    // we simply grab the first state from the map.
    state = next.values().iterator().next();
    
    previousInstructions = program.getControlFlowGraph().getPrevious(lastInstruction);
    if(previousInstructions.size() == 1)
    {
      iter = previousInstructions.iterator();
      while(iter.hasNext())
      {
        Instruction instruction = iter.next();
        if( !(instruction instanceof CallInstruction ||
            instruction instanceof JaeInstruction ||
            instruction instanceof JaJnbeInstruction ||
            instruction instanceof JbeInstruction ||
            instruction instanceof JbInstruction ||
            instruction instanceof JecxzInstruction ||
            instruction instanceof JeInstruction ||
            instruction instanceof JgInstruction ||
            instruction instanceof JgeInstruction ||
            instruction instanceof JleInstruction ||
            instruction instanceof JlInstruction ||
            instruction instanceof JmpInstruction ||
            instruction instanceof JnbInstruction ||
            instruction instanceof JneInstruction ||
            instruction instanceof JnsInstruction ||
            instruction instanceof JnzInstruction ||
            instruction instanceof JsInstruction ||
            instruction instanceof RetInstruction) )
        {
          add( new WorkListElement(instruction, (State)state.clone()) );
        }
      }
    }
  }

  /**
   * String representation.
   */
  public String toString()
  {
    return worklist.toString();
  }
}

package x86Interpreter;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;
import x86InstructionSet.Address;
import x86InstructionSet.Instruction;

import com.sun.corba.se.impl.io.TypeMismatchException;

/**
 * Maintains a list of Instruction objects.  Provides operations for 
 * manipulating and searching the list. Instructions in this list
 * are sorted by <code>Address</code>.
 */
public class InstructionList
{
	private LinkedList<Instruction> instructions	= new LinkedList<Instruction>();
	
	/**
   * Identifies whether this list is currently sorted.
   * To improve performance, the list is only be sorted when absolutely 
   * necessary (such as just before a search is performed).  Thus,
	 * if an operation requires the list to be sorted, remember to call the sort()
	 * function.  This function will only sort the list if it is not already sorted.
	 * Likewise, if you modify the order of the list, call the setNotSorted() method
	 * to indicate that the list is no longer sorted.
   */
	private boolean isSorted = true;
	
	/**
	 * Adds an instruction to this list. No two instructions should share 
	 * the same address.  Results are undefined if two instructions share 
	 * the same address.
	 */
	public void add(Instruction instruction)
	{
		instructions.add(instruction);
		setNotSorted();
	}
	
	/**
	 * Returns true if this list contains the input instruction.
	 */
	public boolean contains(Instruction instruction)
	{
		return find(instruction.getAddress()) >= 0;
	}
	
	/**
	 * Returns the index of the instruction with the input address.
	 * Return a negative number if no such instruction exists in this list.
	 */
	private int find(Address address)
	{
		// comparator used to search the list.  The comparator is needed
		// because we will be comparing Instructions to Addresses, and there
		// is no function available for this kind of comparison, so we
		// define one here.
		Comparator instructionComparator = new Comparator() 
		{
			public int compare(Object o1, Object o2) 
			{
				if(o1 instanceof Instruction && o2 instanceof Address)
					return ((Instruction)o1).getAddress().compareTo(o2);
				else if(o1 instanceof Address && o2 instanceof Instruction)
					return ((Instruction)o2).getAddress().compareTo(o1);
				else 
					throw new TypeMismatchException();
			};

		};
		
		int			index;
		
		sort();
		index = Collections.binarySearch(instructions, address, instructionComparator);
		return index;
	}
	
	/**
	 * Returns the specified instruciton.  index specifies the index of the 
	 * instruction to receive.
	 */
	public Instruction get(int index) 
	{
    sort();
		return (Instruction)instructions.get(index);
	}
	
	/**
	 * Returns the instruction with the input address.  Returns null if no
	 * instruction with the address is found.
	 */
	public Instruction get(Address address)
	{
		boolean		itemFound;
		int			index;
		
		index = find(address);
		itemFound = (index >= 0);
		if(itemFound)
			return (Instruction)instructions.get(index);
		else
			return null;
	}
	
  /**
   * Returns the first <code>Instruction</code> in this list.
   * If this list is empty, then <code>null</code> is returend.
   * @return the first <code>Instruction</code> in this list.
   */
  public Instruction getFirst()
  {
    Instruction result;
    
    result = null;
    if(size() > 0)
      result = get(0);
    return result;
  }
  
	/**
	 * Returns the index of the instruction with the input address.
	 * Returns -1 if there is no instruction with the input address. 
	 */
	public int getIndex(Address address)
	{
	  int numOfInstructions;
    
    sort();
    numOfInstructions = instructions.size();
		for(int i = 0; i < numOfInstructions; i++) 
		{
		  if( ((Instruction)instructions.get(i)).getAddress().equals(address))
				return i;
		}
		return -1;
	}

  /**
   * Returns the last <code>Instruction</code> in this list.  
   * If this list is empty, <code>null</code> is returned.
   * @return last <code>Instruction</code> in this list.
   */
  public Instruction getLast()
  {
    Instruction result;
    
    result = null;
    if(size() > 0)
      result = get(size() - 1);
    return result;
  }
  
	/**
	 * Returns the instruction directly following the instruction with the
	 * input address. 
	 * Returns null if there is no next instruction.
	 */
	public Instruction getNext(Address address)
	{
		int			  indexOfInputInstruction;
		int			  indexOfNextInstruction;
		boolean		itemFound;
		boolean		nextInstructionExists;
		
		indexOfInputInstruction = find(address);
		itemFound = (indexOfInputInstruction >= 0);
		if(itemFound)
		{
			indexOfNextInstruction = indexOfInputInstruction + 1;
			nextInstructionExists = (indexOfNextInstruction < size());
			if(nextInstructionExists)
				return (Instruction)instructions.get(indexOfNextInstruction);
		}
		
		return null;
	}
	
	/**
	 * Returns an iterator that can be used to iterate this list.
	 */
	public java.util.Iterator iterator() { sort(); return new Iterator(); }
	public class Iterator implements java.util.Iterator
	{
    private java.util.Iterator  iter;

    public Iterator()		{ iter = instructions.iterator(); }
    public boolean      hasNext() 	{ return iter.hasNext(); }
    public Instruction  next() 		{ return (Instruction)iter.next(); }
    public void         remove()  { throw new NotImplementedException(); }
	}
	
	/**
	 * Sets and internal data member to indicate that the ordering of this list
	 * has changed and is no longer sorted.
	 */
	private void setNotSorted()
	{
		isSorted = false;
	}
	
	/**
	 * Returns the number of instructions in this list.
	 */
	public int size()
	{
		return instructions.size();
	}
	
	/**
	 * Sorts this list, but only if the list is not already sorted. 
	 */
	public void sort()
	{
		if(!isSorted)
		{
			Collections.sort(instructions);
			isSorted = true;
		}
	}

	/**
	 * String representation
	 */
	public String toString()
	{
    sort();
		return instructions.toString();
	}
}

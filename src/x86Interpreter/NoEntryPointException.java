/*
 * NoEntryPointException.java
 * Created on May 10, 2005
 */
package x86Interpreter;

/**
 * Thrown by the Interpreter when trying to interpret a 
 * program that doesn't contain an entry point.
 */
public class NoEntryPointException extends Exception
{
    NoEntryPointException() { super(); }
    
    NoEntryPointException(String msg) { super(msg); }
}

/*
 * ExportedFunctionDescription.java
 * Created on May 21, 2005
 */
package x86Interpreter;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Maintains information for a function exported by a stub dll.
 */
public class ExportedFunctionDescription
{
    /**
     * Address of the function. 
     */
    private int address;
    
    /**
     * Name of the function.
     */
    private String name;
    
    /**
     * Returns the address of this fuction.
     */
    public int getAddress()
    {
        return address;
    }
    
    /**
     * Returns the name of this function.
     */
    public String getName()
    {
        return name;
    }
    
    /**
     * Reads one exported function description from the input stream. The
     * description should be of the form:<br>
     * FUNCTIONNAME &lt;WHITESPACE&gt; ADDRESS<br> 
     * Contents are case sensitive.
     * @return true if successful, false if EOF is reached.
     */
    public boolean read(BufferedReader in)
    {
        String 					line;			// a line read from the input file.
        String[] 				tokens;			// line from file seperated into tokens
        
        // read a line 
        try
        {
	        line = in.readLine();
	        if(line == null)
	            return false;
	        // If a line begins with #, it is a comment, so skip it.  Also skip empty lines.
	        while(line.trim().equals("") || line.charAt(0) == '#')
	        {
	            line = in.readLine();
	            if(line == null)
	                return false;
	        }
	        
	        //break the input line into tokens
	        tokens = line.split("[ \t]+");
	        
	        // make sure there is a token for each data member
	        if(tokens.length != 2)
	        {
	            System.out.println("ExportedFunctionDescription.read: File format not correct.");
	            throw new Error("File format not correct.");
	        }
	        
	        // assign values to data members
	        name = tokens[0];
	        address = Integer.parseInt(tokens[1], 16);
        }
        catch(IOException e)
        {
            return false;
        }

        return true;
    }
    
    /**
     * String representation.  mainly for debugging purposes.
     */
    public String toString()
    {
        return name + " --> " + address;
    }
}

package x86Interpreter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;
import java.util.jar.*;

import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.PlatformUI;

import detectObfuscation.Preferences;

/**
 * Loads the external functions located in the stubs path (specified
 * in the preference page).  The external functions are placed in
 * files with the extension .asm and a description of the functions
 * exported by each file is contained in .exports (one .exports file
 * for each .asm file). 
 */
public class ExternalFunctionLoader
{
  /** extension of files that contain the exported function prototypes. */
  private final static String EXPORTS_FILE_EXTENSION = ".exports";
  
  /** this filter only accepts .exports files. */
  private static FileFilter exportsFilter = new FileFilter() 
  { 
    public boolean accept(File pathname)
    {
      return pathname.isFile() && 
        pathname.getName().endsWith(EXPORTS_FILE_EXTENSION);
    }
  };

  
  /**
   * Extracts the DLL name from a filename.  The DLL name is
   * the filename, minus the extension.  Thus, 
   * "mydll.exports" becomes "mydll" and "mydll.a.exports"
   * becomes "mydll.a".
   * @param filename filename of the DLL's exports file (actually,
   *          it is not required to end with .exports or contain
   *          any export information at all, but it usually will.
   * @return the DLL name.
   */
  private static String getDllnameFromFilename(String filename)
  {
    return filename.substring(0, filename.lastIndexOf("."));
  } 

  /**
   * Retrieves the exports path preference option from the
   * preference store.
   * @return the exports path.
   */
  private static String getExportsPathFromPreferenceStore()
  {
	IPreferenceStore store;
    store = PlatformUI.getPreferenceStore();
    return store.getString(Preferences.STUB_FILE_PATH);
  }
  
  /**
   * Loads the exported function tables for all .exports files found in the imports
   * directory.  See README.txt for information on setting up the exports directory.
 * @throws IOException 
 * @throws URISyntaxException 
   */
  public static ExportTableSet loadExternalFunctionTables() throws IOException, URISyntaxException
  {
    ExportTableSet    exportedTables; 
    File[]            exportedFiles; 
    File              exportsFolder; 
    String            exportsPath;
    
    exportedTables  = new ExportTableSet();
    exportsPath = getExportsPathFromPreferenceStore();
    
    if(exportsPath != "")
    {
      exportsFolder = new File(exportsPath);
      exportedFiles = exportsFolder.listFiles(exportsFilter);
      
      if(exportedFiles != null)
      {
        for(File exportedFile : exportedFiles)
        {
          exportedTables.add(
              getDllnameFromFilename(exportedFile.getName()), 
              exportedFile.getPath() );
        }
      }
    }
        
    return exportedTables;
  }

	public static ExportTableSet loadExternalFunctionTables(String importPathSwitch) {
		ExportTableSet    exportedTables; 
	    File[]            exportedFiles; 
	    File              exportsFolder; 
	    String            exportsPath;
	    
	    exportedTables  = new ExportTableSet();
		    
    	//for now the stand alone version of DOC will need the imports
        //folder in it's directory.  Imports is inside the jar, but I
        //don't know how to access an entire directory from within the
        //the same jar
        exportsFolder = new File(importPathSwitch);
        exportedFiles = exportsFolder.listFiles(exportsFilter);
        
        if(exportedFiles != null)
        {
          for(File exportedFile : exportedFiles)
          {
            exportedTables.add(
                getDllnameFromFilename(exportedFile.getName()),
                exportedFile.getPath());
            	System.out.println("Import Path Test : " + exportedFile.getPath());
          }
        }
	        
	    return exportedTables;
	}
}

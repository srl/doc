/*
 * InterpretationReport.java
 * Created on May 29, 2005
 */
package x86Interpreter;

import java.util.LinkedList;
import java.util.List;

/**
 * A report containing any notes that may be of interest that
 * occurred while interpreting a program.
 */
public class InterpretationReport
{
    List<InterpretationReportItem> items = new LinkedList<InterpretationReportItem>();
    
    /**
     * Adds an item to the report.
     */
    public void add(InterpretationReportItem item)
    {
        items.add(item);
    }
    
    /**
     * Returns an iterator that can be used to traverse the items in this report.
     */
    public Iterator iterator()
    {
        return new Iterator();
    }
    
    public class Iterator
    {
        private java.util.Iterator iter = items.iterator();
        public boolean hasNext() 				{ return iter.hasNext(); };
        public InterpretationReportItem next() 	{ return (InterpretationReportItem)iter.next(); };
    }

    /**
     * String representation
     */
    public String toString()
    {
        StringBuffer result = new StringBuffer();
        Iterator iter = iterator();
        while(iter.hasNext())
        {
            result.append(iter.next() + "\r\n");
        }
        return result.toString();
    }
}

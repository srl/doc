/*
 * WorkList.java
 * Created on Apr 13, 2005
 */
package x86Interpreter;


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import x86InstructionSet.Address;
import x86InstructionSet.Instruction;
import x86InstructionSet.Program;
import x86InstructionSet.UnknownAddress;
import controlFlowGraph.ControlFlowGraph;
import controlFlowGraph.ControlFlowGraphNode;
import domain.State;


/**
 * A list of WorkListElements.  Used by the Program interpreter to track
 * which instructions need to be interpreted.
 * 
 * WorkList implements the ControlFlowGraph.Observer interface.  This
 * allows it to listen to changes made to the ControlFlowGraph and
 * thus choose the most appropriate instruction to interpret next.
 * By linking this WorkList with the Program's ControlFlowGraph,
 * you allow it to choose Instructions to interpret in the most
 * efficient order.
 */
public class ForwardAnalysisWorklist implements Worklist 
{
    private LinkedList<WorkListElement> elements = new LinkedList<WorkListElement>();
    
    /* (non-Javadoc)
     * @see x86Interpreter.IWorklist#add(x86Interpreter.WorkListElement, x86Interpreter.NodeDependencyTable, controlFlowGraph.ControlFlowGraph)
     */
    public void add(final WorkListElement element)
    {
    	WorkListElement item;
    	
    	item = find(element.getInstruction().getAddress(), element.getState());
      if(item != null)
      	item.getState().merge(element.getState());
      else
      	elements.addLast(element);
    }
    
    /* (non-Javadoc)
     * @see x86Interpreter.IWorklist#getNext(x86Interpreter.NodeDependencyTable, controlFlowGraph.ControlFlowGraph)
     */
    public WorkListElement getNext(NodeDependencyTable dependencyTable, ControlFlowGraph cfg)
    {
    	List<ControlFlowGraphNode> nodes = new LinkedList<ControlFlowGraphNode>();
    	
    	Iterator iter = elements.iterator();
    	while(iter.hasNext())
    		nodes.add( cfg.getNode( ((WorkListElement)iter.next()).getInstruction()) );
    	
    	ControlFlowGraphNode node = (ControlFlowGraphNode)dependencyTable.getNext(nodes).getObject();

    	WorkListElement element = find(node.getInstruction().getAddress(), node.getInstruction().getEndState());
    	remove(node.getInstruction().getAddress(), null);
    	dependencyTable.decrementAll(node.getSuccessors());
    	return element;
    }    

    /* (non-Javadoc)
     * @see x86Interpreter.IWorklist#find(x86InstructionSet.Address)
     */
    public WorkListElement find(Address address, State state)
    {
    	Iterator iter;
  		WorkListElement next;
    	
    	iter = elements.iterator();
    	while(iter.hasNext())
  		{
    		next = (WorkListElement)iter.next();
    		if(next.getInstruction().getAddress().equals(address))
    			return next;
  		}
    	return null;
    }
    
    /* (non-Javadoc)
     * @see x86Interpreter.IWorklist#has(x86InstructionSet.Address)
     */
    public boolean has(Address address, State state)
    {
    	return find(address, state) != null;
    }

    /* (non-Javadoc)
     * @see x86Interpreter.Worklist#init(x86InstructionSet.Program)
     */
    public void init(Program program, State initialState, Address initialInstruction)
    {
      if(!initialInstruction.equals(UnknownAddress.UNKNOWN))
        add(new WorkListElement(program.getInstruction(initialInstruction), 
            (State)initialState.clone())); 
    }
    
    /* (non-Javadoc)
     * @see x86Interpreter.IWorklist#remove(x86InstructionSet.Address)
     */
    public boolean remove(Address address, State state)
    {
    	Iterator iter = elements.iterator();
    	while(iter.hasNext())
    	{
    		if( ((WorkListElement)iter.next()).getInstruction().getAddress().equals(address) )
				{
    			iter.remove();
    			return true;
				}
    	}
    	return false;
    }
    
    /* (non-Javadoc)
     * @see x86Interpreter.Worklist#size()
     */
    public int size()
    {
      return elements.size();  
    }
    
    /* (non-Javadoc)
     * @see x86Interpreter.IWorklist#isEmpty()
     */
    public boolean isEmpty()
    {
        return elements.isEmpty();
    }

    /* (non-Javadoc)
     * @see x86Interpreter.Worklist#update(x86InstructionSet.Program, x86InstructionSet.Instruction)
     */
    public void update(Program program, Instruction lastInstruction, Map<Address, State> next)
    {
      Iterator<Instruction> iter;
      InstructionList nextInstructionsToInterpret;
      State state;
      
      // Although the function accepts a map of addresses to states, this method ignores that
      // parameters.  Instead, it uses the next instructions given by the control flow
      // graph and it picks the first state from the mapping, because in this implementation,
      // all of the states in the mapping will be the same.
      state = next.values().iterator().next();
            
      nextInstructionsToInterpret = program.getControlFlowGraph().getNext(lastInstruction);
      iter = nextInstructionsToInterpret.iterator();
      while(iter.hasNext())
        add(new WorkListElement(iter.next(), (State)state.clone()));        
    }
    
    /**
     * string representation
     */
    public String toString()
    {
        return elements.toString();
    }
}

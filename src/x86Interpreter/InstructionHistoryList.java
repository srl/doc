package x86Interpreter;

import x86InstructionSet.Instruction;
import domain.State;

/**
 * Maintains a record of which instructions have been interpreted. The
 * Interpreter uses this to determine if an instruction should be 
 * interpreted as is, if the states should be merged, or if the 
 * instruction can be skipped.
 * <br>
 * For example, if the interpreter comes across an instruction that has
 * already been interpreted and the state hasn't changed, then there
 * is no need to interpret the instruction a second time.
 */
public interface InstructionHistoryList
{
  /**
   * Informs the InstructionHistoryList that the given instruction has
   * just been interpreted.  The begin and end state of the instruction
   * should be updated prior to calling this function.
   * 
   * @param instruction The instruction that has been interpreted.
   */
  public void interpreted(Instruction instruction);
  
  /**
   * Determines if the given instruction has been interpreted.
   * 
   * @param instruction Instruction to determine if it has been interpreted
   *                    already.
   * @param state       The new incoming state.
   * @return  true if this instruction has been interpreted already, false otherwise.
   */
  public boolean isInterpreted(Instruction instruction, State state);
}

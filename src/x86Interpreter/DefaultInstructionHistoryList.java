package x86Interpreter;

import x86InstructionSet.Instruction;
import domain.State;
 
/**
 * Determines if an instruction has been interpreted by checking
 * if it has a non-null state.
 */
public class DefaultInstructionHistoryList implements InstructionHistoryList
{
  /**
   * Adds an instruction to the interpreted list.
   */
  public void interpreted(Instruction instruction)
  {
    // Nothing to do here.  
  }
  
  /**
   * Returns true if the instruction has been interpreted.
   */
  public boolean isInterpreted(Instruction instruction, State state)
  {
    return instruction.getBeginState() != null;
  }
}

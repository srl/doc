/*
 * ExportTable.java
 * Created on May 21, 2005
 */
package x86Interpreter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Contains a collection of Function descriptions for a specific
 * DLL.  Each function description contains a function name and
 * an address.  The ExportTable provides methods for retrieving
 * the address of some specific function.  This is used when
 * a program makes a system call.  The interpreter can look
 * up the address of the system call in one of the ExportTables
 * and then transfer control to that address, assuming the
 * system function (or a stub of the system function) 
 * has been loaded at that address. 
 */
public class ExportTable
{
    /**
     * The name of the dll that this export table belongs to.
     */
    private String dllname;
    
    private List<ExportedFunctionDescription> exports = 
      new LinkedList<ExportedFunctionDescription>();
    
    /**
     * Creates a new export table initialized to the input dllname.
     */
    public ExportTable(String dllname)
    {
        this.dllname = dllname;
    }
    
    /**
     * Adds an exported function to this table.
     */
    private void addExport(ExportedFunctionDescription description)
    {
        exports.add(description);
    }
    
    /**
     * Retrieves the address of the specified function.  Function
     * name is case-sensitive.
     * @param functionname the name of the function whose address
     * 		you wish to retrieve.
     * @return address of requested function
     * @throws FunctionNotFoundException
     */
    public int getAddress(String functionname) throws FunctionNotFoundException
    {
        Iterator iter;
        iter = exports.iterator();
        while(iter.hasNext())
        {
            ExportedFunctionDescription export = (ExportedFunctionDescription)iter.next();
            if(export.getName().equals(functionname))
                return export.getAddress();
        }
        throw new FunctionNotFoundException();
    }
    
    /**
     * Returns the DLL name that this export table is associated with.
     */
    public String getDllName()
    {
        return dllname;
    }
    
    /**
     * Loads the export table contents from filename.  See
     * ExportedFunctionDescription for the format of the file.
     */
    public boolean load(String filename)
    {
        try
        {
	        BufferedReader in = new BufferedReader(new FileReader(new File(filename)));
	        ExportedFunctionDescription desc = new ExportedFunctionDescription();
	        while(desc.read(in))
	        {
	            addExport(desc);
	            desc = new ExportedFunctionDescription();
	        }
        }
        catch(FileNotFoundException e)
        {
            return false;
        }
        return true;
    }
    
    /**
     * String representation.  Mainly for debugging purposes.
     */
    public String toString()
    {
        StringBuffer	result = new StringBuffer();
        Iterator		iter;
        
        result.append(dllname + "\n");
        
        iter = exports.iterator();
        if(iter.hasNext())
        {
            result.append(iter.next().toString());
	        while(iter.hasNext())
	            result.append("\n" + iter.next().toString());
        }
        return result.toString();
    }
}

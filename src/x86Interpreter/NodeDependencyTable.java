package x86Interpreter;

import controlFlowGraph.ControlFlowGraph;
import controlFlowGraph.ControlFlowGraphNode;
import controlFlowGraph.ControlFlowGraphNodeList;
import dependencyTable.DependencyTable;
import dependencyTable.Item;

/**
 * Extends the DependencyTable to add a few functions
 * specifically for the handling of CFG nodes.
 */
public class NodeDependencyTable extends DependencyTable implements ControlFlowGraph.Observer
{
	public void nodeAdded(ControlFlowGraph activatedBy, ControlFlowGraphNode node, boolean duplicate)
	{
		int dependencyCount;
		
		if(!duplicate)
		{
			dependencyCount = node.getPredecessors().size();
			add(new Item(node, dependencyCount, new dependencyTable.Priority()) );
		}
	}
	
	public void edgeAdded(ControlFlowGraph activatedBy, ControlFlowGraphNode source, 
			ControlFlowGraphNode target, boolean duplicate)
	{
		increment(target);
		ControlFlowGraphNodeList visited = new ControlFlowGraphNodeList();
		visited.add(target);
		incrementChildren(activatedBy, target, visited);
			
	}
	
	public void increment(ControlFlowGraphNode node)
	{
		if(find(node).getDependencies() < node.getPredecessors().size())
			find(node).increaseDependencies();
	}
	
	public void incrementChildren(ControlFlowGraph cfg, ControlFlowGraphNode node, 
			ControlFlowGraphNodeList visited)
	{
		ControlFlowGraphNodeList children = node.getSuccessors();
		ControlFlowGraphNodeList.Iterator iter = children.iterator();
		
		while(iter.hasNext())
		{
			ControlFlowGraphNode child = iter.next();
			if(!visited.has(child))
			{
				increment(child);
				visited.add(child);
				incrementChildren(cfg, child, visited);
			}
		}
	}
	/**
	 * TODO: COMMENT - Misnomer. The method is called 'decrement', but it calls
	 * 'increaseDependencies()' which increments.
	 *
	 */
	
	public void decrement(ControlFlowGraphNode node)
	{
		if(find(node).getDependencies() < node.getPredecessors().size())
			find(node).increaseDependencies();
	}
	
	public ControlFlowGraphNodeList getNodesWithCount(int desiredCount)
	{
		Item item;
		Iterator iter;
		ControlFlowGraphNodeList result = new ControlFlowGraphNodeList();
		
		iter = iterator();
		while(iter.hasNext())
		{
			item = iter.next();
			if(item.getDependencies() == desiredCount)
				result.add( (ControlFlowGraphNode)item.getObject() );
		}
		return result;
	}
	
	public void decrementAll(ControlFlowGraphNodeList nodes)
	{
		ControlFlowGraphNodeList.Iterator iter = nodes.iterator();
		
		while(iter.hasNext())
			decrement(iter.next());
	}
}

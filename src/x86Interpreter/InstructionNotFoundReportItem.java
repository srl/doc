/*
 * InstructionNotFoundReportItem.java
 * Created on May 29, 2005
 */
package x86Interpreter;

import x86InstructionSet.Address;

/**
 * Indicates that an instruction with a particular address was not found
 * and could not be interpreted.
 */
public class InstructionNotFoundReportItem implements InterpretationReportItem
{
    private Address address;
    
    /**
     * Creates this, initialized to the input address.
     */
    public InstructionNotFoundReportItem(Address address)
    {
        this.address = address;
    }
    
    /**
     * Returns the address of the instruction that could not be found.
     */
    public Address getAddress()
    {
        return address;
    }

    /**
     * String representation
     */
    public String toString()
    {
        return "Failed to find instruction at address " + address;
    }
}

/*
 * ExportTableSet.java
 * Created on May 21, 2005
 */
package x86Interpreter;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Maintains a collection of Export Tables.  Each export table describes
 * the exported functions from a particular dll file, but instead of 
 * containing complete implementations, the exported functions only
 * include short stubs designed to mimic the behavior of the real 
 * DLL function counterparts. 
 */
public class ExportTableSet
{
    private List<ExportTable> exportTables = new LinkedList<ExportTable>();
    
    /**
     * Adds an export table for the specified dll and loads the 
     * export table contents from filename.
     * @param dllname a unique name assigned to this entry table.
     * 		The name can be any arbitrary string, but usually, you
     * 		will want to give it the name of the DLL.
     * @param filename the name and path of the file that contains
     * 		the list of functions & function addresses that the
     * 		dll exports. 
     * @return true if successful, false if an error occurred.
     */
    public boolean add(String dllname, String filename)
    {
        ExportTable table = new ExportTable(dllname);
        if(table.load(filename))
        {
            exportTables.add(table);
            return true;
        }
        return false;
    }
    
    /**
     * Returns the address of the specified function contained
     * in the specified dll.  The dllname is not case-sensitive
     * but the functionname is.
     * @param dllname The dll where the function is located.
     * @param functionname The name of the function to look up.
     * @return address of input function
     * @throws FunctionNotFoundException
     */
    public int getAddress(String dllname, String functionname) throws FunctionNotFoundException
    {
        Iterator iter;
        iter = exportTables.iterator();
        while(iter.hasNext())
        {
            ExportTable table = (ExportTable)iter.next();
            if(table.getDllName().equalsIgnoreCase(dllname))
                return table.getAddress(functionname);
        }
        throw new FunctionNotFoundException();
    }
    
    /**
     * String representation.  Mainly for debugging purposes.
     */
    public String toString()
    {
        Iterator 		iter;
        StringBuffer	result 	= new StringBuffer();
        
        iter = exportTables.iterator();
        if(iter.hasNext())
        {
            result.append(iter.next().toString());
	        while(iter.hasNext())
	            result.append("\n" + iter.next().toString());
        }
        return result.toString();
    }
}

/*
 * InterpretationReportItem.java
 * Created on May 29, 2005
 */
package x86Interpreter;

/**
 * An individual item in the InterpretationReport.  Items intended to
 * be included in the report should implement this interface.  The user
 * can determine what the item describes by checking the item's type
 * (that is, use instanceof).  Also, each individual item may offer
 * implement additional functions that offer additional information.
 */
public interface InterpretationReportItem
{
}

/*
 * Log.java
 * Created on May 30, 2005
 */
package logger;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import detectObfuscation.Strings;


/**
 * Provides methods for interacting with the log.
 */
public class Log
{
  // Status indicators.
  static public int ERROR   = IStatus.ERROR;
  static public int INFO    = IStatus.INFO;
  static public int WARNING = IStatus.WARNING;
  
  /**
   * Writes a message to the log.
   * @param severity Severity of the message. Must be ERROR, WARNING, or INFO.
   * @param message The message to write.
   */
  static public void write(int severity, String message)
  {
    if(severity != ERROR && severity != INFO && severity != WARNING)
      throw new IllegalArgumentException();

    // plugin can be null during unit testing
    if(ResourcesPlugin.getPlugin() != null)
      ResourcesPlugin.getPlugin().getLog().log( new Status(severity, Strings.PLUGIN_ID,
          Status.OK, message, null) );
  }
  
  /**
   * Writes a message to the log along with exception information.
   * @param severity Severity of the message. Must be ERROR, WARNING, or INFO.
   * @param message The message to write.
   * @param exception The exception containing additional information about the error.
   */
  static public void write(int severity, String message, Throwable exception)
  {
    if(severity != ERROR && severity != INFO && severity != WARNING)
      throw new IllegalArgumentException();
    
    // plugin can be null during unit testing
    if(ResourcesPlugin.getPlugin() != null)
      ResourcesPlugin.getPlugin().getLog().log( new Status(severity, Strings.PLUGIN_ID,
          Status.OK, message, exception) );
  }
}

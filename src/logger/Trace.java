package logger;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import domain.State;
import ui.directedGraph.DirectedGraphViewer;
import ui.directedGraph.GraphVisitor;
import vsaAsgDomain.EndStateStackGraphContentProvider;
import vsaAsgDomain.VsaAsgStateStackGraphContentProvider;
import vsaAsgDomain.VsaAsgState;
import vsaAsgDomain.StackGraphLabelProvider;
import x86InstructionSet.Address;
import x86InstructionSet.Instruction;
import x86Interpreter.InterpretAction;

import ui.registers.EndStateRegisterContentProvider;
import ui.registers.BeginStateRegisterContentProvider;
import ui.registers.RegisterContentProvider;
import ui.registers.RegisterValueContextPair;
import ui.registers.RegisterLabelProvider;
import ui.registers.StateRegisterContentProvider;

public class Trace {
    private static Boolean m_flag = false;
    private static GraphVisitor graphVisitor = null;
    
    public static void enable() { m_flag = true; }
    public static void disable() { m_flag = false; }
    public static void toggle() { m_flag = ! m_flag;}
    
    public static void traceRegisters (VsaAsgState state, String contextstring){
    	
    	RegisterContentProvider rcp = new StateRegisterContentProvider();
    	RegisterLabelProvider rlp = new RegisterLabelProvider();
	      
    	StringBuffer result = new StringBuffer();
    	Object[] regs = rcp.getElements(state);
	    System.out.println("Context " + contextstring +"\r\n");
    	for (Integer i = 0; i < regs.length; i++) {
    		RegisterValueContextPair r_end = (RegisterValueContextPair) regs[i];
    		result.append(" " + regInfo(r_end)+"\r\n");
    	}
    	System.out.println(result);
    }
    
    public static void traceStack (VsaAsgState state) {
    	//////////// ASG //////////////////
    	DirectedGraphViewer   viewer = null;
    	//set instruction whose ASG we want
    	viewer = new DirectedGraphViewer(graphVisitor);
    	viewer.setNoContentMessage("NO_CONTENT_MESSAGE");

    	viewer.setContentProvider(new VsaAsgStateStackGraphContentProvider());

    	viewer.setLabelProvider(new StackGraphLabelProvider());
    	viewer.setInput(state);
	      
    	System.out.println("ASG - START");
    	//causes instruction's ASG to be output
    	viewer.refresh();
    	System.out.println("ASG - END");

    }
    private static String regInfo(RegisterValueContextPair reg) {
    	RegisterLabelProvider rlp = new RegisterLabelProvider();
    	return rlp.getColumnText(reg, 0) + " " +
	           rlp.getColumnText(reg, 1) + " " +
	           rlp.getColumnText(reg, 2);
    }
    public static void traceDiff (VsaAsgState state1, VsaAsgState state2, String contextstring){
     	   	
    	//rcp provides the register data for the state
	      RegisterContentProvider rcp_end = new StateRegisterContentProvider();
	      RegisterContentProvider rcp_begin = new StateRegisterContentProvider();
	      //rcp seems to be made of rlp elements
	      RegisterLabelProvider rlp = new RegisterLabelProvider();
	      
	      StringBuffer result = new StringBuffer();
	     
	      Object[] regs_end = rcp_end.getElements(state2);
	      Object[] regs_begin = rcp_begin.getElements(state1);
	      Boolean found = false;
	      if(regs_end.length==regs_begin.length){
	          //result.append("Registers - START\r\n");
		      for (Integer i = 0; i < regs_end.length; i++) {
		    	  RegisterValueContextPair r_end = (RegisterValueContextPair) regs_end[i];
		    	  RegisterValueContextPair r_beg = (RegisterValueContextPair) regs_begin[i];
		    	 
		    	  if (!r_end.equals(r_beg)) {
		    		  // print the context string ONCE, only if a difference is found
		    		  if (!found) {
		    			  result.append("Context " + contextstring + "\r\n");
		    		  }
		    		  found = true;
		    	      // result.append("BEFORE ");
		    	      result.append(regInfo(r_beg));
		    	      // result.append("\r\n");
		    	      result.append(" --> ");
		    	      result.append(regInfo(r_end));
		    	      result.append("\r\n");
		    	  }
		      }
		      //result.append("Registers - END");
		      if (found) System.out.println(result);
	      }
	      else{
	    	  System.out.println("Part of Trace was skipped because regs_end.length != regs_begin.length");
	    	  System.out.println("regs_end.length = " + regs_end.length);
	    	  System.out.println("regs_begin.length = " + regs_begin.length);
	      }
    }
    
    public static void widen(String message, State state1, State state2){
    	if (!m_flag) return;
	    System.out.println("\n---------------------------");
	    System.out.println("WIDEN " + message + "\r\n");
	    traceRegisters((VsaAsgState) state1, "Wideing");
        traceStack((VsaAsgState) state1);
		  
    }
    public static void marker(Instruction instruction) {
    	if (!m_flag) return;
	      System.out.println("\n---------------------------");		    
		  System.out.println(instruction);
   	
    }
	public static void analysis(String message, Instruction instruction, Map<Address, State> next)
	  {
		if (!m_flag) return;
		System.out.println("Action: " + message);
		  traceRegisterDiffs(instruction);		  
		  traceASG(instruction);
	      
	      ////////////////////////////////////////////////////////
	      if (next != null) {
	    	  // Address of next instructions
	    	  Iterator<Address> iter;
	    	  Address address;
	    	  iter = next.keySet().iterator();
	    	  System.out.print("Next: ");
	    	  while(iter.hasNext())
	    	  {
	    		  address = iter.next();
	    		  System.out.print(address.toString() + " ");
	    	  }
	    	  System.out.println();
	      }
	  }
	
	public static void traceRegisterDiffs(Instruction instruction) {
		VsaAsgState state1 = (VsaAsgState) instruction.getBeginState();
		  VsaAsgState state2 = (VsaAsgState) instruction.getEndState();

		  if(state1 != null && state2 != null) {
			  // Not sure when/why this may happen
			  // but its happening in 2calls.asm
			  
	       // Limited to subclasses of VsaAsgState //
		  Map<String,VsaAsgState> contextMap1 = state1.getMapping();
		  Map<String,VsaAsgState> contextMap2 = state2.getMapping();
		  Set<String> keys1 = contextMap1.keySet();
		  Set<String> keys2 = contextMap2.keySet();
		  
		  // choose the larger key set
		  if (keys1.size() < keys2.size()) keys1 = keys2;
		  Iterator<String> contextIter = keys1.iterator();
		  while (contextIter.hasNext()) {
			  // get a context string
			  String contextString = contextIter.next();
			  VsaAsgState substate1 = contextMap1.get(contextString);
			  VsaAsgState substate2 = contextMap2.get(contextString);
			  //System.out.println("Context --> " + contextString);
			  if (substate1 == null) {
				  traceRegisters(substate2, contextString);
			  } else if (substate2 == null) {
				  System.out.println("Unsual -- context " + contextString + " only in State1");
				  traceRegisters(substate1, contextString);
			  } else {
				  // System.out.println("Difference State1, State2");
				  traceDiff(substate1, substate2, contextString);
			  }
		  }
		  }
	}
	
	public static void traceASG(Instruction instruction) {
		DirectedGraphViewer viewer;
		/////////////////////////////////////////////////////
			//set instruction whose ASG we want
	      viewer = new DirectedGraphViewer(graphVisitor);
	      viewer.setNoContentMessage("NO_CONTENT_MESSAGE");

	      viewer.setContentProvider(new EndStateStackGraphContentProvider());

	      viewer.setLabelProvider(new StackGraphLabelProvider());
	      viewer.setInput(instruction);
	      
		  System.out.println("ASG - START");
	      //causes instruction's ASG to be output
	      viewer.refresh();
	      System.out.println("ASG - END");
	      System.out.println();
	}
	
	public static void setGraphVisitor(GraphVisitor passedGraphVisitor) {
		graphVisitor = passedGraphVisitor;
	}
}

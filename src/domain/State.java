package domain;

import java.util.Iterator;

import x86InstructionSet.AddressList;
import x86InstructionSet.Instruction;
import abstractBoolean.AbstractBoolean;

/**
 * Defines methods for retrieving and storing values from/to registers and memory.  
 * Each new domain is expected to inherit from this class.
 */
public interface State extends Cloneable
{
  /**
   * Returns a copy of this state.  Any subclasses should override
   * this function.
   */
  public Object clone();

  /**
   * Create a new unitialized value.
   */
  public Value createNewValue();

  /**
   * Creates a new initialized value.
   */
  public Value createNewValue(long v);

  /**
   * Returns the value in the input flag register.  
   * @param flagRegister see flags.FlagRegisterStore for possible values. 
   */
  public AbstractBoolean getFlagValue(int flagRegister);

  /**
   * Returns a register's value as a Value containing integers, 
   * instead of an AbstractBoolean.  The translation from 
   * AbstractBoolean to Value is:
   *  Top		--> {0,1}
   *  True	--> {1}
   *  False	--> {0}
   *  Bottom	--> {0,1}
   */
  public Value getFlagValueInteger(int flagRegister);

  /**
   * Returns the value of the specified register.  Does not return
   * the actual value; returns a clone of the value instead.  If you
   * want to modify the register value, you must first modify the value
   * followed by a call to setRegisterValue.  Note that some analyses,
   * in particular live variable analysis, may result in a state
   * change by calling this function.  Thus, if you want to be sure
   * the state is not modified, clone this state before calling this 
   * function.
   */
  public Value getRegisterValue(Register register);
  
  /**
   * Returns a list of addresses that are currently on the 
   * top of the stack, i.e. the return address.  Does not
   * modify the stack pointer (esp).  This function is
   * similar to called State.load(esp), however this
   * function should try to make a best-effort attempt
   * at reducing the amount of over-approximation (whenever possible), 
   * because over-approximating the return address can reduce the
   * analysis results.  Calling State.load(esp) on the other
   * hand may result in too many over-approximations to be
   * useful.
   */
  public AddressList getReturnAddressAsList();

  /**
   * Returns the instructions that created the values on the top of the stack.
   */
  public Instruction[] getStackTopCreators();

  /**
   * Resets any initialization that might be needed.  This should be
   * called prior to perform any interpretation.  If a second file 
   * is interpreted, then this method should be called again. 
   * This is to allow the state to reinitialize any static members.
   */
  public void init();

  /**
   * Returns an iterator that can be used to traverse each piece
   * of memory in the state that contains a value (registers
   * not included).  The Iterator.next() function returns a 
   * MemoryValue object that contains the address of the 
   * piece of memory and the value stored there.
   */
  public Iterator iterator();

  /**
   * Pops the value from the top of the stack.  Returns a copy of
   * the value that is on the top (does not return a reference).
   * @return the value at the top of the stack before popping.
   */
  public Value pop();

  /**
   * Pushes a value onto the stack.
   */
  public void push(Value value);

  /**
   * Sets the value of the specified register.
   */
  public void setRegisterValue(Register register, final Value newVal);

  /**
   * Sets the value of the input flag register.  
   * @param flagRegister see flags.FlagRegisterStore for possible values.
   */
  public void setFlagValue(int flagRegister, AbstractBoolean value);

  /**
   * Loads the value stored at the specified source address.  Does not return
   * the actual value; returns a clone of the value instead.  If you
   * want to modify the value in memory, you must first modify the value
   * followed by a call to store
   * 
   * @param source the address to load from.
   * @param size how many bits to load.  For instance, word ptr ss:[esp]
   * 		instructs the interpreter to load only 16 bits.
   */
  public Value load(Value source, BitCount size);

  /**
   * Merges this state with rhs.  
   */
  public void merge(final State rhs);

  /**
   * Returns true if this state is either an over-approximation
   * of rhsIn or is equal to rhsIn.  A state over-approximates another state if
   * each member of this state over-approximates each member in the other state.
   */
  public boolean overapproximates(final State rhs);

  /**
   * Stores a specified value at the specified destination address.
   * @param dest the address to write to.
   * @param value the value to store
   */
  public void store(Value dest, Value value);

  /**
   * Widens this state.  
   * @param rhs state used to determine which values should be widened.
   */
  public void widen(final State rhs);

}
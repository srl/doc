package domain;

/**
 * Stores a memory address and value.  Used by State.Iterator.
 * 
 * @see domain.State#iterator()
 */
public class MemoryValue 
{
	private Value	address;
	private	Value value;
	
	/**
	 * Creates this.
	 */
	public MemoryValue(Value address, Value value)
	{
		this.address = address;
		this.value = value;
	}
	
	/**
	 * Returns address
	 */
	public Value getAddress()
	{
		return address;
	}
	
	/**
	 * Returns value
	 */
	public Value getValue()
	{
		return value;
	}

	/**
	 * String representation
	 */
	public String toString()
	{
		return "(" + address + ", " + value + ")";
	}
}

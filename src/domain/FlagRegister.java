/*
 * FlagRegister.java
 * Created on Jul 20, 2005
 */
package domain;

import x86Interpreter.Interpreter;
import abstractBoolean.AbstractBoolean;

/**
 * Maintains the value stored in a flag register.  Flag
 * registers can store either true or false.  In the 
 * context of this project, a flag register may hold
 * \bottom, true, false, and \top, where true \join false
 * equals \top and true \meet false equals \bottom.  Each
 * FlagRegister is initialized to \top.
 */
public class FlagRegister 
{
	private AbstractBoolean	value = AbstractBoolean.Top;
	
	/**
	 * Returns this register's value.
	 */
	public AbstractBoolean getValue()
	{
		return value;
	}
	
	/**
	 * Returns this register's value as a Value, instead of an
	 * AbstractBoolean.  The translation from AbstractBoolean
	 * to Value is:
	 *  Top		--> {0,1}
	 *  True	--> {1}
	 *  False	--> {0}
	 *  Bottom	--> {0,1}
	 */
	public Value getInteger()
	{
		Value result;
		
		if(value == AbstractBoolean.Top)
			result = Interpreter.getActiveAnalysis().createDomainFactory().createValue(0, 1);
		
		else if(value == AbstractBoolean.True)
			result = Interpreter.getActiveAnalysis().createDomainFactory().createValue(1);
		
		else if(value == AbstractBoolean.False)
			result = Interpreter.getActiveAnalysis().createDomainFactory().createValue(0);
		
		else
			result = Interpreter.getActiveAnalysis().createDomainFactory().createValue(0, 1);
		
		return result;
	}
	
	/**
	 * Assigns this register to a new value.
	 */
	public void setValue(AbstractBoolean newValue)
	{
		value = newValue;
	}

	/**
	 * String representation
	 */
	public String toString()
	{
		return value.toString();
	}
}

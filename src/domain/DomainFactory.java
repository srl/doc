package domain;


/**
 * Abstract factory object. Provides interface for constructing
 * objects of the target domain.
 */
public abstract class DomainFactory 
{
	/**
	 * Creates a new instance of State.
	 */
	abstract public State createState();
	
	/**
	 * Creates a new instance of Value.
	 */
	abstract public Value createValue();
	
	/**
	 * Creates a new instance of Value, initialized to input.
	 */
	abstract public Value createValue(int init);
	
	/**
	 * Creates a new instance of Value, initialized to a range of numbers.
	 */
	abstract public Value createValue(int start, int end);
}

/*
 * Created on Mar 11, 2005
 */
package domain;

import java.util.Iterator;

import x86InstructionSet.AddressList;
import x86InstructionSet.Instruction;

import abstractBoolean.AbstractBoolean;

/**
 * Provides some basic functionality that other states can inherit from.
 */
public abstract class AbstractState implements Cloneable, State
{
  /** storage for flag registers. */
	private FlagRegisterStore	flagRegisters = new FlagRegisterStore();
	
    /* (non-Javadoc)
     * @see domain.State#clone()
     */
    public Object clone() 
    {
        try
        {
            return super.clone();
        }
        catch(CloneNotSupportedException e)
        {
            throw new Error("This should never happen");
        }
    }
    
    /* (non-Javadoc)
     * @see domain.State#createNewValue()
     */
    public abstract Value createNewValue();
    
    /* (non-Javadoc)
     * @see domain.State#createNewValue(long)
     */
    public abstract Value createNewValue(long v);
    
    /* (non-Javadoc)
     * @see domain.State#getFlagValue(int)
     */
    public AbstractBoolean getFlagValue(int flagRegister)
    {
    	return flagRegisters.getRegisterValue(flagRegister);
    }
    
	/* (non-Javadoc)
   * @see domain.State#getFlagValueInteger(int)
   */
    public Value getFlagValueInteger(int flagRegister)
    {
    	return flagRegisters.getRegisterValueInteger(flagRegister);
    }
    
    /* (non-Javadoc)
     * @see domain.State#getRegisterValue(int)
     */
    public abstract Value getRegisterValue(Register register);

    /* (non-Javadoc)
     * @see domain.State#getReturnAddressAsList()
     */
    public abstract AddressList getReturnAddressAsList();
    
    /* (non-Javadoc)
     * @see domain.State#getStackTopCreators()
     */
    public abstract Instruction[] getStackTopCreators();
    
    /* (non-Javadoc)
     * @see domain.State#init()
     */
    public abstract void init();

    /* (non-Javadoc)
     * @see domain.State#iterator()
     */
    public abstract Iterator iterator(); 
    
    /* (non-Javadoc)
     * @see domain.State#pop()
     */
    public abstract Value pop();
    
    /* (non-Javadoc)
     * @see domain.State#push(domain.Value)
     */
    public abstract void push(Value value);
    
    /* (non-Javadoc)
     * @see domain.State#setRegisterValue(int, domain.Value)
     */
    public abstract void  setRegisterValue(Register register, final Value newVal);
    
    /* (non-Javadoc)
     * @see domain.State#setFlagValue(int, abstractBoolean.AbstractBoolean)
     */
    public void setFlagValue(int flagRegister, AbstractBoolean value)
    {
    	flagRegisters.setRegisterValue(flagRegister, value);
    }
    
    /* (non-Javadoc)
     * @see domain.State#load(domain.Value, domain.BitCount)
     */
    public abstract Value load(Value source, BitCount size);
    
    /* (non-Javadoc)
     * @see domain.State#merge(domain.State)
     */
    public abstract void merge(final State rhs);
    
    /* (non-Javadoc)
     * @see domain.State#overapproximates(domain.State)
     */
    public abstract boolean overapproximates(final State rhs);
    
    /* (non-Javadoc)
     * @see domain.State#store(domain.Value, domain.Value)
     */
    public abstract void  store(Value dest, Value value);
    
    /* (non-Javadoc)
     * @see domain.State#widen(domain.State)
     */
    public abstract void widen(final State rhs);
}
/*
 * BitSize.java
 * Created on May 22, 2005
 */
package domain;

/**
 * Stores a size in bits.
 */
public class BitCount
{
    public static BitCount _32Bits = new BitCount() 
    {
        public boolean equals(Object rhs)
        {
            return rhs == this;
        };
    };
    
    public static BitCount _16Bits = new BitCount() 
    {
        public boolean equals(Object rhs)
        {
            return rhs == this;
        };
    };
    
    public static BitCount _8Bits = new BitCount() 
    {
        public boolean equals(Object rhs)
        {
            return rhs == this;
        };
    };
}

package domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ui.registers.RegisterValueContextPair;

/**
 * Defines all possible registers.
 */
public enum Register
{
  EAX, EBX, ECX, EDX, ESP, EBP, ESI, EDI,
  CS, DS, ES, FS, GS, SS,
  AX, AL, AH, BX, BL, BH, CX, CL, CH, DX, DL, DH,
  SP, BP, SI, DI;

  /** Number of registers */
  public static final int NUM_REGISTERS = values().length;
//  public static final Register regList[] = {EAX, EBX, ECX, EDX, ESP, EBP, EDI, ESI};
  public static ArrayList<Register> regList = 
		  new ArrayList<Register>(Arrays.asList(EAX, EBX, EBP, EDI, ESI, ESP));
}

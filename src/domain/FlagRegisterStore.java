/*
 * FlagRegisterStore
 * Created on Jul 20, 2005
 */
package domain;

import abstractBoolean.AbstractBoolean;


/**
 * Maintains the collection of flag registers.
 */
public class FlagRegisterStore 
{
	public static final int AUXILIARY_FLAG 			= 0;
	public static final int CARRY_FLAG 				= 1;
	public static final int OVERFLOW_FLAG			= 2;
	public static final int SIGN_FLAG				= 3;
	public static final int PARITY_FLAG				= 4;
	public static final int ZERO_FLAG				= 5;
	public static final int DIRECTION_FLAG			= 6;
	public static final int INTERUPT_ENABLE_FLAG 	= 7;
	public static final int TRAP_FLAG				= 8;

	public static final int REGISTER_COUNT			= 9;
	
	private FlagRegister	registers[]; 
	
	/**
	 * Creates this.
	 *
	 */
	public FlagRegisterStore()
	{
		// initialize registers array.
		registers = new FlagRegister[REGISTER_COUNT];
		for(int i = 0; i < REGISTER_COUNT; i++)
			registers[i] = new FlagRegister();
	}
	
	/**
	 * Returns the value in the input register.
	 */
	public AbstractBoolean getRegisterValue(int register)
	{
		if(register < 0 || register >= REGISTER_COUNT)
			throw new IllegalArgumentException("parameter 'register' is out of range.");
		return registers[register].getValue();
	}
	
	/**
	 * Returns this register's value as a Value containing integers, 
	 * instead of an AbstractBoolean.  The translation from 
	 * AbstractBoolean to Value is:
	 *  Top		--> {0,1}
	 *  True	--> {1}
	 *  False	--> {0}
	 *  Bottom	--> {0,1}
	 */
	public Value getRegisterValueInteger(int register)
	{
		if(register < 0 || register >= REGISTER_COUNT)
			throw new IllegalArgumentException("parameter 'register' is out of range.");
		return registers[register].getInteger(); 
	}
	
	/**
	 * Sets the value for the input register.
	 */
	public void setRegisterValue(int register, AbstractBoolean value)
	{
		if(register < 0 || register >= REGISTER_COUNT)
			throw new IllegalArgumentException("parameter 'register' is out of range.");
		registers[register].setValue(value);
	}

	/**
	 * String representation
	 */
	public String toString()
	{
		String			prefix;
		StringBuffer 	result = new StringBuffer();

		result.append("[");
		if(registers.length > 0)
		{
			result.append("Auxiliary flag=" + registers[0]);
			for(int i = 1; i < registers.length; i++)
			{
				switch(i)
				{
					case 0:	prefix = "Auxiliary flag=";	break;
					case 1:	prefix = "Carry flag=";		break;
					case 2:	prefix = "Overflow flag=";	break;
					case 3:	prefix = "Sign flag=";		break;
					case 4:	prefix = "Parity flag=";	break;
					case 5:	prefix = "Zero flag=";		break;
					case 6:	prefix = "Direction flag=";	break;
					case 7:	prefix = "Interupt enable flag=";	break;
					case 8:	prefix = "Trap flag=";	break;
					default: throw new Error("This should never happend");
				}
				result.append(", " + prefix + registers[i]);
			}
		}
		result.append("]");
		
		return result.toString();
	}
}

/*
 * Value.java
 * Created on Mar 11, 2005
 */
package domain;

import abstractBoolean.AbstractBoolean;

/**
 * Represents the contents within registers and memory.  Each new domain
 * should provide a class that implements this interface.  Functions 
 * should be provided for manipulating <code>Values</code> (and their associated state)
 * in an implementation-independent way.  
 */
public interface Value extends Cloneable
{
    /**
     * Adds two values
     */
    public Value add(final Value rhs);
   
    /**
     * Returns a clone of this object.
     */
    public Object clone();
    
    /**
     * Divides this value by another value.
     */
    public Value divide(final Value rhs);
    
    /**
     * Returns true if <code>this</code> equals rhs. 
     */
    public boolean equals(Object rhs);

    /**
     * Returns the size of this value in bits.  
     */
    public BitCount getBitSize();
    
    /**
     * Returns this == rhs.
     * @return AbstractBoolean.True if this and rhs both contain only one
     * 				member AND they are the same number.<br>
     *         AbstractBoolean.False if there is no member of this that is
     *         		equal to some member of rhs.<br>
     *         AbstractBoolean.Top   if there is a member of this that is
     *         		equal to some member of rhs AND there is a member of this
     *         		that is not equal to some member of rhs.
     */
    public AbstractBoolean isEqualTo(Value rhs);
    
    /**
     * Returns this > rhs.
     * @return AbstractBoolean.True if all members of this
     *      		are greater than all members of rhs.<br>
     *         AbstractBoolean.False if there is no member of this
     *         		greater than at least one member of rhs.<br>
     *         AbstractBoolean.Top   if there is at least one member of
     *         		this that is greater than at least one member of rhs AND
     *         		there is at least one member of this that is not greater 
     *         		than at least one member of rhs.
     */
    public AbstractBoolean isGreaterThan(final Value rhs);
    
    /**
     * Returns this &lt; rhs.  
     * @return AbstractBoolean.True if all members of this
     *      		are less than all members of rhs.<br>
     *         AbstractBoolean.False if there is no member of this
     *         		less than at least one member of rhs.<br>
     *         AbstractBoolean.Top   if there is at least one member of
     *         		this that is less than at least one member of rhs AND
     *         		there is at least one member of this that is not less 
     *         		than at least one member of rhs.
     */
    public AbstractBoolean isLessThan(final Value rhs);

    /**
     * Returns an iterator that can be used to traverse all numbers represented 
     * by this Value.
     */
    public Iterator iterator();
    
    /** 
     * Iterator for traversing all possible values represented by
     * this <code>Value</code> as if they were a set of <code>long</code>s.
     */
    public interface Iterator
    {
        public boolean 	hasNext();
        public long	 	next();
    }
    
    /**
     * Multiplies two values
     */
    public Value multiply(final Value rhs);
    
    /**
     * Assigns this to a constant
     */
    public void set(long value);
    
    /**
     * Assigns this value to the same value represented by newVal
     */
    public void set(Value newVal);
    
    /**
     * Assigns the size of this value in bits.
     */
    public void setBitSize(BitCount size);
    
    /**
     * Returns the number of elements represented by this Value.
     * If the value contains an infinite number of elements, then
     * 0 is returned.
     */
    public long size();

    /**
     * Subtracts a value from this value.
     */
    public Value subtract(final Value rhs);

    /**
     * Returns a String representation of this Value.
     */
    public String toString();

    /**
     * Returns the XOR of this and rhs.
     * @return this XOR rhs
     */
    public Value xor(final Value rhs);

	public Value or(final Value rhs);

	public Value and(final Value rhs);
}

package analyzer;

/**
 * A result obtained by an <code>Analyzer</code>. Each <code>Analyzer</code>
 * return its own result.  Since each result is specified to the 
 * <code>Analyzer</code> that produces it, this interface contains no
 * methods. Thus, the class that receives the <code>AnalysisResult</code>
 * from the <code>Analyzer</code> will have to cast it to the correct
 * type before using it. 
 */
public interface AnalysisResult
{
}

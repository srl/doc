package analyzer;

import x86InstructionSet.Program;

/**
 * Analyzes a program for specific behavior or attributes. 
 * Returns a result specific to the <code>Aalyzer</code>.
 * Each <code>Analysis</code> can provide its own 
 * <code>Analyzer</code> to perform any pre or 
 * post-analysis work.
 */
public interface Analyzer
{
  /**
   * Analyzes the input program.
   * @param program the program to be analyzed.
   * @return the result.  
   */
  public AnalysisResult analyze(Program program);
}
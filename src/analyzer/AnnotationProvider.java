package analyzer;

import org.eclipse.core.resources.IResource;

import ui.programViewer.ProgramViewer;

/**
 * Capable of updating/creating the annotations on a resource given
 * an <code>AnalysisResult</code>  
 */
public interface AnnotationProvider
{
  /**
   * Annotates a resource using the information supplied from
   * an <code>AnalysisResult</code>.
   * @param resource the resource to be annotated.
   * @param result the result from an <code>Analyzer</code>
   * @param viewer the viewer.
   */
  public void annotate(IResource resource, AnalysisResult result, ProgramViewer viewer);
}

package csAnalysisDomain;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;


import x86InstructionSet.Instruction;
import x86Interpreter.InstructionHistoryList;
import x86Interpreter.Interpreter;
import domain.State;

/**
 * Determines if an instruction has been interpreted by lookin at
 * context.  This class is intended for use ONLY by the classes
 * in the contextSensitiveAnalysisDomain package and may not function correctly
 * if used outside that package. 
 */
public class ContextAwareInstructionHistoryList implements InstructionHistoryList {

	public void interpreted(Instruction instruction) {
		//		 Nothing to do here.  

	}

	/**
   * Determines if an instruction has already been interpreted by looking at
   * the context.  If the instruction has been interpreted already, but in a 
   * different context, then false is returned.   If an instruction has
   * been intepreted in the same context as the current state, then true
   * is returned.
	 */
	public boolean isInterpreted(Instruction instruction, State state) 
  {
	String stackString = ((csAnalysisDomain)state).getContextString();

    if(instruction.getBeginState() != null)
    {
    	  Entry<String, ArrayList> entryContext;
		  Iterator<Entry<String, ArrayList>> iterContext;
		  ArrayList list;
		  String addressInstruction = instruction.getAddress().toString();
		
		  Map<String, ArrayList> map = Interpreter.getMappingAddress();
		  list = map.get(addressInstruction);  

		  if (list != null && list.contains(stackString)) return true;
    }
    return false;

	}
		
		
}

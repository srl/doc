package csAnalysisDomain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import controlFlowGraph.ControlFlowGraph;
import controlFlowGraph.ControlFlowGraphNode;
import vsaAsgDomain.VsaAsgState;
import x86InstructionSet.Address;
import x86InstructionSet.Instruction;
import x86InstructionSet.Program;
import x86InstructionSet.RetInstruction;
import x86InstructionSet.UnknownAddress;
import x86Interpreter.ForwardAnalysisWorklist;
import x86Interpreter.NodeDependencyTable;
import x86Interpreter.WorkListElement;
import x86Interpreter.Worklist;
import domain.State;

/**
 * Worklist designed specificaly for the ContextSensitiveAnalysisDomain.
 * This worklist does not use the control flow graph when deciding which
 * instructions to add to the worklist, because the control flow graph 
 * is not context-sensitive.
 */
public class ContextSensitiveWorklist implements Worklist
{
  /* (non-Javadoc)
   * @see x86Interpreter.Worklist#update(x86InstructionSet.Program, x86InstructionSet.Instruction)
   */

	   private LinkedList<WorkListElement> elements = new LinkedList<WorkListElement>();
	    
	    /* (non-Javadoc)
	     * @see x86Interpreter.IWorklist#add(x86Interpreter.WorkListElement, x86Interpreter.NodeDependencyTable, controlFlowGraph.ControlFlowGraph)
	     */
	    public void add(final WorkListElement element)
	    {
	 
	    	WorkListElement item;
	    	item = find(element.getInstruction().getAddress(), element.getState());
	    	if(item != null){
	    			
	    		
				item.getState().merge(element.getState());
				
				Entry<String, VsaAsgState> entry;
				Iterator<Entry<String, VsaAsgState>> iter;	
				
				Map<String, VsaAsgState> mapitem = ((csAnalysisDomain) item.getState()).getMapping();
				Map<String, VsaAsgState> mapelem = ((csAnalysisDomain) element.getState()).getMapping();
				
				iter = mapelem.entrySet().iterator();
				
				while(iter.hasNext())
				{
					entry = iter.next();
					String stackString = entry.getKey();
					if (!(mapitem.containsKey(stackString))){
						mapitem.put(stackString, (VsaAsgState) mapelem.get(stackString).clone());
					}
				}
				
	    	}   else 	      	elements.addLast(element);
	    		    	
	    }
	    
	    /* (non-Javadoc)
	     * @see x86Interpreter.IWorklist#getNext(x86Interpreter.NodeDependencyTable, controlFlowGraph.ControlFlowGraph)
	     */
	    public WorkListElement getNext(NodeDependencyTable dependencyTable, ControlFlowGraph cfg)
	    {
	    	int length = 0;
	    	int lengthtemp = 0;
	    	State state;
	    	Iterator iter = elements.iterator();
	    	WorkListElement next = null;
	    		   	    	
	    	while(iter.hasNext()) {
	    		WorkListElement worklist = 	(WorkListElement)iter.next();		
	    		state = worklist.getState();
	    		String stackString = ((csAnalysisDomain) state).getContextString();
	    		length = stackString.length();
	    		
	    		if (stackString.contains("+")) length += 100;
	    		
	    		if (length > lengthtemp) {
	    			next = worklist;
	    			lengthtemp = length;
	    		}
	    	
	    	}
	    	
	       	remove(next.getInstruction().getAddress(), next.getState());
	       	return next;
	    }    

	    /* (non-Javadoc)
	     * @see x86Interpreter.IWorklist#find(x86InstructionSet.Address)
	     */
	    public WorkListElement find(Address address, State state)
	    {
	    	Iterator iter;
	  		WorkListElement next;
	    	
	    	iter = elements.iterator();
	    	while(iter.hasNext())
	  		{
	    		next = (WorkListElement)iter.next();
	    		String stackString = ((csAnalysisDomain)next.getState()).getContextString();
	    		
   		   		if ((next.getInstruction().getAddress().equals(address)) && ((csAnalysisDomain)state).getContextString().equals(stackString) )
	    					return next;
	  		}
	    	return null;
	    }
	    //	   )
	    
	    /* (non-Javadoc)
	     * @see x86Interpreter.IWorklist#has(x86InstructionSet.Address)
	     */
	    public boolean has(Address address, State state)
	    {
	    	return find(address, state) != null;
	    }

	    /* (non-Javadoc)
	     * @see x86Interpreter.Worklist#init(x86InstructionSet.Program)
	     */
	    public void init(Program program, State initialState, Address initialInstruction)
	    {
	      if(!initialInstruction.equals(UnknownAddress.UNKNOWN))
	        add(new WorkListElement(program.getInstruction(initialInstruction), 
	            (State)initialState.clone())); 
	    }
	    
	    /* (non-Javadoc)
	     * @see x86Interpreter.IWorklist#remove(x86InstructionSet.Address)
	     */
	    public boolean remove(Address address, State state)
	    {
	    	Iterator iter = elements.iterator();
	    	while(iter.hasNext())
	    	{
	    		WorkListElement next = (WorkListElement) iter.next();
	    		String stackString = ((csAnalysisDomain)next.getState()).getContextString();
	    		
	       		
	    		if ( ( next.getInstruction().getAddress().equals(address) )
	    				&& (((csAnalysisDomain)state).getContextString().equals(stackString)) )
					{
	    			iter.remove();
	    			return true;
					}
	    	}
	    	return false;
	    }
	    
	    /* (non-Javadoc)
	     * @see x86Interpreter.Worklist#size()
	     */
	    public int size()
	    {
	      return elements.size();  
	    }
	    
	    /* (non-Javadoc)
	     * @see x86Interpreter.IWorklist#isEmpty()
	     */
	    public boolean isEmpty()
	    {
	        return elements.isEmpty();
	    }

	public void update(Program program, Instruction lastInstruction, Map<Address, State> next)
	{
	Instruction instruction;
	State state;
	Entry<Address, State> entry;
	Iterator<Entry<Address, State>> iter;
	
	//updateMapping(lastInstruction, lastInstruction.getEndState());
	
    iter = next.entrySet().iterator();
    	
    	while(iter.hasNext()){
    		entry = iter.next();
    		instruction = program.getInstruction( entry.getKey() );
    		state = (State) entry.getValue();
    		    		
    		add( new WorkListElement( instruction, (State)state.clone() ) );
    	 	    	
    	}
	}
	
    /**
     * string representation
     */
    public String toString()
    {
        return elements.toString();
    }
	
   
	/**
	 *  @return mapping of Address to stack strings
	 */
//	public static Map getMappingAddress(){
		
	//	return mapping;
			
//	}
	
	/**
	 *  @return mapping of stack strings to context
	 */
	//public static Map getMappingContexts(){
		
		//return map;
			
	//}
	
	



}











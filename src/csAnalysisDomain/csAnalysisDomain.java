package csAnalysisDomain;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import domain.AbstractState;
import domain.BitCount;
import domain.Register;
import domain.State;
import domain.Value;
import vsaAsgDomain.AbstractStackGraph;
import vsaAsgDomain.IMemoryTable;
import vsaAsgDomain.IStackLocationSet;
import vsaAsgDomain.StackLocation;
import vsaAsgDomain.VsaAsgState;
import x86InstructionSet.Instruction;

public class csAnalysisDomain extends VsaAsgState {

	/**
	 * The mapping for each stack-string constructed to certain state in the VsaAsgState.    
	 */
	protected Map<String, VsaAsgState> map = new HashMap<String, VsaAsgState>();
	/**
	 * The stack-string to keep the contexts based on the program-locations (program labels)
	 */
	protected String contextString;

	public csAnalysisDomain() {
		super();
	}

	/**
	 * Returns a copy of this State.
	 */
	public Object clone() {
			csAnalysisDomain clone;
			
			clone = (csAnalysisDomain)super.clone();
			
		    clone.map = new HashMap<String, VsaAsgState>();
		    
			Entry<String, VsaAsgState> entry;
			Iterator<Entry<String, VsaAsgState>> iter;
			iter = map.entrySet().iterator();
						
			while(iter.hasNext())
			{
				entry = iter.next();
				String stackStringCloned = new String (entry.getKey());
				clone.map.put(stackStringCloned, 
							(VsaAsgState) entry.getValue().clone());
			}
			
			clone.contextString = contextString;
				
			return clone;
	}

	/**
	 * Returns a reference to the AbstractStackGraph of this state based on the current context.
	 */
	public AbstractStackGraph getAbstractStackGraph() {
		return get(getContextString()).getAbstractStackGraph();
	}

	/**
	 * Returns a reference to the internal memory table based on the current context.
	 */
	public IMemoryTable getMemoryTable() {
		return get(getContextString()).getMemoryTable();
	}

	/**
	 * Returns the "current" context string string of this. The current context is 
	 * used get the current VsaState.
	 */
	public String getContextString() {
		return contextString;
	}

	public String changeContextString(String string) {
		contextString = string;
		return contextString;
	}

	/**
	 * Accesses the mapping of stack-strings to VsaASgStates.
	 */
	public Map getMapping() {
		return map;
	}

	/**
	 * Returns the state corresponding to the context
	 * 
	 * @return State mapped to the context, to be modified by x86 operations. 
	 *  Null if no mapping exists.
	 */
	public VsaAsgState get(String context){
		return map.get(context);
	}
	
	/**
	 * Returns the current active state by looking at the mapping
	 * 
	 * @return Active state to be modified by x86 operations.  Null if
	 * 		   no mapping exists.
	 */

	public VsaAsgState getCurrentState() {
        return map.get(contextString);
	}
	/**
	 * Returns the stack location that is the top of the stack.  This method
	 * assumes that there is only one stack location at the top of the stack.
	 * That will be true in only some situations (such as immediately following
	 * a push instruction).  Returns null if there is more than one stack location
	 * or less than one stack location at the top of the stack.
	 */
	protected StackLocation getStackTop(VsaAsgState state) {
		IStackLocationSet stackTops;
		StackLocation stackTop = null;
		
		stackTops = state.getStackTop();
		if(stackTops.size() == 1)
			stackTop = stackTops.iterator().next();
		
		return stackTop;
	}

	/**
	 * Returns a new, unitialized Value designed to work with this State.
	 */
	public Value createNewValue() {
		return new vsaAsgDomain.Value();
	}

	/**
	 * Returns a new, initialized value designed to work with this State.
	 */
	public Value createNewValue(long v) {
		return new vsaAsgDomain.Value(v);
	}

	/**
	 * Returns the value of the register by looking for its value in the current state
	 * 
	 */
	public Value getRegisterValue(Register register) {
		return (Value) get(getContextString()).getRegisterValue(register);
	}

	/**
	 * Accesses the stack locations at the top of the stack based on the current context.
	 */
	public IStackLocationSet getStackTop() {
		return get(getContextString()).getStackTop();
	}

	/**
	 * Returns the instructions that created the values on the top of the stack.
	 */
	public Instruction[] getStackTopCreators() {
		return get(getContextString()).getStackTopCreators();
	}

	/**
	 * Resets any initialization that might be needed.  This should be
	 * called prior to perform any interpretation.  If a second file 
	 * is interpreted, then this method should be called again. 
	 * Any registers/memory initialization takes place here.
	 */
	public void init() {
		VsaAsgState initialState = new VsaAsgState();
		contextString = new String("T");
	    map.clear();
		map.put(contextString, initialState);
		
		super.init();
		initialState.init();
	
	}

	/**
	 * @see AbstractState#iterator()
	 */
	public Iterator iterator() {
		
		return get(getContextString()).iterator();
		
	}

	/**
	 * @see domain.AbstractState#load(Value, BitCount)
	 */
	public Value load(Value source, BitCount size) {
		
		return (Value) get(getContextString()).load(source,size);
		
	}

	/**
	 * Widens this state with rhs checking if is the same context. If is the same the widen 
	 * is performed. Otherwise not.
	 */
	public void widen(State rhs) {
	
		csAnalysisDomain rhsIn = (csAnalysisDomain)rhs;
		if (getContextString().equals(rhsIn.getContextString())){
			get(getContextString()).widen(rhsIn.get(getContextString()));
		
		}
			
			
	}

	/**
	 * Merges this state with rhs checking if is the same context. If is the same the merge 
	 * is performed. Otherwise not.
	 */
	public void merge(State rhs) {
		
		csAnalysisDomain rhsIn = (csAnalysisDomain)rhs;
		if (getContextString().equals(rhsIn.getContextString()))
			get(getContextString()).merge(rhsIn.get(getContextString()));
	}

	/**
	 * Returns true if this state is either an over-approximation
	 * of rhsIn or is equal to rhsIn.  A state over-approximates another state if
	 * each member of this state over-approximates each member in the other state.
	 */
	public boolean overapproximates(State rhs) {
		
		csAnalysisDomain rhsIn = (csAnalysisDomain)rhs;
		if (getContextString().equals(rhsIn.getContextString()))
			return get(getContextString()).overapproximates(rhsIn.get(getContextString()));
		else
			return false;
				
	}

	/**
	 * Assigns a value to a register.
	 * @param register The register being written to.  Must be >= REGISTER_MIN and
	 * 			<= REGISTER_MAX.  See detectObfuscation.State for possible values.
	 * @param newValIn The value to be written.
	 */
	public void setRegisterValue(Register register, Value newVal) {
		
		get(getContextString()).setRegisterValue(register, newVal);
	}

	/**
	 * Stores a specified value at the specified destination address.
	 */
	public void store(Value dest, Value value) {
		
		get(getContextString()).store(dest, value);
		
	}

	/**
	 * Updates the mapping so that the given stack string is mapped to
	 * the given state.
	 * 
	 * @param stackString
	 * @param state
	 */
	public void updateAddMapping(String stackString, VsaAsgState state) {
		
		map.put(stackString, state);
	
	}

	public String toString() {
		return "<" + contextString + ">";
	}

}
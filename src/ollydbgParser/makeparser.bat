@echo off
rem runs jjtree & javacc on the input file x86.jjt & x86.jj.  
rem Once done, it places the text "package x86Parser;" 
rem at the beginning of the newly created file x86Parser.java.  
rem (This programs assumes all output files will be placed in
rem a package called x86Parser.)

call jjtree x86.jjt
if errorlevel 1 exit

echo.
call javacc x86.jj
if errorlevel 1 exit

rem This part prepends the .java files
rem with the package name.
rem This is necessary because javacc recreates some files from
rem scratch everytime, so the package name must be added back 
rem everytime.

for %%f in (*.java) do call prepend.bat %%f "package ollydbgParser;"

echo.
echo DONE.
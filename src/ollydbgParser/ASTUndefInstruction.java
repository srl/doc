package ollydbgParser;  

import java.util.Map;

import instructionInterpreters.InstructionInterpreter;
import ui.outputters.Outputter;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.Operand;
import domain.State;

/* Generated By:JJTree: Do not edit this line. ASTUndefInstruction.java */

public class ASTUndefInstruction  extends Instruction implements x86InstructionSet.UndefInstruction 
{
  public ASTUndefInstruction(int id) {
    super(id);
  }

  public ASTUndefInstruction(X86Parser p, int id) {
    super(p, id);
  }


  /* (non-Javadoc)
   * @see x86InstructionSet.Instruction#getNextQuickly(x86InstructionSet.InstructionAddress)
   */
  public AddressList getNextQuickly(Address nextAddress) 
  {
    return new AddressList(nextAddress);
  }

  /* (non-Javadoc)
   * @see ollydbgParser.UndefInstruction#getDstOperand()
   */
  public Operand getDstOperand()
  {
    return (Operand)jjtGetChild(0);
  }
  
  /* (non-Javadoc)
   * @see ollydbgParser.UndefInstruction#interpret(domain.State, ollydbgParser.Address)
   */
  public Map<Address, State> interpret(InstructionInterpreter interpreter,
      State state, Address nextAddress)
  {
    return interpreter.interpret(this, state, nextAddress);
  }
  
  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  public String toString()
  {
    return Outputter.getInstance().toString(this);
  }
}

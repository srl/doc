/*
 * Created on Mar 8, 2004
 */

package ollydbgParser;


import java.io.FileNotFoundException;

import parser.Parser;
import x86InstructionSet.Instruction;
import x86InstructionSet.Program;
import x86InstructionSet.UnknownAddress;

/**
 * Parses a .asm file and returns a Program representing the parsed file.
 */
public class OllydbgParser extends Parser {
	
	public OllydbgParser() {
	}
	
	/* (non-Javadoc)
   * @see ollydbgParser.PParser#parse(java.lang.String, x86InstructionSet.Program)
   */
	public Program parse(String filepath, Program initialProgram) throws FileNotFoundException, ParseException
	{
	    return parse(new java.io.FileReader(new java.io.File(filepath)), initialProgram);
	}

	/* (non-Javadoc)
   * @see ollydbgParser.PParser#parse(java.io.Reader, x86InstructionSet.Program)
   */
	public Program parse(java.io.Reader stream, Program initialProgram) throws ParseException
	{
	    int 		numOfInstructions;	// number of instructions read from file. 
	    X86Parser 	parser;
	    
        parser = new X86Parser(stream);

        try
        {
            parser.Parse();
        }
        catch(Error e)
        {
            throw new ParseException(e.getMessage());
        }
	    
	    numOfInstructions = parser.jjtree.rootNode().jjtGetNumChildren();
	    for(int i = 0; i < numOfInstructions; i++)
	        initialProgram.addInstruction( (Instruction)parser.jjtree.rootNode().jjtGetChild(i) );
	    
	    if( !parser.entryPointAddress.equals(UnknownAddress.UNKNOWN) )
	        initialProgram.setEntryPoint(parser.entryPointAddress);
	    
	    return initialProgram;	    
	}
	
	/* (non-Javadoc)
   * @see ollydbgParser.PParser#parse(java.lang.String)
   */
	public Program parse(String filePath) throws FileNotFoundException, ParseException
	{
	    return parse(new java.io.FileReader(new java.io.File(filePath)));
	}

	/* (non-Javadoc)
   * @see ollydbgParser.PParser#parse(java.io.Reader)
   */
	public Program parse(java.io.Reader stream) throws ParseException
	{
	    return parse(stream, new Program());
	}
}

package instructionInterpreters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import lCallContextAnalysisDomain.LCallContext;



import vsaAsgDomain.VsaAsgState;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.MemoryOperand;
import x86InstructionSet.MovInstruction;
import x86InstructionSet.Operand;
import x86InstructionSet.RegisterOperand;
import x86InstructionSet.RetInstruction;
import domain.Register;
import domain.State;
import domain.Value;

/**
 * Knows how to interpreter x86 instructions.  Uses context-sensitivity
 * when interpreting RET instructions so that each calling instruction
 * is given the correct state when returning. 
 */
public class LCallInterpreter extends ContextInsensitiveInterpreter
{
	
	static final Map<String, State> contexts = new HashMap<String, State>();
	
  /**
   * Interprets a RET instruction.
   * 
   * @param instruction   The instruction to interpret.
   * @param state         The incoming state.
   * @param nextAddress   The address of the very next instruction in the program.
   *                      This parameter is ignored by this method.
   * @return              A mapping of addresses to states.  The addresses belong
   *                      to the instructions that should be added to the worklist.
   *                      Each address is mapped to a separate state.  The state
   *                      is the incoming state that should be used when interpreting
   *                      each instruction. 
   *                   
   */
  public Map<Address, State> interpret(RetInstruction instruction, State state, Address nextAddress)
  

    // This method interpret a RET instruction and return the
    // addresses of the instructions to interpret next.  Each address is mapped
    // to a state.  The addresses are obtained by calling state.getReturnAddressesAsList.
    // The state for each instruction is obtained by the method Context.get(Address).  This method 
    // create a new Context object that ONLY contains the state for the given address.  
  	{
  
    
    // This instruction interpreter assumes that each return address will use
    // its state belonging to its context  (context-sensitive).  
    
	    Value       esp;
	    AddressList   nextAddresses  = new AddressList();
	    Map<Address, State> next = new HashMap<Address, State>();
	    nextAddresses = state.getReturnAddressAsList();
	    String stackString = ((LCallContext) state).getContextString();

	    
	    if (stackString.endsWith("+")) {
	   	 
	       	Iterator<String> iter;
	    	ArrayList<String> contexts = ((LCallContext)state).findContexts();
	    	
	    	for ( iter = contexts.iterator(); iter.hasNext(); ) {
	    		String iteration = iter.next();
	    		LCallContext context = (LCallContext) state.clone();
	    		
	    		if (!(iteration.endsWith("+"))) {
	    			context.pop();
	    			VsaAsgState vsaState = (VsaAsgState) context.get(stackString);
	    			context.updateAddMapping(iteration,  vsaState);
		    		context.changeContextString(iteration);
		    		
		    	} 
	    		context.changeTop();
	    		
	    		for(Address address : nextAddresses) 
	    			next.put((Address) address.clone(), context);
	    		
	    	}
	    	
	    	return next;
	    }
	    
	  	state.pop();
		    
	  	if(instruction.getDstOperand() != null)
		    {
		      esp = state.getRegisterValue(Register.ESP);
		      state.setRegisterValue( Register.ESP,
		          esp.add(instruction.getDstOperand().interpret(state)) );
		    }
	  	
	  	((LCallContext)state).changeTop();
		    // just one address because the return address is already right because the stack-context
		for(Address address : nextAddresses)
			      next.put(address, state);
  	
	    return next;
    	
  	}


/* (non-Javadoc)
 * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MovInstruction, domain.State, x86InstructionSet.Address)
 */
public Map<Address, State> interpret(MovInstruction instruction, State state,
    Address nextAddress)
{
  Map<Address, State> next = new HashMap<Address, State>();
  Operand dstOperand;
  Operand srcOperand;
  
  dstOperand = instruction.getDstOperand();
  srcOperand = instruction.getSrcOperand();
  
  if(dstOperand instanceof RegisterOperand)
    state.setRegisterValue( ((RegisterOperand)dstOperand).getRegister(), 
            srcOperand.interpret(state));

  else if(dstOperand instanceof MemoryOperand)
  {
    state.store( ((MemoryOperand)dstOperand).getTarget(state), 
        srcOperand.interpret(state) );
  }

  else
  {
    System.out.println("MovInstruction.interpret: jjtGetChild(0) is of an unknown type.");
    throw new Error("MovInstruction.interpret: jjtGetChild(0) is of an unknown type.");
  }

  next.put(nextAddress, state);
  return next;
}
}




package instructionInterpreters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import csAnalysisDomain.csAnalysisDomain;


import lStackContextAnalysisDomain.LStackContext;
import logger.Log;

import vsaAsgDomain.IReducedIntervalCongruence;
import vsaAsgDomain.IStackLocationSet;
import vsaAsgDomain.StackLocation;
import vsaAsgDomain.StackLocationSet;
import vsaAsgDomain.VsaAsgState;
import x86InstructionSet.AddInstruction;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.Memory16Operand;
import x86InstructionSet.Memory32Operand;
import x86InstructionSet.Memory8Operand;
import x86InstructionSet.MemoryOperand;
import x86InstructionSet.Operand;
import x86InstructionSet.PopInstruction;
import x86InstructionSet.Register16Operand;
import x86InstructionSet.Register32Operand;
import x86InstructionSet.Register8Operand;
import x86InstructionSet.RegisterOperand;
import x86InstructionSet.RetInstruction;
import x86InstructionSet.SubInstruction;
import domain.BitCount;
import domain.Register;
import domain.State;
import domain.Value;

/**
 * Knows how to interpreter x86 instructions.  Uses context-sensitivity
 * when interpreting RET instructions so that each calling instruction
 * is given the correct state when returning. 
 */
public class LStackInterpreter extends ContextInsensitiveInterpreter
{
		
  /**
   * Interprets a RET instruction.
   * 
   * @param instruction   The instruction to interpret.
   * @param state         The incoming state.
   * @param nextAddress   The address of the very next instruction in the program.
   *                      This parameter is ignored by this method.
   * @return              A mapping of addresses to states.  The addresses belong
   *                      to the instructions that should be added to the worklist.
   *                      Each address is mapped to a separate state.  The state
   *                      is the incoming state that should be used when interpreting
   *                      each instruction. 
   *                   
   */
  public Map<Address, State> interpret(RetInstruction instruction, State state, Address nextAddress)
  
    // This method interpret a RET instruction and return the
    // addresses of the instructions to interpret next.  Each address is mapped
    // to a state.  The addresses are obtained by calling state.getReturnAddressesAsList.
    // The state for each instruction is obtained by the method Context.get(Address).  This method 
    // create a new Context object that ONLY contains the state for the given address.  
  	{
  
    
    // This instruction interpreter assumes that each return address will use
    // its state belonging to its context  (context-sensitive).  
    
	    Value       esp;
	    AddressList   nextAddresses  = new AddressList();
	    Map<Address, State> next = new HashMap<Address, State>();
	    nextAddresses = state.getReturnAddressAsList();
	    String stackString = ((csAnalysisDomain) state).getContextString();

	    
	    if (stackString.endsWith("+")) {
	   	 
	       	Iterator<String> iter;
	    	ArrayList<String> contexts = ((LStackContext)state).findContexts();
	    	
	    	for ( iter = contexts.iterator(); iter.hasNext(); ) {
	    		String iteration = iter.next();
	    		LStackContext context = (LStackContext) state.clone();
	    		
	    		if (!(iteration.endsWith("+"))) {
	    			context.pop();
	    			VsaAsgState vsaState = (VsaAsgState) context.get(stackString);
	    			context.updateAddMapping(iteration,  vsaState);
		    		context.changeContextString(iteration);
		    		
		    	} 
	    		context.changeTop();
	    		
	    		for(Address address : nextAddresses) 
	    			next.put((Address) address.clone(), context);
	    		
	    	}
	    	
	    	return next;
	    }
	    
	  	state.pop();
		    
	  	if(instruction.getDstOperand() != null)
		    {
		      esp = state.getRegisterValue(Register.ESP);
		      state.setRegisterValue( Register.ESP,
		          esp.add(instruction.getDstOperand().interpret(state)) );
		    }
	  	
	  	((LStackContext)state).changeTop();
		    // just one address because the return address is already right because the stack-context
		for(Address address : nextAddresses)
			      next.put(address, state);
  	
	    return next;
    	
  	}
  
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PopInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PopInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    String stackString = ((csAnalysisDomain) state).getContextString();

    if (stackString.endsWith("+")) {
	   	 
       	Iterator<String> iter;
    	ArrayList<String> contexts = ((LStackContext)state).findContexts();
    	
    	for ( iter = contexts.iterator(); iter.hasNext(); ) {
    		String iteration = iter.next();
    		LStackContext context = (LStackContext) state.clone();
    		
    		if (!(iteration.endsWith("+"))) {
    			Value valuePopped = context.pop();
    			Operand dstOperand;
    	        
    	        dstOperand = instruction.getDstOperand();
    	        if(dstOperand instanceof Register32Operand)
    	        {
    	            // for registers, simply save the value to the specified register.
    	            state.setRegisterValue( ((Register32Operand)dstOperand).getRegister(),
    	                    valuePopped);
    	        }
    	        else if(dstOperand instanceof Register16Operand)
    	        {
    	            // for registers, simply save the value to the specified register.
    	            state.setRegisterValue( ((Register16Operand)dstOperand).getRegister(),
    	                    valuePopped);
    	        }
    	        else if(dstOperand instanceof Memory32Operand)
    	        {
    	            state.store( ((Memory32Operand)dstOperand).getTarget(state), valuePopped);      
    	        }
    	        else if(dstOperand instanceof Memory16Operand)
    	        {
    	            state.store( ((Memory16Operand)dstOperand).getTarget(state), valuePopped);      
    	        }
    	        else
    	        {
    	          Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
    	          throw new Error("PopInstruction.interpret: unknown operand type for jjtGetChild(0)");
    	        }
    			
    			
    			VsaAsgState vsaState = (VsaAsgState) context.get(stackString);
    			context.updateAddMapping(iteration,  vsaState);
	    		context.changeContextString(iteration);
	    		
	    	} 
    		context.changeTop();
    		
    		next.put((Address) nextAddress.clone(), context);
    		
    	}
    	
    	return next;
    }
    
   Value valuePopped = state.pop();
   ((LStackContext)state).changeTop();
   Operand dstOperand;
        
        dstOperand = instruction.getDstOperand();
        if(dstOperand instanceof Register32Operand)
        {
            // for registers, simply save the value to the specified register.
            state.setRegisterValue( ((Register32Operand)dstOperand).getRegister(),
                    valuePopped);
        }
        else if(dstOperand instanceof Register16Operand)
        {
            // for registers, simply save the value to the specified register.
            state.setRegisterValue( ((Register16Operand)dstOperand).getRegister(),
                    valuePopped);
        }
        else if(dstOperand instanceof Memory32Operand)
        {
            state.store( ((Memory32Operand)dstOperand).getTarget(state), valuePopped);      
        }
        else if(dstOperand instanceof Memory16Operand)
        {
            state.store( ((Memory16Operand)dstOperand).getTarget(state), valuePopped);      
        }
        else
        {
          Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
          throw new Error("PopInstruction.interpret: unknown operand type for jjtGetChild(0)");
        }
        
        next.put(nextAddress, state);
        return next;
    
  }

  public Map<Address, State> interpret(AddInstruction instruction, State state, Address nextAddress)
  {
    
	Map<Address, State> result = new HashMap<Address, State>();
    
    Value sourceValue;
    Value targetValue;
    int n =0;
  
    sourceValue = instruction.getSrcOperand().interpret(state);
    
    if(instruction.getDstOperand() instanceof Register32Operand)
    {
      Register target = ((Register32Operand)instruction.getDstOperand()).getRegister();
      targetValue = state.getRegisterValue(target);
      targetValue = targetValue.add(sourceValue);
      state.setRegisterValue(target, targetValue);
      
      if (target.toString().equals("ESP")){
          IReducedIntervalCongruence ric = ((vsaAsgDomain.Value)sourceValue).getRIC();
      	 
      	  if (ric.size() == 1) {
      		  n = (int)ric.getMinimumValue(); 
      		  n = n/4;
      	  }
     
       	  State temp = ((csAnalysisDomain)state).getCurrentState();
      	  String index = Integer.toString(instruction.getIndex()+1);
      	  
      	  if (n >= 0) {
      		((LStackContext)state).popSuffix(n);
        	((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
      	  } else if (n < 0) {
      		  for (int t=1; t <= Math.abs(n); t++) 
      			  ((LStackContext)state).pushSuffix((index + "|" + (Integer.toString(t))));
        	  ((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
      	  } 
      	}
    }
    else if(instruction.getDstOperand() instanceof Register16Operand)
    {
      Register target = ((Register16Operand)instruction.getDstOperand()).getRegister();
      targetValue = state.getRegisterValue(target);
      targetValue = targetValue.add(sourceValue);
      state.setRegisterValue(target, targetValue);
      
      if (target.toString().equals("ESP")){
          IReducedIntervalCongruence ric = ((vsaAsgDomain.Value)sourceValue).getRIC();
      	 
      	  if (ric.size() == 1) {
      		  n = (int)ric.getMinimumValue(); 
      		  n = n/4;
      	  }
      	 
      	  State temp = ((csAnalysisDomain)state).getCurrentState();
     	  String index = Integer.toString(instruction.getIndex()+1);
     	  
     	  if (n >= 0) {
     		((LStackContext)state).popSuffix(n);
     		((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  } else if (n < 0) {
     		  for (int t=1; t <= Math.abs(n); t++) 
     			  ((LStackContext)state).pushSuffix((index + "|" + (Integer.toString(t))));
     		  ((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  }
   
      	}
    }
    else if(instruction.getDstOperand() instanceof Register8Operand)
    {
      Register target = ((Register8Operand)instruction.getDstOperand()).getRegister();
      targetValue = state.getRegisterValue(target);
      targetValue = targetValue.add(sourceValue);
      state.setRegisterValue(target, targetValue);
      
      if (target.toString().equals("ESP")){
          IReducedIntervalCongruence ric = ((vsaAsgDomain.Value)sourceValue).getRIC();
      	 
      	  if (ric.size() == 1) {
      		  n = (int)ric.getMinimumValue(); 
      		  n = n/4;
      	  }
      	  Map map = ((csAnalysisDomain)state).getMapping();
     	  State temp = (State) map.get(((csAnalysisDomain)state).getContextString());
     	  String index = Integer.toString(instruction.getIndex()+1);
     	  
     	  if (n >= 0) {
     		((LStackContext)state).popSuffix(n);
     		((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  } else if (n < 0) {
     		  for (int t=1; t <= Math.abs(n); t++) 
     			  ((LStackContext)state).pushSuffix((index + "|" + (Integer.toString(t))));
     		  ((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  }
      	}
    }
    else if(instruction.getDstOperand() instanceof Memory32Operand)
    {
      Value target = ((Memory32Operand)instruction.getDstOperand()).getTarget(state);
      targetValue = state.load(target, BitCount._32Bits);
      targetValue = targetValue.add(sourceValue);
      state.store(target, targetValue);
      
      if (target.toString().equals("ESP")){
          IReducedIntervalCongruence ric = ((vsaAsgDomain.Value)sourceValue).getRIC();
      	 
      	  if (ric.size() == 1) {
      		  n = (int)ric.getMinimumValue(); 
      		  n = n/4;
      	  }
      	  Map map = ((csAnalysisDomain)state).getMapping();
     	  State temp = (State) map.get(((csAnalysisDomain)state).getContextString());
     	  String index = Integer.toString(instruction.getIndex()+1);
     	  
     	  if (n >= 0) {
     		((LStackContext)state).popSuffix(n);
     		((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  } else if (n < 0) {
     		  for (int t=1; t <= Math.abs(n); t++) 
     			  ((LStackContext)state).pushSuffix((index + "|" + (Integer.toString(t))));
     		  ((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  }
      	}
    }
    else if(instruction.getDstOperand() instanceof Memory16Operand)
    {
      Value target = ((Memory16Operand)instruction.getDstOperand()).getTarget(state);
      targetValue = state.load(target, BitCount._16Bits);
      targetValue = targetValue.add(sourceValue);
      state.store(target, targetValue);
      
      if (target.toString().equals("ESP")){
          IReducedIntervalCongruence ric = ((vsaAsgDomain.Value)sourceValue).getRIC();
      	 
      	  if (ric.size() == 1) {
      		  n = (int)ric.getMinimumValue(); 
      		  n = n/4;
      	  }
      	  Map map = ((csAnalysisDomain)state).getMapping();
     	  State temp = (State) map.get(((csAnalysisDomain)state).getContextString());
     	  String index = Integer.toString(instruction.getIndex()+1);
     	  
     	  if (n >= 0) {
     		((LStackContext)state).popSuffix(n);
     		((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  } else if (n < 0) {
     		  for (int t=1; t <= Math.abs(n); t++) 
     			  ((LStackContext)state).pushSuffix((index + "|" + (Integer.toString(t))));
     		  ((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  }
      	}
    }
    else if(instruction.getDstOperand() instanceof Memory8Operand)
    {
      Value target = ((Memory8Operand)instruction.getDstOperand()).getTarget(state);
      targetValue = state.load(target, BitCount._8Bits);
      targetValue = targetValue.add(sourceValue);
      state.store(target, targetValue);
      
      if (target.toString().equals("ESP")){
          IReducedIntervalCongruence ric = ((vsaAsgDomain.Value)sourceValue).getRIC();
      	 
      	  if (ric.size() == 1) {
      		  n = (int)ric.getMinimumValue(); 
      		  n = n/4;
      	  }
      	  Map map = ((csAnalysisDomain)state).getMapping();
     	  State temp = (State) map.get(((csAnalysisDomain)state).getContextString());
     	  String index = Integer.toString(instruction.getIndex()+1);
     	  
     	  if (n >= 0) {
     		((LStackContext)state).popSuffix(n);
     		((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  } else if (n < 0) {
     		  for (int t=1; t <= Math.abs(n); t++) 
     			  ((LStackContext)state).pushSuffix((index + "|" + (Integer.toString(t))));
     		  ((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  }
      	}
    }
    else
    {
      throw new Error("AddInstruction.java: Invalid operand type encountered.");
    }

    
    result.put(nextAddress, state);
    return result;
  }
  
  public Map<Address, State> interpret(SubInstruction instruction, State state, Address nextAddress)
  {
	
	Map<Address, State> next = new HashMap<Address, State>();
    Value sourceValue;
    Value targetValue;
    int n = 0;
    String index;
 
    sourceValue = instruction.getSrcOperand().interpret(state);
    
    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
      Register target = ((RegisterOperand)instruction.getDstOperand()).getRegister();
      targetValue = state.getRegisterValue(target);
      targetValue = targetValue.subtract(sourceValue);
      state.setRegisterValue(target, targetValue);
      
      if (target.toString().equals("ESP")){
          IReducedIntervalCongruence ric = ((vsaAsgDomain.Value)sourceValue).getRIC();
      	 
      	  if (ric.size() == 1) {
      		  n = (int)ric.getMinimumValue(); 
      		  n = n/4;
      	  }
      	  State temp = ((csAnalysisDomain)state).getCurrentState();
     	  index = Integer.toString(instruction.getIndex()+1);
     	  
     	  if (n < 0) {
     		((LStackContext)state).popSuffix(Math.abs(n));
     		((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  } else if (n >= 0) {
     		  for (int t=1; t <= n; t++) 
     			  ((LStackContext)state).pushSuffix((index + "|" + (Integer.toString(t))));
     		  ((csAnalysisDomain)state).updateAddMapping(((csAnalysisDomain)state).getContextString(), (VsaAsgState) temp );
     	  }
      	}
      
    }
    else if(instruction.getDstOperand() instanceof MemoryOperand)
    {
      Value target = ((MemoryOperand)instruction.getDstOperand()).getTarget(state);
      targetValue = state.load(target, BitCount._32Bits);
      targetValue = targetValue.subtract(sourceValue);
      state.store(target, targetValue);
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - Invalid operand type encountered");
      throw new Error("SubInstruction.java: Invalid operand type encountered.");
    }

    next.put(nextAddress, state);
    return next;
  }
  
   
}

package instructionInterpreters;

import java.util.HashMap;
import java.util.Map;

import logger.Log;
import objdumpParser.ASTLeaOperand;

import org.eclipse.core.runtime.IStatus;

import x86InstructionSet.AdcInstruction;
import x86InstructionSet.AddInstruction;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.AndInstruction;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.CdqInstruction;
import x86InstructionSet.CldInstruction;
import x86InstructionSet.CmpInstruction;
import x86InstructionSet.CwdInstruction;
import x86InstructionSet.DecInstruction;
import x86InstructionSet.DivInstruction;
import x86InstructionSet.EnterInstruction;
import x86InstructionSet.HltInstruction;
import x86InstructionSet.IdivInstruction;
import x86InstructionSet.ImulInstruction1op;
import x86InstructionSet.ImulInstruction2op;
import x86InstructionSet.ImulInstruction3op;
import x86InstructionSet.InInstruction;
import x86InstructionSet.IncInstruction;
import x86InstructionSet.JaJnbeInstruction;
import x86InstructionSet.JaeInstruction;
import x86InstructionSet.JbInstruction;
import x86InstructionSet.JbeInstruction;
import x86InstructionSet.JeInstruction;
import x86InstructionSet.JecxzInstruction;
import x86InstructionSet.JgInstruction;
import x86InstructionSet.JgeInstruction;
import x86InstructionSet.JlInstruction;
import x86InstructionSet.JleInstruction;
import x86InstructionSet.JmpInstruction;
import x86InstructionSet.JnbInstruction;
import x86InstructionSet.JneInstruction;
import x86InstructionSet.JnsInstruction;
import x86InstructionSet.JnzInstruction;
import x86InstructionSet.JsInstruction;
import x86InstructionSet.LeaInstruction;
import x86InstructionSet.LeaveInstruction;
import x86InstructionSet.LockInstruction;
import x86InstructionSet.LodsInstruction;
import x86InstructionSet.LoopdInstruction;
import x86InstructionSet.Memory16Operand;
import x86InstructionSet.Memory32Operand;
import x86InstructionSet.Memory8Operand;
import x86InstructionSet.MemoryOperand;
import x86InstructionSet.MovInstruction;
import x86InstructionSet.MovsInstruction;
import x86InstructionSet.MovsxInstruction;
import x86InstructionSet.MovzxInstruction;
import x86InstructionSet.MulInstruction;
import x86InstructionSet.NegInstruction;
import x86InstructionSet.NopInstruction;
import x86InstructionSet.NotInstruction;
import x86InstructionSet.Operand;
import x86InstructionSet.OrInstruction;
import x86InstructionSet.PopInstruction;
import x86InstructionSet.PopadInstruction;
import x86InstructionSet.PopfdInstruction;
import x86InstructionSet.PushInstruction;
import x86InstructionSet.PushadInstruction;
import x86InstructionSet.PushfdInstruction;
import x86InstructionSet.Register16Operand;
import x86InstructionSet.Register32Operand;
import x86InstructionSet.Register8Operand;
import x86InstructionSet.RegisterOperand;
import x86InstructionSet.RepMovsInstruction;
import x86InstructionSet.RepStosInstruction;
import x86InstructionSet.RepneScasInstruction;
import x86InstructionSet.RepzMovsInstruction;
import x86InstructionSet.RepzStosInstruction;
import x86InstructionSet.RetInstruction;
import x86InstructionSet.RolInstruction;
import x86InstructionSet.RorInstruction;
import x86InstructionSet.SarInstruction;
import x86InstructionSet.SbbInstruction;
import x86InstructionSet.SetaeSetnbInstruction;
import x86InstructionSet.SetbSetnaeInstruction;
import x86InstructionSet.SetbeSetnaInstruction;
import x86InstructionSet.SeteSetzInstruction;
import x86InstructionSet.SetgSetnleInstruction;
import x86InstructionSet.SetgeSetnlInstruction;
import x86InstructionSet.SetlSetngeInstruction;
import x86InstructionSet.SetleSetngInstruction;
import x86InstructionSet.SetneSetnzInstruction;
import x86InstructionSet.ShlInstruction;
import x86InstructionSet.ShrInstruction;
import x86InstructionSet.StdInstruction;
import x86InstructionSet.StosInstruction;
import x86InstructionSet.SubInstruction;
import x86InstructionSet.TestInstruction;
import x86InstructionSet.UndefInstruction;
import x86InstructionSet.XchgInstruction;
import x86InstructionSet.XorInstruction;
import abstractBoolean.AbstractBoolean;
import domain.BitCount;
import domain.FlagRegisterStore;
import domain.Register;
import domain.State;
import domain.Value;

/**
 * Interprets instructions in a way that immitated the instructions true
 * runtime behavior.  This interpreter is context-insensitive.  That is
 * to say that after interpreting a function, the same state is used when
 * returning to each calling point.  If this interpreter was context-sensitive,
 * then a different state would be sent to each calling point. 
 */
public class ContextInsensitiveInterpreter implements InstructionInterpreter
{
  static private ContextInsensitiveInterpreter instance = null;
  
  /**
   * Returns the one and only instance of this class.
   * 
   * @return      An instance of this class.
   * @deprecated  Use the constructor instead.  There is no good reason for this
   *              to be a singleton.
   */
  static public ContextInsensitiveInterpreter getInstance()
  {
    if(instance == null)
      instance = new ContextInsensitiveInterpreter();
    return instance;
  }
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.AdcInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(AdcInstruction instruction, State state, Address nextAddress)
  { 
    Map<Address, State> result = new HashMap<Address, State>();
    
    // For now, this implementation just sets everything that is modified
    // to undefined values.  It also reads from any registers that this
    // instruction typically reads from simply to give correct
    // live register analysis results.
    
    // load destination operand.
    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
      state.getRegisterValue( 
          ((RegisterOperand)instruction.getDstOperand()).getRegister() );
    }
    else if(instruction.getDstOperand() instanceof MemoryOperand)
    {
      state.load( ((MemoryOperand)instruction.getDstOperand()).getTarget(state),
          ((MemoryOperand)instruction.getDstOperand()).getBitSize() );
    }
    
    if(instruction.getSrcOperand() instanceof RegisterOperand)
    {
      state.getRegisterValue( 
          ((RegisterOperand)instruction.getSrcOperand()).getRegister() );
    }
    else if(instruction.getSrcOperand() instanceof MemoryOperand)
    {
      state.load( ((MemoryOperand)instruction.getSrcOperand()).getTarget(state),
          ((MemoryOperand)instruction.getSrcOperand()).getBitSize() );
    }
    
    // save result
    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
      state.setRegisterValue( 
          ((RegisterOperand)instruction.getDstOperand()).getRegister(), 
          state.createNewValue() );
    }
    if(instruction.getDstOperand() instanceof MemoryOperand)
    {
      state.store(
          ((MemoryOperand)instruction.getDstOperand()).getTarget(state),
          state.createNewValue() );
    }

    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.AddInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(AddInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();
    
    Value sourceValue;
    Value targetValue;
    
    sourceValue = instruction.getSrcOperand().interpret(state);
    
    if(instruction.getDstOperand() instanceof Register32Operand)
    {
      Register target = ((Register32Operand)instruction.getDstOperand()).getRegister();
      targetValue = state.getRegisterValue(target);
      targetValue = targetValue.add(sourceValue);
      state.setRegisterValue(target, targetValue);
    }
    else if(instruction.getDstOperand() instanceof Register16Operand)
    {
      Register target = ((Register16Operand)instruction.getDstOperand()).getRegister();
      targetValue = state.getRegisterValue(target);
      targetValue = targetValue.add(sourceValue);
      state.setRegisterValue(target, targetValue);
    }
    else if(instruction.getDstOperand() instanceof Register8Operand)
    {
      Register target = ((Register8Operand)instruction.getDstOperand()).getRegister();
      targetValue = state.getRegisterValue(target);
      targetValue = targetValue.add(sourceValue);
      state.setRegisterValue(target, targetValue);
    }
    else if(instruction.getDstOperand() instanceof Memory32Operand)
    {
      Value target = ((Memory32Operand)instruction.getDstOperand()).getTarget(state);
      targetValue = state.load(target, BitCount._32Bits);
      targetValue = targetValue.add(sourceValue);
      state.store(target, targetValue);
    }
    else if(instruction.getDstOperand() instanceof Memory16Operand)
    {
      Value target = ((Memory16Operand)instruction.getDstOperand()).getTarget(state);
      targetValue = state.load(target, BitCount._16Bits);
      targetValue = targetValue.add(sourceValue);
      state.store(target, targetValue);
    }
    else if(instruction.getDstOperand() instanceof Memory8Operand)
    {
      Value target = ((Memory8Operand)instruction.getDstOperand()).getTarget(state);
      targetValue = state.load(target, BitCount._8Bits);
      targetValue = targetValue.add(sourceValue);
      state.store(target, targetValue);
    }
    else
    {
      throw new Error("AddInstruction.java: Invalid operand type encountered.");
    }

    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.AndInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(AndInstruction instruction, State state, Address nextAddress)
 {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    Operand srcOperand;
    
    dstOperand = instruction.getDstOperand();
    srcOperand = instruction.getSrcOperand();
    
    if(dstOperand instanceof RegisterOperand)
    {
      Register target   = ((RegisterOperand)dstOperand).getRegister();
      Value operand1 = state.getRegisterValue(target);
      Value operand2 = ((Operand)srcOperand).interpret(state);
      
      Value result = operand1.and(operand2);
      state.setRegisterValue(target, result);
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      Value target    = ((MemoryOperand)dstOperand).getTarget(state);
      Value operand1  = ((Operand)dstOperand).interpret(state);
      Value operand2  = ((Operand)srcOperand).interpret(state);
      
      Value result = operand1.and(operand2);
      state.store(target, result);
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CallInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CallInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    Value       callTarget;
    
    //result.put(nextAddress, state);
    callTarget = instruction.getTargetOperand().interpret(state);
    state.push( state.createNewValue(nextAddress.toLong()) );
    
    Value.Iterator iter = callTarget.iterator();
    while(iter.hasNext())
      result.put( new Address(iter.next()), state );
    
    return result;  
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CdqInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CdqInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    // CDQ modifies the EDX register.  This instruction is not yet
    // handled, so just set EDX to undefined for now.
    state.getRegisterValue(Register.EAX);
    state.setRegisterValue(Register.EAX, state.createNewValue());
    state.setRegisterValue(Register.EDX, state.createNewValue());
    
    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CldInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CldInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    // do nothing for now, since this instruction only modifies the
    // direction flag and we don't track that flag.
    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CmpInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CmpInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    Value arg1 = instruction.getDstOperand().interpret(state);
    Value arg2 = instruction.getSrcOperand().interpret(state);
    
    if(arg1.isGreaterThan(arg2) == AbstractBoolean.True)
    {
      state.setFlagValue( FlagRegisterStore.ZERO_FLAG, AbstractBoolean.False);
      state.setFlagValue( FlagRegisterStore.SIGN_FLAG, 
          state.getFlagValue(FlagRegisterStore.OVERFLOW_FLAG));
    }
    if(arg1.isGreaterThan(arg2) == AbstractBoolean.Top)
    {
      state.setFlagValue( FlagRegisterStore.ZERO_FLAG, 
          state.getFlagValue(FlagRegisterStore.ZERO_FLAG).join(AbstractBoolean.False));
      state.setFlagValue( FlagRegisterStore.SIGN_FLAG,
          state.getFlagValue(FlagRegisterStore.SIGN_FLAG).join(state.getFlagValue(FlagRegisterStore.OVERFLOW_FLAG)));
    }       
    
    if(arg1.isEqualTo(arg2) == AbstractBoolean.True)
    {
      state.setFlagValue( FlagRegisterStore.ZERO_FLAG, AbstractBoolean.True);
    }
    if(arg1.isEqualTo(arg2) == AbstractBoolean.Top)
    {
      state.setFlagValue( FlagRegisterStore.ZERO_FLAG, 
          state.getFlagValue(FlagRegisterStore.ZERO_FLAG).join(AbstractBoolean.True));
    }

    if(arg1.isLessThan(arg2) == AbstractBoolean.True)
    {
      state.setFlagValue( FlagRegisterStore.SIGN_FLAG, 
          state.getFlagValue(FlagRegisterStore.OVERFLOW_FLAG));
    }
    if(arg1.isLessThan(arg2) == AbstractBoolean.Top)
    {
      state.setFlagValue( FlagRegisterStore.SIGN_FLAG, 
          state.getFlagValue(FlagRegisterStore.SIGN_FLAG).join(
              state.getFlagValue(FlagRegisterStore.OVERFLOW_FLAG)));
    }

    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CwdInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CwdInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    // CWD modifies the DX register.  This instruction is not yet
    // handled, so just set DX to undefined for now.
    state.getRegisterValue(Register.AX);
    state.getRegisterValue(Register.EAX);

    state.setRegisterValue(Register.AX, state.createNewValue());
    state.setRegisterValue(Register.EAX, state.createNewValue());
    state.setRegisterValue(Register.DX, state.createNewValue());
    state.setRegisterValue(Register.EDX, state.createNewValue());

    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.DecInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(DecInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    Operand operand = instruction.getDstOperand();
    Value   initialValue = operand.interpret(state);
    Value   finalValue   = initialValue.subtract( state.createNewValue(1) );
    
    if(operand instanceof RegisterOperand)
    {
        RegisterOperand register = (RegisterOperand)operand;
        state.setRegisterValue(register.getRegister(), finalValue);
    }
    else if(operand instanceof MemoryOperand)
    {
        MemoryOperand memory = (MemoryOperand)operand;
        state.store(memory.getTarget(state), finalValue);
    }
    else
    {
      Log.write(IStatus.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
      throw new Error("DecInstruction.interpret: unknown operand type for jjtGetChild(0)");
    }
    
    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.DivInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(DivInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    // DIV modifies eax and sometimes edx.  Since we don't yet handle
    // DIV, just replace eax and edx with undefined values.
    state.getRegisterValue(Register.EAX);
    state.getRegisterValue(Register.EDX);
    state.setRegisterValue(Register.EAX, state.createNewValue());
    state.setRegisterValue(Register.EDX, state.createNewValue());
    
    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
      state.getRegisterValue(
          ((RegisterOperand)instruction.getDstOperand()).getRegister());
    }

    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.EnterInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(EnterInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    Value frameTemp;
    long  nestingLevelLong;
    Value nestingLevel;
    Value size;
        
    // This instruction is of the form: ENTER size, nestingLevel
    size = instruction.getSizeOperand().interpret(state);
    nestingLevel = instruction.getNestingLevelOperand().interpret(state);
    
    // For this instruction, it is guaranteed that nestingLevel consists
    // of exactly one number, so convert the Value to a long just
    // to make things easier.  Normally, we can't do this because a Value
    // can represent multiple numbers, but that is not the case here.
    nestingLevelLong = nestingLevel.iterator().next();
    
    // PUSH ebp
    state.setRegisterValue( Register.ESP,
            state.getRegisterValue(Register.ESP).subtract(state.createNewValue(4)));
    state.store( state.getRegisterValue(Register.ESP),
            state.getRegisterValue(Register.EBP));
    
    // frameTemp <-- esp
    frameTemp = (Value)state.getRegisterValue(Register.ESP).clone();
    
    nestingLevelLong = nestingLevelLong % 32;
    if(nestingLevelLong > 0)
    {
        for(int i = 1; i < nestingLevelLong; i++)
        {
            // ebp <-- ebp - 4
            state.setRegisterValue(Register.EBP, 
                    state.getRegisterValue(Register.EBP).subtract(state.createNewValue(4)));
            // PUSH [ebp]
            state.setRegisterValue(Register.ESP,
                    state.getRegisterValue(Register.ESP).subtract(state.createNewValue(4)));
            state.store(state.getRegisterValue(Register.ESP),
                    state.load(state.getRegisterValue(Register.EBP), BitCount._32Bits));
        }
        
        // PUSH frameTemp
        state.setRegisterValue(Register.ESP,
                state.getRegisterValue(Register.ESP).subtract(state.createNewValue(4)));
        state.store(state.getRegisterValue(Register.ESP), frameTemp);
    }
    
    // ebp <-- frameTemp
    state.setRegisterValue(Register.EBP, frameTemp);
    // esp <-- esp - size
    state.setRegisterValue(Register.ESP, 
            state.getRegisterValue(Register.ESP).subtract(size));

    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.HltInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(HltInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    // do nothing.
    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.IdivInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(IdivInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    Operand dstOperand = instruction.getDstOperand();
    
    if(dstOperand instanceof RegisterOperand)
      state.getRegisterValue( ((RegisterOperand)dstOperand).getRegister() );
    
    if(dstOperand instanceof Register32Operand ||
        dstOperand instanceof Memory32Operand)
    {
      state.getRegisterValue(Register.EAX);
      state.getRegisterValue(Register.EDX);
      state.setRegisterValue(Register.EAX, state.createNewValue());
      state.setRegisterValue(Register.EDX, state.createNewValue());
    }
    else if(dstOperand instanceof Register16Operand ||
        dstOperand instanceof Memory16Operand)
    {
      state.getRegisterValue(Register.AX);
      state.getRegisterValue(Register.DX);

      state.setRegisterValue(Register.AX, state.createNewValue());
      state.setRegisterValue(Register.DX, state.createNewValue());
    }
    else if(dstOperand instanceof Register8Operand ||
        dstOperand instanceof Memory8Operand)
    {
      state.getRegisterValue(Register.AX);

      state.setRegisterValue(Register.AL, state.createNewValue());
      state.setRegisterValue(Register.AH, state.createNewValue());
    }
    else
    {
      Log.write(IStatus.ERROR, "interpret() - unknown operand type encountered");
      throw new Error("IdivInstruction.interpret: unknown operand type encountered.");
    }

    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ImulInstruction1op, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ImulInstruction1op instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    BitCount size = BitCount._32Bits;
    
    // IMUL (the 1-op version) always modifies register eax.  Since we 
    // do not handle multiplication yet, just set eax to an undefined value.
    if(instruction.getDstOperand() instanceof RegisterOperand)
      size = ((RegisterOperand)instruction.getDstOperand()).getBitSize();
    else if(instruction.getDstOperand() instanceof MemoryOperand)
      size = ((MemoryOperand)instruction.getDstOperand()).getBitSize();
    
    if(size == BitCount._32Bits)
    {
      state.getRegisterValue(Register.EAX);
      state.setRegisterValue(Register.EAX, state.createNewValue());
      state.setRegisterValue(Register.EDX, state.createNewValue());
    }
    else if(size == BitCount._16Bits)
    {
      state.getRegisterValue(Register.AX);
      state.setRegisterValue(Register.AX, state.createNewValue());
      state.setRegisterValue(Register.DX, state.createNewValue());
    }
    else
    {
      state.getRegisterValue(Register.AL);
      state.setRegisterValue(Register.AX, state.createNewValue());
    }
    
    if(instruction.getDstOperand() instanceof RegisterOperand)
      state.getRegisterValue( 
          ((RegisterOperand)instruction.getDstOperand()).getRegister() );
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ImulInstruction2op, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ImulInstruction2op instruction, State state, Address nextAddress)
  {
    Map<Address, State> result = new HashMap<Address, State>();

    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
      state.getRegisterValue( 
          ((RegisterOperand)instruction.getDstOperand()).getRegister() );
      state.setRegisterValue( 
          ((RegisterOperand)instruction.getDstOperand()).getRegister(),
          state.createNewValue() );
    }
    
    if(instruction.getSrcOperand() instanceof RegisterOperand)
    {
      state.getRegisterValue( 
          ((RegisterOperand)instruction.getSrcOperand()).getRegister() );
    }

    result.put(nextAddress, state);
    return result;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ImulInstruction3op, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ImulInstruction3op instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
      state.setRegisterValue( 
          ((RegisterOperand)instruction.getDstOperand()).getRegister(),
          state.createNewValue() );
    }
    
    if(instruction.getSrc1Operand() instanceof RegisterOperand)
    {
      state.getRegisterValue( 
          ((RegisterOperand)instruction.getSrc1Operand()).getRegister() );
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.IncInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(IncInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand operand       = instruction.getDstOperand();
    Value   initialValue  = operand.interpret(state);
    Value   finalValue    = initialValue.add( state.createNewValue(1) );
    
    if(operand instanceof RegisterOperand)
    {
        RegisterOperand register = (RegisterOperand)operand;
        state.setRegisterValue(register.getRegister(), finalValue);
    }
    else if(operand instanceof MemoryOperand)
    {
        MemoryOperand memory = (MemoryOperand)operand;
        state.store(memory.getTarget(state), finalValue);
    }
    else
    {
      Log.write(IStatus.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
      throw new Error("IncInstruction.interpret: unknown operand type for jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.InInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(InInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    // read from source operand
    if(instruction.getSrcOperand() instanceof RegisterOperand)
    {
      state.getRegisterValue(
          ((RegisterOperand)instruction.getSrcOperand()).getRegister() );
    }
    
    // write to destination operand.
    RegisterOperand dest = (RegisterOperand)instruction.getDstOperand();
    state.setRegisterValue(dest.getRegister(), state.createNewValue());

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JaeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JaeInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Value           jumpTarget;
    Value.Iterator  iter;
      
    // get jump target
    jumpTarget = instruction.getTargetOperand().interpret(state);
    
    if (jumpTarget.size() > 2)
    {
      Log.write(IStatus.WARNING, 
          "interpret() - Jump target contains " + jumpTarget.size() + " addresses.");
    }
      
    // now add the instruction following this one to the array.  This way,
    // we will interpret both paths.
    next.put(nextAddress, state);
    for(iter = jumpTarget.iterator(); iter.hasNext(); )
      next.put(new Address(iter.next()), state);
    
    return next;  
  }
 
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JaJnbeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JaJnbeInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Value           jumpTarget;
    Value.Iterator  iter;
      
    // get jump target
    jumpTarget = instruction.getTargetOperand().interpret(state);
    
    if (jumpTarget.size() > 2)
    {
      Log.write(IStatus.WARNING, 
          "interpret() - Jump target contains " + jumpTarget.size() + " addresses.");
    }
      
    // now add the instruction following this one to the array.  This way,
    // we will interpret both paths.
    next.put(nextAddress, state);
    for(iter = jumpTarget.iterator(); iter.hasNext(); )
      next.put(new Address(iter.next()), state);
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JbeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JbeInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand         operand     = instruction.getTargetOperand();
    Value           jumptarget  = operand.interpret(state);
    Value.Iterator  iter; 

    next.put(nextAddress, state);
    iter = jumptarget .iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (jumptarget.size() > 2)
    {
      Log.write(IStatus.WARNING, "interpret() - Jump target contains " + jumptarget.size() + " addresses.");
    }
      
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JbInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JbInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand         operand     = instruction.getTargetOperand();
    Value           jumptarget  = operand.interpret(state);
    Value.Iterator  iter; 

    next.put(nextAddress, state);
    iter = jumptarget .iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (jumptarget.size() > 2)
    {
      Log.write(IStatus.WARNING, "interpret() - Jump target contains " + 
          jumptarget.size() + " addresses.");
    }

    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JecxzInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JecxzInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand         operand     = instruction.getTargetOperand();
    Value           jumptarget  = operand.interpret(state);
    Value.Iterator  iter; 

    next.put(nextAddress, state);
    iter = jumptarget .iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (jumptarget.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          jumptarget.size() + " addresses.");
    }
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JeInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand       operand     = instruction.getTargetOperand();
    Value         jumptarget  = operand.interpret(state);
    
    next.put(nextAddress, state);
    Value.Iterator iter = jumptarget .iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (jumptarget.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          jumptarget.size() + " addresses.");
    }
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JgeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JgeInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand     operand     = instruction.getTargetOperand();
    Value       jumptarget  = operand.interpret(state);
    
    next.put(nextAddress, state);
    Value.Iterator iter = jumptarget .iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (jumptarget.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          jumptarget.size() + " addresses.");
    }
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JgInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JgInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand       operand     = instruction.getTargetOperand();
    Value         jumptarget  = operand.interpret(state);
    
    next.put(nextAddress, state);
    Value.Iterator iter = jumptarget .iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (jumptarget.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " 
          + jumptarget.size() + " addresses.");
    }
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JleInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JleInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand     operand     = instruction.getTargetOperand();
    Value       jumptarget  = operand.interpret(state);
    
    next.put(nextAddress, state);
    Value.Iterator iter = jumptarget .iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (jumptarget.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          jumptarget.size() + " addresses.");
    }
    
    return next;
  }
 
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JlInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JlInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand     operand   = instruction.getTargetOperand();
    Value       jumptarget  = operand.interpret(state);
    
    next.put(nextAddress, state);
    Value.Iterator iter = jumptarget .iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (jumptarget.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          jumptarget.size() + " addresses.");
    }
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JmpInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JmpInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Value.Iterator  iter;
    Value           jumpTarget;

    jumpTarget = (Value)instruction.getTargetOperand().interpret(state);
    
    iter = jumpTarget.iterator();
    while(iter.hasNext())
      next.put( new Address(iter.next()), state );
    
    if (jumpTarget.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          jumpTarget.size() + " addresses.");
    }

    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JnbInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JnbInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand       operand = instruction.getTargetOperand();
    Value         target  = operand.interpret(state);
    
    next.put(nextAddress, state);
    Value.Iterator iter = target.iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (target.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          target.size() + " addresses.");
    }
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JneInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JneInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand     operand = instruction.getTargetOperand();
    Value       target  = operand.interpret(state);
    
    next.put(nextAddress, state);
    Value.Iterator iter = target.iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (target.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          target.size() + " addresses.");
    }
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JnsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JnsInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand     operand = instruction.getTargetOperand();
    Value       target = operand.interpret(state);
    
    next.put(nextAddress, state);

    if (target.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          target.size() + " addresses.");
    }

    Value.Iterator iter = target.iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JnzInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JnzInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand     operand = instruction.getTargetOperand();
    Value       target = operand.interpret(state);

    next.put(nextAddress, state);
    Value.Iterator iter = target.iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    if (target.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          target.size() + " addresses.");
    }
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JsInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand     operand = instruction.getTargetOperand();
    Value       target = operand.interpret(state);
    
    next.put(nextAddress, state);

    if (target.size() > 2)
    {
      Log.write(Log.WARNING, "interpret() - Jump target contains " + 
          target.size() + " addresses.");
    }

    Value.Iterator iter = target.iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);
    
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LeaInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LeaInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand   dstOperand;
    Value     source;
    Operand   srcOperand;
    Register  target;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof Register32Operand)
      target = ((Register32Operand)dstOperand).getRegister();
    else if(dstOperand instanceof Register16Operand)
        target = ((Register16Operand)dstOperand).getRegister();
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
      throw new Error("LeaInstruction.interpret: unknown operand type for jjtGetChild(0)");
    }
        
    srcOperand = instruction.getSrcOperand();
    if(srcOperand instanceof Memory32Operand)
        source = ((Memory32Operand)srcOperand).getTarget(state);
    else if(srcOperand instanceof Memory16Operand)
        source = ((Memory16Operand)srcOperand).getTarget(state);
    else if(srcOperand instanceof ASTLeaOperand)
        source = ((ASTLeaOperand)srcOperand).getTarget(state);
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(1)");
      throw new Error("LeaInstruction.interpret: unknown operand type for jjtGetChild(1)");
    }
    
    state.setRegisterValue(target, source);
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LeaveInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LeaveInstruction instruction, State state, Address nextAddress)
  {    
    Map<Address, State> next = new HashMap<Address, State>();

    // ESP <-- EBP
    state.setRegisterValue(Register.ESP, state.getRegisterValue(Register.EBP));
    
    // EBP <-- Pop()
    state.setRegisterValue(Register.EBP, state.pop());
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LockInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LockInstruction instruction, State state,
      Address nextAddress)
  {
    return instruction.getChildInstruction().interpret(this, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LodsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LodsInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand srcOperand;
    
    srcOperand = instruction.getSrcOperand();
    if(srcOperand instanceof Memory8Operand)
    {
        Value byteToWrite = 
            state.load(state.getRegisterValue(Register.ESI), BitCount._8Bits);
        
        state.setRegisterValue(Register.AL, byteToWrite);
    }
    else if(srcOperand instanceof Memory16Operand)
    {
        Value wordToWrite = 
            state.load(state.getRegisterValue(Register.ESI), BitCount._16Bits);
        
        state.setRegisterValue(Register.AX, wordToWrite);
    }
    else if(srcOperand instanceof Memory32Operand)
    {
        Value dwordToWrite = 
            state.load(state.getRegisterValue(Register.ESI), BitCount._32Bits);
        
        state.setRegisterValue(Register.EAX, dwordToWrite);
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type of jjtGetChild(0)");
      throw new Error("LodsInstruction.interpret: unknown operand type of jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LoopdInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LoopdInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand     operand = instruction.getDstOperand();
    Value       target  = operand.interpret(state);
    
    state.getRegisterValue(Register.ECX);
    state.setRegisterValue(Register.ECX, state.createNewValue());
    
    next.put(nextAddress, state);
    Value.Iterator iter = target.iterator();
    while(iter.hasNext())
      next.put(new Address(iter.next()), state);

    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MovInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MovInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    Operand srcOperand;
    
    dstOperand = instruction.getDstOperand();
    srcOperand = instruction.getSrcOperand();
    
    if(dstOperand instanceof RegisterOperand)
    {
      //System.out.println("Luke Here! Is AX an instance of a register operand?");
      state.setRegisterValue( ((RegisterOperand)dstOperand).getRegister(), 
              srcOperand.interpret(state));
    }

    else if(dstOperand instanceof MemoryOperand)
    {
      state.store( ((MemoryOperand)dstOperand).getTarget(state), 
          srcOperand.interpret(state) );
    }

    else
    {
      System.out.println("MovInstruction.interpret: jjtGetChild(0) is of an unknown type.");
      throw new Error("MovInstruction.interpret: jjtGetChild(0) is of an unknown type.");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MovsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MovsInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    MemoryOperand target = (MemoryOperand)instruction.getDstOperand();
    state.store( target.getTarget(state), state.createNewValue() );
    
    // interpret source operand just update the def/use values.
    instruction.getSrcOperand().interpret(state);

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MovsxInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MovsxInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    // Right now, the interpretation of MOVSX just sets the target to undefined.  
    // this may be fixed later.
    RegisterOperand target = (RegisterOperand)instruction.getDstOperand();
    state.setRegisterValue(target.getRegister(), state.createNewValue());
    
    // interpret source operand just update the def/use values.
    instruction.getSrcOperand().interpret(state);

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MovzxInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MovzxInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    // Right now, the interpretation of MOVZX just sets the register to undefined.  
    // this may be fixed later.
    RegisterOperand target = (RegisterOperand)instruction.getDstOperand();
    state.setRegisterValue(target.getRegister(), state.createNewValue());
    
    // interpret source operand just update the def/use values.
    instruction.getSrcOperand().interpret(state);

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MulInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MulInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    // MUL always modifies register eax.  Since we do not handle multiplication
    // yet, just set eax to an undefined value.
    state.getRegisterValue(Register.EAX);
    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
      state.getRegisterValue(
          ((RegisterOperand)instruction.getDstOperand()).getRegister() );
    }

    state.setRegisterValue(Register.EAX, state.createNewValue());
    state.setRegisterValue(Register.EDX, state.createNewValue());
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.NegInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(NegInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.getRegisterValue(target.getRegister());
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
      throw new Error("NegInstruction.interpret: unknown operand type for jjtGetChild(0)");
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.NopInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(NopInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.NotInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(NotInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.getRegisterValue(target.getRegister());
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
      throw new Error("NotInstruction.interpret: unknown operand type for jjtGetChild(0)");
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.OrInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(OrInstruction instruction, State state, 
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    Operand srcOperand;
    
    dstOperand = instruction.getDstOperand();
    srcOperand = instruction.getSrcOperand();
    
    if(dstOperand instanceof RegisterOperand)
    {
      Register target   = ((RegisterOperand)dstOperand).getRegister();
      Value operand1 = state.getRegisterValue(target);
      Value operand2 = ((Operand)srcOperand).interpret(state);
      
      Value result = operand1.or(operand2);
      state.setRegisterValue(target, result);
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      Value target    = ((MemoryOperand)dstOperand).getTarget(state);
      Value operand1  = ((Operand)dstOperand).interpret(state);
      Value operand2  = ((Operand)srcOperand).interpret(state);
      
      Value result = operand1.or(operand2);
      state.store(target, result);
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PopInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PopInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Value valuePopped = state.pop();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof Register32Operand)
    {
        // for registers, simply save the value to the specified register.
        state.setRegisterValue( ((Register32Operand)dstOperand).getRegister(),
                valuePopped);
    }
    else if(dstOperand instanceof Register16Operand)
    {
        // for registers, simply save the value to the specified register.
        state.setRegisterValue( ((Register16Operand)dstOperand).getRegister(),
                valuePopped);
    }
    else if(dstOperand instanceof Memory32Operand)
    {
        state.store( ((Memory32Operand)dstOperand).getTarget(state), valuePopped);      
    }
    else if(dstOperand instanceof Memory16Operand)
    {
        state.store( ((Memory16Operand)dstOperand).getTarget(state), valuePopped);      
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
      throw new Error("PopInstruction.interpret: unknown operand type for jjtGetChild(0)");
    }
    
    next.put(nextAddress, state);
    return next;
  }
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PopadInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PopadInstruction instruction,
      State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    state.setRegisterValue(Register.EDI, state.pop());
    state.setRegisterValue(Register.ESI, state.pop());
    state.setRegisterValue(Register.EBP, state.pop());
    state.pop();
    state.setRegisterValue(Register.EBX, state.pop());
    state.setRegisterValue(Register.EDX, state.pop());
    state.setRegisterValue(Register.ECX, state.pop());
    state.setRegisterValue(Register.EAX, state.pop());
    
    next.put(nextAddress, state);
    return next;
  }
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PopfdInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PopfdInstruction instruction,
      State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    state.pop();
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PushadInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PushadInstruction instruction,
      State state, Address nextAddress)
  {
    final int NUM_REGISTERS_TO_PUSH = 8;
    final int SIZE_OF_EACH_REGISTER = 4;
    final int NUM_BYTES_TO_BUSH = NUM_REGISTERS_TO_PUSH * SIZE_OF_EACH_REGISTER;

    Map<Address, State> next = new HashMap<Address, State>();
    
    Value esp;
    Value temp;
    
    // we cannot use multiple pushes in the same instruction
    // because the Abstract stack graph will think we should
    // re-use the same node over and over again, so instead,
    // we subtract all stack-locations first, then fill
    // in the values after.
    temp = state.getRegisterValue(Register.ESP);

    state.setRegisterValue(Register.ESP,
        state.getRegisterValue(Register.ESP).subtract(
            state.createNewValue(NUM_BYTES_TO_BUSH)));
    
    esp = state.getRegisterValue(Register.ESP);
    
    state.store(esp.add(state.createNewValue(7*4)), 
        state.getRegisterValue(Register.EAX));
    
    state.store(esp.add(state.createNewValue(6*4)), 
        state.getRegisterValue(Register.ECX));

    state.store(esp.add(state.createNewValue(5*4)), 
        state.getRegisterValue(Register.EDX));

    state.store(esp.add(state.createNewValue(4*4)), 
        state.getRegisterValue(Register.EBX));

    state.store(esp.add(state.createNewValue(3*4)), 
        temp);

    state.store(esp.add(state.createNewValue(2*4)), 
        state.getRegisterValue(Register.EBP));

    state.store(esp.add(state.createNewValue(1*4)), 
        state.getRegisterValue(Register.ESI));

    state.store(esp.add(state.createNewValue(0*4)), 
        state.getRegisterValue(Register.EDI));

    next.put(nextAddress, state);
    return next;
  }
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PushfdInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PushfdInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    state.push(state.createNewValue());

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PushInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PushInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Value valueToPush;
    
    valueToPush = instruction.getDstOperand().interpret(state);
    state.push(valueToPush);

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepMovsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepMovsInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof Memory8Operand)
    {
        Value numBytesToCopy  = state.getRegisterValue(Register.ECX);
        Value readPosition    = state.getRegisterValue(Register.ESI);
        Value writePosition   = state.getRegisterValue(Register.EDI);
        
        while(numBytesToCopy.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
        {
            Value byteToCopy = state.load(readPosition, BitCount._8Bits);
            state.store(writePosition, byteToCopy);
            numBytesToCopy = numBytesToCopy.subtract(state.createNewValue(1));
            readPosition  = readPosition.add(state.createNewValue(1));
            writePosition = writePosition.add(state.createNewValue(1));              
        }
        state.setRegisterValue(Register.ECX, numBytesToCopy);
        state.setRegisterValue(Register.ESI, readPosition);
        state.setRegisterValue(Register.EDI, writePosition);
    }

    else if(dstOperand instanceof Memory16Operand)
    {
        Value numWordsToCopy  = state.getRegisterValue(Register.ECX);
        Value readPosition    = state.getRegisterValue(Register.ESI);
        Value writePosition   = state.getRegisterValue(Register.EDI);
        
        while(numWordsToCopy.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
        {
            Value wordToCopy = state.load(readPosition, BitCount._16Bits);
            state.store(writePosition, wordToCopy);
            numWordsToCopy = numWordsToCopy.subtract(state.createNewValue(1));
            readPosition  = readPosition.add(state.createNewValue(2));
            writePosition = writePosition.add(state.createNewValue(2));              
        }

        state.setRegisterValue(Register.ECX, numWordsToCopy);
        state.setRegisterValue(Register.ESI, readPosition);
        state.setRegisterValue(Register.EDI, writePosition);
    }
    
    else if(dstOperand instanceof Memory32Operand)
    {
        Value numDwordsToCopy = state.getRegisterValue(Register.ECX);
        Value readPosition    = state.getRegisterValue(Register.ESI);
        Value writePosition   = state.getRegisterValue(Register.EDI);
        
        while(numDwordsToCopy.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
        {
            Value dwordToCopy = state.load(readPosition, BitCount._32Bits);
            state.store(writePosition, dwordToCopy);
            numDwordsToCopy = numDwordsToCopy.subtract(state.createNewValue(1));
            readPosition  = readPosition.add(state.createNewValue(4));
            writePosition = writePosition.add(state.createNewValue(4));              
        }
        
        state.setRegisterValue(Register.ECX, numDwordsToCopy);
        state.setRegisterValue(Register.ESI, readPosition);
        state.setRegisterValue(Register.EDI, writePosition);
    }

    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type of jjtGetChild(0)");
      throw new Error("RepMovsInstruction.interpret: unknown operand type of jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepneScasInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepneScasInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    // this instruction is not handled by the interpreter.  Simply set
    // EDI & ECX to undefined, because these are the two registers that this
    // instruction modifies.
    state.getRegisterValue(Register.EAX);
    state.getRegisterValue(Register.ECX);
    state.getRegisterValue(Register.EDI);
    
    state.setRegisterValue(Register.ECX, state.createNewValue());

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepStosInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepStosInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof Memory8Operand)
    {
        Value numBytesToWrite = state.getRegisterValue(Register.ECX);
        Value byteToWrite     = state.getRegisterValue(Register.AL);
        Value position    = state.getRegisterValue(Register.EDI);
        
        while(numBytesToWrite.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
        {
            state.store(position, byteToWrite);
            numBytesToWrite = numBytesToWrite.subtract(state.createNewValue(1));
            position = position.add(state.createNewValue(1));              
        }
    }
    else if(dstOperand instanceof Memory16Operand)
    {
        Value numWordsToWrite = state.getRegisterValue(Register.ECX);
        Value wordToWrite     = state.getRegisterValue(Register.AX);
        Value position    = state.getRegisterValue(Register.EDI);
        
        while(numWordsToWrite.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
        {
            state.store(position, wordToWrite);
            numWordsToWrite = numWordsToWrite.subtract(state.createNewValue(2));
            position = position.add(state.createNewValue(2));              
        }
    }
    else if(dstOperand instanceof Memory32Operand)
    {
        Value numDwordsToWrite = state.getRegisterValue(Register.ECX);
        Value dwordToWrite     = state.getRegisterValue(Register.EAX);
        Value position     = state.getRegisterValue(Register.EDI);
        
        while(numDwordsToWrite.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
        {
            state.store(position, dwordToWrite);
            numDwordsToWrite = numDwordsToWrite.subtract(state.createNewValue(4));
            position = position.add(state.createNewValue(4));
        }
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type of jjtGetChild(0)");
      throw new Error("RepStosInstruction.interpret: unknown operand type of jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }
 
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepzMovsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepzMovsInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof Memory8Operand)
    {
      Value numBytesToWrite = state.getRegisterValue(Register.ECX);
      Value byteToWrite     = state.getRegisterValue(Register.AL);
      Value position    = state.getRegisterValue(Register.EDI);
        
      while(numBytesToWrite.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
      {
        state.store(position, byteToWrite);
        numBytesToWrite = numBytesToWrite.subtract(state.createNewValue(1));
        position = position.add(state.createNewValue(1));              
      }
    }
    else if(dstOperand instanceof Memory16Operand)
    {
      Value numWordsToWrite = state.getRegisterValue(Register.ECX);
      Value wordToWrite     = state.getRegisterValue(Register.AX);
      Value position    = state.getRegisterValue(Register.EDI);
        
      while(numWordsToWrite.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
      {
        state.store(position, wordToWrite);
        numWordsToWrite = numWordsToWrite.subtract(state.createNewValue(2));
        position = position.add(state.createNewValue(2));              
      }
    }
    else if(dstOperand instanceof Memory32Operand)
    {
      Value numDwordsToWrite = state.getRegisterValue(Register.ECX);
      Value dwordToWrite     = state.getRegisterValue(Register.EAX);
      Value position     = state.getRegisterValue(Register.EDI);
        
      while(numDwordsToWrite.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
      {
        state.store(position, dwordToWrite);
        numDwordsToWrite = numDwordsToWrite.subtract(state.createNewValue(4));
        position = position.add(state.createNewValue(4));
      }
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type of jjtGetChild(0)");
      throw new Error("RepStosInstruction.interpret: unknown operand type of jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepzStosInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepzStosInstruction instruction, State state,
      Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof Memory8Operand)
    {
      Value numBytesToWrite = state.getRegisterValue(Register.ECX);
      Value byteToWrite     = state.getRegisterValue(Register.AL);
      Value position    = state.getRegisterValue(Register.EDI);
      
      while(numBytesToWrite.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
      {
        state.store(position, byteToWrite);
        numBytesToWrite = numBytesToWrite.subtract(state.createNewValue(1));
        position = position.add(state.createNewValue(1));              
      }
    }
    else if(dstOperand instanceof Memory16Operand)
    {
      Value numWordsToWrite = state.getRegisterValue(Register.ECX);
      Value wordToWrite     = state.getRegisterValue(Register.AX);
      Value position    = state.getRegisterValue(Register.EDI);
      
      while(numWordsToWrite.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
      {
        state.store(position, wordToWrite);
        numWordsToWrite = numWordsToWrite.subtract(state.createNewValue(2));
        position = position.add(state.createNewValue(2));              
      }
    }
    else if(dstOperand instanceof Memory32Operand)
    {
      Value numDwordsToWrite = state.getRegisterValue(Register.ECX);
      Value dwordToWrite     = state.getRegisterValue(Register.EAX);
      Value position     = state.getRegisterValue(Register.EDI);
      
      while(numDwordsToWrite.isGreaterThan(state.createNewValue(0)) == AbstractBoolean.True)
      {
        state.store(position, dwordToWrite);
        numDwordsToWrite = numDwordsToWrite.subtract(state.createNewValue(4));
        position = position.add(state.createNewValue(4));
      }
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type of jjtGetChild(0)");
      throw new Error("RepzStosInstruction.interpret: unknown operand type of jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RetInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RetInstruction instruction, State state,
      Address nextAddress)
  {
    Value       esp;
    AddressList   nextAddresses  = new AddressList();
    Map<Address, State> next = new HashMap<Address, State>();
  
    nextAddresses = state.getReturnAddressAsList();
    state.pop();
    
    if(instruction.getDstOperand() != null)
    {
      esp = state.getRegisterValue(Register.ESP);
      state.setRegisterValue( Register.ESP,
          esp.add(instruction.getDstOperand().interpret(state)) );
    }
  
    if(next.size() > 5)
    {
      Log.write(Log.WARNING, "interpret() - At instruction " + 
          instruction.getAddress() + ", there are " + next.size() + " return values.");
    }
    
    // This instruction interpreter assumes that each return address will use
    // the same state (context-insensitive).  
    for(Address address : nextAddresses)
      next.put(address, state);
    
    return next;
  }
  

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RolInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RolInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
        RegisterOperand target = (RegisterOperand)dstOperand;
        state.getRegisterValue(target.getRegister());
        state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
        MemoryOperand target = (MemoryOperand)dstOperand;
        state.store(target.getTarget(state), state.createNewValue());
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
      throw new Error("RolInstruction.interpret: unknown operand type for jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RorInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RorInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.getRegisterValue(target.getRegister());
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
      throw new Error("RoRInstruction.interpret: unknown operand type for jjtGetChild(0)");
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SarInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SarInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.getRegisterValue(target.getRegister());
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - Unknown operand type for jjtGetChild(0)");
      throw new Error("ShrInstruction.interpret: Unknown operand type for jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SbbInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SbbInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand destOp;   
    Value   destValue;  
    Operand sourceOp;   
    Value   sourceValue;
    
    destOp      = instruction.getDstOperand();
    sourceOp    = instruction.getSrcOperand();
    sourceValue = sourceOp.interpret(state);
    sourceValue = sourceValue.add(state.getFlagValueInteger(FlagRegisterStore.CARRY_FLAG));
    destValue   = destOp.interpret(state);
    destValue   = destValue.subtract(sourceValue);

    if(destOp instanceof RegisterOperand)
      state.setRegisterValue( ((RegisterOperand)destOp).getRegister(), destValue);
    
    else if(destOp instanceof MemoryOperand)
      state.store( ((MemoryOperand)destOp).getTarget(state), destValue);
    
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type for jjtGetChild(0)");
      throw new Error("RoRInstruction.interpret: unknown operand type for jjtGetChild(0)");
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetaeSetnbInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetaeSetnbInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
        RegisterOperand target = (RegisterOperand)dstOperand;
        state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
        MemoryOperand target = (MemoryOperand)dstOperand;
        state.store(target.getTarget(state), state.createNewValue());
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetbeSetnaInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetbeSetnaInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetbSetnaeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetbSetnaeInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SeteSetzInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SeteSetzInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
        RegisterOperand target = (RegisterOperand)dstOperand;
        state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
        MemoryOperand target = (MemoryOperand)dstOperand;
        state.store(target.getTarget(state), state.createNewValue());
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetgeSetnlInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetgeSetnlInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetgSetnleInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetgSetnleInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
   
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
        RegisterOperand target = (RegisterOperand)dstOperand;
        state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
        MemoryOperand target = (MemoryOperand)dstOperand;
        state.store(target.getTarget(state), state.createNewValue());
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetlSetngeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetlSetngeInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
        RegisterOperand target = (RegisterOperand)instruction.getDstOperand();
        state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(instruction.getDstOperand() instanceof MemoryOperand)
    {
        MemoryOperand target = (MemoryOperand)instruction.getDstOperand();
        state.store(target.getTarget(state), state.createNewValue());
    }
    
    next.put(nextAddress, state);
    return next;
  }
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetleSetngInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetleSetngInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    
    next.put(nextAddress, state);
    return next;  
  }
  

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetneSetnzInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetneSetnzInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ShlInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ShlInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      target.interpret(state);  // read from register just to trigger the read (for LVA)
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
        MemoryOperand target = (MemoryOperand)dstOperand;
        target.interpret(state);  // read from register just to trigger the read (for LVA)
        state.store(target.getTarget(state), state.createNewValue());
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - Unknown operand type for jjtGetChild(0)");
      throw new Error("ShlInstruction.interpret: Unknown operand type for jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ShrInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ShrInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.getRegisterValue(target.getRegister());
      state.setRegisterValue(target.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand target = (MemoryOperand)dstOperand;
      state.store(target.getTarget(state), state.createNewValue());
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - Unknown operand type for jjtGetChild(0)");
      throw new Error("ShrInstruction.interpret: Unknown operand type for jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.StdInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(StdInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    // since the state doesn't yet support flags, do nothing for now.
    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.StosInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(StosInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof Memory8Operand)
    {
        Value byteToWrite = state.getRegisterValue(Register.AL);
        Value position    = state.getRegisterValue(Register.EDI);
        
        state.store(position, byteToWrite);
    }
    else if(dstOperand instanceof Memory16Operand)
    {
        Value wordToWrite = state.getRegisterValue(Register.AX);
        Value position    = state.getRegisterValue(Register.EDI);
        
        state.store(position, wordToWrite);
    }
    else if(dstOperand instanceof Memory32Operand)
    {
        Value dwordToWrite = state.getRegisterValue(Register.EAX);
        Value position     = state.getRegisterValue(Register.EDI);
        
        state.store(position, dwordToWrite);
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type of jjtGetChild(0)");
      throw new Error("StosInstruction.interpret: unknown operand type of jjtGetChild(0)");
    }

    next.put(nextAddress, state);
    return next;  
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SubInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SubInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Value sourceValue;
    Value targetValue;
    
    sourceValue = instruction.getSrcOperand().interpret(state);
    
    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
      Register target = ((RegisterOperand)instruction.getDstOperand()).getRegister();
      targetValue = state.getRegisterValue(target);
      targetValue = targetValue.subtract(sourceValue);
      state.setRegisterValue(target, targetValue);
    }
    else if(instruction.getDstOperand() instanceof MemoryOperand)
    {
      Value target = ((MemoryOperand)instruction.getDstOperand()).getTarget(state);
      targetValue = state.load(target, BitCount._32Bits);
      targetValue = targetValue.subtract(sourceValue);
      state.store(target, targetValue);
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - Invalid operand type encountered");
      throw new Error("SubInstruction.java: Invalid operand type encountered.");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.TestInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(TestInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();

    if(instruction.getDstOperand() instanceof RegisterOperand)
    {
      state.getRegisterValue(
          ((RegisterOperand)instruction.getDstOperand()).getRegister());
    }
    
    if(instruction.getSrcOperand() instanceof RegisterOperand)
    {
      state.getRegisterValue(
          ((RegisterOperand)instruction.getSrcOperand()).getRegister());
    }
    
    next.put(nextAddress, state);
    return next;  
  }
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.UndefInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(UndefInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    
    dstOperand = instruction.getDstOperand();
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand operand = (RegisterOperand)dstOperand;
      state.setRegisterValue(operand.getRegister(), state.createNewValue());
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand operand = (MemoryOperand)dstOperand;
      state.store(operand.getTarget(state), state.createNewValue());
    }
    else
    {
      System.out.println("UndefInstruction.interpret: Unknown type of jjtGetChild(0).");
      throw new Error("UndefInstruction.interpret: Unknown type of jjtGetChild(0).");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.XchgInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(XchgInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    Value   left;
    Value   right;
    Operand srcOperand;
    
    dstOperand = instruction.getDstOperand();
    srcOperand = instruction.getSrcOperand();
    left = dstOperand.interpret(state);
    right = srcOperand.interpret(state);
    
    if(dstOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)dstOperand;
      state.setRegisterValue(target.getRegister(), right);
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      MemoryOperand memory = (MemoryOperand)dstOperand;
      state.store(memory.getTarget(state), right);
    }
    else
    {
      Log.write(Log.ERROR, "interpret() - unknown operand type encountered");
      throw new Error("XchgInstruction.interpret: unknown operand type encountered.");
    }

    if(srcOperand instanceof RegisterOperand)
    {
      RegisterOperand target = (RegisterOperand)srcOperand;
      state.setRegisterValue(target.getRegister(), left);
    }
    else if(srcOperand instanceof MemoryOperand)
    {
      MemoryOperand memory = (MemoryOperand)srcOperand;
        state.store(memory.getTarget(state), left);
    }
    else
    {
        System.err.println("XchgInstruction.interpret: unknown operand type encountered.");
        throw new Error("XchgInstruction.interpret: unknown operand type encountered.");
    }

    next.put(nextAddress, state);
    return next;
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.XorInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(XorInstruction instruction, State state, Address nextAddress)
  {
    Map<Address, State> next = new HashMap<Address, State>();
    Operand dstOperand;
    Operand srcOperand;
    
    dstOperand = instruction.getDstOperand();
    srcOperand = instruction.getSrcOperand();
    
    if(dstOperand instanceof RegisterOperand)
    {
      Register target   = ((RegisterOperand)dstOperand).getRegister();
      Value operand1 = state.getRegisterValue(target);
      Value operand2 = ((Operand)srcOperand).interpret(state);
      
      Value result = operand1.xor(operand2);
      state.setRegisterValue(target, result);
    }
    else if(dstOperand instanceof MemoryOperand)
    {
      Value target    = ((MemoryOperand)dstOperand).getTarget(state);
      Value operand1  = ((Operand)dstOperand).interpret(state);
      Value operand2  = ((Operand)srcOperand).interpret(state);
      
      Value result = operand1.xor(operand2);
      state.store(target, result);
    }

    next.put(nextAddress, state);
    return next;
  }
}

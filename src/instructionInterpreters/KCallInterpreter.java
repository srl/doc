package instructionInterpreters;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import kCallContextAnalysisDomain.KCallContext;
import csAnalysisDomain.csAnalysisDomain;
import logger.Log;
import vsaAsgDomain.VsaAsgState;
import x86InstructionSet.Address;
import x86InstructionSet.AddressList;
import x86InstructionSet.RetInstruction;
import domain.Register;
import domain.State;
import domain.Value;

/**
 * Knows how to interpreter x86 instructions.  Uses context-sensitivity
 * when interpreting RET instructions so that each calling instruction
 * is given the correct state when returning. 
 */
public class KCallInterpreter extends ContextInsensitiveInterpreter
{
	
	static final Map<String, State> contexts = new HashMap<String, State>();
	
  /**
   * Interprets a RET instruction.
   * 
   * @param instruction   The instruction to interpret.
   * @param state         The incoming state.
   * @param nextAddress   The address of the very next instruction in the program.
   *                      This parameter is ignored by this method.
   * @return              A mapping of addresses to states.  The addresses belong
   *                      to the instructions that should be added to the worklist.
   *                      Each address is mapped to a separate state.  The state
   *                      is the incoming state that should be used when interpreting
   *                      each instruction. 
   *                   
   */
  public Map<Address, State> interpret(RetInstruction instruction, State state, Address nextAddress)
  

    // This method interpret a RET instruction and return the
    // addresses of the instructions to interpret next.  Each address is mapped
    // to a state.  The addresses are obtained by calling state.getReturnAddressesAsList.
    // The state for each instruction is obtained by the method Context.get(Address).  This method 
    // create a new Context object that ONLY contains the state for the given address.  
  	{
  
    // This instruction interpreter assumes that each return address will use
    // its state belonging to its context  (context-sensitive).  
	  String stackString = ((csAnalysisDomain) state).getContextString();
	  
	  if (stackString.equals("T"))
	  {
		    Value       esp;
		    AddressList   nextAddresses  = new AddressList();
		    Map<Address, State> next = new HashMap<Address, State>();
		  
		    nextAddresses = state.getReturnAddressAsList();
		    state.pop();
		    
		    if(instruction.getDstOperand() != null)
		    {
		      esp = state.getRegisterValue(Register.ESP);
		      state.setRegisterValue( Register.ESP,
		          esp.add(instruction.getDstOperand().interpret(state)) );
		    }
		  
		    if(next.size() > 5)
		    {
		      Log.write(Log.WARNING, "interpret() - At instruction " + 
		          instruction.getAddress() + ", there are " + next.size() + " return values.");
		    }
		    
		    // This instruction interpreter assumes that each return address will use
		    // the same state (context-insensitive).  
		    for(Address address : nextAddresses)
		      next.put(address, state);
		    
		    return next;
	   
	  } else {
		  
		Value       esp;
	    AddressList   nextAddresses  = new AddressList();
	    Map<Address, State> next = new HashMap<Address, State>();
	    nextAddresses = state.getReturnAddressAsList();
	    
	    state.pop();
		    
	  	if(instruction.getDstOperand() != null)
		    {
		      esp = state.getRegisterValue(Register.ESP);
		      state.setRegisterValue( Register.ESP,
		          esp.add(instruction.getDstOperand().interpret(state)) );
		    }
	  	
	  	((KCallContext)state).changeTop();
		    // if more than one address return the same state (context-insensitive in this case) more than k
		for(Address address : nextAddresses)
			      next.put(address, state);
  	
	    return next;
	  }
  	}
    	
  	
}


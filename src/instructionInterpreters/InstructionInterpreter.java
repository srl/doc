package instructionInterpreters;

import java.util.Map;

import x86InstructionSet.AdcInstruction;
import x86InstructionSet.AddInstruction;
import x86InstructionSet.Address;
import x86InstructionSet.AndInstruction;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.CdqInstruction;
import x86InstructionSet.CldInstruction;
import x86InstructionSet.CmpInstruction;
import x86InstructionSet.CwdInstruction;
import x86InstructionSet.DecInstruction;
import x86InstructionSet.DivInstruction;
import x86InstructionSet.EnterInstruction;
import x86InstructionSet.HltInstruction;
import x86InstructionSet.IdivInstruction;
import x86InstructionSet.ImulInstruction1op;
import x86InstructionSet.ImulInstruction2op;
import x86InstructionSet.ImulInstruction3op;
import x86InstructionSet.InInstruction;
import x86InstructionSet.IncInstruction;
import x86InstructionSet.JaJnbeInstruction;
import x86InstructionSet.JaeInstruction;
import x86InstructionSet.JbInstruction;
import x86InstructionSet.JbeInstruction;
import x86InstructionSet.JeInstruction;
import x86InstructionSet.JecxzInstruction;
import x86InstructionSet.JgInstruction;
import x86InstructionSet.JgeInstruction;
import x86InstructionSet.JlInstruction;
import x86InstructionSet.JleInstruction;
import x86InstructionSet.JmpInstruction;
import x86InstructionSet.JnbInstruction;
import x86InstructionSet.JneInstruction;
import x86InstructionSet.JnsInstruction;
import x86InstructionSet.JnzInstruction;
import x86InstructionSet.JsInstruction;
import x86InstructionSet.LeaInstruction;
import x86InstructionSet.LeaveInstruction;
import x86InstructionSet.LockInstruction;
import x86InstructionSet.LodsInstruction;
import x86InstructionSet.LoopdInstruction;
import x86InstructionSet.MovInstruction;
import x86InstructionSet.MovsInstruction;
import x86InstructionSet.MovsxInstruction;
import x86InstructionSet.MovzxInstruction;
import x86InstructionSet.MulInstruction;
import x86InstructionSet.NegInstruction;
import x86InstructionSet.NopInstruction;
import x86InstructionSet.NotInstruction;
import x86InstructionSet.OrInstruction;
import x86InstructionSet.PopInstruction;
import x86InstructionSet.PopadInstruction;
import x86InstructionSet.PopfdInstruction;
import x86InstructionSet.PushInstruction;
import x86InstructionSet.PushadInstruction;
import x86InstructionSet.PushfdInstruction;
import x86InstructionSet.RepMovsInstruction;
import x86InstructionSet.RepStosInstruction;
import x86InstructionSet.RepneScasInstruction;
import x86InstructionSet.RepzMovsInstruction;
import x86InstructionSet.RepzStosInstruction;
import x86InstructionSet.RetInstruction;
import x86InstructionSet.RolInstruction;
import x86InstructionSet.RorInstruction;
import x86InstructionSet.SarInstruction;
import x86InstructionSet.SbbInstruction;
import x86InstructionSet.SetaeSetnbInstruction;
import x86InstructionSet.SetbSetnaeInstruction;
import x86InstructionSet.SetbeSetnaInstruction;
import x86InstructionSet.SeteSetzInstruction;
import x86InstructionSet.SetgSetnleInstruction;
import x86InstructionSet.SetgeSetnlInstruction;
import x86InstructionSet.SetlSetngeInstruction;
import x86InstructionSet.SetleSetngInstruction;
import x86InstructionSet.SetneSetnzInstruction;
import x86InstructionSet.ShlInstruction;
import x86InstructionSet.ShrInstruction;
import x86InstructionSet.StdInstruction;
import x86InstructionSet.StosInstruction;
import x86InstructionSet.SubInstruction;
import x86InstructionSet.TestInstruction;
import x86InstructionSet.UndefInstruction;
import x86InstructionSet.XchgInstruction;
import x86InstructionSet.XorInstruction;
import domain.State;

/**
 * Responsible for defining how each <code>Instruction</code> 
 * is to be interpreted. Ideally, there will be only one implementation
 * of this interface that is generic enough to apply to all
 * situations.  However, if this isn't the case, new 
 * <code>InstructionInterpreter</code> implementations can be 
 * created to handle specific situations that aren't covered by the
 * original <code>InstructionInterpreter</code> 
 */
public interface InstructionInterpreter
{
  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
	/**
	 * TODO: This looks quite crazy. There is an interpret function for each
	 * instruction, and its coded for each analysis type.
	 */
  public Map<Address, State> interpret(AdcInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(AddInstruction instruction, State state, Address nextAddress);
  
  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(AndInstruction instruction, State state, Address nextAddress);
  
  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(CallInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(CdqInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(CldInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(CmpInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(CwdInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(DecInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(DivInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(EnterInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(HltInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(IdivInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(ImulInstruction1op instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(ImulInstruction2op instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(ImulInstruction3op instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(IncInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(InInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JaeInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JaJnbeInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JbeInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JbInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JecxzInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JeInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JgeInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JgInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JleInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JlInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JmpInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JnbInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JneInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JnsInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JnzInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(JsInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(LeaInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(LeaveInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(LockInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(LodsInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(LoopdInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(MovInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(MovsInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(MovsxInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(MovzxInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(MulInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(NegInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(NopInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(NotInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(OrInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(PopInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(PopfdInstruction instruction, State state, Address nextAddress);
  
  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(PopadInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(PushadInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(PushfdInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(PushInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(RepMovsInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(RepneScasInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(RepStosInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(RepzMovsInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(RepzStosInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(RetInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(RolInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(RorInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SarInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SbbInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SetaeSetnbInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SetbeSetnaInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SetbSetnaeInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SeteSetzInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SetgeSetnlInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SetgSetnleInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SetleSetngInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SetlSetngeInstruction instruction, State state, Address nextAddress);
  
  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SetneSetnzInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(ShlInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(ShrInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(StdInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(StosInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(SubInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(TestInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(UndefInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(XchgInstruction instruction, State state, Address nextAddress);

  /**
   * Interprets the input instruction.
   * 
   * @param instruction   the instruction to be interpreted
   * @param state         the beginning state
   * @param nextAddress   the address of the instruction directly following the
   *                      input instruction in the program.
   * @return Mapping of address to state, where the address is the address of the
   *         instruction to interpret next and the state is the state to use when
   *         interpretting that address.
   */
  public Map<Address, State> interpret(XorInstruction instruction, State state, Address nextAddress);
}
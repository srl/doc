package instructionInterpreters;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import liveRegisterAnalysisDomain.DeadValue;
import liveRegisterAnalysisDomain.LiveValue;
import liveRegisterAnalysisDomain.LraState;
import x86InstructionSet.AdcInstruction;
import x86InstructionSet.AddInstruction;
import x86InstructionSet.Address;
import x86InstructionSet.AndInstruction;
import x86InstructionSet.CallInstruction;
import x86InstructionSet.CdqInstruction;
import x86InstructionSet.CldInstruction;
import x86InstructionSet.CmpInstruction;
import x86InstructionSet.CwdInstruction;
import x86InstructionSet.DecInstruction;
import x86InstructionSet.DivInstruction;
import x86InstructionSet.EnterInstruction;
import x86InstructionSet.HltInstruction;
import x86InstructionSet.IdivInstruction;
import x86InstructionSet.ImulInstruction1op;
import x86InstructionSet.ImulInstruction2op;
import x86InstructionSet.ImulInstruction3op;
import x86InstructionSet.InInstruction;
import x86InstructionSet.IncInstruction;
import x86InstructionSet.Instruction;
import x86InstructionSet.JaJnbeInstruction;
import x86InstructionSet.JaeInstruction;
import x86InstructionSet.JbInstruction;
import x86InstructionSet.JbeInstruction;
import x86InstructionSet.JeInstruction;
import x86InstructionSet.JecxzInstruction;
import x86InstructionSet.JgInstruction;
import x86InstructionSet.JgeInstruction;
import x86InstructionSet.JlInstruction;
import x86InstructionSet.JleInstruction;
import x86InstructionSet.JmpInstruction;
import x86InstructionSet.JnbInstruction;
import x86InstructionSet.JneInstruction;
import x86InstructionSet.JnsInstruction;
import x86InstructionSet.JnzInstruction;
import x86InstructionSet.JsInstruction;
import x86InstructionSet.LeaInstruction;
import x86InstructionSet.LeaveInstruction;
import x86InstructionSet.LockInstruction;
import x86InstructionSet.LodsInstruction;
import x86InstructionSet.LoopdInstruction;
import x86InstructionSet.MovInstruction;
import x86InstructionSet.MovsInstruction;
import x86InstructionSet.MovsxInstruction;
import x86InstructionSet.MovzxInstruction;
import x86InstructionSet.MulInstruction;
import x86InstructionSet.NegInstruction;
import x86InstructionSet.NopInstruction;
import x86InstructionSet.NotInstruction;
import x86InstructionSet.OrInstruction;
import x86InstructionSet.PopInstruction;
import x86InstructionSet.PopadInstruction;
import x86InstructionSet.PopfdInstruction;
import x86InstructionSet.PushInstruction;
import x86InstructionSet.PushadInstruction;
import x86InstructionSet.PushfdInstruction;
import x86InstructionSet.RepMovsInstruction;
import x86InstructionSet.RepStosInstruction;
import x86InstructionSet.RepneScasInstruction;
import x86InstructionSet.RepzMovsInstruction;
import x86InstructionSet.RepzStosInstruction;
import x86InstructionSet.RetInstruction;
import x86InstructionSet.RolInstruction;
import x86InstructionSet.RorInstruction;
import x86InstructionSet.SarInstruction;
import x86InstructionSet.SbbInstruction;
import x86InstructionSet.SetaeSetnbInstruction;
import x86InstructionSet.SetbSetnaeInstruction;
import x86InstructionSet.SetbeSetnaInstruction;
import x86InstructionSet.SeteSetzInstruction;
import x86InstructionSet.SetgSetnleInstruction;
import x86InstructionSet.SetgeSetnlInstruction;
import x86InstructionSet.SetlSetngeInstruction;
import x86InstructionSet.SetleSetngInstruction;
import x86InstructionSet.SetneSetnzInstruction;
import x86InstructionSet.ShlInstruction;
import x86InstructionSet.ShrInstruction;
import x86InstructionSet.StdInstruction;
import x86InstructionSet.StosInstruction;
import x86InstructionSet.SubInstruction;
import x86InstructionSet.TestInstruction;
import x86InstructionSet.UndefInstruction;
import x86InstructionSet.XchgInstruction;
import x86InstructionSet.XorInstruction;
import defUseAnalysisDomain.DefUseState;
import domain.Register;
import domain.State;

/**
 * An instruction interpreter that marks registers either dead or
 * live based on the def-use state at the given instruction.  The
 * def-use state should be previously computed (by the 
 * <code>DefUseAnalysis</code> maybe). 
 */
public class LraInstructionInterpreter implements InstructionInterpreter
{
  /**
   * Converts a 8-bit or 16-bit register name to the respective
   * 32-bit register.  For instance, AX or AL becomes EAX.
   * @param register register to convert
   * @return associated 32-bit register name.
   */
  private Register convertTo32Bit(Register register)
  {
    switch(register)
    {
    case AH:
    case AL:
    case AX:
      return Register.EAX;
        
    case BH:
    case BL:
    case BX:
      return Register.EBX;
      
    case CH:
    case CL:
    case CX:
      return Register.ECX;
      
    case DH:
    case DL:
    case DX:
      return Register.EDX;
    }
    
    return register;
  }
  
  /**
   * Interprets the instruction using the live register analysis algorithm.
   * This function is generic enough to handle any type of instruction.
   * All <code>interpret</code> functions in this class call this method
   * to do the actual work. 
   * 
   * @param instruction       the <code>instruction</code> to be interpreted.
   * @param state             the begining <code>state</code>.
   * @param nextAddress       the <code>Address</code> of the instruction 
   *                          following this instruciton in the prorgram.
   * @param assignsToMemory   specifies whether this instruction assigns a 
   *                          value to a memory location.  This function 
   *                          assumes that all memory locations are live 
   *                          and therefore does not assume any 
   *                          assignments to memory are dead code.
   * @return  a list of <code>Address</code>es of the instruction to be
   *          interpreted next.  This function always returns an empty list
   *          and expects the <code>Worklist</code> in use to override
   *          this value and supply the correct next instruction for
   *          interpretation.
   */
  private Map<Address, State> doInterpret(Instruction instruction, State state, Address nextAddress)
  {
    DefUseState   defUseState;
    Set<Object>   defset;
    Set<Object>   useset;
    
    if( !(instruction.getPrevAnalysisEndState() instanceof DefUseState) )
      throw new IllegalArgumentException();
    
    defUseState = (DefUseState)instruction.getPrevAnalysisEndState();
    
    // if everything being defined is dead, then everything being used should
    // be dead.
    boolean isEverythingDefinedDead = true;
    
    if(defUseState.writesToMemory())
      isEverythingDefinedDead = false;
    
    defset = defUseState.getDefRegisterSet();
    for(Object o : defset)
    {
      if(o instanceof Register)
      {
        if(state.getRegisterValue( (Register)o ) instanceof LiveValue)
          isEverythingDefinedDead = false;
      }
      else if(o instanceof Integer)
      {
        if( ((LraState)state).getStackValue( (Integer)o ) instanceof LiveValue)
          isEverythingDefinedDead = false;
      }
      else
        throw new Error("Unknown object type.");
    }
    
    if(!isEverythingDefinedDead)
    {
      // set all defined registers to dead and all used register to live.
      defset = defUseState.getDefRegisterSet();
      for(Object o : defset)
      {
        if(o instanceof Register)
          state.setRegisterValue( convertTo32Bit((Register)o), new DeadValue() );
        if(o instanceof Integer)
          ((LraState)state).setStackValue( (Integer)o, new DeadValue() );
      }
        
      useset = defUseState.getUseRegisterSet();
      for(Object o : useset)
      {
        if(o instanceof Register)
          state.setRegisterValue( convertTo32Bit((Register)o), new LiveValue());
        if(o instanceof Integer)
          ((LraState)state).setStackValue( (Integer)o, new LiveValue() );
      }
    }
    
    return new HashMap<Address, State>();
  }
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.AdcInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(AdcInstruction instruction, State state, Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.AddInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(AddInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.AndInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(AndInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CallInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CallInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CdqInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CdqInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CldInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CldInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CmpInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CmpInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.CwdInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(CwdInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.DecInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(DecInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.DivInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(DivInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.EnterInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(EnterInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.HltInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(HltInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.IdivInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(IdivInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ImulInstruction1op, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ImulInstruction1op instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ImulInstruction2op, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ImulInstruction2op instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ImulInstruction3op, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ImulInstruction3op instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.IncInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(IncInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.InInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(InInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JaeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JaeInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JaJnbeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JaJnbeInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JbeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JbeInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JbInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JbInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JecxzInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JecxzInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JeInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JgeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JgeInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JgInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JgInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JleInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JleInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JlInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JlInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JmpInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JmpInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JnbInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JnbInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JneInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JneInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JnsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JnsInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JnzInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JnzInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.JsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(JsInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LeaInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LeaInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LeaveInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LeaveInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LockInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LockInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LodsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LodsInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.LoopdInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(LoopdInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MovInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MovInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MovsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MovsInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MovsxInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MovsxInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MovzxInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MovzxInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.MulInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(MulInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.NegInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(NegInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.NopInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(NopInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.NotInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(NotInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.OrInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(OrInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PopInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PopInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PopInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PopfdInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PopadInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PopadInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PushfdInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PushfdInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PushadInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PushadInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }
  
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.PushInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(PushInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepMovsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepMovsInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepneScasInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepneScasInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepStosInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepStosInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepzMovsInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepzMovsInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RepzStosInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RepzStosInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RetInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RetInstruction instruction, State state, Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RolInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RolInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.RorInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(RorInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SarInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SarInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SbbInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SbbInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetaeSetnbInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetaeSetnbInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetbeSetnaInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetbeSetnaInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetbSetnaeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetbSetnaeInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SeteSetzInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SeteSetzInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetgeSetnlInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetgeSetnlInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }
 
  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetgSetnleInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetgSetnleInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetleSetngInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetleSetngInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetlSetngeInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetlSetngeInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SetneSetnzInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SetneSetnzInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ShlInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ShlInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.ShrInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(ShrInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.StdInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(StdInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.StosInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(StosInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.SubInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(SubInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.TestInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(TestInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.UndefInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(UndefInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.XchgInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(XchgInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }

  /* (non-Javadoc)
   * @see instructionInterpreters.InstructionInterpreter#interpret(x86InstructionSet.XorInstruction, domain.State, x86InstructionSet.Address)
   */
  public Map<Address, State> interpret(XorInstruction instruction, State state,
      Address nextAddress)
  {
    return doInterpret(instruction, state, nextAddress);
  }
}
